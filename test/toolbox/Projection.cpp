#include <vigra/random.hxx>
#include <vigra/unittest.hxx>

#include <hookers/interface/Project.h>
#include <hookers/interface/Sequence.h>

#include <hookers/toolbox/PoseEstimation/PoseEstimation.h>
#include <hookers/toolbox/Projection/SoftwareProjection.h>
#include <hookers/toolbox/Projection/OpenGLProjection.h>

#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>

using namespace hookers;
using namespace hookers::interface;
using namespace hookers::toolbox;

/** @todo create some dummy project that can test the reprojection using hard values */
int main() {


    /* test the reprojection stuff for one point */
    {
        Projection::Vec<double,6> pose;
        pose[0] = -50;
        pose[2] = -30;

        pose[1] = M_PI/20;
        pose[5] = M_PI/5;

        Projection::Vec<double,3> object{50,70,300};

        Projection::Vec<double,4> intrinsics;
        intrinsics[0] = 1850;
        intrinsics[1] = 1850;
        intrinsics[2] = 2560/2;
        intrinsics[3] = 1080/2;


        auto projection = Projection::project3d(object, pose, intrinsics);
        auto reprojection = Projection::reproject3d(projection, pose, intrinsics);

        for(int i : {0,1,2}) {
            shouldEqualTolerance(object[i],reprojection[i], 1e-3);
        }
    }


    Vector4f intrinsics;
    intrinsics[0] = 1850;
    intrinsics[1] = 1850;
    intrinsics[2] = 2560/2;
    intrinsics[3] = 1080/2;

    Vector6f pose;
    pose[0] = -129.02;
    pose[1] = -19.026;
    pose[2] = 110.726;
    pose[3] = -0.0399;
    pose[4] = -2.2733;
    pose[5] = 2.22;

    Project p = Project::open();
    if(!p.isValid() || p.sequences().empty())
        throw std::runtime_error("Could not open project with sequence.");
    Sequence s = p.sequences().front();

    if(!s.isValid())
        throw std::runtime_error("Could not open Sequence");

    Frame f = s.frames()[1000];

    std::cout << ">> Opening geometry" << std::endl;
    Geometry g = s.geometry();


    std::cout << ">> Reading pointcloud" << std::endl;
    Vector3fArray cloud;
    g.getPointCloud(cloud);

    std::cout << ">> Reprojecting (CPU)" << std::endl;
    Array2f zBuffer(f.size());
    std::unordered_set<int> pointSet;
    SoftwareProjection proj(cloud, intrinsics);
    proj.setPose(pose);
    proj.getDepth(zBuffer);
    proj.getPointList(pointSet);


    std::cout << ">> Reprojecting (GPU)" << std::endl;
    Array2f zBufferGL(f.size());
    Array2i pointsGL(f.size());
    std::unordered_set<int> pointSetGL;
    OpenGLProjection projgl(cloud, intrinsics);
    projgl.setPose(pose);
    projgl.getDepth(zBufferGL);
    projgl.getPoints(pointsGL);
    projgl.getPointList(pointSetGL);

//    {
//        using namespace vigra::multi_math;
//        should(any(points == std::numeric_limits<float>::infinity()));
//    }


    int min, max;
    pointsGL.minmax(&min, &max);

    std::cout << "point set size:" << pointSet.size()  << ", " << pointSetGL.size() << std::endl;
    std::cout << "min, max: " << min << "," << max << std::endl;

    /* check if there are any projections and check that CPU and GPU implementation
     * show equal results */
    should(pointSet.size() > 10);
    should(pointSetGL.size() > 10);

    /* display da shit */
    {
        cv::Mat zBufferMat = cv::Mat(zBufferGL.shape(1), zBufferGL.shape(0), CV_32FC1, zBufferGL.data());
        cv::Mat pointsMat = cv::Mat(pointsGL.shape(1), pointsGL.shape(0), CV_32SC1, pointsGL.data());
        zBufferMat.setTo(0.0, zBufferMat == std::numeric_limits<float>::infinity());
        cv::imshow("depth-float", zBufferMat);
        cv::imshow("ids", pointsMat);
        while(cv::waitKey(0) != 'q');
    }

}

