#include <iostream>
#include <string>
#include <vector>

#include <hookers/interface/Project.h>
#include <hookers/interface/Sequence.h>
#include <hookers/interface/Frame.h>
#include <hookers/interface/NumericTypes.h>

#include <hookers/toolbox/Sampling/GroundtruthSource.h>

#include <hookers/toolbox/export/Sources/ExporterSource.h>
#include <hookers/toolbox/export/Sources/MapSource.h>
#include <hookers/toolbox/export/Sources/RawSource.h>
#include <hookers/toolbox/export/Sources/ViewSource.h>
#include <hookers/toolbox/export/Sources/ContourSource/ContourSource.h>

#include <hookers/toolbox/export/Sinks/ExporterSink.h>
#include <hookers/toolbox/export/Sinks/MapExporterSink.h>
#include <hookers/toolbox/export/Sinks/HDFExporterSink.h>
#include <hookers/toolbox/export/Sinks/PNGExporterSink.h>
#include <hookers/toolbox/export/Sinks/VideoExporterSink.h>
#include <hookers/toolbox/export/Sinks/DisplaySink.h>

using namespace hookers;
using namespace hookers::interface;
using namespace hookers::toolbox;

std::string ExporterSink::m_path(".");
std::string ExporterSink::m_sequenceName("unamed_sequence");


int main(int argc, char ** argv){

	std::vector<ExporterSource*> sources;
	std::vector<ExporterSink*> sinks;

	/* ---- setup sources  -----*/

	/* check if we are in interface or in raw mode */

	/* create map sources for all requested maps */
    //sources.push_back(new GroundtruthSource());
    //sources.push_back(new MapSource("test_map"));
    //sources.push_back(new ViewSource(Frame::View::Stereo));
    ContourSource* testCS = new ContourSource();
    testCS->setJsonPath("/home/alexander/029_contours_3500-25hz_1-3500_merged_20160404.json");
    sources.push_back(testCS);


	/* ---- setup sinks -----*/

    sinks.push_back(new MapExporterSink());
//    sinks.push_back(new HDFExporterSink());
//	sinks.push_back(new PNGExporterSink());
//	sinks.push_back(new VideoExporterSink());
//	sinks.push_back(new DisplaySink());



	/* collect all available sources */
	ExporterSource::SourceIdentifierList sourceIds;
	for (ExporterSource * s : sources) {
		ExporterSource::SourceIdentifierList localList = s->provides();
		std::copy(localList.cbegin(), localList.cend(), std::back_inserter(sourceIds));
	}

    /*open requested sequence*/

    const std::string name = "test_exporter";


    std::cout << "#" << std::endl
        << "## Create new project file" << std::endl
        << "#" << std::endl;

    Project project = Project::create(name);

    Frame::Map testMapEntry(vigra::Shape3(3, 2560, 1080));

    /*create a simple yellow and green image (3channels)*/
    for (int y = 0; y < testMapEntry.shape(2); y++)
    {
        for (int x = 0; x < testMapEntry.shape(1); x++)
        {
            if ((x % 20) / 10 == (y % 20) / 10)
                testMapEntry(0, x, y) = 255;
            else
                testMapEntry(2, x, y) = 255;
            testMapEntry(1, x, y) = 255;
        }
    }


    /* prepare frame view */
    Image testViewEntry(2560, 1080);
    testViewEntry = 10;

    testViewEntry.subarray(Image::difference_type(1200, 600),
        Image::difference_type(1400, 800)) = 255;

       /*create a simple black and white image(1channel)*/
        for (int y = 0; y < testViewEntry.shape(1); y++)
        {
            for (int x = 0; x < testViewEntry.shape(0); x++)
           {
                if ((x % 20) / 10 == (y % 20) / 10)
                    testViewEntry(x, y) = 0;
                else
                    testViewEntry(x, y) = 255;

            }
        }



    int FRAMES_ADDED = 0;

    /* open a scope in order to avoid redefinition errors */
    {
        //			should(project.isValid());
        //			should(project.parts().empty());

        Sequence s1 = project.addSequence("0_0061");
        //			should(s1.isValid());

        Frame lastFrame;
        for (int i = 0; i < 100; i++) {
            std::cout << "Adding frame " << i << std::endl;

            lastFrame = s1.addFrame();

            lastFrame.setView(testViewEntry);
            lastFrame.setView(testViewEntry, Frame::View::Stereo);
            lastFrame.setMap("test_map", testMapEntry);
            FRAMES_ADDED++;

        }

        //			should(lastFrame.isValid());

    }

    std::cout << "#" << std::endl
        << "## Closing file" << std::endl
        << "#" << std::endl;

    project.save();
    //		should(!project.isModified());

    /* setting the reference to an empty project
    * will unload the existing one. */
    project = Project();
    //		should(!project.isValid());


    std::cout << "#" << std::endl
        << "## Reopen file" << std::endl
        << "#" << std::endl;

    project = Project::open(name);

    SequenceList sequences = project.sequences();

    /* check for map sinks and set corresponding sequence */
    for(auto it = sinks.begin(); it != sinks.cend(); it++) {
        if(dynamic_cast<MapExporterSink*>(*it)) {
            dynamic_cast<MapExporterSink*>(*it)->setFrames(sequences[0].frames());
        }
    }

	/* number of frames in the sequence must be equal to the number of frames
	* that we have added before */
    std::cout << "number of frames " << sequences[0].frames().size() << std::endl;
    /* read  map values from frame */
    FrameList frames = sequences[0].frames();
	Frame::Map map0;
	Image view0;

	frames[1].getView(view0);

    for (size_t i = 0; i < frames.size(); i++) {
		frames[i].getView(view0);
		frames[i].getView(view0, Frame::View::Stereo);
		if (!frames[i].hasMap("test_map"))
			std::cout << "frame number" << i;
	}

	for (ExporterSink * s : sinks) {
		s->m_path = project.base();
        s->m_sequenceName = sequences[0].name();
		s->setup(sourceIds);
	}

    cv::Mat tempMat;

    ExporterSink::setSequenceName(sequences[0].name());
	std::cout << "start to export ... \n";
    for (size_t i = 0; i < frames.size(); i++){
        //ExporterSource::SourceMap sourceMap;
        for (ExporterSource *src : sources){
            for (ExporterSink * s : sinks){
                s->push(src->sources(frames[i]), i);
            }
        }
    }



	/* delete all sources */
	for (ExporterSource * s : sources) {
		delete s;
	}

	//if(rawSource)
	// delete rawSource;

	/* delete all sinks. This should signal the sinks that we are finished and
	* that they should clean up as well */
	for (ExporterSink * s : sinks) {
		delete s;
	}


	std::cout << "Please check \""<< std::getenv("BOSCHGT_BASE")<<"\" for the outputs!\n";
}
