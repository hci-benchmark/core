#include <vigra/random.hxx>
#include <vigra/unittest.hxx>

#include <hookers/interface/Project.h>
#include <hookers/interface/Sequence.h>
#include <hookers/interface/Frame.h>

#include <hookers/toolbox/Projection/SoftwareProjection.h>
#include <hookers/toolbox/Projection/OpenGLProjection.h>

#include <hookers/toolbox/Sampling/StereoSampling.h>

#include <hookers/toolbox/export/Sources/ExporterSource.h>
#include <hookers/toolbox/export/Sinks/DisplaySink.h>

#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

using namespace hookers;
using namespace hookers::interface;
using namespace hookers::toolbox;

int main(int argc, char ** argv)
{



    /* test the histogram class used in the sampler */
    {

        /* test the scaling functions */
        {
            typedef vigra::TinyVector<double,1> TV;
            FlowSampling::LogAxis<1,double> scaler(-20.0, 20.0);

            std::cout << "scaler: " << scaler.physicalToScaled(TV{0.0})
                      << ", " << scaler.physicalToScaled(TV{20.0})
                      << ", " << scaler.physicalToScaled(TV{-20.0})
                      << std::endl;

            should(scaler.physicalToScaled(TV{0.0}) == TV{0.0});
            should(scaler.physicalToScaled(TV{20.0}) == TV{20.0});
            should(scaler.scaledToLogical(TV{20.0}) == TV{1.0});
            should(scaler.scaledToLogical(TV{-20.0}) == TV{0.0});

            should(scaler.scaledToPhysical(scaler.physicalToScaled(TV{20.0})) == TV{20.0});
            should(scaler.scaledToPhysical(scaler.physicalToScaled(TV{0.0})) == TV{0.0});



        }


        vigra::RandomMT19937 rnd;
        typedef FlowSampling::FlowEntry Bla;
        {
            std::cout << "---- normal distribution test" << std::endl;

//            FlowSampling::Histogram h(1001,  {-20, -20, -20}, {20, 20, 20});
            FlowSampling::Histogram h(1001, FlowSampling::LogAxis<3,double>(-29.0, 31.0));

            const double p_mean = 1.0;
            const double p_stddev = 1.0;

            for(int i = 0; i < 100000; i++) {
                h.place(FlowSampling::FlowEntry(rnd.normal(p_mean,p_stddev)));
            }

            auto mean = h.percentile(0.5);
            auto stddev = (h.percentile(0.5+0.5*0.683)-h.percentile(0.5-0.5*0.683))/2.0;
//            std::cout << "stddev" << mean << ", " << stddev << std::endl;
            should(abs(stddev[0]-p_stddev) < 0.05);
            should(abs(mean[0]-p_mean) <= 0.05);

        }

        {
            std::cout << "---- discrete symmetric I test" << std::endl;
//            FlowSampling::Histogram h(1001,  {-20, -20, -20}, {20, 20, 20});
            FlowSampling::Histogram h(1001, FlowSampling::LogAxis<3,double>(-10, 10.0));


            for(int i = 0; i < 20; i++) {
                h.place(FlowSampling::FlowEntry(5));
                h.place(FlowSampling::FlowEntry(-5));
            }

//            for(int i = 0; i < 10; i++) {
//                h.place(FlowSampling::FlowEntry(6));
//                h.place(FlowSampling::FlowEntry(-6));
//            }

            should(abs(h.percentile(0.5)[0]-(-5.0)) <= 0.02);


        }

        {
            std::cout << "---- discrete symmetric II test" << std::endl;
//            FlowSampling::Histogram h0(1001,  {-20, -20, -20}, {20, 20, 20});
            FlowSampling::BaseHistogram<FlowSampling::IdentityAxis<3,double>> h0(1001,
                                                                                 FlowSampling::IdentityAxis<3,double>(-20.0, 20.0));
            FlowSampling::BaseHistogram<FlowSampling::LogAxis<3,double>> h1(1001,
                                                                            FlowSampling::LogAxis<3,double>(-20, 20));

            for(int i = 0; i < 10; i++) {
                h0.place(Bla(-5.0));
                h0.place(Bla(0.0));
                h0.place(Bla(5.0));

                h1.place(Bla(-5.0));
                h1.place(Bla(0.0));
                h1.place(Bla(5.0));
            }

            should(abs(h0.IQR()[0]-10.0) <= 0.05);
            should(abs(h0.percentile(0.5)[0]) <= 0.05);

            should(abs(h1.IQR()[0]-10.0) <= 0.05);
            should(abs(h1.percentile(0.5)[0]) <= 0.05);

        }

    }


    Vector4d intrinsics;
    intrinsics[0] = 1850;
    intrinsics[1] = 1850;
    intrinsics[2] = 2560/2;
    intrinsics[3] = 1080/2;


    Vector6d poseLeft;
    poseLeft[0] = -129.02;
    poseLeft[1] = -19.026;
    poseLeft[2] = 110.726;
    poseLeft[3] = -0.0399;
    poseLeft[4] = -2.2733;
    poseLeft[5] = 2.22;

    Vector6d poseRight = poseLeft;
    poseRight[0] += 0.3;

    Project p = Project::open();
    if(!p.isValid() || p.sequences().empty())
        throw std::runtime_error("Could not open project with sequence.");
    Sequence s = p.sequences().front();

    if(!s.isValid())
        throw std::runtime_error("Could not open Sequence");

    Frame f = s.frames()[1000];

    std::cout << ">> Opening geometry" << std::endl;
    Geometry g = s.geometry();

    std::cout << ">> Reading pointcloud" << std::endl;
    Vector3fArray cloud;
    g.getPointCloud(cloud);

    Array2f depth(f.size());
//    SoftwareProjection proj(cloud, intrinsics);
    OpenGLProjection proj(cloud, intrinsics);
    proj.setPose(poseLeft);
    proj.getDepth(depth);



    // pointSet is trying to be a reduced point cloud where backfaces are eliminated
    std::unordered_set<int> pointSet;
    // This uses reprojection matrix and finds the points which could be projected and are visible in the image.
    proj.getPointList(pointSet);


    std::cout << ">> Sampling" << std::endl;

    /* run the sampler */
    {
        // scloud stands for sampling cloud, it is a reduced cloud
        Vector3fArray scloud(pointSet.size());
        // filling scloud
        {
            size_t ii = 0;
            for (auto it : pointSet) {
                scloud(ii) = cloud(it);
                ++ii;
            }
        }


        // Sampling object samples flow between two cameras.
        FlowSampling sampling(scloud,50);

        // First we need to set the camera intrinsics and both poses
        std::cout << "Frame intrinsics uncertainties: " << f.intrinsicsUncertainty() << std::endl;

        Vector6d poseUncertainty;
        poseUncertainty[0] = 4e-4;
        poseUncertainty[1] = 0.021;
        poseUncertainty[2] = 0.01446;
        poseUncertainty[3] = 2.18e-7;
        poseUncertainty[4] = 1.37e-7;
        poseUncertainty[5] = 9.17e-8;

        // Reduce uncertainty for debugging purpose.
        //poseUncertainty *= .02;
        poseUncertainty *= .02;


        // Then we can call the main sampling function
        {
            Array2d currentUncertainty(6,6);
            currentUncertainty.diagonal() = Array1dView(Array1dShape(6), poseUncertainty.data());
            Array2d intrinsicsUncertainty(6,6);
            intrinsicsUncertainty.diagonal() = Array1dView(Array1dShape(4), f.intrinsicsUncertainty().data());

            FlowSampling::StereoSampler poseSampler(poseLeft, poseRight,
                                                    currentUncertainty, currentUncertainty,
                                                    intrinsics, intrinsicsUncertainty);
            sampling.sample<FlowSampling::StereoSampler>(f.size(),poseSampler);
        }

        std::cout << ">> Fetching results" << std::endl;
        FlowSampling::FlowField flow(f.size());
        FlowSampling::FlowField flowVar(f.size());
        sampling.getFlow(flow);
        sampling.getFlowUncertainty(flowVar);


        cv::Mat flowVis = cv::Mat(flow.shape(1), flow.shape(0), CV_8UC1);
        flowVis = cv::Scalar(0,0,0);

        cv::Mat flowVisVar = cv::Mat(flow.shape(1), flow.shape(0), CV_8UC1);
        flowVisVar = cv::Scalar(0,0,0);


        for(int i = 0; i < flow.shape(1); i++) {
            for(int j = 0; j < flow.shape(0); j++) {
//                flowVis.at<unsigned char>(i,j) = 5*flow(j,i).magnitude();
//                flowVisVar.at<unsigned char>(i,j) = 20*flowVar(j,i).magnitude();
                flowVis.at<unsigned char>(i,j) = unsigned(5*depth(j,i));
                flowVisVar.at<unsigned char>(i,j) = unsigned(5*flowVar(j,i).magnitude());
            }
        }


        ExporterSource::SourceMap sources;
        sources[ExporterSource::SourceIdentifier("blub","bla")] = flowVis;

        DisplaySink sink;
        sink.push(sources, 0);
        cv::waitKey(0);


    }





    std::cout << "<< Finished" << std::endl;


}

