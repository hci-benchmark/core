#include <assert.h>
#include <iomanip>

#include <vigra/random.hxx>
#include <vigra/unittest.hxx>

#include <QTime>
#include <QDebug>

#include <opencv2/core/core.hpp>
#include <opencv2/calib3d/calib3d.hpp>

#include <hookers/interface/Landmark.h>

#include <hookers/toolbox/PoseEstimation/PoseEstimation.h>
#include <hookers/toolbox/RunningStats.h>
#include <hookers/toolbox/intrinsics2mat.cpp>

using namespace hookers;
using namespace hookers::interface;
using namespace hookers::toolbox;

/**
 * @brief getTvecRvec Extract translation vector (rvec) and rotation vector (tvec) from
 * intrinsics 6-vector.
 * @param tvec
 * @param rvec
 */
template<class Vec>
void getTvecRvec(cv::Mat& tvec, cv::Mat& rvec, const Vec& extrinsics) {
    tvec = cv::Mat(3,1,CV_32F);
    rvec = cv::Mat(3,1,CV_32F);
    tvec.at<float>(0,0) = extrinsics[0];
    tvec.at<float>(1,0) = extrinsics[1];
    tvec.at<float>(2,0) = extrinsics[2];
    rvec.at<float>(0,0) = extrinsics[3];
    rvec.at<float>(1,0) = extrinsics[4];
    rvec.at<float>(2,0) = extrinsics[5];
}

template<class Vec>
void vec2mat(const Vec& vec, cv::Mat& mat) {
    const size_t mySize = vec.size();
    mat = cv::Mat(mySize, 1, CV_32F);
    for (size_t ii = 0; ii < mySize; ++ii) {
        mat.at<float>(ii,0) = vec[ii];
    }
}


template<class T>
T sqr(const T val) {
    return val * val;
}

void compareProjections() {
    const double threshold = 1e-10;
    RunningStats poseEstimationVsOpenCvStats, poseEstimationVsOpenCvStatsX, poseEstimationVsOpenCvStatsY;
    std::cerr << "Testing projections..." << std::endl;
    for (size_t ii = 0; ii < 300*1000; ++ii) {
        Vector6d extrinsics;
        for (double &d : extrinsics) {
            d = vigra::randomMT19937().normal();
        }
        Vector4d intrinsics;
        for (double &d : intrinsics) {
            d = vigra::randomMT19937().normal();
        }
        intrinsics[0] = 1 + std::abs(intrinsics[0]);
        intrinsics[1] = 1 + std::abs(intrinsics[1]);

        Vector3d point3D;
        for (double &d : point3D) {
            d = vigra::randomMT19937().normal();
        }

        auto rp3d = hookers::toolbox::PoseEstimation::reproject3d(point3D, extrinsics, intrinsics);

        cv::Mat m_rvec, m_tvec, m_cameraMatrix;
        intrinsics2mat(intrinsics, m_cameraMatrix);
        getTvecRvec(m_tvec, m_rvec, extrinsics);
        std::vector<cv::Point2f> positions2d;
        std::vector<cv::Point3f> m_point3D(1);
        m_point3D[0].x = point3D[0];
        m_point3D[0].y = point3D[1];
        m_point3D[0].z = point3D[2];
        cv::projectPoints(m_point3D, m_rvec, m_tvec, m_cameraMatrix, cv::noArray(), positions2d);

        const double poseEstimationVsOpenCV = std::sqrt(sqr(positions2d[0].x - rp3d[0]) + sqr(positions2d[0].y - rp3d[1]));
        poseEstimationVsOpenCvStats.push(poseEstimationVsOpenCV);
        poseEstimationVsOpenCvStatsX.push(positions2d[0].x - rp3d[0]);
        poseEstimationVsOpenCvStatsY.push(positions2d[0].y - rp3d[1]);


//        std::cerr << positions2d[0] << std::endl;
//        std::cerr << rp3d << std::endl;
//        std::cerr << "Intrinsics: " << intrinsics << std::endl;
//        std::cerr << "Camera matrix: " << std::endl << m_cameraMatrix << std::endl;
//        std::cerr << "####################" << std::endl;

    }
    std::cerr << "poseEstimationVsOpenCvStats: " << poseEstimationVsOpenCvStats.print() << std::endl;
    std::cerr << "x-diff: " << poseEstimationVsOpenCvStatsX.print() << std::endl;
    std::cerr << "y-diff: " << poseEstimationVsOpenCvStatsY.print() << std::endl;
}

void testMatrixConversion2() {
    const double threshold = 1e-10;
    std::cerr << "Testing matrix conversion" << std::endl;
    RunningStats errorStat, nonUnitStat, detStat;
    for (size_t ii = 0; ii < 1000; ++ii) {
        vigra::Matrix<double> original(4,4);
        for (size_t jj = 0; jj < 3; ++jj) {
            original(jj,3) = vigra::randomMT19937().normal();
        }
        original(3,3) = 1;
        vigra::Matrix<double> initRand(3,3), q(3,3), r(3,3);
        for (double &d : initRand) {
            d = vigra::randomMT19937().normal();
        }
        vigra::linalg::qrDecomposition(initRand, q, r);
        if (vigra::linalg::determinant(q) < 0) {
            q.bindInner(0) *= -1;
        }

        original.subarray({0,0},{3,3}) = q;


        auto midResult = PoseEstimation::vectorFromMatrix(original);
        auto mat2 = PoseEstimation::matrixFromVector(midResult);
        const double error = (original - mat2).norm();
        errorStat.push(error);

        auto shouldBeUnit = vigra::Matrix<double>(original.subarray({0,0},{3,3})) * vigra::Matrix<double>(original.subarray({0,0},{3,3})).transpose();
        auto isUnit = vigra::Matrix<double>(3,3);
        isUnit(0,0) = isUnit(1,1) = isUnit(2,2) = 1;
        nonUnitStat.push((shouldBeUnit - isUnit).norm());
        detStat.push(vigra::linalg::determinant(original.subarray({0,0},{3,3})));

//        if (error > threshold) {
//            std::cerr << "Error! " << error << std::endl;
//            std::cerr << original << std::endl << mat2 << std::endl;
//            std::cerr << vigra::Matrix<double>(original.subarray({0,0},{3,3})) * vigra::Matrix<double>(original.subarray({0,0},{3,3})).transpose() << std::endl;
//        }
        should(threshold > error);

    }
//    std::cerr << "Errors: " << errorStat.print() << std::endl;
//    std::cerr << "Non-unity: " << nonUnitStat.print() << std::endl;
//    std::cerr << "Determinant: " << detStat.print() << std::endl;
}

void create_rt(float* pose6, float* rt)
{
    float r[9];
    ceres::AngleAxisToRotationMatrix(pose6+3, r);
    std::copy(r, r + 3, rt);
    std::copy(r+3, r + 6, rt+4);
    std::copy(r+6, r + 9, rt+8);
    std::copy(pose6, pose6 + 3, rt+12);
    rt[15] = 1;
}

void testMatrixConversion3() {
    const double threshold = 1e-5;
    std::cerr << "Testing matrix conversion" << std::endl;
    for (size_t ii = 0; ii < 1000; ++ii) {
        Vector6f original;
        for (float &d : original) {
            d = vigra::randomMT19937().normal();
        }
        vigra::Matrix<float> mat(4,4), mat2(4,4);
        create_rt(original.data(), mat.data());
        Vector6f midResult = PoseEstimation::vectorFromMatrix(mat);
        create_rt(midResult.data(), mat2.data());
        const double error = (mat - mat2).norm();

        if (error > threshold) {
            std::cerr << "Error! " << error << std::endl;
            std::cerr << mat << std::endl << mat2 << std::endl;
        }
        should(threshold > error);

    }
}


int main() {

    QTime timer;
    timer.start();

    compareProjections();

    qDebug() << "Testing projections took " << timer.elapsed() << "ms";
}

