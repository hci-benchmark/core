
#include <vigra/random.hxx>
#include <assert.h>
#include <vigra/unittest.hxx>

#include <hookers/toolbox/PoseEstimation/PoseEstimation.h>
#include <hookers/toolbox/RunningStats.h>

using namespace hookers;
using namespace hookers::toolbox;

void testMatrixConversion1() {
    const double threshold = 1e-10;
    std::cerr << "Testing matrix conversion" << std::endl;
    for (size_t ii = 0; ii < 1000; ++ii) {
        Vector6d original;
        for (double &d : original) {
            d = vigra::randomMT19937().normal();
        }
        auto mat = PoseEstimation::matrixFromVector(original);
        auto midResult = PoseEstimation::vectorFromMatrix(mat);
        auto mat2 = PoseEstimation::matrixFromVector(midResult);
        const double error = (mat - mat2).norm();

        if (error > threshold) {
            std::cerr << "Error! " << error << std::endl;
            std::cerr << mat << std::endl << mat2 << std::endl;
        }
        should(threshold > error);

    }
}

void testMatrixConversion2() {
    const double threshold = 1e-10;
    std::cerr << "Testing matrix conversion" << std::endl;
    RunningStats errorStat, nonUnitStat, detStat;
    for (size_t ii = 0; ii < 1000; ++ii) {
        vigra::Matrix<double> original(4,4);
        for (size_t jj = 0; jj < 3; ++jj) {
            original(jj,3) = vigra::randomMT19937().normal();
        }
        original(3,3) = 1;
        vigra::Matrix<double> initRand(3,3), q(3,3), r(3,3);
        for (double &d : initRand) {
            d = vigra::randomMT19937().normal();
        }
        vigra::linalg::qrDecomposition(initRand, q, r);
        if (vigra::linalg::determinant(q) < 0) {
            q.bindInner(0) *= -1;
        }

        original.subarray({0,0},{3,3}) = q;


        auto midResult = PoseEstimation::vectorFromMatrix(original);
        auto mat2 = PoseEstimation::matrixFromVector(midResult);
        const double error = (original - mat2).norm();
        errorStat.push(error);

        auto shouldBeUnit = vigra::Matrix<double>(original.subarray({0,0},{3,3})) * vigra::Matrix<double>(original.subarray({0,0},{3,3})).transpose();
        auto isUnit = vigra::Matrix<double>(3,3);
        isUnit(0,0) = isUnit(1,1) = isUnit(2,2) = 1;
        nonUnitStat.push((shouldBeUnit - isUnit).norm());
        detStat.push(vigra::linalg::determinant(original.subarray({0,0},{3,3})));

//        if (error > threshold) {
//            std::cerr << "Error! " << error << std::endl;
//            std::cerr << original << std::endl << mat2 << std::endl;
//            std::cerr << vigra::Matrix<double>(original.subarray({0,0},{3,3})) * vigra::Matrix<double>(original.subarray({0,0},{3,3})).transpose() << std::endl;
//        }
        should(threshold > error);

    }
//    std::cerr << "Errors: " << errorStat.print() << std::endl;
//    std::cerr << "Non-unity: " << nonUnitStat.print() << std::endl;
//    std::cerr << "Determinant: " << detStat.print() << std::endl;
}

void create_rt(float* pose6, float* rt)
{
    float r[9];
    ceres::AngleAxisToRotationMatrix(pose6+3, r);
    std::copy(r, r + 3, rt);
    std::copy(r+3, r + 6, rt+4);
    std::copy(r+6, r + 9, rt+8);
    std::copy(pose6, pose6 + 3, rt+12);
    rt[15] = 1;
}

void testMatrixConversion3() {
    const double threshold = 1e-5;
    std::cerr << "Testing matrix conversion" << std::endl;
    for (size_t ii = 0; ii < 1000; ++ii) {
        Vector6f original;
        for (float &d : original) {
            d = vigra::randomMT19937().normal();
        }
        vigra::Matrix<float> mat(4,4), mat2(4,4);
        create_rt(original.data(), mat.data());
        Vector6f midResult = PoseEstimation::vectorFromMatrix(mat);
        create_rt(midResult.data(), mat2.data());
        const double error = (mat - mat2).norm();

        if (error > threshold) {
            std::cerr << "Error! " << error << std::endl;
            std::cerr << mat << std::endl << mat2 << std::endl;
        }
        should(threshold > error);

    }
}


int main() {

    Vector4f intrinsics;
    intrinsics[0] = 2560;
    intrinsics[1] = 1080;
    intrinsics[2] = 2560/2;
    intrinsics[3] = 1080/2;

    Vector3fArray objects(200);


    Vector6f pose(0);
    pose[0] = 0.0;
    pose[1] = 0.0;
    pose[2] = 50.0;
    pose[3] = 0.0;
    pose[4] = 0.0;
    pose[5] = M_PI/10.0;


    for(int i = 0; i < objects.size()/4; i++) {
        objects(i*4 + 0) = Vector3f(i,   0, 0);
        objects(i*4 + 1) = Vector3f(i, 1+i, 0);
        objects(i*4 + 2) = Vector3f(i,   0, 2+2*i);
        objects(i*4 + 3) = Vector3f(i, 1+i, 2+2*i);
    }

    for(int i = 1; i < objects.size(); i++) {
        objects(i) += Vector3f(0, 0, 5);
    }


    /* test solve pnp with RANSAC */
    {
        std::cout << "Points: " << std::endl;
        Vector2fArray images(200);
        for(int i = 0; i < objects.size(); i++) {
            images(i) = PoseEstimation::reproject(objects(i), pose, intrinsics);
            std::cout << objects(i) << "-> " << images(i) << std::endl;
        }

        /* create some outliers */
        for(int i = images.size()-50; i < images.size(); i++) {
            images(i) = Vector2f(6000, 6000);
        }

        Vector6f newPose = PoseEstimation::solvePnPRansac(objects, images, intrinsics, 10);
        std::cout << "Pose: " << pose << " -> " << newPose << std::endl;

        std::cout << "Points: " << std::endl;
        for(int i = 0; i < objects.size(); i++) {
            std::cout << objects(i) << "-> " << PoseEstimation::reproject(objects(i), newPose, intrinsics)
                      << " ==? " << images(i)
                      << std::endl;
        }
    }


    /* test bundle adjustment */
    {

        typedef BundleAdjustmentProblem::ObjectParametrization OP;

        OP objectsp(objects);

        /* set some of the objects to be unlinked */
        auto unlinkedIndices = { 1,2, 4, 10};
        for(int i : unlinkedIndices) {
            std::get<OP::FLAGS>(objectsp[i]) = BundleAdjustmentProblem::ObjectUnlinked;
            std::get<OP::POS>(objectsp[i]) = {0.0, 0.0, 0.0};
        }

        std::vector<BundleAdjustmentProblem::ImageParametrization> img;
        std::vector<Vector6d> poses;

        for(int i = 0; i < 6; i++) {
            Vector6d p;
            p[0] = i/6.0;
            p[1] = 0.0;
            p[2] = 50.0+i*5;
            p[3] = 0.0;
            p[4] = 0.0;
            p[5] = M_PI/10.0;

            poses.push_back(p);
            Vector2fArray tmpImages(objects.size());
            Array1i tmpCorresp(objects.size());

            for(int j = 0; j < objects.size(); j++) {
                tmpImages(j) = PoseEstimation::reproject(objects(j), Vector6f(p), intrinsics);
                tmpCorresp(j) = j;
            }

            img.push_back({tmpImages, tmpCorresp});
        }

        BundleAdjustmentProblem problem(objectsp, img, intrinsics, poses);

        /* check if triangulation succeedd */
        problem.triangulate();
        {
            for(int i : unlinkedIndices) {
                auto &pos =  std::get<OP::POS>(problem.objects()[i]);
                auto diff = (pos-objects(i)).magnitude();
                should((pos-objects(i)).magnitude() < 1e-3);
            }
        }


        problem.solve();

        /* check that fetching covariance with rank deficient but constant parameters work */
        {

            auto pose = problem.poses();
            problem.setConstantObjectPositions(true);
            auto covs = problem.poseUncertainties();
            auto pcovs = problem.posePairwiseUncertainties({std::make_pair(0, 1)});

        }

        /* check that fetching covariance with rank deficient but constant parameters work.
         * this test case should work as well, because we know that only points in the trangulation
         * set are causing rank deficient jacobians */
        {

            auto pose = problem.poses();
            problem.setConstantObjectPositions(false);
            problem.setConstantTriangulatedObjectPositions(true);
            auto covs = problem.poseUncertainties();
            auto pcovs = problem.posePairwiseUncertainties({std::make_pair(0, 1)});

        }


        /* this may throw an error due to rank deficient jacobian but
         * is not garuanteed to fail. but we want to test it anyways */
        try {
            problem.setConstantObjectPositions(false);
            auto covs = problem.poseUncertainties();
            auto pcovs = problem.posePairwiseUncertainties({std::make_pair(0, 1)});

        } catch (...) {
            std::cout << "TEST: Rank deficient Jacobian has been calculated. This was expected." << std::endl;
        }



    }



    testMatrixConversion1();
    testMatrixConversion2();
    testMatrixConversion3();


}

