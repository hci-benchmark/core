#include <vigra/unittest.hxx>
#include <vigra/timing.hxx>

#include <hookers/interface/Project.h>
#include <hookers/interface/Sequence.h>
#include <hookers/interface/Frame.h>


using namespace hookers;
using namespace hookers::interface;

int main()
{
	const std::string name = "test_framesMapList";


	std::cout << "#" << std::endl
		<< "## Create new project file" << std::endl
		<< "#" << std::endl;

	Project project = Project::create(name);


    Frame::List testListEntry(vigra::Shape2(512, 256));
    /*create a simple black and white image*/
    for (int y = 0; y < testListEntry.shape(1); y++)
    {
        for (int x = 0; x < testListEntry.shape(0); x++)
        {
            if ((x % 20) / 10 == (y % 20) / 10)
                testListEntry(x, y) = 0;
            else
                testListEntry(x, y) = 255;
        }
    }

    Frame::Map testMapEntry(vigra::Shape3(512, 256,3));

    /*create a simple black and white image*/
    for (int y = 0; y < testMapEntry.shape(1); y++)
    {
        for (int x = 0; x < testMapEntry.shape(0); x++)
        {
            if ((x % 20) / 10 == (y % 20) / 10)
                testMapEntry(x, y,0) = 255;
            else
                testMapEntry(x, y,2) = 255;
            testMapEntry(x, y, 1) = 255;
        }
    }


    /* prepare frame view */
    Image testViewEntry(2560, 1080);
    testViewEntry = 10;

    testViewEntry.subarray(Image::difference_type(1200, 600),
                           Image::difference_type(1400, 800)) = 255;

//    /*create a simple black and white image*/
//    for (int y = 0; y < testViewEntry.shape(1); y++)
//    {
//        for (int x = 0; x < testViewEntry.shape(0); x++)
//        {
//            if ((x % 20) / 10 == (y % 20) / 10)
//                testViewEntry(x, y) = 0;
//            else
//                testViewEntry(x, y) = 255;

//        }
//    }



    unsigned int FRAMES_ADDED = 0;

	/* open a scope in order to avoid redefinition errors */
	{
		should(project.isValid());
		should(project.parts().empty());

		Sequence s1 = project.addSequence("Sequence");
		should(s1.isValid());

		USETICTOC

		Frame lastFrame;
        for (int i = 0; i < 100; i++) {
            std::cout << "Adding frame " << i << std::endl;

            lastFrame = s1.addFrame();



            TIC;
            lastFrame.setView(testViewEntry);
            lastFrame.setView(testViewEntry, Frame::View::Stereo);
            TOC;

            FRAMES_ADDED++;

        }

		should(lastFrame.isValid());

        FrameList frames = s1.frames();
        frames[1].setList("test_list", testListEntry);
        frames[1].setMap("test_map", testMapEntry);

	}

	std::cout << "#" << std::endl
		<< "## Closing file" << std::endl
		<< "#" << std::endl;

	project.save();
	should(!project.isModified());

	/* setting the reference to an empty project
	* will unload the existing one. */
	project = Project();
	should(!project.isValid());


	std::cout << "#" << std::endl
		<< "## Reopen file" << std::endl
		<< "#" << std::endl;

	project = Project::open(name);
	should(project.isValid());
	should(!project.isModified());


	SequenceList sequences = project.sequences();
	should(sequences.size() == 1);

	should(sequences[0].isValid());

    /* number of frames in the sequence must be equal to the number of frames
     * that we have added before */
    should(sequences[0].frames().size() == FRAMES_ADDED);

    /* read the list and map values from frame */
    FrameList frames = sequences[0].frames();
    Frame::List list0;
    Frame::Map map0;
    Image view0;

    frames[1].getView(view0);

    for(unsigned int i = 0; i < frames.size(); i++) {
        USETICTOC;
        TIC;
        frames[i].getView(view0);
        frames[i].getView(view0, Frame::View::Stereo);
        TOC;
    }


	if (frames[1].hasList("test_list"))
		std::cout << "frame[1] has a list \n";
	if (frames[1].hasMap("test_map"))
		std::cout << "frame[1] has a map \n";


    frames[1].getList("test_list", list0);
    frames[1].getMap("test_map", map0);

    /* compare them with the input file */
    should(map0 == testMapEntry);
    should(list0 == testListEntry);
    should(view0 == testViewEntry);




    should(frames[1].size() == Frame::Size(testViewEntry.width(), testViewEntry.height()));

    /* check null-frames as well */
    should(Frame().size() == Frame::Size());

}
