#include <vigra/unittest.hxx>
#include <hookers/interface/Project.h>
#include <hookers/interface/Sequence.h>
#include <hookers/interface/Track.h>

using namespace hookers;
using namespace hookers::interface;

int main()
{
    const std::string name = "test_tracks";


    Vector3d w1(0, 1, 1), w2(2, 2, 2), w3(2, 5, 3);


	std::cout << "#" << std::endl
			  << "## Create new project file" << std::endl
			  << "#" << std::endl;


    unsigned int ADDED_TRACKS = 0;

	/* open a scope in order to avoid redefinition errors */
	{
        Project project = Project::create(name);

		should(project.isValid());
		should(project.parts().empty());

		Sequence s1 = project.addSequence("Sequence1");
        should(s1.isValid());


        /* first create a few tracks. each creation
         * forces the cache to load and unload here */
        for(int i = 0; i < 100; i++) {
            Track t = s1.addTrack();
            t.setPosition(w1);
            ADDED_TRACKS++;
        }

        /* secondly create a bunch of tracks with
         * a persistent track. this avoids loading and dropping
         * the cache all the same */
        {
            Track persistentTrack;
            for(int i = 0; i < 100000; i++) {
                persistentTrack = s1.addTrack();
                persistentTrack.setPosition(w1);
                ADDED_TRACKS++;
            }
        }

        std::cout << "#" << std::endl
                  << "## Closing file" << std::endl
                  << "#" << std::endl;

        project.save();
        should(!project.isModified());

	}

	std::cout << "#" << std::endl
			  << "## Reopen file" << std::endl
			  << "#" << std::endl;

    Project project = Project::open(name);
	should(project.isValid());
	should(!project.isModified());


	SequenceList sequences = project.sequences();
    TrackList tracks = sequences[0].tracks();

    should(sequences.size() == 1);
    should(tracks.size() == ADDED_TRACKS);

    std::cout << "Track list aquired..." << std::endl;

    for(unsigned int i = 0; i < ADDED_TRACKS; i++) {
        should(tracks[i].isValid());
        should(tracks[i].position() == w1);
        std::cout << "track id: " << i << ", " << tracks[i].id().toString() << std::endl;
    }

    /* test the list interface */
    Vector3fArray trackPositions;
    tracks.getPositions(trackPositions);
    for(unsigned int i = 0; i < ADDED_TRACKS; i++) {
        should(trackPositions(i) == w1);
    }

    std::cout << "track position checked..." << std::endl;
}
