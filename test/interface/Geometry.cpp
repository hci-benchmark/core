#include <vigra/unittest.hxx>
#include <vigra/random.hxx>

#include <hookers/interface/Project.h>
#include <hookers/interface/Geometry.h>
#include <hookers/interface/Part.h>

using namespace hookers;
using namespace hookers::interface;

int main()
{
    const std::string name = "test_geometry";
    Vector3d w0(0, 1, 1), w1(2, 2, 2), w2(2, 5, 3);


	std::cout << "#" << std::endl
			  << "## Create new project file" << std::endl
			  << "#" << std::endl;

	Project project = Project::create(name);

    int totalAddedLandmarksG1 = 0;

	/* open a scope in order to avoid redefinition errors */
	{
		should(project.isValid());
        should(project.geometries().empty());

        Geometry g0 = project.addGeometry("Geometry0");

        should(g0.isValid());
        should(g0.name() == "Geometry0");


        /* add some landmarks so we can test
         * the nearest neighbour and radius search */
        Vector3fArray points(9);
        points(0) = w0;
        points(1) = w1;
        points(2) = w2;
        points(3) = Vector3f(0  , 200,   0);
        points(4) = Vector3f(0  ,   0, 200);
        points(5) = Vector3f(200,   0,   0);
        points(6) = Vector3f(0  ,   0, 300);
        points(7) = Vector3f(300,   0,   0);
        points(8) = Vector3f(  0, 300,   0);
        g0.addLandmarks(points);

        /* test that knn search works  */
        LandmarkList knn =  g0.nearestNeighbourSearch(Vector3d(0, 199, 0), 2);
        should(knn.size() == 2);
        should(knn.front().pos() == points(3));

//        LandmarkList radius = g0.radiusSearch(Vector3f(0, 0, 0), 210);
//        should(radius.size() == 6);

        Part p0 = g0.addPart("Part0");
        should(p0.isValid());

        for(auto l : knn) {
            std::cout << "adding landmark to part... " << l.pos() << std::endl;
            p0.addLandmark(l);
        }

        std::cout << " LALALA" << p0.landmarks().size() << std::endl;
        should(p0.landmarks().size() == 2);
        should(p0.landmarks()[0].pos() == points(3));

        Part p1 = g0.addPart("Part1");
        should(p1.isValid());



	}

//    return 0;

    /* do some heavy lifting test here */
    std::vector<Vector3f> knnLandmarkPoints;
    {
        Geometry g1 = project.addGeometry("Geometry1");
        should(g1.isValid());
        should(g1.name() == "Geometry1");

        Part p0 = g1.addPart("Part0");
        should(p0.isValid());
        should(p0.name() == "Part0");


        std::cout << ">> Generating Random Point Cloud"
                  << std::endl << std::endl;

        /* define a list of points where we seed random further points around.
         * This gives us the formal structure of the test */
        std::vector<Vector3f> seeds =
        {
            { 300,  100, 0},
            { 300, -120, 0},
            {-300,  100, 0},
            {-300, -120, 0}
        };
        const int randomSize = 0xFFFFF;

        std::cout << "Seeds: " << seeds.size() << "," << randomSize << std::endl;


        vigra::RandomNumberGenerator<> rng;

        Vector3fArray points(seeds.size()*randomSize);
        for(unsigned int i = 0; i < seeds.size(); i++) {

            std::cout << seeds[i] << std::endl;
            for(int j = 0; j < randomSize; j++) {

                /* initialize the vector with some random components */
                Vector3f& vec = points(i*randomSize+j);
                for(int k = 0; k < Vector3d::static_size; k++) {
                    vec[k] = rng.normal(0.0, 150.0);
                }

                /* then overlay the seed point */
                vec += seeds[i];

                /* count how many items we have added so far */
                totalAddedLandmarksG1++;

            }
        }

        std::cout << "Points size: " << points.size()
                  << points(0) << points(1*randomSize) << points(seeds.size()*randomSize-100)
                  << std::endl;


        std::cout << "<< Generating Random Point Cloud"
                  << std::endl << std::endl;


        /* now add the points to the geometry object */
        g1.addLandmarks(points);

        std::vector<Vector3f> knnPoints;
        knnPoints.push_back(Vector3f(-300, -120, 0));
        knnPoints.push_back(Vector3f(-300, -100, 0));
        knnPoints.push_back(Vector3f(300, -120, 0));
        knnPoints.push_back(Vector3f(300, -100, 0));

        std::vector<LandmarkList> knn = g1.nearestNeighbourSearches(knnPoints, 3);
        should(knn.size() == 4);

        for(auto ll : knn) {
            should(ll.size() == 3);
            knnLandmarkPoints.push_back(ll.front().pos());
            p0.addLandmark(ll.front());
        }

        g1.removeLandmarks(knn.front());

    }



	std::cout << "#" << std::endl
			  << "## Closing file" << std::endl
			  << "#" << std::endl;

	project.save();

    /* setting the reference to an empty project
     * will unload the existing one. */
    project = Project();
    should(!project.isValid());



	std::cout << "#" << std::endl
			  << "## Reopen file" << std::endl
			  << "#" << std::endl;

	project = Project::open(name);
	should(project.isValid());

	/*
	 * Check if there are exactly three parts
	 */
    GeometryList geometries = project.geometries();
    should(geometries.size() == 2);


	/*
	 * and if their properties are preserved.
	 */
    {
        should(geometries[0].isValid());
        should(geometries[0].name() == "Geometry0");
    }

    {
        Geometry g1 = geometries[1];
        should(g1.isValid());
        should(g1.name() == "Geometry1");

        should(g1.parts().size() == 1);

        std::vector<Vector3f> points;
        points.push_back(Vector3f(-300, -120, 0));
        points.push_back(Vector3f(-300, -100, 0));
        points.push_back(Vector3f(300, -120, 0));
        points.push_back(Vector3f(300, -100, 0));

        std::vector<LandmarkList> knn = g1.nearestNeighbourSearches(points, 3);
        should(knn.size() == 4);
        std::cout << knn.back().back().pos() << std::endl;


        should(g1.parts()[0].landmarks().size() == 4);
        for(unsigned int i = 0; i < knnLandmarkPoints.size(); i++) {
            should(knnLandmarkPoints[i] == g1.parts()[0].landmarks()[i].pos());
        }

        Vector3fArray cloud;
        g1.getPointCloud(cloud);
        should(cloud.size() == totalAddedLandmarksG1-3);
    }


}
