#include <vigra/unittest.hxx>
#include <vigra/timing.hxx>
#include <hookers/interface/Project.h>
#include <hookers/interface/Sequence.h>


using namespace hookers;
using namespace hookers::interface;

int main()
{
    const std::string name = "test_frames";


    Vector3d w1(0, 1, 1), w2(2, 2, 2), w3(2, 5, 3);

    Vector6d p1;
    p1[0] = 1.0; p1[1]  = 1.0; p1[2] = 1.0;
    p1[3] = 0.0; p1[4]  = 0.0; p1[5] = 0.0;
    Vector6d p2(p1);
    p2[5] = 100;

    double time = 1001.0;

    Vector4d intrins(1005, 2005, 2, -3);
    Vector4d intrinsUncertainty(5, 5, 0.1, 0.3);
    Vector6d translation(1);
    translation[0] = 1;
    Vector6d translationUncertainty(2);
    translationUncertainty[0] = 1;
    Vector6d translationStereo(3);
    translationStereo[0] = 1;
    Vector6d translationStereoUncertainty(4);
    translationStereoUncertainty[0] = 1;


	std::cout << "#" << std::endl
			  << "## Create new project file" << std::endl
			  << "#" << std::endl;

	Project project = Project::create(name);

    unsigned int FRAMES_ADDED = 0;

	/* open a scope in order to avoid redefinition errors */
	{
		should(project.isValid());
		should(project.parts().empty());

        Sequence s1 = project.addSequence("Sequence");
        should(s1.isValid());

        USETICTOC

        Frame lastFrame;
        for(int i = 0; i < 1050; i++) {
            std::cout << "Adding frame " << i << std::endl;
            TIC
            lastFrame = s1.addFrame();
            lastFrame.setTime(time);
            lastFrame.setPose(p1);
            lastFrame.setPoseUncertainty(p2);
            lastFrame.setViewTransformation(translation, Frame::View::Base);
            lastFrame.setViewTransformationUncertainty(translationUncertainty, Frame::View::Base);
            lastFrame.setViewTransformation(translationStereo, Frame::View::Stereo);
            lastFrame.setViewTransformationUncertainty(translationStereoUncertainty, Frame::View::Stereo);

            lastFrame.setIntrinsics(intrins);
            lastFrame.setIntrinsicsUncertainty(intrinsUncertainty);
            TOC
            std::cout << std::endl;

            FRAMES_ADDED++;

        }

        should(lastFrame.isValid());

	}



	std::cout << "#" << std::endl
			  << "## Closing file" << std::endl
			  << "#" << std::endl;

	project.save();
	should(!project.isModified());

    /* setting the reference to an empty project
     * will unload the existing one. */
    project = Project();
	should(!project.isValid());


	std::cout << "#" << std::endl
			  << "## Reopen file" << std::endl
			  << "#" << std::endl;

	project = Project::open(name);
	should(project.isValid());
	should(!project.isModified());


	SequenceList sequences = project.sequences();
    should(sequences.size() == 1);

	should(sequences[0].isValid());

    /* number of frames in the sequence must be equal to the number of frames
     * that we have addef before */
    should(sequences[0].frames().size() == FRAMES_ADDED);

    for(int i : {10, int(FRAMES_ADDED)-1}) {
        Frame frame = sequences[0].frames()[i];

        std::cout << "id: " << i << ", " << frame.id().toString() << std::endl;
        should(frame.isValid());
        should(frame.time() == time);
        should(frame.pose() == p1);
        should(frame.poseUncertainty() == p2);
        should(frame.intrinsics() == intrins);
        should(frame.intrinsicsUncertainty() == intrinsUncertainty);
        should(frame.viewTransformation(Frame::View::Base) == translation);
        should(frame.viewTransformationUncertainty(Frame::View::Base) == translationUncertainty);
        should(frame.viewTransformation(Frame::View::Stereo) == translationStereo);
        should(frame.viewTransformationUncertainty(Frame::View::Stereo) == translationStereoUncertainty);

    }






}
