#include <vigra/unittest.hxx>
#include <hookers/interface/Objects.h>

#include <cstdlib>

using namespace hookers::interface;

int main()
{

    Object::Id randId = Object::Id::create();

    std::cout << "Random Ids: " << randId.toString()
              << std::endl;

    should(randId != Object::Id());

    Object::Id id1(0xFF, 0xAA, 0xCC, 0xDD);

    std::cout << "Id1: " << id1.toString() << std::endl;
    std::cout << "Id1 uuid: " << id1.toUuid() << std::endl;

    should(id1.project() == 0xFF);
    should(id1.r1() == 0xAA);
    should(id1.r2() == 0xCC);
    should(id1.object() == 0xDD);


}


