#include <vigra/unittest.hxx>
#include <hookers/interface/Project.h>

using namespace hookers::interface;

int main()
{
    const std::string name = "test";
    std::cout << "### Test default constructor" << std::endl;

    /* open a scope here. this makes sure that the project will be closed
     * when we leave the scope */
    {
        /* create an empty project and check if the default values are correct */
        Project project;
        should(!project.isValid());
        should(!project.isModified());
        should(project.parts().empty());

        std::cout << "### Create new project file" << std::endl;

        /* create a new project */
        project = Project::create(name);
        should(project.isValid());
        should(!project.isModified());
        should(project.parts().empty());
    }


  std::cout << "### Reopen file" << std::endl;

  /* reopen it again */
  Project project = Project::open(name);
  should(project.isValid());

}


