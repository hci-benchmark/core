#include <vigra/unittest.hxx>
#include <vigra/timing.hxx>

#include <hookers/interface/Project.h>
#include <hookers/interface/Sequence.h>
#include <hookers/interface/Track.h>


using namespace hookers;
using namespace hookers::interface;

int main()
{
    const std::string name = "test_linking_frames";


    Vector2d v1(110, 120), v2(210, 220), v3(410, 420), v4(-1, -1);
    Vector3d w1(0, 1, 1), w2(2, 2, 2), w3(2, 5, 3);


	std::cout << "#" << std::endl
			  << "## Create new project file" << std::endl
			  << "#" << std::endl;


	/* open a scope in order to avoid redefinition errors */
    unsigned int FRAMES_ADDED = 0;
    unsigned int TRACKS_ADDED = 0;

	{
        Project project = Project::create(name);
		should(project.isValid());
		should(project.parts().empty());

        Sequence s1 = project.addSequence("Sequence");
        should(s1.isValid());

        /* add frames and tracks */
        Frame f1 = s1.addFrame();
        FRAMES_ADDED++;
        Frame f2 = s1.addFrame();
        FRAMES_ADDED++;
        Frame f3 = s1.addFrame();
        FRAMES_ADDED++;

        Track t1 = s1.addTrack();
        TRACKS_ADDED++;
        Track t2 = s1.addTrack();
        TRACKS_ADDED++;
        Track t3 = s1.addTrack();
        TRACKS_ADDED++;
		Track t4 = s1.addTrack();
        TRACKS_ADDED++;

        /* now add a huge bunch of tracks */
        for(int i = 0; i < 5000; i++) {
            s1.addTrack();
            TRACKS_ADDED++;
        }


        for(int i = 0; i < 100; i++) {
            Frame f = s1.addFrame();
            FRAMES_ADDED++;
        }


        should(s1.isValid());
        should(f1.isValid());
        should(f2.isValid());
        should(f3.isValid());
        should(t1.isValid());
        should(t2.isValid());
        should(t3.isValid());

        f1.linkTrack(t1, v1);
        /* check relinking here */
        f1.linkTrack(t1, v2);
        f1.linkTrack(t2, v2);

        f2.linkTrack(t1, v2);
        f2.linkTrack(t2, v3);
        f2.linkTrack(t3, v1);
        f2.linkTrack(t3, v1);

        f3.linkTrack(t2, v1);
        f3.linkTrack(t3, v2);

        /* check that linking to the stereo view works */
        f3.linkTrack(t2, v1, Frame::View::Stereo);
        f3.linkTrack(t3, v2, Frame::View::Stereo);


        TrackList tracks = s1.tracks();
        FrameList frames = s1.frames();
        Frame f4 = frames[3];
        for(unsigned int i = 0; i < TRACKS_ADDED; i++) {
            f4.linkTrack(tracks[i], v2);
        }


//        std::vector<Frame> f_vector;
//        for(int i = 4; i < FRAMES_ADDED; i++) {
//            USETICTOC;

//            std::cout << "Frame: " << i << std::endl;

//            TIC;
//            Frame f = frames.at(i);
//            TOC;

//            /* for each frame, add some new tracks and discard some old ones */
//            TIC;
//            for(int j = 0; j < 40; j++) {


//                tracks.erase(tracks.begin());
//                tracks.push_back(s1.addTrack());
//                TRACKS_ADDED++;
//            }
//            TOC;

//            TIC;
//            for(int k = 0; k < 10; k++) {
//                f_vector.push_back(frames[i]);
//            }

//            if(i > 20)
//            {
//                for(int k = 0; k < 10; k++) {
//                    f_vector.erase(f_vector.begin());
//                }
//            }
//            TOC;

//            TIC;
//            for(Track & t : tracks) {
//                f.linkTrack(t, (i%2)?v1:v2);
//            }
//            TOC;
//        }


        std::cout << "#" << std::endl
                  << "## Closing file" << std::endl
                  << "#" << std::endl;


        /* actually this is not necessary because
         * the project will be saved automaticly if it
         * goes out of scope, which would occour
         * within the next three lines */
        project.save();
        should(!project.isModified());


    }

	std::cout << "#" << std::endl
			  << "## Reopen file" << std::endl
			  << "#" << std::endl;

    Project project = Project::open(name);
	should(project.isValid());
	should(!project.isModified());

	SequenceList sequences = project.sequences();
    should(sequences.size() == 1);
    should(sequences[0].isValid());


    /* check that all frames are valid */
    FrameList frames = sequences[0].frames();
    should(frames.size() == FRAMES_ADDED);
    should(frames[0].isValid());
    should(frames[1].isValid());
    should(frames[2].isValid());


    /* check that all tracks are valid */
    TrackList tracks = sequences[0].tracks();
    should(tracks.size() == TRACKS_ADDED);
    should(tracks[0].isValid());
    should(tracks[1].isValid());
    should(tracks[2].isValid());
    should(tracks[3].isValid());



    /* check that frames have correct number of linked tracks
     * and that their position matches */
    should(frames[0].tracks().size() == 2);
    should(frames[0].tracks()[0] == tracks[0]);
    should(frames[0].tracks()[1] == tracks[1]);
    should(frames[0].trackPosition(tracks[0]) == v2);
    should(frames[0].trackPosition(tracks[1]) == v2);
    should(frames[0].trackPosition(tracks[3]) == v4); // not linked

    should(frames[1].tracks().size() == 3);
    should(frames[1].tracks()[0] == tracks[0]);
    should(frames[1].tracks()[1] == tracks[1]);
    should(frames[1].tracks()[2] == tracks[2]);
    should(frames[1].trackPosition(tracks[0]) == v2);
    should(frames[1].trackPosition(tracks[1]) == v3);
    should(frames[1].trackPosition(tracks[2]) == v1);
    should(frames[1].trackPosition(tracks[3]) == v4); // not linked

    should(frames[2].tracks().size() == 2);
    should(frames[2].tracks()[0] == tracks[1]);
    should(frames[2].tracks()[1] == tracks[2]);
    should(frames[2].trackPosition(tracks[1]) == v1);
    should(frames[2].trackPosition(tracks[2]) == v2);
    should(frames[2].trackPosition(tracks[3]) == v4); // not linked

    should(frames[2].tracks(Frame::View::Stereo).size() == 2);
    should(frames[2].tracks(Frame::View::Stereo)[0] == tracks[1]);
    should(frames[2].tracks(Frame::View::Stereo)[1] == tracks[2]);

    /* check that resizing the track table works */
    should(frames[3].tracks().size() == TRACKS_ADDED);

    /* check unlinking works */
    frames[0].unlinkTrack(tracks[0]);
    should(frames[0].tracks().size() == 1);
    should(frames[0].tracks()[0] == tracks[1]);

    frames[1].unlinkTrack(tracks[1]);
    should(frames[1].tracks().size() == 2);
    should(frames[1].tracks()[0] == tracks[0]);
    should(frames[1].tracks()[1] == tracks[2]);

    frames[2].unlinkTrack(tracks[1], Frame::View::Stereo);
    should(frames[2].tracks(Frame::View::Stereo).size() == 1);
    should(frames[2].tracks(Frame::View::Stereo)[0] == tracks[2]);

    /* make sure that, even though we remove front tracks,
     * tracks that were added later shouldn't be added twice */
    frames[3].unlinkTrack(tracks[0]);
    frames[3].linkTrack(tracks[TRACKS_ADDED-1], v3);
    should(frames[3].tracks().size() == TRACKS_ADDED-1);


}
