#include <vigra/unittest.hxx>

#include <iostream>
#include <unordered_set>
#include <algorithm>

#include <hookers/interface/AbstractObjectList.h>
#include <hookers/interface/SharedType.h>


using namespace hookers;
using namespace hookers::interface;

class Class;
class ClassData : public SharedData {


public:

    typedef Class Frontend;

    int i;
    ClassData(const int & i) : i(i) {}

    operator Class();

};

class Class {

    friend class ClassData;

    template <typename T, typename S>
    friend class hookers::interface::AbstractSharedTypeList;

private:

    SharedDataPointer<ClassData> d;
    Class(ClassData * cd) : d(cd) {}
    Class(SharedDataPointer<ClassData> & sd) : d(sd) {}
public:
    int i;

    Class(const int & i)  {
        d = new ClassData(i);
    }

    operator int() {
        if(!d)
            return -1;
        return d->i;
    }

    bool operator==(const Class & other)  const {
        return d == other.d;
    }
};

inline ClassData::operator Class (){
       return Class(this);
}

typedef AbstractSharedTypeList<ClassData> TypeList;
typedef AbstractObjectSet<Class> TypeSet;

typedef AbstractObjectList<int> List;
typedef AbstractObjectSet<int> Set;

int main(int argn, char ** argv)
{

    Class c1(1);
    Class c2(2);
    Class c3(3);
    Class c4(4);

    TypeList l1;
    l1.push_back(c1);
    l1.push_back(c2);
    l1.push_back(c3);

    should(l1.size() == 3);

    should(l1[0] == c1);
    should(l1.front() == c1);
    should(*(l1.begin()) == c1);
    should(l1[1] == c2);
    should(l1[2] == c3);
    should(l1.back() == c3);

    l1.push_back(c4);
    should(l1.size() == 4);
    should(l1[3] == c4);

    l1.remove(c1);
    should(l1[0] == c2);
    should(l1.front() == c2);
    should(l1[1] == c3);
    should(l1[2] == c4);
    should(l1.back() == c4);

    should(l1.find(c2) == l1.begin());
    should(l1.find(c3) == (l1.begin()+1));
    should((l1.find(c3) -l1.begin()) == 1);
    should((l1.find(c4) -l1.begin()) == 2);

    Set A;
    A.insert(1);
    A.insert(2);
    A.insert(3);

    Set B;
    B.insert(3);
    B.insert(4);
    B.insert(5);

    Set C;
    C.insert(1);
    C.insert(5);

    should((A | B).size() == 5);
    should((A | C).size() == 4);
    should((B | C).size() == 4);

    Set AandB = A & B;
    Set AandC = A & C;
    Set BandC = B & C;
    should(AandB.size() == 1);
    should(AandC.size() == 1);
    should(BandC.size() == 1);
    should(AandB.contains(3));
    should(AandC.contains(1));
    should(BandC.contains(5));


    Set AminB = A-B;
    Set AminC = A-C;
    Set BminA = B-A;
    Set BminC = B-C;
    Set CminA = C-A;
    Set CminB = C-B;
    should(AminB.size() == 2);
    should(AminC.size() == 2);
    should(BminA.size() == 2);
    should(BminC.size() == 2);
    should(CminA.size() == 1);
    should(CminB.size() == 1);
    should(CminA.contains(5));
    should(CminB.contains(1));

    A &= B;
    C &= B;
    should(A.size() == 1);
    should(C.size() == 1);

    A |= B;
    C |= B;
    should(A.size() == 3);
    should(C.size() == 3);


    List a;
    a.push_back(1);
    a.push_back(2);
    a.push_back(3);

    should(a[0] == 1);
    should(a[1] == 2);
    should(a[2] == 3);

    Set A_list = Set::fromList(a);

    /* check that the set is converted back to a list properly */
    List a_prime = A_list.toList();
    should(a_prime.size() == a.size());
    should(a_prime.contains(1));
    should(a_prime.contains(2));
    should(a_prime.contains(3));


    /* check that removal of a single object works */
    Set A_remove = Set::fromList(a);
    A_remove.remove(1);
    A_remove.remove(10);
    should(A_remove.size() == 2);
    should(A_remove.contains(2));
    should(A_remove.contains(3));


    return 0;

}



