#include <vigra/unittest.hxx>
#include <hookers/interface/Project.h>
#include <hookers/interface/Sequence.h>
#include <hookers/interface/Track.h>


using namespace hookers;
using namespace hookers::interface;

int main()
{
	const std::string name = "test_sequences";


    Vector3d w1(0, 1, 1), w2(2, 2, 2), w3(2, 5, 3);

    Vector6d p1;
    p1[0] = 1.0; p1[1]  = 1.0; p1[2] = 1.0;
    p1[3] = 0.0; p1[4]  = 0.0; p1[5] = 0.0;

    double s1Time = 100000;
    double s2Time = 105000;


	std::cout << "#" << std::endl
			  << "## Create new project file" << std::endl
			  << "#" << std::endl;


	/* open a scope in order to avoid redefinition errors */
	{
        /* when this instance goes out of scope, then
         * it will be closed */
        Project project = Project::create(name);
		should(project.isValid());
		should(project.parts().empty());

		Sequence s1 = project.addSequence("Sequence1");
        should(s1.isValid());
        s1.setTime(s1Time);

        Frame f11 = s1.addFrame();
        Frame f12 = s1.addFrame();
        Frame f13 = s1.addFrame();
        should(f11.isValid());
        should(f12.isValid());
        should(f13.isValid());

        f11.setPose(p1);
        should(f11.pose() == p1);



		Track t11 = s1.addTrack();
		t11.setPosition(w1);
		Track t12 = s1.addTrack();
		t12.setPosition(w2);
		Track t13 = s1.addTrack();
		t13.setPosition(w3);


        /* do some tests about TrackSet */
        TrackSet setA;
        setA.insert(t11);
        setA.insert(t12);

        TrackSet setB;
        setB.insert(t12);
        setB.insert(t13);

        should((setA-setB).contains(t11));
        should((setB-setA).contains(t13));
        should((setA | setB).size() == 3);


		Sequence s2 = project.addSequence("Sequence2");
        should(s2.isValid());
        s2.setTime(s2Time);

		Frame f21 = s2.addFrame();
		Frame f22 = s2.addFrame();
		Track t21 = s2.addTrack();
		t21.setPosition(w2);
		Track t22 = s2.addTrack();
		t22.setPosition(w3);


		Sequence s3 = project.addSequence("Sequence3");
        should(s3.isValid());

		Frame f31 = s3.addFrame();
		Track t31 = s3.addTrack();
		t31.setPosition(w3);



		should(project.sequences().size() == 3);

		should(s1.isValid());
		should(f11.isValid());
		should(f12.isValid());
		should(f13.isValid());
		should(t11.isValid());
		should(t12.isValid());
		should(t13.isValid());


		should(s2.isValid());
		should(f21.isValid());
		should(f22.isValid());
		should(t21.isValid());
		should(t22.isValid());


		should(s3.isValid());
		should(f31.isValid());
		should(t31.isValid());

        std::cout << "#" << std::endl
                  << "## Closing file" << std::endl
                  << "#" << std::endl;

        project.save();
        should(!project.isModified());
	}

	std::cout << "#" << std::endl
			  << "## Reopen file" << std::endl
			  << "#" << std::endl;

    Project project = Project::open(name);
	should(project.isValid());
	should(!project.isModified());


	SequenceList sequences = project.sequences();
	should(sequences.size() == 3);

	should(sequences[0].isValid());
    should(sequences[0].time() == s1Time);
    should(sequences[0].frames()[0].pose() == p1);
	should(sequences[0].frames().size() == 3);
	should(sequences[0].tracks().size() == 3);
	should(sequences[0].tracks()[0].isValid());
	should(sequences[0].tracks()[0].position() == w1);
	should(sequences[0].tracks()[1].isValid());
	should(sequences[0].tracks()[1].position() == w2);
	should(sequences[0].tracks()[2].isValid());
	should(sequences[0].tracks()[2].position() == w3);

	should(sequences[1].isValid());
    should(sequences[1].time() == s2Time);
	should(sequences[1].frames().size() == 2);
	should(sequences[1].tracks().size() == 2);
	should(sequences[1].tracks()[0].isValid());
	should(sequences[1].tracks()[0].position() == w2);
	should(sequences[1].tracks()[1].isValid());
	should(sequences[1].tracks()[1].position() == w3);

	should(sequences[2].isValid());
	should(sequences[2].frames().size() == 1);
	should(sequences[2].tracks().size() == 1);
	should(sequences[2].tracks()[0].isValid());
	should(sequences[2].tracks()[0].position() == w3);

    std::cout << sequences[0].id().toString() << std::endl;
    std::cout << sequences[1].id().toString() << std::endl;
    std::cout << sequences[2].id().toString() << std::endl;

}
