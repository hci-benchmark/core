#include <vigra/unittest.hxx>

#include <hookers/interface/SharedType.h>

using namespace hookers::interface;

int count_derived_gone = 0;
int count_typedata_gone = 0;

int count_ref_called = 0;
int m_deref_called = 0;

class TestPassed {
public:
    TestPassed() {}
};

class TypeData : public SharedData {
public:

    virtual ~TypeData() {
        std::cout << "~TypeData() called: _ref = " << __ref << std::endl;
        count_typedata_gone++;
	}
};

class DerivedTypeData : public TypeData {
public:

    ~DerivedTypeData() {
        std::cout << "~DerivedTypeData() called: _ref = " << __ref << std::endl;
        count_derived_gone++;
	}

    template<class T>
    void deref() {
        std::cout << "DerivedTypeData::deref<TypeData>()..." << std::endl;
        m_deref_called++;
    }

    template<class T>
    void ref() {
        std::cout << "DerivedTypeData::ref<TypeData>()..." << std::endl;
        count_ref_called++;
    }

};


int main(int argn, char ** argv)
{

	DerivedTypeData * d1 = new DerivedTypeData();
    DerivedTypeData * d2 = new DerivedTypeData();
    DerivedTypeData * d3 = new DerivedTypeData();
    DerivedTypeData * d4 = new DerivedTypeData();
    should(d1->__ref == 0);

    SharedDataPointer<DerivedTypeData> p11(d1);
    SharedDataPointer<DerivedTypeData> p21(d2);
    SharedDataPointer<DerivedTypeData> p31(d3);
    SharedDataPointer<DerivedTypeData> p41(d4);
    WeakSharedDataPointer<DerivedTypeData> w11(p11);
    WeakSharedDataPointer<DerivedTypeData> w21(p21);
    WeakSharedDataPointer<DerivedTypeData> w31(p31);
    WeakSharedDataPointer<DerivedTypeData> w41(p41);



    should(d1->__ref == 1);

	{
        SharedDataPointer<DerivedTypeData> p12(d1);
        should(d1->__ref == 2);

        SharedDataPointer<DerivedTypeData> p13(p12);
        should(d1->__ref == 3);
	}


    should(d1->__ref == 1);

    SharedDataPointer<DerivedTypeData,TypeData> p12(d1);
    SharedDataPointer<DerivedTypeData,TypeData> p22(d2);
    WeakSharedDataPointer<DerivedTypeData,TypeData> w12(p12);
    WeakSharedDataPointer<DerivedTypeData,TypeData> w22(p22);

    should(w12.lock() == p12);
    should(w22.lock() == p22);

    should(w11.lock() == p11);
    should(w21.lock() == p21);
    should(w31.lock() == p31);
    should(w41.lock() == p41);

    /* reset all of the non specialized pointer */
    p11.reset();
    p21.reset();
    p31.reset();
    p41.reset();

    /* only d3 and d4 should be deleted by now */
    should(count_typedata_gone == 2);
    should(count_derived_gone == 2);
    should(count_ref_called == 2);
    should(w31.expired());
    should(w41.expired());

    /* now d1 and d2 should be gone as well */
    p12.reset(); p22.reset();
    should(m_deref_called == 2);
    should(count_typedata_gone == 4);
    should(count_derived_gone == 4);

    /* weak refs must be expired by now */
    should(w12.expired());
    should(w22.expired());
    should(!w12);
    should(!w22);


	return 0;
}



