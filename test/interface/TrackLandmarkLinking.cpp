#include <vigra/unittest.hxx>
#include <hookers/interface/Project.h>
#include <hookers/interface/Sequence.h>
#include <hookers/interface/Track.h>

using namespace hookers;
using namespace hookers::interface;

int main()
{
    const std::string name = "test_linking";


    Vector3d w0(0, 1, 10), w1(20, 200, 2000), w2(2, 5, 3);


	std::cout << "#" << std::endl
			  << "## Create new project file" << std::endl
			  << "#" << std::endl;


	/* open a scope in order to avoid redefinition errors */
	{
        Project project = Project::create(name);

		should(project.isValid());
		should(project.parts().empty());


        Geometry g0 = project.addGeometry("Geometry0");
        should(g0.isValid());
        should(g0.name() == "Geometry0");

        /* add some landmarks so we can test
         * the nearest neighbour and radius search */
        Vector3fArray points(9);
        points(0) = w0;
        points(1) = w1;
        points(2) = w2;
        points(3) = Vector3f(0  , 200,   0);
        points(4) = Vector3f(0  ,   0, 200);
        points(5) = Vector3f(200,   0,   0);
        points(6) = Vector3f(0  ,   0, 300);
        points(7) = Vector3f(300,   0,   0);
        points(8) = Vector3f(  0, 300,   0);
        g0.addLandmarks(points);


        Sequence s1 = project.addSequence("Sequence");
        should(s1.isValid());

        /* add frames and tracks */
        Frame f1 = s1.addFrame();
        Frame f2 = s1.addFrame();
        Frame f3 = s1.addFrame();

        Track t1 = s1.addTrack();
        Track t2 = s1.addTrack();
        Track t3 = s1.addTrack();
		Track t4 = s1.addTrack();

        should(s1.isValid());
        should(f1.isValid());
        should(f2.isValid());
        should(f3.isValid());
        should(t1.isValid());
        should(t2.isValid());
        should(t3.isValid());


        std::vector<LandmarkList> knnLists;
        std::vector<Vector3f> knnSearchCentrum;
        knnSearchCentrum.push_back(w0);
        knnSearchCentrum.push_back(w1);
        knnSearchCentrum.push_back(w2);
        knnLists = g0.nearestNeighbourSearches(knnSearchCentrum, 1);

        should(knnLists.size() == 3);
        Landmark l1 = knnLists[0][0];
        Landmark l2 = knnLists[1][0];
        Landmark l3 = knnLists[2][0];

        should(l1.isValid());
        should(l2.isValid());
        should(l3.isValid());

        /* link  tracks to a landmark */
        should(t1.linkToLandmark(l1) == true);
        should(t2.linkToLandmark(l2) == true);
        should(t3.linkToLandmark(l3) == true);


        std::cout << "#" << std::endl
                  << "## Closing file" << std::endl
                  << "#" << std::endl;

        /* actually this is not necessary because
         * the project will be saved automaticly if it
         * goes out of scope, which would occour
         * within the next three lines */
        project.save();
        should(!project.isModified());
    }


	std::cout << "#" << std::endl
			  << "## Reopen file" << std::endl
			  << "#" << std::endl;

    Project project = Project::open(name);
	should(project.isValid());
	should(!project.isModified());

    Geometry g0 = project.geometries()[0];

	SequenceList sequences = project.sequences();
    should(sequences.size() == 1);

	should(sequences[0].isValid());
	should(sequences[0].frames().size() == 3);

    /* Check that the lists contain the right number of elements.
     * Project has four tracks, one is not linked. */
    should(sequences[0].tracks().size() == 4);
    should(sequences[0].tracks().annotated().size() == 3);

    LandmarkList landmarks;
    std::vector<LandmarkList> knnLists;
    std::vector<Vector3f> knnSearchCentrum;
    knnSearchCentrum.push_back(w0);
    knnSearchCentrum.push_back(w1);
    knnSearchCentrum.push_back(w2);
    knnLists = g0.nearestNeighbourSearches(knnSearchCentrum, 1);
    should(knnLists.size() == 3);
    for(auto knn: knnLists) {
        landmarks.push_back(knn.front());
    }
    should(landmarks.size() == 3);

    should(landmarks[0].isValid());
    should(landmarks[1].isValid());
    should(landmarks[2].isValid());

    /* check that all positions of landmarks are right */
    should(landmarks[0].pos() == w0);
    should(landmarks[1].pos() == w1);
    should(landmarks[2].pos() == w2);

    /* check that all links are existing
     * and that the position is set right */
	should(sequences[0].tracks()[0].isValid());
    should(sequences[0].tracks()[0].linkedLandmark() == landmarks[0]);
    should(sequences[0].tracks()[0].hasLink());
	should(sequences[0].tracks()[1].isValid());
    should(sequences[0].tracks()[1].linkedLandmark() == landmarks[1]);
    should(sequences[0].tracks()[1].hasLink());
	should(sequences[0].tracks()[2].isValid());
    should(sequences[0].tracks()[2].linkedLandmark() == landmarks[2]);
    should(sequences[0].tracks()[2].hasLink());

	/* check that the last track is not linked */
	should(sequences[0].tracks()[3].isValid());
	should(sequences[0].tracks()[3].linkedLandmark() == Landmark());
    should(sequences[0].tracks()[3].hasLink() == false);

    /* check that the linked landmarks list is correctly filled */
    should(sequences[0].tracks().annotated().landmarks()[0] == landmarks[0]);
    should(sequences[0].tracks().annotated().landmarks()[1] == landmarks[1]);
    should(sequences[0].tracks().annotated().landmarks()[2] == landmarks[2]);

    /* check that unlinking works */
    sequences[0].tracks()[0].unlinkLandmark();
    should(!sequences[0].tracks()[0].hasLink());
    should(sequences[0].tracks().annotated().size() == 2);
}
