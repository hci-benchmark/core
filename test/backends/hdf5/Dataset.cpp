#include <vigra/unittest.hxx>

#include <hookers/interface/backends/hdf/HDFFile.h>
#include <hookers/interface/backends/hdf/HDFGroup.h>
#include <hookers/interface/backends/hdf/HDFDataset.h>


#include <vigra/timing.hxx>
#include <vigra/multi_math.hxx>

using namespace hookers;
using namespace hdf5;


class TestFailed : public std::exception {
    const char * what() const throw() {
        return "A Test failed...";
    }
};

int main()
{

    std::cout << "### Test a HDF Dataset Class." << std::endl;

    const std::string fileName = "test_hdf5_dataset.h5";

    {


        std::string dsetName = "/test";

        typedef int DSetType;
        const unsigned int N = 3;
        typedef vigra::MultiArrayShape<N>::type Shape;
        typedef vigra::MultiArray<N, DSetType> Array;


        Shape initial;
        Shape chunking;
        Shape max;

        for(int i = 0; i < Shape::static_size; i++) {
            initial[i] = 1000;
            chunking[i] = 200;
            max[i] = 5*initial[i];
        }

        chunking[N-1] = 1;
        max[N-1] = H5S_UNLIMITED;

        {
            USETICTOC;

            hdf5::File file;
            file.open(fileName, hdf5::File::OpenMode::New);
            HDFGroup root = HDFGroup(file.getGroupHandle("/"));

            /* first create a dataset that it completely empty.*/
            hdf5::Dataset dset = file.createDataset<DSetType, N>(dsetName,
                                                              Shape(),
                                                              200,
                                                              chunking,max,
                                                              9);


            /* So far so good. Now write a block of data that extends the current shape of
             * the array. This should happen automaticly.
             * Also set the value of the block different from zero, so we can check
             * the correct assignment of the block afterwards */
            Shape arrayShape(initial);
            arrayShape[N-1] = 1;
            Array array(arrayShape);
            array = 20;

            vigra::MultiArray<N-1, Array::value_type>::difference_type sliceShape;
            for(unsigned int i = 0; i < N-1; i++)
                sliceShape[i] = arrayShape[i];
            vigra::MultiArray<N-1, Array::value_type> slice(sliceShape);
            slice = 30;

            Shape offset;
            offset[N-1] = 10000;
            TIC;
            file.write(dsetName, offset, array);
            TOC;
            offset[N-1] = 5000;
            TIC;
            file.write(dsetName, offset, slice);
            TOC;


            std::cout << std::endl << ">>> EXPECTED ERROS" << std::endl << std::endl;
            try {
                offset[N-2] = 5000;
                file.write(dsetName, offset, array);

                /* we should't get here as we expect the write to fail */
                throw TestFailed();

            } catch (TestFailed) {
                should(false);
            } catch (std::runtime_error) {}

            std::cout << std::endl  << "<<<< EXPECTED ERROS" << std::endl << std::endl;

            file.close();
        }

        {

            hdf5::File file;
            file.open(fileName, hdf5::File::OpenMode::ReadWrite);
            HDFGroup root = HDFGroup(file.getGroupHandle("/"));
            should(root.isValid());
            should(!root.entries().empty());


            Shape offset;


            Shape arrayShape(initial);
            arrayShape[N-1] = 1;
            Array array(arrayShape);

            vigra::MultiArray<N-1, Array::value_type>::difference_type sliceShape;
            for(unsigned int i = 0; i < N-1; i++)
                sliceShape[i] = arrayShape[i];
            vigra::MultiArray<N-1, Array::value_type> slice(sliceShape);


            using namespace vigra::multi_math;



            offset[N-1] = 10000;
            file.read(dsetName, offset, array);
            file.read(dsetName, offset, slice);
            should(vigra::multi_math::all(array == 20));
            should(vigra::multi_math::all(slice == 20));

            offset[N-1] = 5000;
            file.read(dsetName, offset, array);
            file.read(dsetName, offset, slice);
            should(vigra::multi_math::all(slice == 30));
            should(vigra::multi_math::all(array == 30));


            /* check that reading little bit aside of a written chunk
             * will return default values. */
            offset[N-1] = 10000-1;
            file.read(dsetName, offset, array);
            offset[N-1] = 5000-1;
            file.read(dsetName, offset, slice);
            should(!vigra::multi_math::all(array == 20));
            should(!vigra::multi_math::all(slice == 30));
            should(vigra::multi_math::all(slice.bindInner(0) == 200));


            std::cout << std::endl << ">>> EXPECTED ERROS" << std::endl << std::endl;
            /* check that reading outside of dimensions will fail */
            try {
                offset[N-1] = 100001;
                file.read(dsetName, offset, array);

                /* we shouldn't get here */
                throw TestFailed();
            } catch (TestFailed) {
                should(false);
            } catch(...) {}
            std::cout << std::endl << "<<<< EXPECTED ERROS" << std::endl << std::endl;

        }
    }




}


