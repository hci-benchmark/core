#include <vigra/unittest.hxx>

#include <hookers/interface/backends/hdf/HDFGroup.h>
#include <hookers/interface/backends/hdf/HDFFile.h>

using namespace hookers;
using namespace hookers::hdf5;

int main()
{

    std::cout << "### Test a HDF File Class." << std::endl;

    const std::string fileName = "test_hdf5_file.h5";

    {
        hdf5::File file;
        file.open(fileName, hdf5::File::OpenMode::New);
        HDFGroup root = HDFGroup(file.getGroupHandle("/"));
        should(root.isValid());

        HDFGroup test = root.mkdir("test");
        should(test.isValid());
        file.close();
    }

    {
        hdf5::File file;
        file.open(fileName, hdf5::File::OpenMode::ReadWrite);
        HDFGroup root = HDFGroup(file.getGroupHandle("/"));
        should(root.contains("test"));
        should(root.entries().size() == 1);
    }


}


