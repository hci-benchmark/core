
#ifndef _OPENCVUTIL_HXX_
#define _OPENCVUTIL_HXX_

#include <opencv2/opencv.hpp>
#include <climits>
#include "CImg.h"
#include <stdexcept>

namespace openCvUtil
{
	/// Create an OpenCV Mat object from an CImg object
	/// @param img	source image
	/// @param type type of the output Mat in OpenCV notation (e.g. CV_32FC3)
	/// @description the source type will be cast to the target type, e.g ushort(256) will result in uchar(0) 


	template<typename T>
	cv::Mat CImg2Mat(const cimg_library::CImg<T>& img, int type = 0)
	{
		cv::Mat dummy(1, 1, type) ;
		if(dummy.depth() == CV_64F)
		{	
			cimg_library::CImg<double> tmp = img.get_channel(0).get_slice(0) ;
			cv::Mat tmpMat(img.height(), img.width(), type, reinterpret_cast<void*>(tmp.data())) ;
			return cv::Mat(tmpMat.clone()) ;
		}
		else if(dummy.depth() == CV_32F)
		{	
			cimg_library::CImg<float> tmp = img.get_channel(0).get_slice(0) ;
			cv::Mat tmpMat(img.height(), img.width(), type, reinterpret_cast<void*>(tmp.data())) ;
			return cv::Mat(tmpMat.clone()) ;
		}
		else if(dummy.depth() == CV_8U)
		{	
			cimg_library::CImg<unsigned char> tmp = img.get_channel(0).slice(0) ;
			cv::Mat tmpMat(img.height(), img.width(), type, reinterpret_cast<void*>(tmp.data())) ;
			return cv::Mat(tmpMat.clone()) ;
		}
		else if(dummy.depth() == CV_8S)
		{	
			cimg_library::CImg<signed char> tmp = img.get_channel(0).slice(0) ;
			cv::Mat tmpMat(img.height(), img.width(), type, reinterpret_cast<void*>(tmp.data())) ;
			return cv::Mat(tmpMat.clone()) ;
		}
		else if(dummy.depth() == CV_16U)
		{	
			cimg_library::CImg<unsigned short> tmp = img.get_channel(0).slice(0) ;
			cv::Mat tmpMat(img.height(), img.width(), type, reinterpret_cast<void*>(tmp.data())) ;
			return cv::Mat(tmpMat.clone()) ;
		}
		else if(dummy.depth() == CV_16S)
		{	
			cimg_library::CImg<signed short> tmp = img.get_channel(0).slice(0) ;
			cv::Mat tmpMat(img.height(), img.width(), type, reinterpret_cast<void*>(tmp.data())) ;
			return cv::Mat(tmpMat.clone()) ;
		}
		else if(dummy.depth() == CV_32S)
		{	
			cimg_library::CImg<int> tmp = img.get_channel(0).slice(0) ;
			cv::Mat tmpMat(img.height(), img.width(), type, reinterpret_cast<void*>(tmp.data())) ;
			return cv::Mat(tmpMat.clone()) ;
		}
		else
		{
			throw std::invalid_argument("CImgList2Mat::Unsupported DataType!") ;
		}
	}

	/// Create an CImg object from an OpenCV Mat object
	/// @param mat	source image
	/// @description the source type will be cast to the target type, e.g ushort(256) will result in uchar(0) 

	template <typename T>
	cimg_library::CImg<T> Mat2CImg(const cv::Mat& mat)
	{
		if(mat.channels() != 1)
		{
			throw std::invalid_argument(
					"Mat2CImg::multi-channels Matrices are "
					"not supported yet") ;
		}
		switch(mat.depth()) {
			case(CV_64F) : {
				double* ptr = reinterpret_cast<double*>(mat.data) ;
				cimg_library::CImg<double> tmp(ptr, mat.cols, mat.rows) ;
				return cimg_library::CImg<T>(tmp) ;
			}
			case(CV_32F) : {
				float* ptr = reinterpret_cast<float*>(mat.data) ;
				cimg_library::CImg<float> tmp(ptr, mat.cols, mat.rows) ;
				return cimg_library::CImg<T>(ptr, mat.cols, mat.rows) ;
			}
			case(CV_32S) : {
				signed int* ptr = reinterpret_cast<signed int*>(mat.data) ;
				cimg_library::CImg<signed int> tmp(ptr, mat.cols, mat.rows) ;
				return cimg_library::CImg<T>(ptr, mat.cols, mat.rows) ;
			}
			case(CV_16S) : {
				signed short* ptr = reinterpret_cast<signed short*>(mat.data) ;
				cimg_library::CImg<signed short> tmp(ptr, mat.cols, mat.rows) ;
				return cimg_library::CImg<T>(tmp) ;
			}
			case(CV_16U) : {
				unsigned short* ptr = reinterpret_cast<unsigned short*>(mat.data) ;
				cimg_library::CImg<unsigned short> tmp(ptr, mat.cols, mat.rows) ;
				return cimg_library::CImg<T>(tmp) ;
			}
			case(CV_8S) : {
				signed char* ptr = reinterpret_cast<signed char*>(mat.data) ;
				cimg_library::CImg<signed char> tmp(ptr, mat.cols, mat.rows) ;
				return cimg_library::CImg<T>(tmp) ;
			}
			case(CV_8U) : {
				unsigned char* ptr = reinterpret_cast<unsigned char*>(mat.data) ;
				cimg_library::CImg<unsigned char> tmp(ptr, mat.cols, mat.rows) ;
				return cimg_library::CImg<T>(tmp) ;
			}
			default : {
				throw std::invalid_argument(
						"Mat2CImg::unsupported DataType!") ;
			}

		}
	}
}
#endif /* _OPENCVUTIL_HXX_ */
