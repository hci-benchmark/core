#include <sstream>
#include <algorithm>
#include <regex>
#include <unordered_map>

//#define cimg_display 0

#include "openCvUtil.hxx"
#include "CImg.h"

#include <boost/lexical_cast.hpp>
//#include <boost/lambda/lambda.hpp>
#include <boost/io/ios_state.hpp>
#include <boost/smart_ptr.hpp>
#include <boost/program_options.hpp>
#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/stats.hpp>
#include <boost/accumulators/statistics/mean.hpp>
#include <boost/accumulators/statistics/median.hpp>

#include <vigra/multi_array.hxx>
#include <vigra/hdf5impex_mod.hxx>
#include <vigra/linear_algebra.hxx>

#include <stack>
#include <ctime>

#include "FileFormat.hxx"

std::stack<clock_t> tictoc_stack;

void tic() {
	tictoc_stack.push(clock());
}

void toc() {
	std::cout << "  Time elapsed: "
			  << ((double)(clock() - tictoc_stack.top())) / CLOCKS_PER_SEC << std::flush ;

	tictoc_stack.pop();
}

namespace po = boost::program_options;

inline std::string lc(float v) {
	return boost::lexical_cast<std::string>(v) ;
}
inline std::string lc(int v) {
	return boost::lexical_cast<std::string>(v) ;
}
inline std::string lc(size_t v) {
	return boost::lexical_cast<std::string>(v) ;
}

typedef vigra::MultiArray<2, double> CorSet ;
typedef vigra::MultiArrayShape<2>::type Shape2;

struct Settings {
	std::string trackFname ;
	cimg_library::CImg<float> intrinsics ;
	bool verbose ;
	bool sverbose ;
	Settings() : verbose(0), sverbose(0) { ;	}
};



void get_dataset_names(vigra::HDF5File & file, std::vector<std::string> & output, std::string subgroup = "/")
{
	std::set<std::string> datasets;
	file.cd(subgroup);
	file.ls(datasets);
	output.resize(datasets.size());
	std::copy(datasets.begin(), datasets.end(), output.begin());
}


void estimateTransform(const Settings& settings, const std::vector<cv::Point3f>& objectPoints ,const std::vector<cv::Point2f>& imagePoints, cv::Mat& R, cv::Mat& T)
{
	using namespace cv ;
	using namespace std ;
	using namespace boost::accumulators ;

	cv::Mat camMat = openCvUtil::CImg2Mat(settings.intrinsics,CV_64F) ;
	double f_x = camMat.at<double>(0,0) ;
	double f_y = camMat.at<double>(1,1) ;
	double c_x = camMat.at<double>(0,2) ;
	double c_y = camMat.at<double>(1,2) ;

	int minInlierCount = objectPoints.size();

	std::vector<double> distCoeffs ;
	distCoeffs.assign(4,0.0) ;
	//if(_distortionCoeffs().size() > 0)
	//	distCoeffs = _distortionCoeffs() ;
	//create output objects

	cv::Mat rotation ;
	cv::Mat translation ;
	std::vector<cv::Point2f> reprojectedImgPoints ;

	std::vector<size_t> inlierIndex ;
	std::vector<size_t> localInlierIndex ;
	std::vector<double> inlierError ;
	
	bool res = solvePnP(objectPoints, imagePoints, camMat, distCoeffs,
		rotation, translation, 
		false) ; // _iterations(), _reprojectionError(), minInlierCount,inlier) ;

	if(!res)
	{
		throw std::runtime_error("solvePnP failed") ;
	}

	if(settings.verbose)
	{
		//save flags of sout stream and restores them at scope exit
		boost::io::ios_all_saver ias(cout) ;
		cout << std::setprecision(3) ;
		cout << std::fixed ;
		using namespace boost ;
		accumulator_set<double, stats<accumulators::tag::mean, accumulators::tag::median> > acc_all ;
		accumulator_set<double, stats<accumulators::tag::mean,accumulators::tag::median> > acc_in ;
		accumulator_set<double, stats<accumulators::tag::mean,accumulators::tag::median> > acc_z_in ;
		accumulator_set<double, stats<accumulators::tag::mean,accumulators::tag::median> > acc_z_all ;


		cv::projectPoints(objectPoints, rotation, translation,camMat,distCoeffs,reprojectedImgPoints) ;
		cout << "Point projections: " << std::endl ;

		for(size_t ii = 0 ; ii < imagePoints.size() ; ii++)
		{	//reprojection error
			double dist =	sqrt(	pow(double(imagePoints[ii].x - reprojectedImgPoints[ii].x),2.0) +
									pow(double(imagePoints[ii].y - reprojectedImgPoints[ii].y),2.0)) ;
			//double z_err = f_x * (objectPoints[ii].x + translation[0]) / pow((c_x - reprojectedImgPoints[ii].x),2.0) * (imagePoints[ii].x - reprojectedImgPoints[ii].x) ;
			double d_c = sqrt(pow((c_x - reprojectedImgPoints[ii].x),2.0) + pow((c_y - reprojectedImgPoints[ii].y),2.0)) ;
			double z_err = f_x * sqrt(pow(objectPoints[ii].x + translation.at<double>(0),2.0) + pow(objectPoints[ii].y + translation.at<double>(1),2.0)) / (d_c * d_c) * dist ;

			/*
			if(dist <= settings.reprojectionError)
			{	
				acc_in(dist) ;
				acc_z_in(z_err) ;
			} */
			if(settings.sverbose)
			{
				cout	<< ii << "\t" << objectPoints[ii].x << ",  " << objectPoints[ii].y << ",  " << objectPoints[ii].z
					<< " -> " << reprojectedImgPoints[ii].x << ",  " << reprojectedImgPoints[ii].y 
					<< "\t\tError: " << dist << std::endl ; //"\tzError: " << z_err << "\tDistance principal point: " << d_c << std::endl ;
			}

			acc_all(dist) ;
			acc_z_all(z_err) ;
		}
		cout << "Mean reprojection error over inliers : " << boost::accumulators::mean(acc_in) << "  median: " << median(acc_in) 
			<< "\nMean reprojection error over all : " << boost::accumulators::mean(acc_all)  << "  median: " << median(acc_all)
			<< "\nMean z error over inliers : " << boost::accumulators::mean(acc_z_in)  << "  median: " << median(acc_z_in)
			<< "\nMean z error over all : " << boost::accumulators::mean(acc_z_all) << "  median: " << median(acc_z_all) << std::endl ;

		if(settings.sverbose)
		{
			unsigned char white[3] = {255,255,255} ;
			unsigned char red[3] = {255,0,0} ;
			int xs = 5 ;
			cimg_library::CImg<unsigned short> img(2560,1080,1,3,0.0) ;
			for(size_t ii = 0 ; ii < imagePoints.size() ; ii++)
			{
				float x = imagePoints[ii].x ;
				float y = imagePoints[ii].y ;
				img.draw_line(x-xs,y-xs,x+xs,y+xs,white) ;
				img.draw_line(x-xs,y+xs,x+xs,y-xs,white) ;
			}
			for(size_t ii = 0 ; ii < reprojectedImgPoints.size() ; ii++)
			{
				float x = reprojectedImgPoints[ii].x ;
				float y = reprojectedImgPoints[ii].y ;
				img.draw_line(x-xs,y-xs,x+xs,y+xs,red) ;
				img.draw_line(x-xs,y+xs,x+xs,y-xs,red) ;
			}
		}
	}

	T = translation ;
	cv::Mat rot ;
	cv::Rodrigues(rotation,rot) ;
	R = rot ;

	if(settings.verbose)
		cout << "Translation:\n" << translation.at<double>(0) << ", " << translation.at<double>(1) << ", " << translation.at<double>(2) << "\n\nRotation:\n" << rot << std::endl ;
	

}

std::string version()
{
	std::stringstream text ;
	text << "Estimate Camera Pose from tracks obtained with voodoo tracker using OpenCV::solvePnP\n"
		 << "and merge voodoo track and annotations files into one."
		<< "\n\rStephan Meister, HCI, Uni Heidelberg 2013: " << " build: " << __DATE__ << " " << __TIME__ << std::endl ;
	return text.str() ;
}

int main(int argc, char* argv[])
{
	using namespace std ;
	
	// Declare the supported options.
	po::options_description desc("Allowed options");
	desc.add_options()
		("help,h", "produce help message")
		("version,V", "show version information")
		("verbose,v","verbose output")
		("debug","even more verbose")
		("tracks,t", po::value<string>()->required(), "path to track file (HDF5)")
		("intrinsics,i",po::value<string>(), "path to internal camera matrix file")
		("intrinsics-values,c",po::value< vector<float> >()->multitoken(),"camera parameters <fx fy cx cy>. Can be given instead of intrinsics");
		//("parallel","perfom multi-threaded pose estimation (only usefull for ransac)")
	po::variables_map vm;
	try{
		po::store(po::command_line_parser(argc, argv).
				options(desc).run(), vm);
	}
	catch(std::exception& err)
	{
		cerr << err.what() << std::endl ;
		return EXIT_FAILURE ;
	}
	Settings settings ;
	if(vm.empty() || vm.count("help"))
	{	cout << version() << endl ;
		cout << desc << endl ;
		return EXIT_SUCCESS ;
	}
	if(vm.count("version"))
	{	cout << version() << endl ;
		return EXIT_SUCCESS ;
	}
	try {
		po::notify(vm) ;
	}
	catch(std::exception& err)
	{
		cerr << err.what() << "\nuse --help for more information" << std::endl ;
		return EXIT_FAILURE ;
	}


	settings.trackFname = vm["tracks"].as<string>() ;

	settings.intrinsics.assign(3,3,1,1,0.0) ;
	bool intAsFile = vm.count("intrinsics") ;
	bool intAsVals = vm.count("intrinsics-values") ;
	
	if(intAsFile)
	{
		try{
			settings.intrinsics.load(vm["intrinsics"].as<string>().c_str()) ;
		}
		catch(std::exception &err)
		{
			cerr << "Could not read intrinsics" << err.what() << std::endl ;
			return EXIT_FAILURE ;
		}
		if(settings.intrinsics.width() != 3 || settings.intrinsics.height() != 3)
		{
			cerr << "Intrinsic Camera Matrix must be 3x3" << std::endl ;
			return EXIT_FAILURE ;
		}
	}
	else if(intAsVals)
	{
		vector<float> values = vm["intrinsics-values"].as<vector<float> >() ;
		if(values.size() != 4)
		{	cerr << "Must provide 4 values f�r camera intrinsics (fx, fy, cx, cy), got " << values.size() << std::endl ;
			return EXIT_FAILURE ;
		}
		settings.intrinsics(0,0) = values[0] ;
		settings.intrinsics(1,1) = values[1] ;
		settings.intrinsics(2,0) = values[2] ;
		settings.intrinsics(2,1) = values[3] ;
		settings.intrinsics(2,2) = 1.0f ;

	}
	else
	{
		cerr << "No intrinsic camera matrix provided" << std::endl ;
		return EXIT_FAILURE ;
	}

	//-----------------------------------------------------------------------


	boost::shared_ptr<vigra::HDF5File> file ;
	try{	
		file.reset(new vigra::HDF5File(settings.trackFname, vigra::HDF5File::Open));
	}
	catch(std::exception& err)
	{
		cerr << "Could not open file \"" << settings.trackFname << "\" : " << err.what() << std::endl ;
		return EXIT_FAILURE ;
	}
	
	cout << "Reading track file..." << std::endl ;
	vigra::MultiArray<2, double> tracks, frames, frame_tracks ;
	vigra::ArrayVector<hsize_t> dims ;

	dims = file->getDatasetShape("tracks") ;
	tracks.reshape(vigra::Shape2(dims[0],dims[1])) ;
	file->read("tracks",tracks) ;
	dims = file->getDatasetShape("frame_tracks") ;
	frame_tracks.reshape(vigra::Shape2(dims[0],dims[1])) ;
	file->read("frame_tracks",frame_tracks) ;

	dims = file->getDatasetShape("frames") ;
	frames.reshape(vigra::Shape2(dims[0],dims[1])) ;
	file->read("frames",frames) ;


	//-----------------------------------------------------------------------------------------
	//read annotations and solve pnp
	std::cout << "Estimating Transformations" << std::endl ;

	int frame_tracks_offset = -1 ;
	int frame_offset = 0 ;
	int currentFrameId = frame_tracks(0, Columns::frame_tracks::frame) ;
	std::vector<cv::Point3f> objectPoints ;
	std::vector<cv::Point2f> imagePoints ;
	bool exit = false ;
	cout << "FrameId: " << currentFrameId << std::flush ;
	while(frame_tracks_offset < frame_tracks.size(0) && !exit)
	{
		frame_tracks_offset++ ;

		int track_offset =frame_tracks(frame_tracks_offset,Columns::frame_tracks::trackNr) ;
		if(tracks(track_offset, Columns::tracks::annotated) > 0)
		{
			float X = tracks(track_offset, Columns::tracks::meanX) ;
			float Y = tracks(track_offset, Columns::tracks::meanY) ;
			float Z = tracks(track_offset, Columns::tracks::meanZ) ;
			objectPoints.push_back(cv::Point3f(X,Y,Z)) ;
			float x = frame_tracks(frame_tracks_offset, Columns::frame_tracks::x) ;
			float y = frame_tracks(frame_tracks_offset, Columns::frame_tracks::y) ;
			imagePoints.push_back(cv::Point2f(x,y)) ;
		}


		int nextFrameId = frame_tracks(frame_tracks_offset, Columns::frame_tracks::frame) ;
		if(nextFrameId != currentFrameId)
		{
			if(currentFrameId % 100 == 0)
				cout << "\rFrameId: " << currentFrameId << std::flush ;
			if(objectPoints.size() < 3)
			{
				objectPoints.clear() ;
				imagePoints.clear() ;
				currentFrameId = nextFrameId ;
				continue ;
			}
			cv::Mat r;
			cv::Mat t;
			estimateTransform(settings, objectPoints, imagePoints,r, t) ;
			objectPoints.clear() ;
			imagePoints.clear() ;
			using namespace Columns::frames ;
			while(frames(frame_offset, Columns::frames::frameId) != currentFrameId)
			{

				frame_offset++ ;
				if(frame_offset >= frames.size(0))
				{
					exit = true ;
					break ;
				}
			}
			if(exit)
				break ;
			frames(frame_offset,e00) = r.at<double>(0,0) ; frames(frame_offset,e10) = r.at<double>(1,0) ; frames(frame_offset,e20) = r.at<double>(2,0) ; frames(frame_offset,e30) = t.at<double>(0) ;
			frames(frame_offset,e01) = r.at<double>(0,1) ; frames(frame_offset,e11) = r.at<double>(1,1) ; frames(frame_offset,e21) = r.at<double>(2,1) ; frames(frame_offset,e31) = t.at<double>(1) ;
			frames(frame_offset,e02) = r.at<double>(0,2) ; frames(frame_offset,e12) = r.at<double>(1,2) ; frames(frame_offset,e22) = r.at<double>(2,2) ; frames(frame_offset,e32) = t.at<double>(2) ;
			vigra::MultiArray<2, double> tmp = vigra::rowVector(frames, frame_offset) ;
			file->writeBlock("frames", vigra::Shape2(frame_offset,0), tmp) ;
		}
		currentFrameId = nextFrameId ;

	}
	cout << "\rFrameId: " << currentFrameId << std::endl ;
	//-----------------------------------------------------------------------------------------


	//file->write("frames",frames) ;

	file->close() ;
	return EXIT_SUCCESS ;
}

