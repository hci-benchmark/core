#include <omp.h>
#include <sstream>
#include <algorithm>
#include <regex>
#define cimg_display 0

#include "openCvUtil.hxx"
#include "CImg.h"

#include <boost/lexical_cast.hpp>
//#include <boost/lambda/lambda.hpp>
#include <boost/io/ios_state.hpp>
#include <boost/smart_ptr.hpp>
#include <boost/program_options.hpp>
#include <boost/tokenizer.hpp>
#include <boost/token_functions.hpp>

#include <boost/filesystem.hpp>

#include <vigra/multi_array.hxx>
#include <vigra/hdf5impex.hxx>

#include <pcl/point_types.h>
#include <pcl/common/eigen.h>
#include <pcl/common/transforms.h>

#include <pcl/io/ply_io.h>
#include "pcl_mod/range_image_planar_mod.h"
#include <pcl/filters/radius_outlier_removal.h>
#include <pcl/filters/conditional_removal.h>


#include <regex>
#include <Eigen/Dense>

#include "FileFormat.hxx"

namespace po = boost::program_options;

inline std::string lc(float v) {
	return boost::lexical_cast<std::string>(v) ;
}
inline std::string lc(int v) {
	return boost::lexical_cast<std::string>(v) ;
}
inline std::string lc(size_t v) {
	return boost::lexical_cast<std::string>(v) ;
}

typedef vigra::MultiArray<2, float> CorSet ;
typedef vigra::MultiArray<3, float> TrackSet ;
typedef vigra::TinyVector<double,4> Vec4;

typedef vigra::MultiArrayShape<2>::type Shape2;
typedef vigra::MultiArrayShape<3>::type Shape3;

struct Settings {
	bool verbose ;
	bool sverbose ;
	std::string trackFname ;
	std::string plyFilename ;
	cimg_library::CImg<float> intrinsics ;
	int width;
	int height;
	float minRange ;
	float noiseLevel;
	float infVal;
	std::string outpath ;
	boost::shared_ptr<vigra::HDF5File> trackFile ;
	int maxtracks ;
	bool writeDepth ;
	string depthPath ;
	bool writeXYZ ;
	string xyzPath ;
	bool writeRGB ;
	string rgbPath ;
	bool reproject ;
	int startframe ;
	int endframe ;

	Settings() :
	trackFile(0) { ; }
};

struct Results {
	size_t depthMaps ;
	size_t annotatedTracks ;
	size_t reprojectedTrackEntries ;
	size_t reprojectedTracks ;
	size_t trackEntriesAtInfinity ;
	size_t tracksAtInfinity ;
	size_t totalTracks ;
	
	Results() :
	depthMaps(0), annotatedTracks(0),reprojectedTrackEntries(0), reprojectedTracks(0),trackEntriesAtInfinity(0), tracksAtInfinity(0), totalTracks(0) { ;	}
} ;



std::string version()
{
	std::stringstream text ;
	text << "Create depth map for individual frames using camera transformation and pointcloud. Can also reproject all feature tracks for that frame back into XYZ world coordinates."
		<< "\n\rStephan Meister, HCI, Uni Heidelberg 2013: " << " build: " << __DATE__ << " " << __TIME__ << std::endl ;
	return text.str() ;
}

void projectDepth(const Settings& settings, Results& results,int frameId, int ftOffset, const pcl::PointCloud<pcl::PointXYZRGB>::Ptr& cloud, Eigen::Affine3f ext,
	vigra::MultiArray<2, double>& tracks,
	vigra::MultiArray<2, double>& frame_tracks,
	std::map<int, Vec4>& trackMeans)
{

	pcl::PointCloud<pcl::PointXYZRGB>::Ptr tCloud(new pcl::PointCloud<pcl::PointXYZRGB>()) ;
	pcl::PointCloud<pcl::PointXYZRGB>::Ptr fCloud(new pcl::PointCloud<pcl::PointXYZRGB>()) ;

	if(settings.verbose)
	{	cout << "\nTransforming point cloud to camera coordinate system..." << std::endl ;
	cout << ext.matrix() << std::endl ;
	}
	pcl::transformPointCloud<pcl::PointXYZRGB>(*cloud,*tCloud, ext) ;
	
	if(settings.verbose)
	{	cout << "Filtering out all Points behind the camera..." << std::endl ;	}
	pcl::ConditionAnd<pcl::PointXYZRGB>::Ptr range_cond (new
		pcl::ConditionAnd<pcl::PointXYZRGB> ());
	range_cond->addComparison (pcl::FieldComparison<pcl::PointXYZRGB>::ConstPtr (new
		pcl::FieldComparison<pcl::PointXYZRGB> ("z", pcl::ComparisonOps::GT, 0.0)));
	// build the filter
	pcl::ConditionalRemoval<pcl::PointXYZRGB> condrem (range_cond);
	condrem.setInputCloud (tCloud);
	condrem.setKeepOrganized(false);
	// apply filter
	condrem.filter (*fCloud);
	
	//fCloud = tCloud ;

	pcl::RangeImagePlanar rImg ;
	if(settings.verbose)
	{	cout << "Projecting points..." << std::endl ;	}

	rImg.createFromPointCloudWithFixedSize(*fCloud.get(), settings.width, settings.height, settings.intrinsics(2,0), settings.intrinsics(2,1), settings.intrinsics(0,0), settings.intrinsics(1,1),
		(Eigen::Affine3f)Eigen::Translation3f(0.0f, 0.0f, 0.0f),
		pcl::RangeImage::CoordinateFrame::CAMERA_FRAME,
		settings.noiseLevel, settings.minRange);
	vigra::MultiArray<2, float>* dv = 0 ;
	if(settings.writeDepth)
		dv = new vigra::MultiArray<2,float>(vigra::MultiArray<2,float>::difference_type(settings.width, settings.height));
	vigra::MultiArray<3, float>* xyzMap = 0 ;
	vigra::MultiArray<3, float>* rgbMap = 0 ;
	if(settings.writeXYZ|| settings.writeRGB)
	{
		xyzMap = new vigra::MultiArray<3,float>(vigra::MultiArray<3,float>::difference_type(3,settings.width, settings.height));
		rgbMap = new vigra::MultiArray<3,float>(vigra::MultiArray<3,float>::difference_type(3,settings.width, settings.height));
	}
	
	if(settings.writeDepth || settings.writeXYZ || settings.writeRGB)
	{
		if(settings.verbose)
			cout << "creating depth map" << std::endl ;
		for(int yy = 0 ; yy < settings.height ; yy++)
		{
			for(int xx = 0 ; xx < settings.width ; xx++)
			{
				
				pcl::PointWithRangeRGB& p = rImg.getPoint(xx,yy) ;
				float val = p.z ;
				if(settings.writeDepth)
				{	dv->operator()(xx,yy) = ((val < 0.0 || val > 1e6 || val != val) ? settings.infVal : val) ;	}
				if(settings.writeXYZ || settings.writeRGB)
				{
					vigra::MultiArray<3,float>& xyz = *xyzMap ;
					vigra::MultiArray<3,float>& rgb = *rgbMap ;
					
					(p.getVector3fMap ()) = ext.inverse()* (p.getVector3fMap ()) ;
					if(	p.x < -1e6 || p.x > 1e6 || p.x != p.x ||
						p.y < -1e6 || p.y > 1e6 || p.y != p.y ||
						p.z < -1e6 || p.z > 1e6 || p.z != p.z)
					{
						xyz(0,xx,yy) = xyz(1,xx,yy) = xyz(2,xx,yy) = settings.infVal ;
						rgb(0,xx,yy) = rgb(1,xx,yy) = rgb(2,xx,yy) = 0.0 ;
					}
					else
					{
						xyz(0,xx,yy) = p.x ; xyz(1,xx,yy) = p.y ; xyz(2,xx,yy) = p.z ;
						rgb(0,xx,yy) = p.r ; rgb(1,xx,yy) = p.g ; rgb(2,xx,yy) = p.b ;
					}
				}
			}
		}
	
		if(settings.writeDepth)
		{
			#pragma omp critical
			{
				++results.depthMaps ;
				char s[128] ;
				sprintf(s,"%s/%06u",settings.depthPath.c_str(), frameId) ;
				settings.trackFile->createDataset<2, float>(s, vigra::Shape2(settings.width,settings.height), 0);
				settings.trackFile->writeBlock(s, vigra::Shape2(0,0), *dv);
				settings.trackFile->flushToDisk() ;
			}
		}

		if(settings.writeXYZ)
		{
			#pragma omp critical
			{
				char s[128] ;
				sprintf(s,"%s/%06u",settings.xyzPath.c_str(), frameId) ;
				settings.trackFile->createDataset<3, float>(s, vigra::Shape3(3,settings.width,settings.height), 0);
				settings.trackFile->writeBlock(s, vigra::Shape3(0,0,0), *xyzMap);
				settings.trackFile->flushToDisk() ;
			}
		}
		if(settings.writeRGB)
		{
			#pragma omp critical
			{
				char s[128] ;
				sprintf(s,"%s/%06u",settings.rgbPath.c_str(), frameId) ;
				settings.trackFile->createDataset<3, float>(s, vigra::Shape3(3,settings.width,settings.height), 0);
				settings.trackFile->writeBlock(s, vigra::Shape3(0,0,0), *rgbMap);
				settings.trackFile->flushToDisk() ;
			
			}

		}

		
	}
	
	delete dv ;
	delete xyzMap ;

	if(settings.reproject)
	{
		int trackIdx = ftOffset ;
		int i = 0 ;
		while(true)
		{
			namespace ft = Columns::frame_tracks ;
			namespace tc = Columns::tracks ;
			if(frame_tracks(trackIdx,ft::frame) != frameId)
			{
				if(i == 0)
				{
					#pragma omp critical
					{	
						cerr << "Offsets don't match! " <<trackIdx << " " << frame_tracks(trackIdx,ft::frame) << " " << frameId << std::endl ;
					}
				}
				break ;
			}
			i++ ;

			int trackOffset = frame_tracks(trackIdx,ft::trackNr) ;
			if(trackOffset > tracks.size(0))
			{
				#pragma omp critical
				{	
					cerr << "trackOffset "<< trackOffset << " bigger than available track list "<< tracks.size(0)<< ". We are working on a partial file!" << std::endl ;
				}
				return ;
			}
			/*
			if(settings.verbose)
			{
				cout << "FrameID: " << frameId << "  Updating trackID: " << tracks(trackOffset,tc::trackID) << " (offset: " << trackOffset << ")" << std::endl ;
			}
			*/
			float x = frame_tracks(trackIdx,ft::x) ;
			float y = frame_tracks(trackIdx,ft::y) ;
			if(x >= 0 && x < rImg.width && y >= 0.0 && y < rImg.height)
			{	pcl::PointWithRangeRGB p = rImg.getPoint(x,y) ;
				(p.getVector3fMap ()) = ext.inverse()* (p.getVector3fMap ()) ;
				if(!(p.x > 1e6 || p.x != p.x || p.y > 1e6 || p.y != p.y || p.z > 1e6 || p.z != p.z))
				{
					#pragma omp critical
					{
						Vec4& v = trackMeans[tracks(trackOffset,tc::trackID)] ;
						v[0] += p.x ; v[1] += p.y ; v[2] += p.z ; v[3] += 1 ;
						++results.reprojectedTrackEntries ;
					}
				}
				else
				{
					#pragma omp critical
					{
						++results.trackEntriesAtInfinity ;
					}
				}
			}
			trackIdx++ ;
		}
	}
}

int main(int argc, char* argv[])
{
	using namespace std ;
	
	// Declare the supported options.
	po::options_description desc("Allowed options");
	desc.add_options()
		("help,h", "produce help message")
		("version,V", "show version information")
		("verbose,v","verbose output")
		("debug","even more verbose")
		("response-file", po::value<string>(), "response-file")
		("tracks,t", po::value<string>(), "path to annotated tracks file (HDF5)")
		("cloud,c",po::value<string>(),"path to point cloud (ply)")
		("intrinsics,i",po::value<string>(), "path to internal camera matrix file")
		("intrinsics-values,I",po::value< vector<float> >()->multitoken(),"camera parameters <fx fy cx cy>. Can be given instead of intrinsics")
		("width",po::value<int>()->default_value(2560),"camera width")
		("height",po::value<int>()->default_value(1080),"camera height")
		("minRange,m",po::value<float>()->default_value(1.0f),"minmum depth")
		("noiseLevel,n", po::value<float>()->default_value(0.2f), "noise level")
		("infVal",po::value<float>()->default_value(1000.0f),"value to use for infinite/invalid depth")
		("write-depth",po::value<bool>()->default_value(true),"write depth maps to file")
		("depthPath", po::value<string>()->default_value("/depthmaps"), "dataset path for depthmaps")
		("write-xyz", po::value<bool>()->default_value(false), "write xyz maps to file")
		("xyzPath", po::value<string>()->default_value("/xyzmaps"),"dataset path for xyz maps")
		("write-rgb", po::value<bool>()->default_value(false), "write point rgb to file")
		("rgbPath", po::value<string>()->default_value("/rgb"),"dataset path for rgb maps")
		("reproject", po::value<bool>()->default_value(true), "reproject tracks to depth maps and update \"tracks\" in file")
		("maxtracks", po::value<int>()->default_value(0), "maximum number of tracks to read (debug)")
		("parallel", po::value<int>()->default_value(4),"run multithreaded, give number of threads (0 for all, high mem-usage (>30 GB)) !")
		("startframe", po::value<int>()->default_value(0), "start frame")
		("endframe",po::value<int>()->default_value(-1),"end frame (-1 == end), exclusive");

	po::variables_map vm;
	try{
		po::store(po::command_line_parser(argc, argv).
				options(desc).run(), vm);
	}
	catch(std::exception& err)
	{
		cerr << err.what() ;
		return EXIT_FAILURE ;
	}
	Settings settings ;
	if(vm.empty())
	{	cout << version() << endl ;
		cout << desc << endl ;
		return EXIT_SUCCESS ;
	}

	if(vm.count("response-file"))
	{
		// Load the file and tokenize it
		ifstream ifs(vm["response-file"].as<string>().c_str());
		if (!ifs) {
			cout << "Could not open the response file\n";
			return EXIT_FAILURE;
		}
		// Read the whole file into a string
		stringstream ss;
		ss << ifs.rdbuf();
		// Split the file content
		boost::char_separator<char> sep(" \n\r");
		std::string ResponsefileContents( ss.str() );
		boost::tokenizer<boost::char_separator<char> > tok(ResponsefileContents, sep);
		vector<string> args;
		copy(tok.begin(), tok.end(), back_inserter(args));
		// Parse the file and store the options
		store(po::command_line_parser(args).options(desc).run(), vm);     
	}

	if (vm.count("help")) 
	{	cout << version() << desc << "\n";
		return EXIT_SUCCESS ;
	}
	
	if(vm.count("version"))
	{	cout << version() << endl ;
		return EXIT_SUCCESS ;
	}

	settings.verbose = (vm.count("verbose") || vm.count("debug")) ;
	settings.sverbose = (vm.count("debug")) ;

	if(vm.count("tracks"))
	{	settings.trackFname = vm["tracks"].as<string>() ;
		if(!boost::filesystem::exists(settings.trackFname))
		{	cerr << "File \"" << settings.trackFname << "\" does not exist" << std::endl ;
			return EXIT_FAILURE ;
		}

	}
	else
	{
		std::cerr << "No track filename given!" << std::endl ;
		return EXIT_FAILURE ;
	}

	if(vm.count("cloud"))
		settings.plyFilename = vm["cloud"].as<string>() ;
	else
	{
		std::cerr << "No point cloud given!" << std::endl ;
		return EXIT_FAILURE ;
	}

	settings.intrinsics.assign(3,3,1,1,0.0) ;
	bool intAsFile = vm.count("intrinsics") ;
	bool intAsVals = vm.count("intrinsics-values") ;
	
	if(intAsFile)
	{
		try{
			settings.intrinsics.load(vm["intrinsics"].as<string>().c_str()) ;
		}
		catch(std::exception &err)
		{
			cerr << "Could not read intrinsics" << err.what() << std::endl ;
			return EXIT_FAILURE ;
		}
		if(settings.intrinsics.width() != 3 || settings.intrinsics.height() != 3)
		{
			cerr << "Intrinsic Camera Matrix must be 3x3" << std::endl ;
			return EXIT_FAILURE ;
		}
	}
	else if(intAsVals)
	{
		vector<float> values = vm["intrinsics-values"].as<vector<float> >() ;
		if(values.size() != 4)
		{	cerr << "Must provide 4 values f�r camera intrinsics (fx, fy, cx, cy), got " << values.size() << std::endl ;
			return EXIT_FAILURE ;
		}
		settings.intrinsics(0,0) = values[0] ;
		settings.intrinsics(1,1) = values[1] ;
		settings.intrinsics(2,0) = values[2] ;
		settings.intrinsics(2,1) = values[3] ;
		settings.intrinsics(2,2) = 1.0f ;

	}
	else
	{
		cerr << "No intrinsic camera matrix provided" << std::endl ;
		return EXIT_FAILURE ;
	}

	settings.width = vm["width"].as<int>() ;
	settings.height = vm["height"].as<int>() ;
	settings.minRange = vm["minRange"].as<float>() ;
	settings.infVal = vm["infVal"].as<float>() ;
	settings.noiseLevel = vm["noiseLevel"].as<float>() ;
	settings.writeDepth = vm["write-depth"].as<bool>() ;
	settings.reproject = vm["reproject"].as<bool>() ;
	settings.depthPath = vm["depthPath"].as<string>() ;

	settings.writeXYZ = vm["write-xyz"].as<bool>() ;
	settings.xyzPath = vm["xyzPath"].as<string>() ;

	settings.writeRGB = vm ["write-rgb"].as<bool>() ;
	settings.rgbPath = vm["rgbPath"].as<string>() ;
	settings.maxtracks = vm["maxtracks"].as<int>() ;

	settings.startframe = vm["startframe"].as<int>() ;
	settings.endframe = vm["endframe"].as<int>() ;

	int threads = 0 ;
	if(vm.count("parallel"))
		threads = vm["parallel"].as<int>() ;


	//-----------------------------------------------------------------------

	Results results ;

	boost::shared_ptr<vigra::HDF5File> file ;
	try{	
		file.reset(new vigra::HDF5File(settings.trackFname, vigra::HDF5File::Open));
	}
	catch(std::exception& err)
	{
		cerr << "Could not open file \"" << settings.trackFname << "\" : " << err.what() << std::endl ;
		return EXIT_FAILURE ;
	}

	settings.trackFile = file ;
	
	if(settings.writeDepth)
		file->mkdir(settings.depthPath);
	
	cout << "Reading track file..." << std::endl ;
	vigra::MultiArray<2, double> tracks, frames, frame_tracks ;
	vigra::ArrayVector<hsize_t> dims ;

	if(settings.reproject)
	{
		dims = file->getDatasetShape("tracks") ;
		cout << dims << std::endl ;
		tracks.reshape(vigra::Shape2(dims[0],dims[1])) ;
		file->read("tracks",tracks) ;
		dims = file->getDatasetShape("frame_tracks") ;
		cout << dims << std::endl ;
		frame_tracks.reshape(vigra::Shape2(dims[0],dims[1])) ;
		file->read("frame_tracks",frame_tracks) ;
		results.totalTracks = tracks.size(0) ;
	}

	dims = file->getDatasetShape("frames") ;
	cout << dims << std::endl ;
	frames.reshape(vigra::Shape2(dims[0],dims[1])) ;
	file->read("frames",frames) ;

	cout << "Reading Point Clouds..." << std::endl ;
	pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZRGB>()) ;

	// read PLY files
	{
		std::regex rg (".[pP][lL][yY]$") ; //match string ending with .pcd  (case insensitive)
		if(std::regex_search(settings.plyFilename.begin(), settings.plyFilename.end(), rg))
		{
			pcl::PLYReader reader ;
			reader.read(settings.plyFilename, *cloud.get()) ;
		}
	}

	if(cloud->size() == 0)
	{	cerr << "No points in cloud" << std::endl ;
		return EXIT_FAILURE ;
	}
	
	std::map<int, Vec4> trackMeans ;
	
	size_t finished = 0 ;
	std::cout << "Finished 0 / " << frames.size(0) << " Frames\t\t"<< std::flush ;

	if(settings.endframe <= 0)
		settings.endframe = frames.size(0) ;

	omp_set_num_threads(vm["parallel"].as<int>()) ;

	#pragma omp parallel for schedule(dynamic,1)
	for(int ii = settings.startframe ; ii < settings.endframe ; ii++)
	{
		#pragma omp atomic
		++finished ;
		
		#pragma omp critical
		{
			#pragma omp flush(finished)
			std::cout << "\rFinished " << finished <<" / " << frames.size(0) << " Frames\t\t"<< std::flush ;
		}
		if(frames(ii,Columns::frames::e00) == 0.0 && frames(ii,Columns::frames::t0) == 0.0)
			continue ; //no rotation, no transformation, probably not annotated
		Eigen::Affine3f t ;
		namespace fr = Columns::frames ;
		
		t(0,0) = frames(ii,fr::e00) ; t(1,0) = frames(ii,fr::e10) ; t(2,0) = frames(ii,fr::e20) ; t(3,0) = 0.0 ;
		t(0,1) = frames(ii,fr::e01) ; t(1,1) = frames(ii,fr::e11) ; t(2,1) = frames(ii,fr::e21) ; t(3,1) = 0.0 ;
		t(0,2) = frames(ii,fr::e02) ; t(1,2) = frames(ii,fr::e12) ; t(2,2) = frames(ii,fr::e22) ; t(3,2) = 0.0 ;
		t(0,3) = frames(ii,fr::t0) ; t(1,3) = frames(ii,fr::t1) ; t(2,3) = frames(ii,fr::t2) ; t(3,3) = 1.0 ;

		try
		{	projectDepth(settings, results, int(frames(ii,fr::frameId)),int(frames(ii,fr::offset)), cloud, t, tracks, frame_tracks, trackMeans) ;
		}
		catch(std::exception& err)
		{
			#pragma omp critical
			{		
				cerr << "Error in Frame " << ii << " : " << err.what() << std::endl ;
				settings.trackFile->flushToDisk() ;
				abort() ;
			}
		}
	}
	std::cout << "\rFinished " << finished <<" / " << frames.size(0) << " Frames"<< std::endl ;

	if(settings.reproject)
	{
		int offset = 0 ;
		std::cout << "Processing " << trackMeans.size() << " Tracks" << std::endl ;
		std::cout << "Track 0 / " << trackMeans.size() << std::flush ;
		size_t ii = 0 ;
		for(auto it = trackMeans.begin() ; it != trackMeans.end() ; it++, ii++)
		{
			std::cout << "\rTrack "<< ii << " / " << trackMeans.size() << std::flush ;
			Vec4& v = it->second ;
			if(v[3] <= 0)
				continue ;
			int idx = it->first ;
			while((offset < tracks.size(0)) && (tracks(offset, Columns::tracks::trackID) < idx))
			{	offset++ ;
			}
			if(offset >= tracks.size(0))
			{
				cout << "Panic!" << std::endl ;
				break ;
			}
			if(tracks(offset, Columns::tracks::trackID) > idx)
			{	
				cout << "Track " << idx << "doesnt exist. The next available is " << tracks(offset, Columns::tracks::trackID) << " at offset " << offset << std::endl ;
				continue ;
			}
			if(tracks(offset,Columns::tracks::annotated) >0) //tracks was annotated, keep data
			{
				++results.annotatedTracks ;
				continue ;
			}
			double x = v[0] / v[3] ;
			double y = v[1] / v[3] ;
			double z = v[2] / v[3] ;
			tracks(offset, Columns::tracks::meanX) = x ;
			tracks(offset, Columns::tracks::meanY) = y ;
			tracks(offset, Columns::tracks::meanZ) = z ;
			tracks(offset, Columns::tracks::medianX) = x ;
			tracks(offset, Columns::tracks::medianY) = y ;
			tracks(offset, Columns::tracks::medianZ) = z ;
			++results.reprojectedTracks ;
		}
		std::cout << "\rTrack "<< trackMeans.size() << " / " << trackMeans.size() << std::endl ;
		
		//std::string columns ;
		//file->readAttribute("tracks","columns",columns) ;
		file->write("tracks", tracks) ;
		//if(!columns.empty())
		//	file->writeAttribute("tracks","columns",columns);
	}
	file->close() ;

}

