#include <sstream>
#include <algorithm>

#include <boost/smart_ptr.hpp>
#include <boost/program_options.hpp>

#include <vigra/multi_array.hxx>
#include <vigra/hdf5impex_mod.hxx>
#include <boost/lexical_cast.hpp>

#include "FileFormat.hxx"

inline std::string lc(float v) {
	return boost::lexical_cast<std::string>(v) ;
}
inline std::string lc(int v) {
	return boost::lexical_cast<std::string>(v) ;
}
inline std::string lc(size_t v) {
	return boost::lexical_cast<std::string>(v) ;
}


namespace po = boost::program_options;

int main(int argc, char* argv[])
{
	vigra::HDF5File file(std::string(argv[1]),vigra::HDF5File::Open) ;
	
	std::cout << "Reading track file..." << std::endl ;
	vigra::MultiArray<2, double> tracks, frame_tracks ;
	vigra::ArrayVector<hsize_t> dims ;

	dims = file.getDatasetShape("tracks") ;
	std::cout << dims << std::endl ;
	tracks.reshape(vigra::Shape2(dims[0],dims[1])) ;
	file.read("tracks",tracks) ;
	dims = file.getDatasetShape("frame_tracks") ;
	std::cout << dims << std::endl ;
	frame_tracks.reshape(vigra::Shape2(dims[0],dims[1])) ;
	file.read("frame_tracks",frame_tracks) ;

	std::map<int, int> tracksId_offset ;
	if(tracks.size(0) == 8)
	{
		std::cout << "Wrong dim" << std::endl ;
		return EXIT_FAILURE ;
	}
	
	for(size_t ii = 0 ; ii < tracks.size(0) ; ii++)
	{
		int id = tracks(ii,Columns::tracks::trackID) ;
		tracksId_offset[id] = ii ;
	}

	if(frame_tracks.size(0) == 5)
	{
		std::cout << "Wrong dim" << std::endl ;
		return EXIT_FAILURE ;
	}
	/*
	int chunkdim = (frame_tracks.size(0) < 1024 ? frame_tracks.size(0) : 1024) ;
	file.createDataset<2, double>("frame_tracks", vigra::Shape2(frame_tracks.size(0),5), -1.0, vigra::Shape2(chunkdim,5));
	{
		using namespace Columns::frame_tracks ;
		file.writeAttribute("frame_tracks", "columns",
			(std::string("frame:") + lc(frame) + " trackNr (references tracks):" + lc(trackNr)
				+ " x:" + lc(x) + " y:" + lc(y) + " x2:" + lc(x2)
			).c_str()
		);
	}
	*/
	//vigra::MultiArray<2, double>  frame_tracks_row(vigra::Shape2(1, 5));
	for(size_t ii = 0 ; ii < frame_tracks.size(0) ; ii++)
	{
		using namespace Columns ;
		if(ii % 10000 == 0)
		{	std::cout << ii << std::endl ;	}
		/*
		frame_tracks_row(0,frame_tracks::frame) = frame_tracks(ii,frame_tracks::frame) ;
		frame_tracks_row(0,frame_tracks::x) = frame_tracks(ii,frame_tracks::x) ;
		frame_tracks_row(0,frame_tracks::y) = frame_tracks(ii,frame_tracks::y) ;
		frame_tracks_row(0,frame_tracks::x2) = frame_tracks(ii,frame_tracks::x2) ;
		*/
		int id = frame_tracks(ii,frame_tracks::trackNr) ;
		frame_tracks(ii,frame_tracks::trackNr) = tracksId_offset[id] ;

	}
	file.writeBlock("frame_tracks",vigra::Shape2(0,0), frame_tracks) ;
	return EXIT_SUCCESS ;
}