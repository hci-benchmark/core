#ifndef _FILEFORMAT_
#define _FILEFORMAT_

//column definitions for hdf5 datasets
namespace Columns {
	namespace tracks {
		static int trackID = 0 ;
		static int meanX = 1 ;
		static int meanY = 2 ;
		static int meanZ = 3 ;
		static int medianX = 4 ;
		static int medianY = 5 ;
		static int medianZ = 6 ;
		static int annotated = 7 ;
		static int columns = 8 ;
	}
	namespace frame_tracks {
		static int frame = 0 ;
		static int trackNr = 1 ; //references row in "tracks"
		static int x = 2 ;
		static int y = 3 ;
		static int x2 = 4 ; //x in second frame (for stereo)
		static int columns = 5 ;
	}
	namespace frames {
		static int frameId = 0 ;
		static int offset = 1 ; //references row in "frame_tracks"
		static int e00 = 2 ; //camera extrinsics, rotation part
		static int e01 = 3 ;
		static int e02 = 4 ;
		static int e10 = 5 ;
		static int e11 = 6 ;
		static int e12 = 7 ;
		static int e20 = 8 ;
		static int e21 = 9 ;
		static int e22 = 10 ;
		static int e30 = 11 ; //camera extrinsics, translation part
		static int e31 = 12 ;
		static int e32 = 13 ;
		static int t0 = e30 ; //syntactic sugar
		static int t1 = e31 ;
		static int t2 = e32 ;
		static int columns = 14 ;
	}

	namespace annotations {
		static int trackId = 0 ;
		static int offset = 1 ; //indexes tracks
		static int columns = 2 ;
	}
}

#endif /* _FILEFORMAT_ */