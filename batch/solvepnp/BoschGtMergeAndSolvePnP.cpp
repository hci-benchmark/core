#include <sstream>
#include <algorithm>
#include <regex>
#include <unordered_map>

//#define cimg_display 0

#include "openCvUtil.hxx"
#include "CImg.h"

#include <boost/lexical_cast.hpp>
//#include <boost/lambda/lambda.hpp>
#include <boost/io/ios_state.hpp>
#include <boost/smart_ptr.hpp>
#include <boost/program_options.hpp>
#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/stats.hpp>
#include <boost/accumulators/statistics/mean.hpp>
#include <boost/accumulators/statistics/median.hpp>

#include <vigra/multi_array.hxx>
#include <vigra/hdf5impex_mod.hxx>
#include <vigra/linear_algebra.hxx>

#include <stack>
#include <ctime>

#include "FileFormat.hxx"

std::stack<clock_t> tictoc_stack;

void tic() {
    tictoc_stack.push(clock());
}

void toc() {
    std::cout << "  Time elapsed: "
              << ((double)(clock() - tictoc_stack.top())) / CLOCKS_PER_SEC << std::flush ;

    tictoc_stack.pop();
}

namespace po = boost::program_options;

inline std::string lc(float v) {
	return boost::lexical_cast<std::string>(v) ;
}
inline std::string lc(int v) {
	return boost::lexical_cast<std::string>(v) ;
}
inline std::string lc(size_t v) {
	return boost::lexical_cast<std::string>(v) ;
}

typedef vigra::MultiArray<2, double> CorSet ;
typedef vigra::MultiArrayShape<2>::type Shape2;

struct Settings {
	bool verbose ;
	bool sverbose ;
	std::string trackFname ;
	std::string annotFname ;
	std::string outFname ;
	cimg_library::CImg<float> intrinsics ;
};



void get_dataset_names(vigra::HDF5File & file, std::vector<std::string> & output, std::string subgroup = "/")
{
    std::set<std::string> datasets;
    file.cd(subgroup);
    file.ls(datasets);
    output.resize(datasets.size());
    std::copy(datasets.begin(), datasets.end(), output.begin());
}


void estimateTransform(const Settings& settings, const CorSet& set, cv::Mat& R, cv::Mat& T)
{
	using namespace cv ;
	using namespace std ;
	using namespace boost::accumulators ;

	//hdf5 indices for data fields
	int index2dx = 1 ;
	int index2dy = 2 ;
	int index3dx = 3 ;
	int index3dy = 4 ;
	int index3dz = 5 ;

	std::vector<cv::Point3f> objectPoints ;
	std::vector<cv::Point2f> imagePoints ;
		
	for(int ii = 0 ; ii < set.size(1) ; ii++)
	{
		cv::Point3d point3(set(index3dx,ii),set(index3dy,ii),set(index3dz,ii)) ;
		objectPoints.push_back(point3) ;
		cv::Point2d point2(set(index2dx,ii),set(index2dy,ii)) ;
		imagePoints.push_back(point2) ;
	}


	cv::Mat camMat = openCvUtil::CImg2Mat(settings.intrinsics,CV_64F) ;
	double f_x = camMat.at<double>(0,0) ;
	double f_y = camMat.at<double>(1,1) ;
	double c_x = camMat.at<double>(0,2) ;
	double c_y = camMat.at<double>(1,2) ;

	int minInlierCount = set.size(1);
	/*
	if(_minInlierCount > 0)
		minInlierCount = int(_minInlierCount()) ;
	else if(_minInlierCount < 0 && _minInlierCount >= -100)
	{
		minInlierCount = int(_minInlierCount() / -100.0 * corrs.size()) ;
	}
	*/

	std::vector<double> distCoeffs ;
	distCoeffs.assign(4,0.0) ;
	//if(_distortionCoeffs().size() > 0)
	//	distCoeffs = _distortionCoeffs() ;
	//create output objects

	cv::Mat rotation ;
	cv::Mat translation ;
	std::vector<cv::Point2f> reprojectedImgPoints ;

	std::vector<size_t> inlierIndex ;
	std::vector<size_t> localInlierIndex ;
	std::vector<double> inlierError ;
	
	bool res = solvePnP(objectPoints, imagePoints, camMat, distCoeffs,
		rotation, translation, 
		false) ; // _iterations(), _reprojectionError(), minInlierCount,inlier) ;

	if(!res)
	{
		throw std::runtime_error("solvePnP failed") ;
	}

	if(settings.verbose)
	{
		//save flags of sout stream and restores them at scope exit
		boost::io::ios_all_saver ias(cout) ;
		cout << std::setprecision(3) ;
		cout << std::fixed ;
		using namespace boost ;
		accumulator_set<double, stats<accumulators::tag::mean, accumulators::tag::median> > acc_all ;
		accumulator_set<double, stats<accumulators::tag::mean,accumulators::tag::median> > acc_in ;
		accumulator_set<double, stats<accumulators::tag::mean,accumulators::tag::median> > acc_z_in ;
		accumulator_set<double, stats<accumulators::tag::mean,accumulators::tag::median> > acc_z_all ;


		cv::projectPoints(objectPoints, rotation, translation,camMat,distCoeffs,reprojectedImgPoints) ;
		cout << "Point projections: " << std::endl ;

		for(size_t ii = 0 ; ii < imagePoints.size() ; ii++)
		{	//reprojection error
			double dist =	sqrt(	pow(double(imagePoints[ii].x - reprojectedImgPoints[ii].x),2.0) +
									pow(double(imagePoints[ii].y - reprojectedImgPoints[ii].y),2.0)) ;
			//double z_err = f_x * (objectPoints[ii].x + translation[0]) / pow((c_x - reprojectedImgPoints[ii].x),2.0) * (imagePoints[ii].x - reprojectedImgPoints[ii].x) ;
			double d_c = sqrt(pow((c_x - reprojectedImgPoints[ii].x),2.0) + pow((c_y - reprojectedImgPoints[ii].y),2.0)) ;
			double z_err = f_x * sqrt(pow(objectPoints[ii].x + translation.at<double>(0),2.0) + pow(objectPoints[ii].y + translation.at<double>(1),2.0)) / (d_c * d_c) * dist ;

			/*
			if(dist <= settings.reprojectionError)
			{	
				acc_in(dist) ;
				acc_z_in(z_err) ;
			} */
			if(settings.sverbose)
			{
				cout	<< ii << "\t" << objectPoints[ii].x << ",  " << objectPoints[ii].y << ",  " << objectPoints[ii].z
					<< " -> " << reprojectedImgPoints[ii].x << ",  " << reprojectedImgPoints[ii].y 
					<< "\t\tError: " << dist << std::endl ; //"\tzError: " << z_err << "\tDistance principal point: " << d_c << std::endl ;
			}

			acc_all(dist) ;
			acc_z_all(z_err) ;
		}
		cout << "Mean reprojection error over inliers : " << boost::accumulators::mean(acc_in) << "  median: " << median(acc_in) 
			<< "\nMean reprojection error over all : " << boost::accumulators::mean(acc_all)  << "  median: " << median(acc_all)
			<< "\nMean z error over inliers : " << boost::accumulators::mean(acc_z_in)  << "  median: " << median(acc_z_in)
			<< "\nMean z error over all : " << boost::accumulators::mean(acc_z_all) << "  median: " << median(acc_z_all) << std::endl ;

		if(settings.sverbose)
		{
			unsigned char white[3] = {255,255,255} ;
			unsigned char red[3] = {255,0,0} ;
			int xs = 5 ;
			cimg_library::CImg<unsigned short> img(2560,1080,1,3,0.0) ;
			for(size_t ii = 0 ; ii < imagePoints.size() ; ii++)
			{
				float x = imagePoints[ii].x ;
				float y = imagePoints[ii].y ;
				img.draw_line(x-xs,y-xs,x+xs,y+xs,white) ;
				img.draw_line(x-xs,y+xs,x+xs,y-xs,white) ;
			}
			for(size_t ii = 0 ; ii < reprojectedImgPoints.size() ; ii++)
			{
				float x = reprojectedImgPoints[ii].x ;
				float y = reprojectedImgPoints[ii].y ;
				img.draw_line(x-xs,y-xs,x+xs,y+xs,red) ;
				img.draw_line(x-xs,y+xs,x+xs,y-xs,red) ;
			}
		}
	}

	T = translation ;
	cv::Mat rot ;
	cv::Rodrigues(rotation,rot) ;
	R = rot ;

	if(settings.verbose)
		cout << "Translation:\n" << translation.at<double>(0) << ", " << translation.at<double>(1) << ", " << translation.at<double>(2) << "\n\nRotation:\n" << rot << std::endl ;
	

}

std::string version()
{
	std::stringstream text ;
	text << "Estimate Camera Pose from tracks obtained with voodoo tracker using OpenCV::solvePnP\n"
		 << "and merge voodoo track and annotations files into one."
		<< "\n\rStephan Meister, HCI, Uni Heidelberg 2013: " << " build: " << __DATE__ << " " << __TIME__ << std::endl ;
	return text.str() ;
}

int main(int argc, char* argv[])
{
	using namespace std ;
	
	// Declare the supported options.
	po::options_description desc("Allowed options");
	desc.add_options()
		("help,h", "produce help message")
		("version,V", "show version information")
		("verbose,v","verbose output")
		("debug","even more verbose")
		("tracks,t", po::value<string>()->required(), "path to track file (HDF5)")
		("annotations,a", po::value<string>()->required(), "path to annotations file")
		("output,o", po::value<string>()->default_value("out.h5"), "path to output file")
		("intrinsics,i",po::value<string>(), "path to internal camera matrix file")
		("intrinsics-values,c",po::value< vector<float> >()->multitoken(),"camera parameters <fx fy cx cy>. Can be given instead of intrinsics")
		("maxtrack", po::value<int>(), "number of tracks to read (debug)")
		("maxframe", po::value<int>(), "number of frames to process (debug)");
		//("parallel","perfom multi-threaded pose estimation (only usefull for ransac)")
	po::variables_map vm;
	try{
		po::store(po::command_line_parser(argc, argv).
				options(desc).run(), vm);
	}
	catch(std::exception& err)
	{
		cerr << err.what() << std::endl ;
		return EXIT_FAILURE ;
	}
	Settings settings ;
	if(vm.empty() || vm.count("help"))
	{	cout << version() << endl ;
		cout << desc << endl ;
		return EXIT_SUCCESS ;
	}
	if(vm.count("version"))
	{	cout << version() << endl ;
		return EXIT_SUCCESS ;
	}
	try {
		po::notify(vm) ;
	}
	catch(std::exception& err)
	{
		cerr << err.what() << "\nuse --help for more information" << std::endl ;
		return EXIT_FAILURE ;
	}

	settings.verbose = (vm.count("verbose") || vm.count("debug")) ;
	settings.sverbose = (vm.count("debug")) ;

	settings.trackFname = vm["tracks"].as<string>() ;
	settings.annotFname = vm["annotations"].as<string>() ;
	settings.outFname = vm["output"].as<string>() ;

	settings.intrinsics.assign(3,3,1,1,0.0) ;
	bool intAsFile = vm.count("intrinsics") ;
	bool intAsVals = vm.count("intrinsics-values") ;
	
	if(intAsFile)
	{
		try{
			settings.intrinsics.load(vm["intrinsics"].as<string>().c_str()) ;
		}
		catch(std::exception &err)
		{
			cerr << "Could not read intrinsics" << err.what() << std::endl ;
			return EXIT_FAILURE ;
		}
		if(settings.intrinsics.width() != 3 || settings.intrinsics.height() != 3)
		{
			cerr << "Intrinsic Camera Matrix must be 3x3" << std::endl ;
			return EXIT_FAILURE ;
		}
	}
	else if(intAsVals)
	{
		vector<float> values = vm["intrinsics-values"].as<vector<float> >() ;
		if(values.size() != 4)
		{	cerr << "Must provide 4 values f�r camera intrinsics (fx, fy, cx, cy), got " << values.size() << std::endl ;
			return EXIT_FAILURE ;
		}
		settings.intrinsics(0,0) = values[0] ;
		settings.intrinsics(1,1) = values[1] ;
		settings.intrinsics(2,0) = values[2] ;
		settings.intrinsics(2,1) = values[3] ;
		settings.intrinsics(2,2) = 1.0f ;

	}
	else
	{
		cerr << "No intrinsic camera matrix provided" << std::endl ;
		return EXIT_FAILURE ;
	}

	int maxtrack = 0 ;
	if(vm.count("maxtrack"))
		maxtrack = vm["maxtrack"].as<int>() ;
	int maxframe = 0 ;
	if(vm.count("maxframe"))
		maxframe = vm["maxframe"].as<int>() ;

	//-----------------------------------------------------------------------

	std::cout << "Reading track file" << std::endl ;
	vigra::HDF5File file(settings.trackFname, vigra::HDF5File::Open);
	std::vector<std::string> dsets;
	get_dataset_names(file, dsets, "/tracks");
	typedef vigra::TinyVector<double,3> Vec3;
	std::map<int, std::vector<Vec3> > track_2_frame; //track_2_frame.rehash(dsets.size()) ;
	std::map<int, std::vector<Vec3> > frame_2_track; 
	int nTrackFrames = 0;
	std::cout << "Processing Tracks:\nTrack 0 / " << dsets.size() << std::flush ;
	if(maxtrack <= 0 || maxtrack > dsets.size())
		maxtrack = dsets.size() ;
	for(int ii = 0; ii < maxtrack; ++ii)
	{
		if(ii% 1000 == 0)
			std::cout << "\rTrack "<< ii << " / " << dsets.size() << flush ;
		vigra::MultiArray<2, double> tmp;
		file.readAndResize(dsets[ii], tmp);
		int trackName = std::atoi(dsets[ii].c_str());
		
		if(tmp.shape(1) == 0)
			cerr << "Dataset #" << ii << " : " << dsets[ii] << " is empty!" << std::endl ;
		
		for(int jj = 0; jj < tmp.shape(1); ++jj)
		{
			int frameNo = tmp(0,jj);
			track_2_frame[trackName].push_back(Vec3(frameNo, tmp(1,jj), tmp(2,jj)));
			frame_2_track[frameNo].push_back(Vec3(trackName,tmp(1,jj), tmp(2,jj)));
			nTrackFrames +=1;
		}
	}
	std::cout << "\rTrack "<< dsets.size() << " / " << dsets.size() << std::endl ;
	std::cout << "nTrackFrames: " << nTrackFrames << std::endl ;
	//-----------------------------------------------------------------------------------------
	//read annotations and solve pnp

	std::cout << "Estimating Transformations" << std::endl ;

	vigra::HDF5File annotfile(settings.annotFname, vigra::HDF5File::Open);
	vector<string> datasets = annotfile.ls() ;
	auto end = std::remove_if(datasets.begin(), datasets.end(), [](std::string s){ return s.back() == '/' ;	}) ;
	datasets.erase(end, datasets.end());
	
	cout << (settings.sverbose ? (lc(datasets.size()) + " Datasets found\n") : "") ;

	map<int, vigra::MultiArray<2, double> > annotations ; //indexed by frame
	std::map<int, Vec3> annotatedTracks ;
	try {
		if(maxframe == 0 || maxframe > datasets.size())
			maxframe = datasets.size() ;

		size_t i = 0 ;
		for(auto it = datasets.begin(); it != datasets.end() && i < maxframe ; it++, i++)
		{
			vigra::MultiArray<2, double> tmp ;
			annotfile.readAndResize(*it, tmp) ;
			if(tmp.size(0) != 6)
				throw std::runtime_error("Dimension mixup!") ;
			for(int ii = 0 ; ii < tmp.size(1) ; ii++)
			{	
				Vec3 v ; v[0] = tmp(3,ii) ; v[1] = tmp(4,ii) ; v[2] = tmp(5,ii) ;
				annotatedTracks[int(tmp(0,ii))] = v ;
			}
			annotations[std::atoi(it->c_str())] = tmp ;
		}
	}
	catch(exception &err)
	{
		cerr << err.what() << std::endl ;
		return EXIT_FAILURE ;
	}
	cout << "Got " << annotations.size() << " Correspondence sets" << std::endl ;
	std::map<int, cv::Mat> R;
	std::map<int, cv::Mat> T;

	for(auto it = annotations.begin() ; it != annotations.end() ; it++)
	{
		if(settings.verbose)
		{
			cout << "\nFrame: " << it->first << std::endl ;
		}
		try {
			estimateTransform(settings, it->second,R[it->first], T[it->first]) ;
		}
		catch(std::runtime_error& err)
		{
			cout << "Failed for frame " << it->first << std::endl ;
		}
	}
	std::cout << "Finished" << std::endl ;
	
	//-----------------------------------------------------------------------------------------

	std::cout << "Merging Data and writing results file" << std::endl ;

	// create output file, datasets and column description attributes
	vigra::HDF5File fileOut(settings.outFname, vigra::HDF5File::New);
	int maxchunkdim = 1024 ;
	int chunkdim = (track_2_frame.size() < maxchunkdim ? track_2_frame.size() : maxchunkdim) ;
	fileOut.createDataset<2, double>("tracks", vigra::Shape2(track_2_frame.size(),8), 0, vigra::Shape2(chunkdim,8));
	{
		using namespace Columns::tracks ;
		fileOut.writeAttribute("tracks","columns",
			(string("trackID:") + lc(Columns::tracks::trackID)
				+ " meanX:" + lc(meanX) + " meanY:" + lc(meanY) + " meanZ:" + lc(meanZ)
				+ " medianX:" + lc(medianX) + " medianY:" + lc(medianY) + " medianZ:" + lc(medianZ)
				+ " annotated:" + lc(annotated)
			).c_str()
		);
	}
	chunkdim = (frame_2_track.size() < maxchunkdim ? frame_2_track.size() : maxchunkdim) ;
	fileOut.createDataset<2, double>("frames", vigra::Shape2(frame_2_track.size(),14), 0, vigra::Shape2(chunkdim, 14));
	{
		using namespace Columns::frames ;
		fileOut.writeAttribute("frames", "columns",
			(string("frameId:") + lc(frameId) + " offset (references frame_tracks):" + lc(offset)
				+ " e00-e32(extrinsics):" + lc(e00) + "-" + lc(e32)
			).c_str()
		);
	}
	chunkdim = (nTrackFrames < maxchunkdim ? nTrackFrames : maxchunkdim) ;
	fileOut.createDataset<2, double>("frame_tracks", vigra::Shape2(nTrackFrames,5), -1.0, vigra::Shape2(chunkdim,5));
	{
		using namespace Columns::frame_tracks ;
		fileOut.writeAttribute("frame_tracks", "columns",
			(string("frame:") + lc(frame) + " trackNr (references tracks):" + lc(trackNr)
				+ " x:" + lc(x) + " y:" + lc(y) + " x2:" + lc(x2)
			).c_str()
		);
	}
	chunkdim = (annotatedTracks.size() < maxchunkdim ? annotatedTracks.size() : maxchunkdim) ;
	fileOut.createDataset<2, double>("annotations", vigra::Shape2(annotatedTracks.size(),2), 0.0, vigra::Shape2(chunkdim,2));
	{
		using namespace Columns::annotations ;
		fileOut.writeAttribute("annotations", "columns",
			(string("trackId:") + lc(trackId) + " offset (references tracks):" + lc(offset)).c_str()
		) ;
	}

	//populate datasets
	std::cout << "Track: " << 0 << " / " << track_2_frame.size() << flush ;
	int aa = 0 ; //current offset in annotations
	size_t jj = 0 ; //current offset in tracks
	
	std::map<int, int> trackId_offset ;
	for(auto iter = track_2_frame.begin(); iter!=track_2_frame.end(); ++iter, jj++)
	{
		if(jj%100== 0)
			std::cout << "\rTrack: " << jj << " / " << track_2_frame.size() << flush ;
		vigra::MultiArray<2, double>  tracks_row(vigra::Shape2(1, 8));
		vigra::MultiArray<2, double> annot_row(vigra::Shape2(1,2)) ;
		tracks_row(0,Columns::tracks::trackID) = iter->first;
		trackId_offset[tracks_row(0,Columns::tracks::trackID)] = jj ;
		if(annotatedTracks.count(iter->first))
		{	tracks_row(0,Columns::tracks::annotated) = 1 ;
			tracks_row(0,Columns::tracks::meanX) = tracks_row(0,Columns::tracks::medianX) = annotatedTracks[iter->first][0] ;
			tracks_row(0,Columns::tracks::meanY) = tracks_row(0,Columns::tracks::medianY) = annotatedTracks[iter->first][1] ;
			tracks_row(0,Columns::tracks::meanZ) = tracks_row(0,Columns::tracks::medianZ) = annotatedTracks[iter->first][2] ;
			annot_row(0,Columns::annotations::trackId) = iter->first;
			annot_row(0,Columns::annotations::offset) = jj ;
			fileOut.writeBlock("annotations", vigra::Shape2(aa++,0), annot_row);
		}

		fileOut.writeBlock("tracks", vigra::Shape2(jj,0), tracks_row);
	}
	std::cout << "\rTrack: " << track_2_frame.size() << " / " << track_2_frame.size() << std::endl ;



	int offset = 0; //offset to first track for each frame in frame_tracks
	int ct = 0; //current (running) offset in frame_tracks
	jj = 0 ; //current offset in frames
	std::cout << "Frame: " << 0 << " / " << frame_2_track.size() << flush ;
	for(auto iter = frame_2_track.begin(); iter!=frame_2_track.end() ; ++iter, jj++)
	{
		
		if(jj % 100 == 0)
		{
			std::cout << "\rFrame: " << jj << " / " << frame_2_track.size() << flush ;
			//toc() ;
			//tic() ;
		}
		vigra::MultiArray<2, double>  frames_row(vigra::Shape2(1, 14));
		frames_row(0,Columns::frames::frameId) = iter->first;
		frames_row(0,Columns::frames::offset) = offset;
		
		auto rm = R.find(iter->first) ; //was transformation estimated for this frame?
		if(rm != R.end())
		{
			const cv::Mat& r = rm->second ;
			const cv::Mat& t = T[iter->first] ;
			using namespace Columns::frames ;
			frames_row(0,e00) = r.at<double>(0,0) ; frames_row(0,e10) = r.at<double>(1,0) ; frames_row(0,e20) = r.at<double>(2,0) ; frames_row(0,e30) = t.at<double>(0) ;
			frames_row(0,e01) = r.at<double>(0,1) ; frames_row(0,e11) = r.at<double>(1,1) ; frames_row(0,e21) = r.at<double>(2,1) ; frames_row(0,e31) = t.at<double>(1) ;
			frames_row(0,e02) = r.at<double>(0,2) ; frames_row(0,e12) = r.at<double>(1,2) ; frames_row(0,e22) = r.at<double>(2,2) ; frames_row(0,e32) = t.at<double>(2) ;
		}

		fileOut.writeBlock("frames", vigra::Shape2(jj,0), frames_row);
		const std::vector<typename Vec3>& vec = iter->second ;
		offset+=vec.size() ;
		//offset+=iter->second.size();
		//#pragma omp parallel for
		vigra::MultiArray<2, double>  frame_tracks_row(vigra::Shape2(1, 5));
		for(int ii = 0; ii< vec.size(); ++ii)
		{
			frame_tracks_row(0,Columns::frame_tracks::frame) = jj;
			if(trackId_offset.count(vec[ii][0]) == 1)
				frame_tracks_row(0,Columns::frame_tracks::trackNr) = trackId_offset[vec[ii][0]] ;
			else
			{
				cout << "Offset for Track ID " << vec[ii][0] << "was not found while processing Frame " << jj << std::endl ;
				return EXIT_FAILURE;
			}
			frame_tracks_row(0,Columns::frame_tracks::x) = vec[ii][1] ;
			frame_tracks_row(0,Columns::frame_tracks::y) = vec[ii][2] ;
			frame_tracks_row(0,Columns::frame_tracks::x2) = -1;
			fileOut.writeBlock("frame_tracks", vigra::Shape2(ct,0), frame_tracks_row);
			++ct;
		}
	}
	std::cout << "\rFrame: " << frame_2_track.size() << " / " << frame_2_track.size() << std::endl ;



	file.close() ;
	fileOut.close() ;
	return EXIT_SUCCESS ;
}

