#include <iostream>
#include <tclap/CmdLine.h>
#include <string>
#include <cstdio>
#include <vigra/hdf5impex.hxx>
#include <map>
#include <vigra/linear_algebra.hxx>


int main(int argc, char** argv)
{

    TCLAP::CmdLine cmd("BoschGT Extra Annotations", ' ', "0.1");
    TCLAP::ValueArg<std::string>  
        inputFile("i",
                     "file1", 
                     "first-file", 
                     false,
                     "I:/boschgt/seq0000_newFileFormat_2100-2200_reprojection_xyzMaps.h5",
                     "string",
                     cmd);

    TCLAP::ValueArg<std::string>  
        outputFile("o", 
                     "output", 
                     "output file", 
                     false,
                     "output.h5",
                     "string",
                     cmd);

    TCLAP::SwitchArg compareOrCreate("t", "test", "compare or test", false);
    cmd.add(compareOrCreate);

    cmd.parse(argc, argv);
    vigra::HDF5File file1(inputFile.getValue(), vigra::HDF5File::Open);
    vigra::HDF5File file2(outputFile.getValue(), compareOrCreate.getValue()?vigra::HDF5File::Open: vigra::HDF5File::New);


    vigra::MultiArray<2, double> tracks1;
    file1.readAndResize("/tracks", tracks1);
    vigra::MultiArray<2, double> frame_tracks1;
    file1.readAndResize("/frame_tracks", frame_tracks1);
    vigra::MultiArray<2, double> frames1;
    file1.readAndResize("/frames", frames1);
    
    if(compareOrCreate.getValue())
    {
        vigra::MultiArray<2, double> tracks2;
        file2.readAndResize("/tracks", tracks2);
        vigra::MultiArray<2, double> frame_tracks2;
        file2.readAndResize("/frame_tracks", frame_tracks2);
        vigra::MultiArray<2, double> frames2;
        file2.readAndResize("/frames", frames2);
        bool resultTracks = (tracks1 == tracks2);
        std::cout << "Tracks Identical:  "  << resultTracks << std::endl;
        bool resultFTracks = (frame_tracks1 == frame_tracks2);
        std::cout << "FrameTracks Identical:  "  << resultFTracks << std::endl;
        bool resultFrames = (frames1 == frames2);
        std::cout << "Frames Identical:  "  << resultFrames <<std::endl;;

        if(resultTracks&&resultFTracks &&resultFrames)
            std::cout << "Test passed\n";
        else
            std::cout << "Test failed\n";

    }
    else
    {
        file2.createDataset<2, double>("tracks", tracks1.shape(),0.0, vigra::Shape2(std::min(1024, (int)tracks1.shape(0)), 8), 0);
        file2.writeBlock("tracks", vigra::Shape2(0,0), tracks1);
        file2.createDataset<2, double>("frame_tracks", frame_tracks1.shape(),0.0, vigra::Shape2(std::min(1024, (int)frame_tracks1.shape(0)), 5), 0);
        file2.writeBlock("frame_tracks", vigra::Shape2(0,0), frame_tracks1);
        file2.createDataset<2, double>("frames", frames1.shape(),0.0, vigra::Shape2(std::min((int)1024, (int)frame_tracks1.shape(0)), 14), 0);
        frames1.subarray(vigra::Shape2(0,2), frames1.shape())*= 0;
        file2.writeBlock("frames", vigra::Shape2(0,0), frames1);
    }
    return 0;
}