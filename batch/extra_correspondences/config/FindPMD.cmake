# - Find PMD
# Find the native PMD includes and library
# This module defines
#  PMD_INCLUDE_DIR, where to find multi_array.hxx, etc.
#  PMD_LIBRARIES, the libraries needed to use vigraimpex.
#  PMD_FOUND, If false, do not try to use VIGRA.

FIND_PATH(PMD_INCLUDE_DIR pmdsdk2.h)

SET(PMD_NAMES ${PMD_NAMES} pmdaccess2)
FIND_LIBRARY(PMD_LIBRARY NAMES ${PMD_NAMES} )

# handle the QUIETLY and REQUIRED arguments and set PMD_FOUND to TRUE if 
# all listed variables are TRUE
INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(PMD DEFAULT_MSG PMD_LIBRARY PMD_INCLUDE_DIR)

IF(PMD_FOUND)
    SET(PMD_LIBRARIES ${PMD_LIBRARY})
ENDIF(PMD_FOUND)
