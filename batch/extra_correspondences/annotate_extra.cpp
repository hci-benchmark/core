#include <iostream>
#include <tclap/CmdLine.h>
#include <string>
#include <cv.h>
#include <highgui.h>
#include <cstdio>
#include <vigra/hdf5impex.hxx>

std::string getXYZFrameName(int number)
{
    std::string result = "/xyzmaps/";
    char num[7];
    std::sprintf(num, "%06d", number);
    return result+ num;
}
const float fac = 10;

std::string getColorFrameName(std::string path, int number)
{
    std::string result = path;
    char num[6];
    std::sprintf(num, "%05d", number);
    result = result+ num;
    result = result+ ".png";
	std::cout << "loading: " << result;
    return result;
}


std::string getDepthFrameName(std::string path, int number)
{
    std::string result = path;
    char num[7];
    std::sprintf(num, "%06d", number);
    return result+ num;
}

struct Track
{
    static int trackCt;
    int trackID;
    cv::Point2f position;
    cv::Point3f position_world;
    int frameno;
    bool isvalid;

    Track(int id = Track::trackCt):trackID(id)
    {
        isvalid =false;
        Track::trackCt+=1;
    }

    std::map<int, cv::Point2f> points;
};

int Track::trackCt = 0;
struct Manager
{
    int framebegin, frameend;
    int curnumc;
    int curnumd;
    vigra::Shape2 depthul;
    vigra::Shape2 depthwshape;
    vigra::Shape2 depthshape;
    
    vigra::Shape2 colorul;
    vigra::Shape2 colorwshape;
    vigra::Shape2 colorshape;
    std::vector<Track> tracks;
    Manager():  curnumc(0),
                curnumd(0),
                depthul(100,100),
                depthwshape(91,91),
                depthshape(2560, 1080),
                colorul(100,100),
                colorwshape(41, 41),
                colorshape(2560, 1080)
    {}
    std::string input_color;
    std::string input_depth;
	std::string depth_path;
	float max_depth ;
};

void trackBarc_Callback(int num,void* userData)
{
    Manager* manager = (Manager*)userData;
    manager->curnumc = num;
    num = num + manager->framebegin;
    cv::Mat color = cv::imread(getColorFrameName(manager->input_color, num));
    
    cv::Mat color_zoom_src = color( cv::Rect(   manager->colorul[0],
                                                manager->colorul[1],
                                                manager->colorwshape[0],
                                                manager->colorwshape[1]));
    cv::Mat color_zoom;
    cv::resize(color_zoom_src, color_zoom, cv::Size(), fac,fac, cv::INTER_NEAREST);
    if(manager->tracks.size() >0 &&manager->tracks.back().isvalid)
    {
        if(manager->tracks.back().points.find(num) != manager->tracks.back().points.end())
        {
            float col = (256 - color.at<unsigned char>(manager->tracks.back().points[num]))/2;
            cv::circle(color, manager->tracks.back().points[num], 3, col, 1);
            cv::circle(color_zoom, fac*(manager->tracks.back().points[num]- cv::Point2f(manager->colorul[0], manager->colorul[1])), 3,col,5);
            cv::circle(color_zoom, fac*(manager->tracks.back().points[num]- cv::Point2f(manager->colorul[0], manager->colorul[1])), 30,col,10);
        }
    }
    cv::imshow("color", color);
    cv::imshow("color_zoom", color_zoom);
}

void trackBard_Callback(int num,void* userData)
{
    Manager* manager = (Manager*)userData;
    manager->curnumd = num;
    num = num + manager->framebegin;
    vigra::HDF5File file(manager->input_depth, vigra::HDF5File::Open);
    cv::Mat depthmat ;
	if(manager->depth_path == "/rgb/")
	{	
		vigra::MultiArray<3, float> depth;
		file.readAndResize(getDepthFrameName(manager->depth_path, num), depth);
		cv::Mat d(depth.shape(2), depth.shape(1), CV_32FC3, depth.data());
		cv::Mat c[3] ;
		cv::split(d, c) ;
		c[0].copyTo(depthmat) ;
	}
	else
	{
		vigra::MultiArray<2, float> depth;
		file.readAndResize(getDepthFrameName(manager->depth_path, num), depth);
		cv::Mat d(depth.shape(1), depth.shape(0), CV_32FC1, depth.data());
		d.copyTo(depthmat) ;
	}
	cv::Mat depthshow;


	cv::Mat dScaled, dScaledShow ;
	depthmat(cv::Rect(manager->depthul[0], manager->depthul[1],
                                            manager->depthwshape[0],
                                            manager->depthwshape[1])).copyTo(dScaled) ;
	
	float scaleLimit = 0.0 ;
	if(manager->depth_path == "/depthmaps/")
	{
		scaleLimit = manager->max_depth ;
	}

	cv::cvtColor(depthmat, depthshow, cv::COLOR_GRAY2RGB);
	if(manager->tracks.size() >0 &&manager->tracks.back().isvalid)
	{
		float color = (200- depthmat.at<float>(manager->tracks.back().position))/2;
		cv::circle(depthshow, manager->tracks.back().position, 3, cv::Scalar(0, 0, 200), 1);
		cv::circle(depthshow, manager->tracks.back().position, 0, cv::Scalar(0, 0, 200), 1);
	}
	depthshow/= 200;
	float min = 1e6 ;
	float max = -1e6 ;
	float* ptr = (float*)dScaled.data ;
	float* end = ptr + (dScaled.rows * dScaled.cols) ;
	while( ptr < end)
	{	
		min = (*ptr < min ? *ptr : min) ;
		if(!(abs(*ptr - scaleLimit) < 1e-5))
			max = (*ptr > max ? *ptr : max) ;
		++ptr ;
	}
	dScaled -= min ;
	dScaled /= (max - min) ;
	cv::cvtColor(dScaled, dScaledShow, cv::COLOR_GRAY2RGB);
	
	//cv::threshold(dScaled, dScaledShow, manager->max_depth,manager->max_depth,cv::THRESH_TRUNC) ;

	cv::imshow("depth", depthshow);

    cv::imshow("depth_zoom", dScaledShow);


}
static void colorZoomOnMouse( int ev, int x, int y, int, void* userData)
{
    float xx = x/fac;
    float yy = y/10.0;
    if( ev != cv::EVENT_LBUTTONDOWN )
        return;
    Manager* manager = (Manager*)userData;
    if(manager->tracks.size() == 0)
        return;
    if(manager->tracks.back().isvalid == false)
        return;
    int num = manager->curnumc +manager->framebegin;
    manager->tracks.back().points[num] = cv::Point2f(xx+manager->colorul[0], yy+manager->colorul[1]);
    std::cout << "Track Position in frame " <<  num << " is "<<manager->tracks.back().points[num] << std::endl;
    trackBarc_Callback(manager->curnumc, manager);
}
static void depthZoomOnMouse( int ev, int x, int y, int, void* userData)
{
    if( ev != cv::EVENT_LBUTTONDOWN )
        return;
    Manager* manager = (Manager*)userData;
    if(manager->tracks.size() == 0)
        return;
    vigra::HDF5File file(manager->input_depth, vigra::HDF5File::Open);
    vigra::MultiArray<3, float> xyz;
    file.readAndResize(getXYZFrameName(manager->curnumd+manager->framebegin), xyz);
    manager->tracks.back().position_world.x = xyz(0, x+manager->depthul[0], y+manager->depthul[1]);
    manager->tracks.back().position_world.y = xyz(1, x+manager->depthul[0], y+manager->depthul[1]);
    manager->tracks.back().position_world.z = xyz(2, x+manager->depthul[0], y+manager->depthul[1]);
    manager->tracks.back().frameno = manager->curnumd+manager->framebegin;
    manager->tracks.back().position = cv::Point2f(x+manager->depthul[0], y+manager->depthul[1]);
    std::cout << "3D Position choosen: " << manager->tracks.back().position_world << std::endl;
    std::cout << "2D Position of 3D: " << manager->tracks.back().position << std::endl;
    manager->tracks.back().isvalid = true;
    trackBard_Callback(manager->curnumd, manager);
}

static void depthOnMouse( int ev, int x, int y, int, void* userData)
{
    if( ev != cv::EVENT_LBUTTONDOWN )
        return;
    Manager* manager = (Manager*)userData;
    x = x-(manager->depthwshape[0]-1)/2;
    y = y-(manager->depthwshape[1]-1)/2;
    if(x < 0)
        x = 0;
    if(y < 0)
        y = 0;
    if(x + manager->depthwshape[0] > manager->depthshape[0])
        x = manager->depthshape[0]- manager->depthwshape[0];
    if(y + manager->depthwshape[1] > manager->depthshape[1])
        y = manager->depthshape[1]- manager->depthwshape[1];

    manager->depthul[0] = x;
    manager->depthul[1] = y;
    trackBard_Callback(manager->curnumd, manager);
}
static void colorOnMouse( int ev, int x, int y, int, void* userData)
{
    if( ev != cv::EVENT_LBUTTONDOWN )
        return;
    Manager* manager = (Manager*)userData;
    x = x-(manager->depthwshape[0]-1)/2;
    y = y-(manager->depthwshape[1]-1)/2;
    if(x < 0)
        x = 0;
    if(y < 0)
        y = 0;
    if(x + manager->colorwshape[0] > manager->colorshape[0])
        x = manager->colorshape[0]- manager->colorwshape[0];
    if(y + manager->colorwshape[1] > manager->colorshape[1])
        y = manager->colorshape[1]- manager->colorwshape[1];

    manager->colorul[0] = x;
    manager->colorul[1] = y;
    trackBarc_Callback(manager->curnumc, manager);
}

void display_help()
{
    std::cout << "Functions:\n";
    std::cout << "a/d prev/next color frame\n";
    std::cout << "j/l prev/next depth frame\n";
    std::cout << "n   new track first choose in depth zoom then in color zoom frames \n";
    std::cout << "m   cancel cuurent track \n";
    std::cout << "h   display help\n";
    std::cout << "click on big image change zoomed ROI\n";
    std::cout << "q  quit, save to outputfile\n";
    std::cout << "to correct 2d position just reannotate\n";
}


int main(int argc, char** argv)
{

    TCLAP::CmdLine cmd("BoschGT Extra Annotations", ' ', "0.1");
    TCLAP::ValueArg<std::string>  
        inputFile("i",
                     "depth-file", 
                     " to hdf5 file containing depth", 
                     false,
                     "I:/boschgt/seq0000_newFileFormat_2100-2200_reprojection_xyzMaps.h5",
                     "string",
                     cmd);
    TCLAP::ValueArg<std::string>  
        inputFile2("u",
                     "color-path", 
                     "path to color images including file prefix", 
                     false,
                     "V:/ImageSequences/HanauSequenzen/00/seq0000_L_rect_",
                     "string",
                     cmd);
    

    TCLAP::ValueArg<std::string>  
        outputFile("o", 
                     "output", 
                     "output file", 
                     false,
                     "output.h5",
                     "string",
                     cmd);

    
    TCLAP::ValueArg<int>  
        framebegin   ("b", 
                     "frame-begin", 
                     "specify start frame", 
                     false,
                      2101,
                     "int",
                     cmd);
    TCLAP::ValueArg<int>  
        frameend   ("e", 
                     "frame-end", 
                     "specify end frame", 
                     false,
                      2200,
                     "int",
                     cmd);
    TCLAP::ValueArg<float>  
        max_depth   ("m", 
                     "max-depth", 
                     "maximum depth for display scaling", 
                     false,
                      1000.0,
                     "float",
                     cmd);
    Manager manager;
    cmd.parse(argc, argv);
    manager.framebegin = framebegin.getValue();
    manager.frameend = frameend.getValue();
    manager.input_color = inputFile2.getValue();
    manager.input_depth= inputFile.getValue();
	manager.max_depth = max_depth.getValue() ;
	manager.depth_path = "/depthmaps/" ;

    cv::namedWindow("color", CV_WINDOW_NORMAL);
    cv::setMouseCallback("color",colorOnMouse , &manager);
    cv::namedWindow("color_zoom", CV_WINDOW_NORMAL);
    cv::setMouseCallback("color_zoom",colorZoomOnMouse , &manager);
    cv::namedWindow("depth", CV_WINDOW_NORMAL);
    cv::setMouseCallback("depth",depthOnMouse , &manager);
    cv::namedWindow("depth_zoom", CV_WINDOW_NORMAL);
    cv::setMouseCallback("depth_zoom",depthZoomOnMouse , &manager);
    cv::createTrackbar("frameselectc", "color", 0, frameend.getValue() - framebegin.getValue(), trackBarc_Callback, &manager);
    cv::createTrackbar("frameselectd", "depth", 0, frameend.getValue() - framebegin.getValue(), trackBard_Callback, &manager);
    trackBarc_Callback(0, &manager);
    trackBard_Callback(0, &manager);
    bool run_flag = true;

    display_help();
    while(run_flag)
    {
        int key = cv::waitKey(1);
        int numc = cv::getTrackbarPos("frameselectc", "color");
        int numd = cv::getTrackbarPos("frameselectd", "depth");
        switch(key)
        {
        case 'h':
            display_help();
            break;
        case 'a':
            cv::setTrackbarPos("frameselectc", "color", numc-1);
            break;
        case 'd':
            cv::setTrackbarPos("frameselectc", "color", numc+1);
            break;
        case 'j':
            cv::setTrackbarPos("frameselectd", "depth", numd-1);
            break;
        case 'l':
            cv::setTrackbarPos("frameselectd", "depth", numd+1);
            break;
		case 'i':
			manager.depth_path = "/rgb/" ;
            trackBarc_Callback(0, &manager);
            trackBard_Callback(0, &manager);
			break ;
		case 'k':
			manager.depth_path = "/depthmaps/" ;
            trackBarc_Callback(0, &manager);
			trackBard_Callback(0, &manager);
			break;
		case 'n':
            std::cout << "NEW TRACK CREATED\n";
            manager.tracks.push_back(Track(manager.tracks.size()));
            trackBarc_Callback(0, &manager);
            trackBard_Callback(0, &manager);
            break;
        case 'm':
            std::cout << "CURRENT TRACK CANCELED\n";
            manager.tracks.pop_back();
            trackBarc_Callback(0, &manager);
            trackBard_Callback(0, &manager);
            break;
        case 'q':
            run_flag = false;
        }
    }
    std::map<int, std::vector<std::pair<cv::Point2f, int> > > frameTracks;
    std::vector<Track> &tracks = manager.tracks;
    int n_frames = manager.frameend- manager.framebegin +1;
    int n_frame_tracks =0;
    for(int ii = 0; ii < tracks.size(); ++ii)
    {
        for(auto iter = tracks[ii].points.begin(); iter != tracks[ii].points.end(); ++iter)
            frameTracks[iter->first].push_back(std::make_pair(iter->second, ii));
        n_frame_tracks += tracks[ii].points.size();
    }
    for(int ii = manager.framebegin; ii <= manager.frameend; ++ii)
            frameTracks[ii];
    vigra::HDF5File fout(outputFile.getValue(), vigra::HDF5File::New);
    fout.createDataset<2, float>("frames", vigra::Shape2(n_frames, 14), float(0.0), vigra::Shape2(1, 14), 0);
    fout.createDataset<2, float>("frame_tracks", vigra::Shape2(n_frame_tracks, 5), float(-1.0), vigra::Shape2(1, 5),0);
    fout.createDataset<2, float>("tracks", vigra::Shape2(tracks.size(), 8), 0.0f, vigra::Shape2(1, 8), 0);
    for(int ii = 0; ii < tracks.size(); ++ii)
    {
        vigra::MultiArray<2, float> curtrack(vigra::Shape2(1, 8));
        curtrack(0,0) = ii;
        curtrack(0,1) = curtrack(0, 4) = tracks[ii].position_world.x;
        curtrack(0,2) = curtrack(0, 5) = tracks[ii].position_world.y;
        curtrack(0,3) = curtrack(0, 6) = tracks[ii].position_world.z;
        curtrack(0,7) = 2;
        fout.writeBlock("tracks", vigra::Shape2(ii, 0), curtrack);
    }
    int ii =0;
    int cur_offset = 0;
    for(auto iter = frameTracks.begin(); iter!= frameTracks.end(); ++iter, ++ii)
    {
        vigra::MultiArray<2, float> curframe(vigra::Shape2(1, 14));
        curframe(0,0) = iter->first;
        curframe(0,1) = cur_offset;
        fout.writeBlock("frames", vigra::Shape2(ii, 0), curframe);
        vigra::MultiArray<2, float> cur_frame_tracks(vigra::Shape2(iter->second.size(), 5), -1);
        for(int jj = 0; jj < iter->second.size(); ++jj)
        {
            cur_frame_tracks(jj, 0) = iter->first;
            cur_frame_tracks(jj, 1) = iter->second[jj].second;
            cur_frame_tracks(jj, 2) = iter->second[jj].first.x;
            cur_frame_tracks(jj, 3) = iter->second[jj].first.y;
            cur_frame_tracks(jj, 4) = -1;
        }
        fout.writeBlock("frame_tracks", vigra::Shape2(cur_offset, 0), cur_frame_tracks);
        cur_offset += iter->second.size();
    }

    return 0;
}