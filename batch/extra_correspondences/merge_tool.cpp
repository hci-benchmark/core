#include <iostream>
#include <tclap/CmdLine.h>
#include <string>
#include <cstdio>
#include <vigra/hdf5impex.hxx>
#include <map>
#include <vigra/linear_algebra.hxx>


int main(int argc, char** argv)
{

    TCLAP::CmdLine cmd("BoschGT Extra Annotations", ' ', "0.1");
    TCLAP::ValueArg<std::string>  
        inputFile("i",
                     "file1", 
                     "first-file", 
                     false,
                     "I:/boschgt/seq0000_newFileFormat_2100-2200_reprojection_xyzMaps.h5",
                     "string",
                     cmd);
    TCLAP::ValueArg<std::string>  
        inputFile2("u",
                     "file2", 
                     "second file", 
                     false,
                     "V:/ImageSequences/HanauSequenzen/00/seq0000_L_rect_",
                     "string",
                     cmd);
    

    TCLAP::ValueArg<std::string>  
        outputFile("o", 
                     "output", 
                     "output file", 
                     false,
                     "output.h5",
                     "string",
                     cmd);

    cmd.parse(argc, argv);
    vigra::HDF5File fin1(inputFile.getValue(), vigra::HDF5File::Open);
    vigra::HDF5File fin2(inputFile2.getValue(), vigra::HDF5File::Open);
    vigra::HDF5File fout(outputFile.getValue(), vigra::HDF5File::New);


    vigra::MultiArray<2, double> tracks1;
    fin1.readAndResize("/tracks", tracks1);
    vigra::MultiArray<2, double> tracks2;
    fin2.readAndResize("/tracks", tracks2);
    vigra::MultiArray<2, double> frames1;
    fin1.readAndResize("/frames", frames1);
    vigra::MultiArray<2, double> frames2;
    fin2.readAndResize("/frames", frames2);
    vigra::MultiArray<2, double> frame_tracks1;
    fin1.readAndResize("/frame_tracks", frame_tracks1);
    vigra::MultiArray<2, double> frame_tracks2;
    fin2.readAndResize("/frame_tracks", frame_tracks2);
    vigra::columnVector(frame_tracks2, 1)+= tracks1.shape(0);
    int chunkHeight = std::min(int(tracks1.shape(0)+tracks2.shape(0)), 1024);
	fout.createDataset<2, double>("tracks", vigra::Shape2(tracks1.shape(0)+tracks2.shape(0), 8), 0.0f, vigra::Shape2(chunkHeight, 8));
	std::cerr << "Writing tracks...\n";
    for(int ii = 0; ii < tracks1.shape(0); ++ii)
	{
		if(ii%int(tracks1.shape(0)/100.0)==0)
			std::cerr << "\r" << ii<< " of "  << tracks1.shape(0);
		vigra::MultiArray<2, double> tmp = vigra::rowVector(tracks1, ii);
		fout.writeBlock("tracks", vigra::Shape2(ii, 0), tmp);
	}
    int track2_offset = tracks1(tracks1.shape(0)-1, 0);
    vigra::columnVector(tracks2, 0)+= track2_offset+1;
	std::cerr << " done first file.\n";
	std::cerr << tracks2.shape(0) << std::endl;
    for(int ii = 0; ii < tracks2.shape(0); ++ii)
	{
		if(ii%(std::max(int(tracks2.shape(0)/100.0),1))==0)
			std::cerr << "\r" << ii << " of "  << tracks2.shape(0);
		vigra::MultiArray<2, double> tmp = vigra::rowVector(tracks2, ii);
        fout.writeBlock("tracks", vigra::Shape2(ii+tracks1.shape(0), 0), tmp);
	}
	std::cerr << " done all.\n";
	std::cerr << "Putting frame tracks into map...";
    std::map<int, std::vector<vigra::MultiArrayView<2, double> > > new_frame_tracks;
    for(int ii = 0; ii < frame_tracks1.shape(0); ++ii)
        new_frame_tracks[(int)frame_tracks1(ii, 0)].push_back(vigra::rowVector(frame_tracks1, ii));
    for(int ii = 0; ii < frame_tracks2.shape(0); ++ii)
        new_frame_tracks[(int)frame_tracks2(ii, 0)].push_back(vigra::rowVector(frame_tracks2, ii));
	std::cerr << " done.\n";
	std::cerr << "Creating new Frame tracks Dataset...";

    int n_frame_tracks = 0;
    for(auto iter = new_frame_tracks.begin(); iter != new_frame_tracks.end(); ++iter)
	{
        n_frame_tracks+= iter->second.size();
	}
    chunkHeight = std::min(int(n_frame_tracks), 1024);
    fout.createDataset<2, double>("frame_tracks", vigra::Shape2(n_frame_tracks, 5), double(-1.0), vigra::Shape2(chunkHeight, 5));
	std::cerr << " done.\n";
    int ft_ct = 0;

	std::cerr << "Writing Frame tracks Dataset...\n";

    for(auto iter = new_frame_tracks.begin(); iter != new_frame_tracks.end(); ++iter)
    {
        for(int ii = 0; ii < iter->second.size(); ++ii, ++ft_ct)
		{	
			if(ft_ct%10000==0)
				std::cerr << "\r" << ft_ct << " of " << n_frame_tracks;
			vigra::MultiArray<2, double> tmp = iter->second[ii];
			fout.writeBlock("frame_tracks", vigra::Shape2(ft_ct, 0), tmp);
		}
	}

	std::cerr << " done.\n";

	std::cerr << "Writing Frame Dataset...\n";
    std::map<int, vigra::MultiArrayView<2, double> >  new_frames;
    for(int ii = 0; ii < frames1.shape(0); ++ii)
        new_frames[(int)frames1(ii, 0)]= vigra::rowVector(frames1, ii);

    for(int ii = 0; ii < frames2.shape(0); ++ii)
    {
        if(new_frames.find((int)frames2(ii, 0))!= new_frames.end())
        {
            continue;
        }
        else
        {
            new_frames[(int)frames2(ii, 0)]= vigra::rowVector(frames2, ii);
        }
    }

    int offset = 0;
    for(auto iter = new_frames.begin(); iter != new_frames.end();++iter)
    {
        iter->second(0,1) = offset;
        offset += new_frame_tracks[iter->first].size();
    }
    chunkHeight = std::min(int(new_frames.size()), 1024);
	fout.createDataset<2, double>("frames", vigra::Shape2(new_frames.size(), 14), double(0.0), vigra::Shape2(chunkHeight, 14), 0);
    ft_ct = 0;
    for(auto iter = new_frames.begin(); iter != new_frames.end();++iter, ++ft_ct)
    {
		vigra::MultiArray<2, double> tmp = iter->second;
        fout.writeBlock("frames", vigra::Shape2(ft_ct, 0), tmp);
    }
	std::cerr << " done.\n";
    return 0;
}