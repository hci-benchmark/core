#include <vigra/multi_array.hxx>
#include <vigra/linear_algebra.hxx>
#include <math.h>

#define Sqrt std::sqrt
#define Sin  std::sin
#define Cos  std::cos
#define Power std::pow


// d/d(q1) R11
#define R11_q1  (-((q1*(Power(q2,2) + Power(q3,2))*(-2*Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2)) + 2*Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2))*Cos(Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2))) + (Power(q1,2) + Power(q2,2) + Power(q3,2))*Sin(Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2)))))/Power(Power(q1,2) + Power(q2,2) + Power(q3,2),2.5)))

// d/d(q2) R11
#define R11_q2  (q2*((2*Power(q1,2)*(-1 + Cos(Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2)))))/Power(Power(q1,2) + Power(q2,2) + Power(q3,2),2) + (Power(q1,2)*Sin(Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2))))/Power(Power(q1,2) + Power(q2,2) + Power(q3,2),1.5) - Sin(Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2)))/Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2))))

// d/d(q3) R11
#define R11_q3  (q3*((2*Power(q1,2)*(-1 + Cos(Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2)))))/Power(Power(q1,2) + Power(q2,2) + Power(q3,2),2) + (Power(q1,2)*Sin(Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2))))/Power(Power(q1,2) + Power(q2,2) + Power(q3,2),1.5) - Sin(Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2)))/Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2))))

// d/d(q1) R12
#define R12_q1  (q2*(-Power(q1,2) + Power(q2,2) + Power(q3,2))*Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2)) - Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2))*(-(Power(q1,2)*q2) + Power(q2,3) + Power(q1,3)*q3 + q1*Power(q2,2)*q3 + q2*Power(q3,2) + q1*Power(q3,3))*Cos(Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2))) + q1*(q1*q2 + q3)*(Power(q1,2) + Power(q2,2) + Power(q3,2))*Sin(Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2))))/Power(Power(q1,2) + Power(q2,2) + Power(q3,2),2.5)

// d/d(q2) R12
#define R12_q2  (q1*(Power(q1,2) - Power(q2,2) + Power(q3,2))*Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2)) - Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2))*(Power(q1,3) - q1*Power(q2,2) + Power(q1,2)*q2*q3 + Power(q2,3)*q3 + q1*Power(q3,2) + q2*Power(q3,3))*Cos(Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2))) + q2*(q1*q2 + q3)*(Power(q1,2) + Power(q2,2) + Power(q3,2))*Sin(Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2))))/Power(Power(q1,2) + Power(q2,2) + Power(q3,2),2.5)

// d/d(q3) R12
#define R12_q3  (-2*q1*q2*q3*Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2)) - q3*Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2))*(-2*q1*q2 + (Power(q1,2) + Power(q2,2))*q3 + Power(q3,3))*Cos(Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2))) - (Power(q1,2) + Power(q2,2) - q1*q2*q3)*(Power(q1,2) + Power(q2,2) + Power(q3,2))*Sin(Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2))))/Power(Power(q1,2) + Power(q2,2) + Power(q3,2),2.5)

// d/d(q1) R13
#define R13_q1  (q3*(-Power(q1,2) + Power(q2,2) + Power(q3,2))*Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2)) + Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2))*(Power(q1,3)*q2 + Power(q1,2)*q3 + q1*q2*(Power(q2,2) + Power(q3,2)) - q3*(Power(q2,2) + Power(q3,2)))*Cos(Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2))) + q1*(-q2 + q1*q3)*(Power(q1,2) + Power(q2,2) + Power(q3,2))*Sin(Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2))))/Power(Power(q1,2) + Power(q2,2) + Power(q3,2),2.5)

// d/d(q2) R13
#define R13_q2  (-2*q1*q2*q3*Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2)) + q2*Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2))*(Power(q1,2)*q2 + Power(q2,3) + 2*q1*q3 + q2*Power(q3,2))*Cos(Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2))) + (Power(q1,2) + Power(q2,2) + Power(q3,2))*(Power(q1,2) + q1*q2*q3 + Power(q3,2))*Sin(Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2))))/Power(Power(q1,2) + Power(q2,2) + Power(q3,2),2.5)

// d/d(q3) R13
#define R13_q3  (q1*(Power(q1,2) + Power(q2,2) - Power(q3,2))*Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2)) + Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2))*(-(q1*(Power(q1,2) + Power(q2,2))) + q2*(Power(q1,2) + Power(q2,2))*q3 + q1*Power(q3,2) + q2*Power(q3,3))*Cos(Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2))) + q3*(-q2 + q1*q3)*(Power(q1,2) + Power(q2,2) + Power(q3,2))*Sin(Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2))))/Power(Power(q1,2) + Power(q2,2) + Power(q3,2),2.5)

// d/d(q1) R21
#define R21_q1  (q2*(-Power(q1,2) + Power(q2,2) + Power(q3,2))*Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2)) + Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2))*(Power(q1,2)*q2 + Power(q1,3)*q3 - q2*(Power(q2,2) + Power(q3,2)) + q1*q3*(Power(q2,2) + Power(q3,2)))*Cos(Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2))) + q1*(q1*q2 - q3)*(Power(q1,2) + Power(q2,2) + Power(q3,2))*Sin(Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2))))/Power(Power(q1,2) + Power(q2,2) + Power(q3,2),2.5)

// d/d(q2) R21
#define R21_q2  (q1*(Power(q1,2) - Power(q2,2) + Power(q3,2))*Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2)) + Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2))*(-Power(q1,3) + Power(q1,2)*q2*q3 + q1*(q2 - q3)*(q2 + q3) + q2*q3*(Power(q2,2) + Power(q3,2)))*Cos(Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2))) + q2*(q1*q2 - q3)*(Power(q1,2) + Power(q2,2) + Power(q3,2))*Sin(Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2))))/Power(Power(q1,2) + Power(q2,2) + Power(q3,2),2.5)

// d/d(q3) R21
#define R21_q3  (-2*q1*q2*q3*Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2)) + q3*Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2))*(2*q1*q2 + (Power(q1,2) + Power(q2,2))*q3 + Power(q3,3))*Cos(Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2))) + (Power(q1,2) + Power(q2,2) + q1*q2*q3)*(Power(q1,2) + Power(q2,2) + Power(q3,2))*Sin(Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2))))/Power(Power(q1,2) + Power(q2,2) + Power(q3,2),2.5)

// d/d(q1) R22
#define R22_q1  (q1*((2*Power(q2,2)*(-1 + Cos(Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2)))))/Power(Power(q1,2) + Power(q2,2) + Power(q3,2),2) + (Power(q2,2)*Sin(Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2))))/Power(Power(q1,2) + Power(q2,2) + Power(q3,2),1.5) - Sin(Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2)))/Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2))))

// d/d(q2) R22
#define R22_q2  (-((q2*(Power(q1,2) + Power(q3,2))*(-2*Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2)) + 2*Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2))*Cos(Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2))) + (Power(q1,2) + Power(q2,2) + Power(q3,2))*Sin(Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2)))))/Power(Power(q1,2) + Power(q2,2) + Power(q3,2),2.5)))

// d/d(q3) R22
#define R22_q3  (q3*((2*Power(q2,2)*(-1 + Cos(Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2)))))/Power(Power(q1,2) + Power(q2,2) + Power(q3,2),2) + (Power(q2,2)*Sin(Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2))))/Power(Power(q1,2) + Power(q2,2) + Power(q3,2),1.5) - Sin(Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2)))/Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2))))

// d/d(q1) R23
#define R23_q1  (-2*q1*q2*q3*Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2)) - q1*Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2))*(Power(q1,3) - 2*q2*q3 + q1*(Power(q2,2) + Power(q3,2)))*Cos(Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2))) - (Power(q1,2) + Power(q2,2) + Power(q3,2))*(Power(q2,2) - q1*q2*q3 + Power(q3,2))*Sin(Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2))))/Power(Power(q1,2) + Power(q2,2) + Power(q3,2),2.5)

// d/d(q2) R23
#define R23_q2  (q3*(Power(q1,2) - Power(q2,2) + Power(q3,2))*Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2)) - Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2))*(Power(q1,3)*q2 + Power(q1,2)*q3 - Power(q2,2)*q3 + Power(q3,3) + q1*q2*(Power(q2,2) + Power(q3,2)))*Cos(Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2))) + q2*(q1 + q2*q3)*(Power(q1,2) + Power(q2,2) + Power(q3,2))*Sin(Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2))))/Power(Power(q1,2) + Power(q2,2) + Power(q3,2),2.5)

// d/d(q3) R23
#define R23_q3  (q2*(Power(q1,2) + Power(q2,2) - Power(q3,2))*Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2)) - Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2))*(Power(q1,2)*q2 + Power(q2,3) + Power(q1,3)*q3 + q1*Power(q2,2)*q3 - q2*Power(q3,2) + q1*Power(q3,3))*Cos(Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2))) + q3*(q1 + q2*q3)*(Power(q1,2) + Power(q2,2) + Power(q3,2))*Sin(Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2))))/Power(Power(q1,2) + Power(q2,2) + Power(q3,2),2.5)

// d/d(q1) R31
#define R31_q1  (q3*(-Power(q1,2) + Power(q2,2) + Power(q3,2))*Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2)) - Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2))*(Power(q1,3)*q2 + q1*Power(q2,3) - Power(q1,2)*q3 + Power(q2,2)*q3 + q1*q2*Power(q3,2) + Power(q3,3))*Cos(Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2))) + q1*(q2 + q1*q3)*(Power(q1,2) + Power(q2,2) + Power(q3,2))*Sin(Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2))))/Power(Power(q1,2) + Power(q2,2) + Power(q3,2),2.5)

// d/d(q2) R31
#define R31_q2  (-2*q1*q2*q3*Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2)) - q2*Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2))*(Power(q1,2)*q2 + Power(q2,3) - 2*q1*q3 + q2*Power(q3,2))*Cos(Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2))) - (Power(q1,2) + Power(q2,2) + Power(q3,2))*(Power(q1,2) - q1*q2*q3 + Power(q3,2))*Sin(Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2))))/Power(Power(q1,2) + Power(q2,2) + Power(q3,2),2.5)

// d/d(q3) R31
#define R31_q3  (q1*(Power(q1,2) + Power(q2,2) - Power(q3,2))*Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2)) - Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2))*(Power(q1,3) + Power(q1,2)*q2*q3 + q1*(q2 - q3)*(q2 + q3) + q2*q3*(Power(q2,2) + Power(q3,2)))*Cos(Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2))) + q3*(q2 + q1*q3)*(Power(q1,2) + Power(q2,2) + Power(q3,2))*Sin(Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2))))/Power(Power(q1,2) + Power(q2,2) + Power(q3,2),2.5)

// d/d(q1) R32
#define R32_q1  (-2*q1*q2*q3*Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2)) + q1*Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2))*(Power(q1,3) + 2*q2*q3 + q1*(Power(q2,2) + Power(q3,2)))*Cos(Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2))) + (Power(q1,2) + Power(q2,2) + Power(q3,2))*(Power(q2,2) + q1*q2*q3 + Power(q3,2))*Sin(Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2))))/Power(Power(q1,2) + Power(q2,2) + Power(q3,2),2.5)

// d/d(q2) R32
#define R32_q2  (q3*(Power(q1,2) - Power(q2,2) + Power(q3,2))*Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2)) + Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2))*(Power(q1,3)*q2 - Power(q1,2)*q3 + (q2 - q3)*q3*(q2 + q3) + q1*q2*(Power(q2,2) + Power(q3,2)))*Cos(Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2))) + q2*(-q1 + q2*q3)*(Power(q1,2) + Power(q2,2) + Power(q3,2))*Sin(Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2))))/Power(Power(q1,2) + Power(q2,2) + Power(q3,2),2.5)

// d/d(q3) R32
#define R32_q3  (q2*(Power(q1,2) + Power(q2,2) - Power(q3,2))*Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2)) + Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2))*(-(q2*(Power(q1,2) + Power(q2,2))) + q1*(Power(q1,2) + Power(q2,2))*q3 + q2*Power(q3,2) + q1*Power(q3,3))*Cos(Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2))) + q3*(-q1 + q2*q3)*(Power(q1,2) + Power(q2,2) + Power(q3,2))*Sin(Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2))))/Power(Power(q1,2) + Power(q2,2) + Power(q3,2),2.5)

// d/d(q1) R33
#define R33_q1  (q1*((2*Power(q3,2)*(-1 + Cos(Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2)))))/Power(Power(q1,2) + Power(q2,2) + Power(q3,2),2) + (Power(q3,2)*Sin(Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2))))/Power(Power(q1,2) + Power(q2,2) + Power(q3,2),1.5) - Sin(Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2)))/Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2))))

// d/d(q2) R33
#define R33_q2  (q2*((2*Power(q3,2)*(-1 + Cos(Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2)))))/Power(Power(q1,2) + Power(q2,2) + Power(q3,2),2) + (Power(q3,2)*Sin(Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2))))/Power(Power(q1,2) + Power(q2,2) + Power(q3,2),1.5) - Sin(Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2)))/Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2))))

// d/d(q3) R33
#define R33_q3  (-(((Power(q1,2) + Power(q2,2))*q3*(-2*Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2)) + 2*Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2))*Cos(Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2))) + (Power(q1,2) + Power(q2,2) + Power(q3,2))*Sin(Sqrt(Power(q1,2) + Power(q2,2) + Power(q3,2)))))/Power(Power(q1,2) + Power(q2,2) + Power(q3,2),2.5)))




/** JRt : 3x6 Jacobian of Rototranslation as used in BA problem. Assuming Column major.
 *   Rt : 1x6 row from BA problem;
 *    x :  3d coordinate of point o consider.
 */
void computeRtJacobian(vigra::MultiArrayView<1, double> x, vigra::MultiArrayView<2, double> Rt,  vigra::MultiArrayView<2, double> JRt)
{
    JRt*=0;
    JRt(0,3) = JRt(1,4) = JRt(2,5) = 1.0;
    double & q1 = Rt(0,0);
    double & q2 = Rt(0,1);
    double & q3 = Rt(0,2);
    JRt(0,0) = R11_q1* x[0]+ R12_q1 *(x[1]) + R13_q1* x[2];
    JRt(1,0) = R21_q1*x[0]+ R22_q1 *x[1] + R23_q1* x[2];
    JRt(2,0) = R31_q1*x[0]+ R32_q1 *x[1] + R33_q1* x[2];
    JRt(0,1) = R11_q2*x[0]+ R12_q2 *x[1] + R13_q2* x[2];
    JRt(1,1) = R21_q2*x[0]+ R22_q2 *x[1] + R23_q2* x[2];
    JRt(2,1) = R31_q2*x[0]+ R32_q2 *x[1] + R33_q2* x[2];
    JRt(0,2) = R11_q3*x[0]+ R12_q3 *x[1] + R13_q3* x[2];
    JRt(1,2) = R21_q3*x[0]+ R22_q3 *x[1] + R23_q3* x[2];
    JRt(2,2) = R31_q3*x[0]+ R32_q3 *x[1] + R33_q3* x[2];
}


void computeUncertaintyIn3DPoint(vigra::MultiArrayView<2, double> JRt, vigra::MultiArrayView<2, double> Cov, vigra::MultiArrayView<2, double> cov_x)
{
    using namespace vigra::linalg;
    cov_x = mmul(mmul(JRt,Cov),JRt.transpose());
}

void computeUncertaintyInDisparitySpace(vigra::MultiArrayView<1, double> x,  
                                        vigra::MultiArrayView<2, double> cov_x, 
                                        vigra::MultiArrayView<2, double> cov_d, 
                                        double fx, double fy, double b = 1.0)
{
    using namespace vigra::linalg;
    /*
         px   fx  0 cx    X
     l*  py =  0 fy cy *  Y
          d    0  0  1    Z

          px(X,Y,Z) = fx* X/Z + cx
          py(X,Y,Z) = fy* Y/Z + cy
          d(X,Y,Z)  = b*f/Z

          J11 = d/dx px = fx/Z    J12 = d/dy px = 0     J13 = d/dz px = -fx*X/(Z*Z)
          J21 = d/dx py = 0       J22 = d/dy py = fy/Z  J23 = d/dz py = -fx*Y/(Z*Z)
          J31 = d/dx d  = 0       J32 = d/dy d  = 0     J33 = d/dz d  = -(b*f)/(Z*Z)
    */
    double inv_z_sqr = 1.0/(x[2]*x[2]);
    cov_d*=0;
    cov_d(0,0) = fx/x[2];
    cov_d(0,1) = 0;
    cov_d(0,2) = -fx*x[0]*inv_z_sqr;
    cov_d(1,0) = 0;
    cov_d(1,1) = fy/x[2];
    cov_d(1,2) = -fx*x[1]*inv_z_sqr;
    cov_d(2,0) = 0;
    cov_d(2,1) = 0;
    cov_d(2,2) = -(b*(fx+fy)/2.0)*inv_z_sqr;
    cov_d = mmul(mmul(cov_d,cov_x),cov_d.transpose());
}