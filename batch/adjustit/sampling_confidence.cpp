#include <ceres/ceres.h>
#include <vigra/hdf5impex.hxx>
#include <vigra/linear_algebra.hxx>
#include <list>
#include <ceres/rotation.h>
#include <tclap/CmdLine.h>
#include "rodrigues_jacobian.hxx"

#include <Eigen/Dense>

#include <boost/random/mersenne_twister.hpp>
#include <boost/random/normal_distribution.hpp>    

/*
  We need a functor that can pretend it's const,
  but to be a good random number generator 
  it needs mutable state.
*/
namespace Eigen {
namespace internal {
template<typename Scalar> 
struct scalar_normal_dist_op 
{
  static boost::mt19937 rng;    // The uniform pseudo-random algorithm
  mutable boost::normal_distribution<Scalar> norm;  // The gaussian combinator

  EIGEN_EMPTY_STRUCT_CTOR(scalar_normal_dist_op)

  template<typename Index>
  inline const Scalar operator() (Index, Index = 0) const { return norm(rng); }
};

template<typename Scalar> boost::mt19937 scalar_normal_dist_op<Scalar>::rng;

template<typename Scalar>
struct functor_traits<scalar_normal_dist_op<Scalar> >
{ enum { Cost = 50 * NumTraits<Scalar>::MulCost, PacketAccess = false, IsRepeatable = false }; };
} // end namespace internal
} // end namespace Eigen

int main(int argc, char** argv)
{
    TCLAP::CmdLine cmd("BoschGT BundleAdjuster", ' ', "0.1");
    TCLAP::ValueArg<std::string>  
        inputFile("i",
                     "infile", 
                     "hdf5 file from solvepnp step", 
                     false,
                     "E:/boschgt/sequences/00/new_input2.h5",
                     "string",
                     cmd);


    TCLAP::ValueArg<std::string>  
        outputFile("o", 
                     "output", 
                     "output file", 
                     false,
                     "E:/boschgt/sequences/00/ba_output.h5",
                     "string",
                     cmd);

    TCLAP::ValueArg<int>  
        repetitions   ("n", 
                     "n-repetitions", 
                     "how much to sample", 
                     false,
                      10000,
                     "int",
                     cmd);
    
    TCLAP::ValueArg<int>  
        framebegin   ("b", 
                     "frame-begin", 
                     "specify start frame (only used if --range-fixed specified)", 
                     false,
                      2101,
                     "int",
                     cmd);
    
    TCLAP::ValueArg<int>  
        frameend   ("e", 
                     "frame-end", 
                     "specify end frame", 
                     false,
                      2101,
                     "int",
                     cmd);

    cmd.parse(argc, argv);
    vigra::HDF5File file(inputFile.getValue(), vigra::HDF5File::Open);
    vigra::HDF5File outfile(outputFile.getValue(), vigra::HDF5File::New);
    int height = 1080;
    int width  = 2560;
    vigra::TinyVector<double, 2> f(1850, 1850);  //focal length
    vigra::TinyVector<double, 2> c(1300, 550);
    vigra::MultiArray<2, double> frames;
    {
        vigra::MultiArray<2, double> tmp_frames;
        file.readAndResize("frames", tmp_frames);
        frames = tmp_frames.transpose();
    }
    int row_begin = 0, row_end = int(frames.shape(1));

    for(int ii = 0; ii < frames.shape(1); ++ii)
    {
        if(frames(0, ii) == framebegin.getValue())
            row_begin = ii;
        if(frames( 0, ii) == frameend.getValue() )
            row_end = ii+1;
    }
    vigra::MultiArray<2, double> cameras(vigra::Shape2(6, frames.shape(1))); 
    for(int ii = row_begin; ii < row_end; ++ii)
    {
        vigra::MultiArray<2, double> rot = vigra::MultiArrayView<2, double>(vigra::Shape2(3,3), &frames(2,ii)).transpose();
        ceres::RotationMatrixToAngleAxis(rot.data(),  &cameras(0,ii));
        std::copy(&frames(11, ii), &frames(11,ii)+3, &cameras(3, ii));
    }

    std::cout << "DATA LOADED!\n";
    int nn=repetitions.getValue();     // How many samples (columns) to draw
    Eigen::internal::scalar_normal_dist_op<double> randN; // Gaussian functor
    Eigen::internal::scalar_normal_dist_op<double>::rng.seed(std::time(0)); // Seed the rng
    for(int ii = row_begin; ii< row_end;++ii)
    {
        std::string xyz = "/xyzmaps/";
        std::string framecov = "/framecov/";
        char num[7];
        std::sprintf(num, "%06d", int(frames(0,ii)));

        vigra::MultiArray<2, double> cov;
        vigra::MultiArray<3, double> xyzmap;
        file.readAndResize(xyz+ num, xyzmap);
        file.readAndResize(framecov+ num, cov);
        Eigen::Map<Eigen::Matrix<double, 6, 6> > eig_cov(cov.data());
        Eigen::Map<Eigen::Matrix<double, 6, 1> > eig_mean(columnVector(cameras, ii).data());
        Eigen::Matrix<double, 6, 6> normTransform;
        Eigen::LLT<Eigen::Matrix<double, 6, 6> > cholSolver(eig_cov);
        // We can only use the cholesky decomposition if 
        // the covariance matrix is symmetric, pos-definite.
        // But a covariance matrix might be pos-semi-definite.
        // In that case, we'll go to an EigenSolver
        if (cholSolver.info()==Eigen::Success) {
            // Use cholesky solver
            normTransform = cholSolver.matrixL();
        } else {
            // Use eigen solver
            Eigen::SelfAdjointEigenSolver<Eigen::MatrixXd> eigenSolver(eig_cov);
            normTransform = eigenSolver.eigenvectors() 
                           * eigenSolver.eigenvalues().cwiseSqrt().asDiagonal();
        }
        Eigen::MatrixXd samples = (normTransform 
                               * Eigen::MatrixXd::NullaryExpr(6,nn,randN)).colwise() 
                               + eig_mean;
        vigra::MultiArray<4, double> per_point_cov_d(vigra::Shape4(3,3, width, height));
#pragma omp parallel for
        for(int jj =0; jj < height; ++jj)
        {
            std::cerr << "\r" << ii-row_begin+1 << " of " <<row_end-row_begin << " done. Row: "<< jj ;
            for(int gg = 0; gg < width; ++gg)
            {
                vigra::MultiArray<2, double> cur_covd = per_point_cov_d.bindOuter(jj).bindOuter(gg);
                vigra::Matrix<double> means(3,1);
                int count = 0;
                for(int hh =0; hh < nn; ++hh)
                {
                    double* cur_cam = &samples(0, hh);
                    vigra::MultiArray<1, double> rotated_pt(vigra::Shape1(3));
                    vigra::MultiArray<1, double> disp_space(vigra::Shape1(3));
                    ceres::AngleAxisRotatePoint(cur_cam, 
                                                xyzmap.bindOuter(jj).bindOuter(gg).data(), 
                                                rotated_pt.data());
                    rotated_pt[0] += cameras(3,ii);
                    rotated_pt[1] += cameras(4,ii);
                    rotated_pt[2] += cameras(5,ii);
                    disp_space[0] = f[0]*rotated_pt[0]/rotated_pt[2] + c[0];
                    disp_space[1] = f[1]*rotated_pt[1]/rotated_pt[2] + c[1];
                    disp_space[2] = ((f[0]+f[1])/2.0)/rotated_pt[2];
                    vigra::linalg::detail::updateCovarianceMatrix(disp_space.insertSingletonDimension(1), count, means, cur_covd);
                }
                cur_covd /= double(3 - 1);
            }
        }
        outfile.write(std::string("/xyzcov_d/")+num, per_point_cov_d);
        std::cerr<<"\n";
    }
}