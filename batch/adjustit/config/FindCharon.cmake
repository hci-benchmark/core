# - Find Charon-core
# Find the native Charon-core includes and library
# This module defines
#  Charon_INCLUDE_DIR, where to find Charon header files, etc.
#  Charon_LIBRARIES, the libraries needed to use Charon-core.
#  Charon_FOUND, If false, do not try to use Charon-core.

FIND_PATH(Charon_INCLUDE_DIR charon-core/ParameterFile.h)

SET(Charon_NAMES ${Charon_NAMES} charon-core)
FIND_LIBRARY(Charon_LIBRARY NAMES ${Charon_NAMES} )

# handle the QUIETLY and REQUIRED arguments and set Charon_FOUND to TRUE if 
# all listed variables are TRUE
INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(Charon DEFAULT_MSG Charon_LIBRARY Charon_INCLUDE_DIR)

IF(Charon_FOUND)
    SET(Charon_LIBRARIES ${Charon_LIBRARY})
ENDIF(Charon_FOUND)
