# - Find VIGRA
# Find the native VIGRA includes and library
# This module defines
#  VIGRA_INCLUDE_DIR, where to find multi_array.hxx, etc.
#  VIGRA_LIBRARIES, the libraries needed to use vigraimpex.
#  VIGRA_FOUND, If false, do not try to use VIGRA.

FIND_PATH(VIGRA_INCLUDE_DIR vigra/multi_array.hxx)

SET(VIGRA_NAMES ${VIGRA_NAMES} vigraimpex)
FIND_LIBRARY(VIGRA_LIBRARY NAMES ${VIGRA_NAMES} )

# handle the QUIETLY and REQUIRED arguments and set VIGRA_FOUND to TRUE if 
# all listed variables are TRUE
INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(VIGRA DEFAULT_MSG VIGRA_LIBRARY VIGRA_INCLUDE_DIR)

IF(VIGRA_FOUND)
    SET(VIGRA_LIBRARIES ${VIGRA_LIBRARY})
ENDIF(VIGRA_FOUND)
