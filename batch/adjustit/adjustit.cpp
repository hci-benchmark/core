#include <ceres/ceres.h>
#include <vigra/hdf5impex.hxx>
#include <vigra/linear_algebra.hxx>
#include <list>
#include <ceres/rotation.h>
#include <tclap/CmdLine.h>
#include "rodrigues_jacobian.hxx"
#if 0
typedef vigra::MultiArrayShape<2>::type Shp2;
void get_dataset_names(vigra::HDF5File & file, std::vector<std::string> & output, std::string subgroup = "/")
{
    std::set<std::string> datasets;
    file.cd(subgroup);
    file.ls(datasets);
    output.resize(datasets.size());
    std::copy(datasets.begin(), datasets.end(), output.begin());
}

void load_datasets(vigra::HDF5File & file, std::vector<std::string> & output, std::vector<vigra::MultiArray<2, double> > & data)
{
    data.resize(output.size());
    int tenpercent = output.size()/20;
    for(int ii =0; ii < output.size(); ++ii)
    {
        file.readAndResize(output[ii], data[ii]);
        if(ii%tenpercent == 0)
        {
            int ntenpercent = ii/tenpercent;
            std::cout << "\r[";
            for(int ii = 0; ii < ntenpercent; ++ii) std::cout << "#";
            for(int ii = ntenpercent; ii < 20; ++ii) std::cout << " ";
            std::cout << "] " << ntenpercent*5 << "% done";
        }
    }
    std::cout << std::endl;
}


void create_strID2intID(std::vector<std::string> & strID, std::map<std::string, int> & intID)
{
    for(int ii = 0; ii < intID.size();++ii)
    {
        intID[strID[ii]] = ii;
    }
}

// TODO check transpose of rotation
void fill_transforms(std::vector<vigra::MultiArray<2, double> > & data, 
                     vigra::MultiArray<2, double> & output, 
                     std::vector<std::string> & intID2strID, 
                     int first = 0, 
                     int beyond_last = 1)
{
    int nFrames =beyond_last - first;
    output.reshape(Shp2(6, nFrames));
    std::vector<int> was_filled(nFrames, 0);
    for(int ii = 0; ii < data.size(); ++ii)
    {
        int curidx = atoi(intID2strID[ii].c_str()) - first;
        if(curidx >= 0 && curidx < nFrames)
        {
            vigra::MultiArray<2, double> Rmat= data[ii].subarray(Shp2(0,0), Shp2(3,3));
            ceres::RotationMatrixToAngleAxis(Rmat.data(), &output(0, curidx));
            output(3, curidx) = data[ii](0, 3);
            output(4, curidx) = data[ii](1, 3);
            output(5, curidx) = data[ii](2, 3);
            //std::cerr << vigra::columnVector(output, curidx) << std::endl;
            was_filled[curidx] = 1;
        }
    }
}

int filter_dataset( std::vector<vigra::MultiArray<2, double> > & data,  
                    std::vector<int> &index_mapping,
                    int first, int beyondlast)
{
    std::vector<int> valid_tracks(data.size(), 0);
#pragma omp parallel for
    for(int ii = 0; ii < data.size(); ++ii)
    {
        std::vector<int> indices;
        for(int jj = 0; jj < data[ii].shape(1); ++jj)
        {
            if(data[ii](0, jj) >= first && data[ii](0,jj) <beyondlast)
            {
                indices.push_back(jj);
            }
        }
        vigra::MultiArray<2, double> tmp = data[ii];
        valid_tracks[ii] = indices.size();
        data[ii].reshape(Shp2(3, indices.size()));
        for(int jj = 0; jj < indices.size(); ++jj)
        {
            vigra::columnVector(data[ii], jj) = vigra::columnVector(tmp, indices[jj]);
        }
    }
    int result = 0;
    std::vector<vigra::MultiArray<2, double> > newData;
    index_mapping.resize(data.size(), -1);
    for(int ii = 0; ii < valid_tracks.size(); ++ii)
    {
        if(valid_tracks[ii] > 0)
        {
            index_mapping[ii]= newData.size();
            newData.push_back(data[ii]);
            ++result;
        }
    }
    data = newData;
    return result;
}
#endif
struct PinningTerm
{
    double x, y, z;
    double alpha_;
    PinningTerm(vigra::TinyVectorView<double, 3> & pos, double alpha=1.0)
        : x(pos[0]), y(pos[1]), z(pos[2]), alpha_(alpha)
    {}
    
    template <typename T> 
    bool operator()(  const T*  const position,
                                                  T* residual) const 
    {
        T alpha = T(alpha_);
        residual[0] =alpha*(position[0] - T(x));
        residual[1] =alpha*(position[1] - T(y));
        residual[2] =alpha*(position[2] - T(z));
        return true;
    }
#if 0
    bool operator()(  const double*  const position,
                                                  double* residual) const 
    {
        double alpha = double(alpha_);
        residual[0] =alpha*(position[0] - double(x));
        residual[1] =alpha*(position[1] - double(y));
        residual[2] =alpha*(position[2] - double(z));
        std::cout << "Pinning: \n";
        std::cout << x << " " << y << " " << z << std::endl;
        std::cout << position[0] << " " << position[1] << " " << position[2] << std::endl;
        return true;
    }
#endif
};


struct ReprojectionErrorTerm
{
    vigra::TinyVector<double, 2> m_;
    vigra::TinyVectorView<double, 2> f_;
    vigra::TinyVectorView<double, 2> c_;
    double baseline_;
    double weight_;
    ReprojectionErrorTerm(vigra::TinyVectorView<double, 2>  measurement,
                          vigra::TinyVectorView<double, 2>  f,
                          vigra::TinyVectorView<double, 2>  c,
                          double weight =   1.0,
                          double baseline = 0.0)
        : m_(measurement), f_(f), c_(c),weight_(weight), baseline_(baseline)
    {}
    template <typename T> bool operator()(  const T*  const transform,
                                            const T*  const position,
                                                  T* residual) const 
    {
        T p[3];
        ceres::AngleAxisRotatePoint(transform, position, p);
        p[0] += transform[3]; p[1] += transform[4]+T(baseline_); p[2] += transform[5];
        T fx = T(f_[0]);
        T fy = T(f_[1]);
        T cx = T(c_[0]);
        T cy = T(c_[1]);

        T xp = fx * p[0] / p[2] + cx;
        T yp = fy * p[1] / p[2] + cy;
        T weight = T(weight_);
        residual[0] = weight*(xp - T(m_[0]));
        residual[1] = weight*(yp - T(m_[1]));
        return true;
    }
#if 0
    bool operator()(  const double*  const transform,
                                            const double*  const position,
                                                  double* residual) const 
    {
        double p[3];
        std::cout << "Reprojection: \n";
        std::cout << position[0] << " " << position[1] << " " << position[2] << std::endl;
        ceres::AngleAxisRotatePoint(transform, position, p);
        std::cout << p[0] << " " << p[1] << " " << p[2] << std::endl;
        p[0] += transform[3]; p[1] += transform[4]+double(baseline_); p[2] += transform[5];
        std::cout << p[0] << " " << p[1] << " " << p[2] << std::endl;
        double fx = double(f_[0]);
        double fy = double(f_[1]);
        double cx = double(c_[0]);
        double cy = double(c_[1]);

        double xp = fx * p[0] / p[2] + cx;
        double yp = fy * p[1] / p[2] + cy;
        //std::cout << "Reprojection: \n";
        std::cout << xp <<" " << yp <<" " << m_[0]<< " " << m_[1] << std::endl;
        //std::cout << p[0] << " " << p[1] << " " << p[2] << std::endl;

        //std::cout << transform[3] << " " << transform[4] << " " << transform[5]<< std::endl;
        //std::cout << fx << " " << fy << " " << cx << " " << cy<< std::endl;
        residual[0] = xp - double(m_[0]);
        residual[1] = yp - double(m_[1]);
        return true;
    }
#endif
};

struct WeightComputer
{
    double compute(const double*  const transform, const double*  const position)
    {
        double p[3];
        ceres::AngleAxisRotatePoint(transform, position, p);
        p[0] += transform[3]; p[1] += transform[4]; p[2] += transform[5];

        return getWeight(p);
    }
    virtual double getWeight(double* p)=0;
};

struct UniformWeightComputer: public WeightComputer
{
    virtual double getWeight(double* p)
    {
        return 1.0;
    }
};

struct GaussianWeightComputer: public WeightComputer
{
    double sigma;
    double min_weight;
    /** weight is half of original value at distance HWHM
        magic number is sqrt(2ln(2));
        minweight has to be between 0 and 1;
    */
    GaussianWeightComputer(double HWHM, double minWeight = 0.0):sigma(HWHM/1.38629), min_weight(minWeight)
    {}
    virtual double getWeight(double* p)
    {
        double zsqr = -p[2]*p[2];
        double variance = sigma*sigma;
        return 		(std::exp(zsqr/variance))*(1-min_weight)+min_weight;
    }
};

int main(int argc, char** argv)
{
    TCLAP::CmdLine cmd("BoschGT BundleAdjuster", ' ', "0.1");
    TCLAP::ValueArg<std::string>  
        inputFile("i",
                     "infile", 
                     "hdf5 file from solvepnp step", 
                     false,
                     "E:/boschgt/sequences/00/new_input2.h5",
                     "string",
                     cmd);

    TCLAP::ValueArg<double>  
        weight_alpha("w",
                     "weight", 
                     "weight for pinning term", 
                     false,
                     double(1.0),
                     "double",
                     cmd); 
     TCLAP::ValueArg<double>  
        hwhm       ("k", 
                     "hwhm", 
                     "distance at which track weight is 50percent of track at distance 0 -1 = no weight", 
                     false,
                      -1.0,
                     "double",
                     cmd);
     TCLAP::ValueArg<double>  
        hwhm_min   ("j", 
                     "min_hwhm_weight", 
                     "minimum weight of each track", 
                     false,
                      0.0,
                     "double",
                     cmd);

    TCLAP::ValueArg<std::string>  
        outputFile("o", 
                     "output", 
                     "output file", 
                     false,
                     "E:/boschgt/sequences/00/ba_output.h5",
                     "string",
                     cmd);

    
    TCLAP::ValueArg<int>  
        framebegin   ("b", 
                     "frame-begin", 
                     "specify start frame (only used if --range-fixed specified)", 
                     false,
                      2101,
                     "int",
                     cmd);
    
    TCLAP::ValueArg<int>  
        frameend   ("e", 
                     "frame-end", 
                     "specify end frame", 
                     false,
                      2101,
                     "int",
                     cmd);
    TCLAP::SwitchArg usePTerm("p", "usePinningTerm", "use Pinning terms as well", false);
    TCLAP::SwitchArg usePVals("l", "usePinnedValsOnly", "use Pinned tracks  only", false);
    TCLAP::SwitchArg doCovariance("c", "covariance", "do covariance analysis", false);
    TCLAP::SwitchArg doOnlyCovariance("x", "only_covariance", "do only covariance analysis (solver iterations set to 1)", false);
    cmd.add(usePTerm);
    cmd.add(usePVals);
    cmd.add(doCovariance);
    cmd.add(doOnlyCovariance);

    cmd.parse(argc, argv);
    bool usePinnedOnly  = usePVals.getValue();
    bool usePinningTerm = usePTerm.getValue();
    WeightComputer* weightComputer;
    if(hwhm.getValue()==-1.0)
    {
        std::cout << " Using Uniform Weighting \n";
        weightComputer = new UniformWeightComputer;
    }
    else
    {
        std::cout << " using Gaussian Weighting\n" <<"HWHM: " << hwhm.getValue() <<"\nmin weight " <<   hwhm_min.getValue() <<std::endl;
        weightComputer = new GaussianWeightComputer(hwhm.getValue(), hwhm_min.getValue());
    }
    vigra::HDF5File file(inputFile.getValue(), vigra::HDF5File::Open);
    vigra::MultiArray<2, double> frames;
    {
        vigra::MultiArray<2, double> tmp_frames;
        file.readAndResize("frames", tmp_frames);
        frames = tmp_frames.transpose();
    }
    int row_begin = 0, row_end = int(frames.shape(1));

    vigra::ArrayVector<hsize_t> frame_tracks_shape = file.getDatasetShape("/frame_tracks");
    int frame_track_begin = 0;
    int frame_track_end   = int(frame_tracks_shape[0]);
    std::cout << "Setting begin and end markers...";
    for(int ii = 0; ii < frames.shape(1); ++ii)
    {
        if(frames(0, ii) == framebegin.getValue())
        {
            row_begin = ii;
            frame_track_begin = int(frames(1, ii));
        }
        if(frames( 0, ii) == frameend.getValue() )
        {
            row_end = ii+1;
            frame_track_end = int((row_end == frames.shape(1))?frame_tracks_shape[0]:frames(1,ii+1));
        }
    }
    std::cout << "done\n";
    std::cout << row_begin << ", " << row_end << "| " << frame_track_begin << ", " << frame_track_end << std::endl;
    
    vigra::MultiArray<2, double> valid_frame_tracks;
    {
        vigra::Shape2 valid_frame_tracks_offset(frame_track_begin, 0);
        vigra::Shape2 valid_frame_tracks_shape(frame_track_end- frame_track_begin, 5);
        vigra::MultiArray<2, double> valid_frame_trackst(valid_frame_tracks_shape);
        file.readBlock("/frame_tracks", valid_frame_tracks_offset, valid_frame_tracks_shape, valid_frame_trackst);
        valid_frame_tracks = valid_frame_trackst.transpose();
    }
    vigra::MultiArray<2, double> cameras(vigra::Shape2(6, frames.shape(1))); 
    vigra::MultiArray<2, double> tracks;
    {
        vigra::MultiArray<2, double> trackst;
        file.readAndResize("/tracks", trackst);
        tracks = trackst.transpose();
    }

    for(int ii = row_begin; ii < row_end; ++ii)
    {
        vigra::MultiArray<2, double> rot = vigra::MultiArrayView<2, double>(vigra::Shape2(3,3), &frames(2,ii)).transpose();
        ceres::RotationMatrixToAngleAxis(rot.data(),  &cameras(0,ii));
        std::copy(&frames(11, ii), &frames(11,ii)+3, &cameras(3, ii));
    }
    std::cout << "DATA LOADED!\n";

    // create ceres Problem
    ceres::Problem problem;
    vigra::TinyVector<double, 2> f(1850, 1850);  //focal length
    vigra::TinyVector<double, 2> c(1300, 550);
    std::vector<int> offsets;
    std::vector<double> baseline;
    offsets.push_back(2);
    baseline.push_back(0);
    double alpha =  weight_alpha.getValue();
    //offsets.push_back(4);
    //baseline.push_back(0);

    int pinCt = 0;
    int trackCt = 0;
    for(int ii =0; ii < valid_frame_tracks.shape(1); ++ii)
    {
        int track_row = int(valid_frame_tracks(1, ii));
        int frame_row = int(valid_frame_tracks(0, ii));

        vigra::TinyVectorView<double, 6> transform(columnVector(cameras, frame_row).data());
        vigra::TinyVectorView<double, 3> pos(&tracks(1, track_row));
        //std::cerr << pos << std::endl;
        if(pos[0] == 0 && pos[1] == 0 && pos[2] == 0)
            continue;
        if(track_row >= tracks.shape(1))
            continue;
        bool isPinned = tracks(7, track_row) >0.5;
        if(isPinned)
            pinCt +=1;
        if(!isPinned &&usePinnedOnly)
            continue;


        //add reprojection term
        for(int jj = 0; jj < offsets.size(); ++jj)
        {

            double x = valid_frame_tracks(offsets[jj], ii);
            double y = valid_frame_tracks(3, ii);

            bool isValid = x != -1;
            isValid = isValid &&(y<950&& y > 19) &&(x>30 && x< 2530);// points outside are bad due to motorhaube and stuff.

            if(isValid)
            {
                double weight = weightComputer->compute(transform.data(), pos.data());
                vigra::TinyVector<double, 2> measurement(x, y);
                ceres::CostFunction * cost = 
                        new ceres::AutoDiffCostFunction<ReprojectionErrorTerm, 2, 6, 3>(new ReprojectionErrorTerm(measurement, f, c, weight, baseline[jj]));
                problem.AddResidualBlock(cost, 0, transform.data(), pos.data());
                trackCt +=1;
            }

        }

        //add pinning term
        if(isPinned && usePinningTerm)
        {
            vigra::TinyVectorView<double, 3> fixed_pos(&tracks(4, track_row));
            ceres::CostFunction * cost = 
                new ceres::AutoDiffCostFunction<PinningTerm, 3, 3>(new PinningTerm(fixed_pos,alpha));
            problem.AddResidualBlock(cost, 0, pos.data());
        }


    }
    std::cout << "Problem created\n";
    std::cout << trackCt << " reprojection terms " << pinCt << " pinning terms.\n";
    ceres::Solver::Options options;        
    options.minimizer_progress_to_stdout = true;
    options.max_num_iterations = 100;


    options.linear_solver_type = ceres::SPARSE_SCHUR;
    options.minimizer_type = ceres::TRUST_REGION;
    if(doOnlyCovariance.getValue())        //if we are doing covariance analysis then 
        options.max_num_iterations = 1;
    ceres::Solver::Summary summary;
    ceres::Solve(options, &problem, &summary);
    std::cout << summary.BriefReport() << "\n";


    for(int ii = row_begin; ii < row_end; ++ii)
    {
        vigra::MultiArray<2, double> rot(vigra::Shape2(3,3));
        ceres::AngleAxisToRotationMatrix(&cameras(0, ii), rot.data());
        vigra::MultiArray<2, double> rott = rot.transpose();
        std::copy(rott.begin(), rott.end(), &frames(2, ii));
        std::copy( &cameras(3, ii),  &cameras(3, ii)+3, &frames(11, ii));
    }
    vigra::HDF5File outfile(outputFile.getValue(), vigra::HDF5File::New);
    {
        vigra::MultiArray<2, double> trackstmp = tracks.transpose();
        outfile.createDataset<2, double>("/tracks" , trackstmp.shape(), 0, vigra::Shape2(std::min<int>(1024, trackstmp.shape(0)), trackstmp.shape(1)));
        outfile.writeBlock("/tracks", vigra::Shape2(0,0), trackstmp);
    }
    {
        vigra::MultiArray<2, double> framestmp = frames.transpose();
        outfile.createDataset<2, double>("/frames" , framestmp.shape(), 0, vigra::Shape2(std::min<int>(1024, framestmp.shape(0)), framestmp.shape(1)));
        outfile.writeBlock("/frames", vigra::Shape2(0,0), framestmp);
    }
    if(doCovariance.getValue()||doOnlyCovariance.getValue())
    {
        std::cerr << "Output written, now doing Covariance Analysis\n";
        ceres::Covariance::Options options2;
        options2.algorithm_type = ceres::SPARSE_QR;
        ceres::Covariance covariance(options2);

        std::vector<std::pair<const double*, const double*> > covariance_blocks;
        for(int ii = row_begin; ii < row_end; ++ii)
        {
            covariance_blocks.push_back(std::make_pair(columnVector(cameras, ii).data(), columnVector(cameras, ii).data()));
        }
        std::cerr << "addded all blocks of interest\n"<<std::endl;
        bool working = covariance.Compute(covariance_blocks, &problem);

        std::cerr << "Covariance compute PART FINISHED\n"<<std::endl;
        int width = 2560;
        int height = 1080;
        vigra::MultiArray<4, double> per_point_cov(vigra::Shape4(3,3, width, height));
        vigra::MultiArray<4, double> per_point_cov_d(vigra::Shape4(3,3, width, height));
        double base = 1.0;
        for(int ii = row_begin; ii < row_end; ++ii)
        {
            if(!working)
                break;
            std::cerr << "\r" << ii-row_begin+1 << " of " <<row_end-row_begin << " done.";
            vigra::MultiArray<2, double> cov(vigra::Shape2(6,6));
            vigra::MultiArray<3, double> xyzmap;
            std::string result = "/xyzmaps/";
            char num[7];
            std::sprintf(num, "%06d", int(frames(0,ii)));
            file.readAndResize(result+ num, xyzmap);
            covariance.GetCovarianceBlock(columnVector(cameras, ii).data(), columnVector(cameras, ii).data(), cov.data());
            outfile.write(std::string("/framecov/")+num, cov);
    #pragma omp parallel for
            for(int jj =0; jj < height; ++jj)
            {
            std::cerr << "\r" << ii-row_begin+1 << " of " <<row_end-row_begin << " done. Row: "<< jj ;
                for(int gg = 0; gg < width; ++gg)
                {
                    vigra::MultiArray<2, double> JRt(vigra::Shape2(3, 6));
                    computeRtJacobian(xyzmap.bindOuter(jj).bindOuter(gg),columnVector(cameras, ii), JRt);
                    computeUncertaintyIn3DPoint(JRt, cov, per_point_cov.bindOuter(jj).bindOuter(gg));
                    vigra::MultiArray<1, double> rotated_pt(vigra::Shape1(3));
                    ceres::AngleAxisRotatePoint(columnVector(cameras, ii).data(), 
                                                xyzmap.bindOuter(jj).bindOuter(gg).data(), 
                                                rotated_pt.data());
                    rotated_pt[0] += cameras(3,ii);
                    rotated_pt[1] += cameras(4,ii);
                    rotated_pt[2] += cameras(5,ii);
                    computeUncertaintyInDisparitySpace(rotated_pt,
                                                        per_point_cov.bindOuter(jj).bindOuter(gg),
                                                        per_point_cov_d.bindOuter(jj).bindOuter(gg),
                                                        f[0], f[1], base);
                    //compute error in xyz map.
                }
            }
            outfile.write(std::string("/xyzcov/")+num, per_point_cov);
            outfile.write(std::string("/xyzcov_d/")+num, per_point_cov_d);
        }
        std::cerr<<"\n";
    }

}