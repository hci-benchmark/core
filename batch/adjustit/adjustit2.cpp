#include <ceres/ceres.h>
#include <vigra/hdf5impex.hxx>
#include <vigra/linear_algebra.hxx>
#include <list>
#include <ceres/rotation.h>
#include <tclap/CmdLine.h>
#include <vigra/timing.hxx>
#include <hash_map>
USETICTOC;

typedef vigra::MultiArrayShape<2>::type Shp2;
void get_dataset_names(vigra::HDF5File & file, std::vector<std::string> & output, std::string subgroup = "/")
{
    std::set<std::string> datasets;
    file.cd(subgroup);
    file.ls(datasets);
    output.resize(datasets.size());
    std::copy(datasets.begin(), datasets.end(), output.begin());
}


int main(int argc, char** argv)
{
    TCLAP::CmdLine cmd("BoschGT FileConverter", ' ', "0.1");
    TCLAP::ValueArg<std::string>  
        solvepnpFile("i", 
                     "input", 
                     "input file", 
                     false,
                     "seq0000_all.h5",
                     "string",
                     cmd);
    
    TCLAP::ValueArg<std::string>  
        outputFile  ("o", 
                     "output", 
                     "outputfile", 
                     false,
                     "seq0000_newLayout.h5",
                     "string",
                     cmd);
    std::string name = solvepnpFile.getValue();
    vigra::HDF5File file(name, vigra::HDF5File::Open);
    std::vector<std::string> dsets;
    get_dataset_names(file, dsets, "/tracks");
    typedef vigra::TinyVector<double,3> Vec3;
    std::map<int, std::vector<Vec3> > track_2_frame;
    std::map<int, std::vector<Vec3> > frame_2_track;
    TIC;
    int nTrackFrames = 0;
    for(int ii = 0; ii < dsets.size(); ++ii)
    {
        if(ii% 1000 == 0)
            std::cerr << ii << std::endl;
        vigra::MultiArray<2, double> tmp;
        file.readAndResize(dsets[ii], tmp);
        int trackName = std::atoi(dsets[ii].c_str());
        for(int jj = 0; jj < tmp.shape(0); ++jj)
        {
            int frameNo = tmp(jj, 0);
            track_2_frame[trackName].push_back(Vec3(frameNo, tmp(jj,1), tmp(jj,2)));
            frame_2_track[frameNo].push_back(Vec3(trackName,tmp(jj,1), tmp(jj,2)));
            nTrackFrames +=1;
        }
    }
    TOC;
    name =  outputFile.getValue();
    vigra::HDF5File fileOut(name, vigra::HDF5File::New);
    fileOut.createDataset<2, double>("tracks", vigra::Shape2(track_2_frame.size(),8), 0, vigra::Shape2(1,8));
    fileOut.createDataset<2, double>("frames", vigra::Shape2(frame_2_track.size(),14), 0, vigra::Shape2(1, 14));
    fileOut.createDataset<2, double>("frame_tracks", vigra::Shape2(nTrackFrames,5), -1.0, vigra::Shape2(1,5));
    int offset = 0;
    int ct = 0;
    vigra::HDF5File fileCameras("seq0000_after_solvepnp2.h5", vigra::HDF5File::Open);
    fileCameras.cd("transformations");
    for(auto iter = frame_2_track.begin(); iter!=frame_2_track.end(); ++iter)
    {
        int jj = std::distance(frame_2_track.begin(), iter);
        std::cerr << jj <<" " << frame_2_track.size() << std::endl;
        vigra::MultiArray<2, double>  frames_row(vigra::Shape2(1, 14));
        frames_row(0,0) = iter->first;
        frames_row(0,1) = offset;
        fileOut.writeBlock("frames", vigra::Shape2(jj,0), frames_row);
        offset+=iter->second.size();
        for(int ii = 0; ii< iter->second.size(); ++ii)
        {
            vigra::MultiArray<2, double>  frame_tracks_row(vigra::Shape2(1, 5));
            frame_tracks_row(0,0) = jj;
            Vec3 & restrow = iter->second[ii];
            std::copy(restrow.begin(), restrow.end(), &frame_tracks_row(0,1));
            frame_tracks_row(0,4) = -1;
            fileOut.writeBlock("frame_tracks", vigra::Shape2(ct,0), frame_tracks_row);
            ++ct;
        }
    }
    for(auto iter = track_2_frame.begin(); iter!=track_2_frame.end(); ++iter)
    {
        int jj = std::distance(track_2_frame.begin(), iter);
        if(jj%100== 0)std::cerr<< jj << std::endl;
        vigra::MultiArray<2, double>  tracks_row(vigra::Shape2(1, 8));
        tracks_row(0,0) = iter->first;
        fileOut.writeBlock("tracks", vigra::Shape2(jj,0), tracks_row);
    }
}