#include <iostream>
#include <vector>

#include <money/Money.h>
#include <money/Frontend/Vector.h>


class MyBlock1 : public money::Block {
public:
    float compute() {
        return 5.0+4.4*4;
    }
};

class MyBlock2 : public money::Block {
public:
    void compute() {
        /* do some stuff here */

    }
};






int main(int argc, char ** argv) {

    money::init(argc, argv);

    int a = 1;
    money::Memoizeable<std::vector<int> > testMem("test");
    if(testMem.changed())
    {
        std::vector<int> & testData = testMem;
        testData.resize(5);
        testData[0] = 1;
        testData[1] = 2;
        testData[2] = 10;
        testData[3] = 2;
        testData[4] = 1;
    } else {
        const std::vector<int> & testData = testMem.cget();
        std::cout << "Loaded from cache for testMem: ";
        for(auto & d: testData)
                  std::cout << d << ", ";
        std::cout << std::endl;
    }


    MyBlock1 bla;
    if(bla.changed() || testMem.changed()) {
        std::cout << "[RUNNING] MyBlock " << bla.compute() << std::endl;
    }


    MyBlock2 blub;
    if(blub.changed() || testMem.changed()) {
        std::cout << "[RUNNING] Myblock2 executed..." << std::endl;
    }

    money::Block([&testMem, &a]{

        /* This block doesn't depend on any memoizable.
         * So it will only be called when it changes */
        std::cout << "[RUNNING] Calling block on line " << __LINE__ << std::endl;


        std::vector<int> & testData = testMem;

        testData[0] = 2;



    });

    money::Block(testMem, [&]{
        /* get a const reference to the data
         * this means, the memoizeable will not be flagged
         * changed. */
        const std::vector<int> & data = testMem;

        if(data.size())
            std::cout << "[RUNNING] second block "
                      << __LINE__ << ": "
                      << data[0] << std::endl;



    });
}



