#include <iostream>
#include <vector>

#include <money/Money.h>
#include <money/Backend/HDF.h>
#include <money/Frontend/Vector.h>


int main(int argc, char ** argv) {

    money::init(argc, argv);
    money::setBackend(new money::HDFBackend());


    money::Memoizeable<std::vector<unsigned> > testMem("test");
    if(testMem.changed())
    {
        std::vector<unsigned> & testData = testMem;
        testData.resize(5);
        testData[0] = 1;
        testData[1] = 2;
        testData[2] = 10;
        testData[3] = 2;
        testData[4] = 1;
    } else {
        const std::vector<unsigned> & testData = testMem.cget();
        std::cout << "Loaded from cache for testMem: ";
        for(auto & d: testData)
                  std::cout << d << ", ";
        std::cout << std::endl;
    }

    money::Memoizeable<std::vector<char> > test2Mem("test1/test2");
    if(test2Mem.changed())
    {
        std::vector<char> & testData = test2Mem;
        testData.resize(5);
        testData[0] = -1;
        testData[1] = -2;
        testData[2] = -10;
        testData[3] = -2;
        testData[4] = -1;
    } else {
        const std::vector<char> & testData = test2Mem.cget();
        std::cout << "Loaded from cache for testMem: ";
        for(auto & d: testData)
                  std::cout << d << ", ";
        std::cout << std::endl;
    }

    money::Memoizeable<std::vector<char> > test22Mem("/test1/2_test2-2_2");
    if(test22Mem.changed())
    {
        std::vector<char> & testData = test22Mem;
        testData.resize(5);
        testData[0] = -1;
        testData[1] = -2;
        testData[2] = -10;
        testData[3] = -2;
        testData[4] = -1;
    } else {
        const std::vector<char> & testData = test22Mem.cget();
        std::cout << "Loaded from cache for test22Mem: ";
        for(auto & d: testData)
                  std::cout << d << ", ";
        std::cout << std::endl;
    }

    money::Memoizeable<std::vector<float> > test3Mem("test2/test2");
    if(testMem.changed())
    {
        std::vector<float> & testData = test3Mem;
        testData.resize(5);
        testData[0] = -1;
        testData[1] = -2;
        testData[2] = -10;
        testData[3] = -2;
        testData[4] = -1;
    } else {
        const std::vector<float> & testData = test3Mem.cget();
        std::cout << "Loaded from cache for testMem: ";
        for(auto & d: testData)
                  std::cout << d << ", ";
        std::cout << std::endl;
    }

    {
        money::Memoizeable<std::vector<double>> testMem("test2/groupa/test1");
        if(testMem.changed())
        {
            std::vector<double> & testData = testMem;
            testData.resize(5);
            testData[0] = -1;
            testData[1] = -2;
            testData[2] = -10;
            testData[3] = -2;
            testData[4] = -1;
        } else {
            const std::vector<double> & testData = testMem.cget();
            std::cout << "Loaded from cache for testMem: ";
            for(auto & d: testData)
                std::cout << d << ", ";
            std::cout << std::endl;

            /* only create a new entry if the previus one already exists */
            {
                money::Memoizeable<std::vector<double>> testMem("test2/groupb/test1");
                if(testMem.changed())
                {
                    std::vector<double> & testData = testMem;
                    testData.resize(5);
                    testData[0] = -1;
                    testData[1] = -2;
                    testData[2] = -10;
                    testData[3] = -2;
                    testData[4] = -1;
                } else {
                    const std::vector<double> & testData = testMem.cget();
                    std::cout << "Loaded from cache for testMem: ";
                    for(auto & d: testData)
                        std::cout << d << ", ";
                    std::cout << std::endl;
                    testMem.get()[2]  += -1;
                }
            }

        }
    }


}



