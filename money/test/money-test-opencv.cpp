#include <money/Money.h>
#include <money/Frontend/OpenCV.h>

int main(int argc, char ** argv)
{
    money::init(argc, argv);

    {
        typedef cv::Vec2i ChanType;
        money::Memoizeable<cv::Mat> testMem("test");
        if(!testMem.changed()) {
            cv::Mat & test = testMem;
            test = cv::Mat(cv::Size(5, 4), CV_32SC2);
            test = cv::Scalar(1,2);
            test.at<ChanType>(0, 0) = ChanType(10, 20);
            test.at<ChanType>(3, 4) = ChanType(10, 20);
        }

        const cv::Mat & test = testMem.cget();

        CV_Assert(test.size() == cv::Size(5, 4));
        CV_Assert(test.depth() == CV_32S);
        CV_Assert(test.channels() == 2);

        CV_Assert(test.at<ChanType>(0,1) == ChanType(1, 2));
        CV_Assert(test.at<ChanType>(0,0) == ChanType(10, 20));
        CV_Assert(test.at<ChanType>(3,4) == ChanType(10, 20));


        std::cout << test << std::endl;
    }

    {
        typedef double ChanType;
        money::Memoizeable<cv::Mat> testMem("test2");
        if(!testMem.changed()) {
            cv::Mat & test = testMem;
            test = cv::Mat(cv::Size(5, 4), CV_64FC1);
            test = cv::Scalar(1.5);
            test.at<ChanType>(0, 0) = ChanType(10.5);
            test.at<ChanType>(3, 4) = ChanType(10.5);
        }

        const cv::Mat & test = testMem.cget();

        CV_Assert(test.size() == cv::Size(5, 4));
        CV_Assert(test.depth() == CV_64F);
        CV_Assert(test.channels() == 1);

        CV_Assert(test.at<ChanType>(0,1) == ChanType(1.5));
        CV_Assert(test.at<ChanType>(0,0) == ChanType(10.5));
        CV_Assert(test.at<ChanType>(3,4) == ChanType(10.5));


        std::cout << test << std::endl;
    }

    return 0;
}


