#include <iostream>
#include <fstream>
#include <functional>
#include <utility>

#include <money/Meta/BlockParser.h>

typedef std::tuple<std::string, std::string> BlockTuple;




std::tuple<std::list<BlockTuple>,
           std::list<BlockTuple>,
           std::string> parseFile(const std::string & fileCode) {

    using namespace money;
    using namespace money::meta;

    std::string s = fileCode;
    std::smatch m;

    std::regex class_start("class ([a-zA-Z0-9]*)[ ]?:[ ]?public[ ]?"+meta::BLOCK_TYPE_NAME+"[ ]?\\{");
    std::regex class_end("\\};");

    std::regex block_start("money::Block[ ]?\\(([a-zA-Z0-9\\, ]*)?\\[([a-zA-Z0-9\\&\\, ]*)?\\][ ]?\\{");
    std::regex block_end("\\}\\);");



    /**
     * We collect a list of blocks. These Blocks can decide on their own,
     * if they need to be rerun.
     */
    std::list<BlockTuple> classBlocks;

    std::list<BlockTuple> inlineBlocks;

    /** for any changes outside of blocks, we have to assume that changes requires
     *  complete recalculation.
     *  We therefor collect anything that doesn't belong to a block
     *  and check if this hasn't changed */
    std::string strippedFile;

    try {
        while (std::regex_search (s,m,class_start)) {

            /* get the class name */
            std::string className = m[1];

            /* append everything located before the found token to the melting pot */
            strippedFile.append(m.prefix().str());

            /* remove anything but the remaining file content after
             * the class definition header */
            s = m.suffix().str();

            /* now look, if we can find the class enclosing brace */
            if(!std::regex_search(s, m, class_end)) {
                throw std::runtime_error("class enclosing brace '};'  is missing.");
            }
            /* and retrieve the whole block */
            std::string block = s.substr(0, m.position());

            classBlocks.push_back(std::make_tuple(className, hash(block)));

            s = m.suffix().str();
        }
        /* append anything else that is left */
        strippedFile.append(s);

        s = strippedFile;
        strippedFile.clear();

        int inlineCount = 0;
        while(std::regex_search(s, m, block_start)) {

            std::cout << "found inline block: " << m[0] << std::endl;
            strippedFile.append(m.prefix().str());
            s = m.suffix().str();

            /* now look, if we can find the class enclosing brace */
            if(!std::regex_search(s, m, block_end)) {
                throw std::runtime_error("inline block enclosing brace '});'  is missing.");
            }

            /* and retrieve the whole block */
            std::string block = s.substr(0, m.position());

            std::stringstream ss;
            ss << std::setfill('0') << std::setw(3) << inlineCount++;

            inlineBlocks.push_back(std::make_tuple(ss.str(), hash(block)));
            s = m.suffix().str();
        }
        strippedFile.append(s);

    } catch(std::regex_error & e) {
        std::cout << "Error at regex: " << e.what() << std::endl;
        classBlocks.clear();
        strippedFile.clear();
    }

    //std::cout << std::endl << std::endl << strippedFile << std::endl << std::endl;
    return std::make_tuple(classBlocks, inlineBlocks, strippedFile);

}

typedef std::vector<std::string> ArgList;
typedef std::function<void(const ArgList &)>  FunctionType;

void cmd_create_header(const ArgList & args) {

    if(args.size() < 2)
        throw std::runtime_error("Number of arguments to command is too small.");


    std::string headerFileName(args[0]);


    std::string oldContent;
    {
        std::ifstream headerOldFile;
        headerOldFile.open(headerFileName);

        if(headerOldFile.is_open()) {
            std::stringstream buffer;
            buffer << headerOldFile.rdbuf();
            oldContent = buffer.str();
        }
    }

    /* defines a regexp for a header line.
     * complicated macros won't here, of course */
    std::regex header_entry("[ ]?#[a-zA-Z]* .*");
    std::smatch m;

    std::stringstream newContentBuffer;

    newContentBuffer << "#include <typeinfo>" << std::endl;
    newContentBuffer << "#include <money/Meta/BlockParser.h>" << std::endl;
    newContentBuffer << std::endl;


    for(unsigned i = 1; i < args.size(); i++) {
        std::string sourceFileName = args[i];
        std::ifstream sourceFile;
        sourceFile.open(sourceFileName);

        if(!sourceFile.is_open())
            throw std::runtime_error("Could not open input file: " + sourceFileName);

        /* fetch file content */
        std::stringstream buffer;
        buffer << sourceFile.rdbuf();
        std::string s = buffer.str();

        /* iterate over all preprocessor directives */
        while (std::regex_search (s,m,header_entry)) {
            newContentBuffer << m[0] << std::endl;
            s = m.suffix();

        }
    }

    newContentBuffer << money::meta::make_fileEntry("\"header\"",
                                             money::meta::get_compileTimeHashExpression(),
                                             "money_file_entry_header")
                     << ";" << std::endl;



    std::string newContent = newContentBuffer.str();

    if(newContent != oldContent) {
        std::ofstream headerFile;
        headerFile.open(headerFileName);

        if(!headerFile.is_open())
            throw std::runtime_error("Could not open header file: " + headerFileName);

        headerFile << newContent;

        std::cout << "[INFO]: Writing to header file: " << headerFileName << std::endl;
    }
}

void cmd_create_source(const ArgList & args) {

        /* check that we have an input and output file name */
        if (args.size() != 2) {
            throw std::runtime_error("Number of argument to command is not equal to 2.");
        }

        std::string outFileName(args[0]);

        std::cout << "### money parser for: " << outFileName << std::endl;


        std::ofstream outFile;
        outFile.open(outFileName);

        if(!outFile.is_open())
            throw std::runtime_error("Could not open output file: " + outFileName);

        std::string inFileName(args[1]);
        std::ifstream inFile;
        inFile.open(inFileName);


        if(!inFile.is_open())
            throw std::runtime_error("Could not open input file: " + inFileName);

        std::stringstream buffer;
        buffer << inFile.rdbuf();
        std::string fileContent = buffer.str();

        std::list<BlockTuple> classBlocks;
        std::list<BlockTuple> inlineBlocks;
        std::string strippedfileContent;
        std::tie(classBlocks, inlineBlocks, strippedfileContent) = parseFile(fileContent);

        /* insert includes */
        outFile << "#include <typeinfo>" << std::endl;
        outFile << "#include <money/Meta/BlockParser.h>" << std::endl;

        /* put actual source into generated file */
        outFile << std::endl;
        outFile << fileContent;
        outFile << std::endl;

        /* for each block insert a BlockEntry */
        int blockCounter = 0;
        for(const auto & b : classBlocks) {
            std::cout << "Found Block: " << std::get<1>(b) << std::endl;
            std::stringstream a;
            a << "money_block_entry_" << blockCounter;

            outFile << money::meta::make_blockEntry(std::get<0>(b),
                                              "\""+std::get<1>(b)+"\"",
                                              a.str())
                    << ";" << std::endl;

            blockCounter++;
        }

        for(const auto & b : inlineBlocks) {
            std::cout << "Found Block: " << std::get<1>(b) << std::endl;
            std::stringstream a;
            a << "money_block_entry_" << blockCounter;

            outFile << money::meta::BLOCKENTRY_TYPE_NAME
                    << " "
                    << "money_block_entry_" << blockCounter
                    << "("
                    << "\""+std::get<0>(b)+"\""
                    << ", "
                    << "\""+std::get<1>(b)+"\""
                    << ");" << std::endl;

            blockCounter++;
        }


        outFile << money::meta::make_fileEntry("\""+money::meta::hash(inFileName)+"\"",
                                        "\"" +money::meta::hash(strippedfileContent)+"\"",
                                        "money_file_entry_"+money::meta::hash(inFileName))
                << ";" << std::endl;



}

void cmd_create_external(const ArgList & args) {

    /* we need exactly one output file */
    if (args.size() != 1) {
        throw std::runtime_error("Number of argument to command is not equal to 1");
    }

    std::string outFileName(args[0]);

    std::ofstream outFile;
    outFile.open(outFileName);

    if(!outFile.is_open())
        throw std::runtime_error("Could not open output file: " + outFileName);

    outFile << "#include <money/Meta/BlockParser.h>"
            << std::endl
            << std::endl;

    outFile << money::meta::make_fileEntry("\"external\"",
                                     money::meta::get_compileTimeHashExpression(),
                                     "money_file_entry_external")
            << ";" << std::endl;
}


int main(int argc, char ** argv) {


    /* register the possible commands */
    std::map<std::string, FunctionType> commands;
    commands["header"] = &cmd_create_header;
    commands["source"] = &cmd_create_source;
    commands["external"] = &cmd_create_external;

    try {

        if(argc < 3) {
            throw std::runtime_error("Too few arguments given.");
        }

        /* create the list of arguments without argv[0] and the command name */
        ArgList args;
        for(int i = 2; i < argc; i++ ) {
            args.push_back(std::string(argv[i]));
        }

        for(auto c : commands) {
            if(std::string("--")+c.first != std::string(argv[1]))
                continue;

            /* call the cmd function */
            c.second(args);
            return 0;
        }

        /* if we reach this point, obviously no such command has been found. */
        throw std::runtime_error("Command not found: " + std::string(argv[1]));

    } catch (std::exception & e) {
        std::cerr << "[ERROR] " << e.what() << std::endl;
        return -1;
    } catch (...) {
        std::cerr << "[ERROR] Something realy realy bad happended..." << std::endl;
        return -1;
    }

    return -1;
}
