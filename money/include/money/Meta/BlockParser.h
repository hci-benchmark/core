#ifndef INCLUDED_MONEY_BLOCKPARSER_H
#define INCLUDED_MONEY_BLOCKPARSER_H

#include <typeinfo>
#include <iostream>
#include <sstream>
#include <string>
#include <map>
#include <tuple>
#include <list>
#include <string>
#include <iomanip>
#include <sstream>
#include <regex>


#include <money/Controller.h>

namespace money {
namespace meta {

const std::string BLOCK_TYPE_NAME("money::Block");
const std::string BLOCKENTRY_TYPE_NAME("money::meta::BlockEntry");
const std::string FILEENTRY_TYPE_NAME("money::meta::BlockEntry");



inline std::string get_compileTimeHash() {
    return std::string(__DATE__) + "  " + std::string(__TIME__);
}

inline std::string get_compileTimeHashExpression() {
    return "std::string(__DATE__) + \"  \" + std::string(__TIME__)";
}

/**
 * @brief make_fileEntry returns a templated FileEntry
 * @param name name of the file
 * @param hash hash value of file
 * @param instance name of the instance
 *
 * @note name and hash won't be escaped in quotes.
 */
inline std::string make_fileEntry(const std::string & name,
                                  const std::string & hash,
                                  const std::string & instance = std::string())
{
    std::stringstream output;
    output << FILEENTRY_TYPE_NAME << " "
           << instance
           << "("
           << "money::meta::BlockEntry::Type::File"
           << ", "
           << name
           << ", "
           << hash
           << ")";


    return output.str();
}

/**
 * @brief make_blockEntry
 * @param typeName
 * @param hash
 * @param instance
 *
 * @see make_fileEntry
 */
inline std::string make_blockEntry(const std::string & typeName,
                                   const std::string & hash,
                                   const std::string & instance = std::string())
{
    std::stringstream output;
    output  << BLOCKENTRY_TYPE_NAME << " "
            << instance
            << "("
            << "typeid(" << typeName << "), "
            << "\"" << typeName << "\""
            << ", "
            <<  hash
            << ")";

    return output.str();

}


/**
 * @warning just copied from the internet, not reviewed.
 */
inline std::string hex_to_string(const std::string& in) {
    std::string output;

    if ((in.length() % 2) != 0) {
        throw std::runtime_error("Provided Hex Stream has odd length.");
    }

    size_t cnt = in.length() / 2;
    output.reserve(cnt);

    for (size_t i = 0; cnt > i; ++i) {
        uint32_t s = 0;
        std::stringstream ss;
        ss << std::hex << in.substr(i * 2, 2);
        ss >> s;

        output.push_back(static_cast<unsigned char>(s));
    }

    return output;
}

inline std::string string_to_hex(const std::string & in) {
    std::stringstream hex;
    hex << std::setw(2);
    for(unsigned i = 0; i < in.size(); i++) {
        hex << std::hex << static_cast<unsigned int>(in[i]);
    }

    return hex.str();
}

inline std::string hash(const std::string & str) {
    std::stringstream out;
    out << std::setw(2);
    out << std::hex << std::hash<std::string>()(str) << std::dec;
    return out.str();
}


/** parsed Block Entry that holds meta information about the status */
class BlockEntry {
public:
    enum class Type {
        Class, /** !< function block is declared as class */
        Inline, /** !< function block is declared inline with a lambda */
        File /** !< function block for files */
    };

private:
    Type m_type;
    const std::type_info & m_typeId;
    std::string m_name;
    std::string m_hash;

    static int countInliners() {
        static int counter = 0;
        return counter++;
    }

    void registerBlock() {

        if(m_type == Type::Class) {
            std::stringstream ss;
            ss << "class_" << m_name;
            money::Controller::registerBlock(ss.str(),
                                                       m_hash,
                                                       m_typeId.hash_code());
        } else if (m_type == Type::Inline){
            std::stringstream ss;
            ss << "inline_" << m_name;

            money::Controller::registerBlock(ss.str(), m_hash);
        } else {
            std::stringstream ss;
            ss << "file_" << m_name;
            money::Controller::registerBlock(ss.str(), m_hash);
        }
    }

public:

    BlockEntry(const std::type_info & id,
               const std::string & name,
               const std::string & hash)
        : m_type(Type::Class), m_typeId(id), m_name(name), m_hash(hash)
    {
        registerBlock();
    }

    BlockEntry(const std::string & name,
               const std::string & hash)
        : m_type(Type::Inline), m_typeId(typeid(void)),
          m_name(name), m_hash(hash)
    {
        registerBlock();
    }

    BlockEntry(const Type & type,
               const std::string & name,
               const std::string & hash)
        : m_type(type), m_typeId(typeid(void)),
          m_name(name), m_hash(hash)
    {
        registerBlock();
    }


    Type type() const {
        return m_type;
    }

    std::string name() const {
        return m_name;
    }

    std::string hash() const {
        return m_hash;
    }

    bool operator==(const BlockEntry & other) {
        return m_name == other.m_name;
    }

};
} // ns meta
} // ns money


#endif // INCLUDED_MONEY_BLOCKPARSER_H
