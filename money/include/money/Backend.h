#ifndef BACKEND_H
#define BACKEND_H


#include <typeinfo>
#include <typeindex>
#include <string>
#include <iostream>
#include <map>

#include <money/Base/Exceptions.h>
#include <money/Base/Memoizeable.h>


namespace money {


class Backend {


protected:
public:

    virtual ~Backend() {
    }
public:

    using DataType = Base::Type::UInt8;
    using DataTypePtr = std::unique_ptr<DataType[]>;

    struct Header {
        Base::TypeEnum type;
        Base::Shape shape;

        Header(const Base::TypeEnum & type, const Base::Shape & shape)
            : type(type), shape(shape) {}
    };

    virtual void readMemoizeable(const Base::Memoizeable & m,
                                 Base::TypeEnum & type,
                                 Base::Shape & shape,
                                 DataTypePtr & data) = 0;

    virtual Header readMemoizeableInfo(const Base::Memoizeable & m) = 0;

    virtual void writeMemoizeable(const Base::Memoizeable & m,
                                  const Base::TypeEnum & type,
                                  const Base::Shape & shape,
                                  const DataType * data) = 0;




    virtual std::vector<Base::BlockInfo> readBlockInfo() = 0;
    virtual void writeBlockInfo(const std::vector<Base::BlockInfo> & info) = 0;



    template<typename T>
    void readMemoizeable(const Base::Memoizeable & m,
                         Base::Shape & shape,
                         std::unique_ptr<T[]> & data)
    {
        DataTypePtr vdata;
        Base::TypeEnum type;
        readMemoizeable(m, type, shape, vdata);

        if(type != Base::getType<T>())
            throw Exceptions::BackendException("Read type is not equal to requested type.");

        data = std::unique_ptr<T[]>(reinterpret_cast<T*>(vdata.release()));
    }

    template<typename T>
    void writeMemoizeable(const Base::Memoizeable & m,
                         const Base::Shape & shape,
                         const T * data)
    {
        writeMemoizeable(m, Base::getType<T>(), shape, (DataType*)data);
    }




};


}


#endif // BACKEND_H

