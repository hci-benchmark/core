#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <regex>

#include <money/Backend.h>


namespace money {

    class Controller {


        /** @name memoization enviroment */
        /** @{ */

        /** list of currently used blocks in executable */
        std::map<std::string, std::string> m_blocks;

        /** mapping from block type (derived class) to a name */
        std::map<std::size_t, std::string> m_classBlockNames;

        std::map<std::string, std::string> m_savedBlocks;


        /** @} */


        Backend * m_backend;

        /** @name information and flags */
        /** @{ */
        std::string m_context;

        /** ignores changes to file */
        bool m_ignoreFiles;

        /** ignores changes to blocks */
        bool m_ignoreBlocks;

        /** recompute no matter what */
        bool m_recompute;

        /** @} */

        static Controller & controller() {
            static Controller c;
            return c;
        }

    public:


        Controller()
            : m_backend(nullptr),
              m_context("default"),
              m_ignoreFiles(false),
              m_ignoreBlocks(false),
              m_recompute(false)
        {
        }

        ~Controller() {

            /* if there's no backend, then we don't have to save anything */
            if(!m_backend)
                return;

            std::vector<money::Base::BlockInfo> blocks;
//            std::cout << "[MONEY] Serializing Blocks:" << std::endl;
            for(auto & b : m_blocks) {
//                std::cout << b.first << ",";
                blocks.push_back(std::make_tuple(b.first, b.second));
            }
//            std::cout << std::endl;

            m_backend->writeBlockInfo(blocks);

            delete m_backend;


        }

        static void registerBlock(const std::string & name,
                           const std::string & hash,
                           const std::size_t & index = typeid(void).hash_code())
        {

            Controller & c = controller();

            if(c.m_blocks.count(name))
                throw money::Exceptions::BackendException("Block already registered: " + name);

            /* check if we should register a class block */
            if(index != typeid(void).hash_code())
                c.m_classBlockNames[index] = name;

            c.m_blocks[name] = hash;

            std::cout << "[MONEY][BLOCK] " << name << std::endl;
        }

        static bool checkMemoChanged() {
            return controller().m_recompute;
        }

        static bool checkBlockChanged(const std::string & name) {

            Controller & c = controller();

            if(checkForFilesChanged())
                return true;

            /* if there is no entry for the block name in any of these
             * maps, then we consider our block as changed */
            if(!c.m_blocks.count(name) || !c.m_savedBlocks.count(name))
                return true;
            return !(c.m_blocks[name] == c.m_savedBlocks[name]) | c.m_recompute;
        }

        static bool checkBlockChanged(const std::size_t & index) {
            Controller & c = controller();
            if(!c.m_classBlockNames.count(index))
                return true;

            return checkBlockChanged(c.m_classBlockNames[index]);
        }

        static bool checkForFilesChanged() {
            Controller & c = controller();


            try {

                for(auto & b : c.m_savedBlocks) {

                    /* if the block is not of type file, omit the check */
                    if(b.first.find("file_") != 0)
                        continue;

                    /* check if each of the blocks in saved blocks can be found again
                     * in the registered block section */
                    if(!c.m_blocks.count(b.first))
                        throw true;

                    /* now check if each of the entries are equal */
                    if(c.m_blocks[b.first] != b.second) {
                        throw true;
                    }
                }

            }
            catch(bool & e)
            {
                /* when we are here, we usually would return true.
                 * however, it can be the case, that this flag is set,
                 * which should ignore any changes to files. */
                return !c.m_ignoreFiles;
            }
            catch (...)
            {
                /* whenever we are in this catch block, something went wrong.
                 * So assume things have changed. */
                return true;
            }

            /* normally we would return false here.
             * But we would like to recompute in any case,
             * when the flag is set. */
            return c.m_recompute;
        }


        static Backend & backend() {

            if(!controller().m_backend) {
                std::stringstream err;
                err << "No MONEY Backend has been set."
                    << "Have you called money::init()?"
                    << std::endl;

                throw std::runtime_error(err.str());
            }

            return *(controller().m_backend);
        }

        static void setBackend(Backend * backend) {

            try {

                if(!backend)
                    throw money::Exceptions::BackendException("No backend given.");

                controller().m_backend = backend;
                std::vector<money::Base::BlockInfo> blocks = backend->readBlockInfo();
                for(auto & b : blocks) {
                    controller().m_savedBlocks[std::get<0>(b)] = std::get<1>(b);
                }
            } catch (money::Exceptions::BackendException & e) {
                std::cout << "[MONEY][Controller] Loading block info:"
                          << e.what() << std::endl;
            }
        }

        static void setContext(const std::string & context) {
            if(context.empty())
                return;

            controller().m_context = context;
        }

        static std::string context() {
            return controller().m_context;
        }

        static void setFlags(const int & flags) {
            Controller & c = controller();
            if(flags & money::IgnoreExtern)
                c.m_ignoreFiles = true;
            if(flags & money::IgnoreBlocks)
                c.m_ignoreBlocks = true;
            if(flags & money::Recompute)
                c.m_recompute = true;
        }

    };



}


#endif // CONTROLLER_H

