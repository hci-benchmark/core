#ifndef MONEY_BACKEND_SIMPLE_H
#define MONEY_BACKEND_SIMPLE_H

#include <iostream>
#include <fstream>
#include <regex>
#include <algorithm>

#include <money/Base/Exceptions.h>
#include <money/Backend.h>
#include <money/Controller.h>

namespace money {



class SimpleBackend : public money::Backend {



public:

    /** @todo expand with MONEY context */
    std::string memoizeableFileName(const money::Base::Memoizeable & m) {
        const std::string context = money::Controller::context();
        std::string name = m.name();
        std::replace(name.begin(), name.end(), '/', '_');
        std::replace(name.begin(), name.end(), '\\', '_');


        return context+"_money_"+name+".bin";
    }

    /** @todo expand with MONEY context */
    std::string blocksFileName() {
        const std::string context = money::Controller::context();
        return context+"_money_blocks.bin";
    }



    void writeMemoizeable(const money::Base::Memoizeable &m,
                          const money::Base::TypeEnum &type,
                          const money::Base::Shape &shape,
                          const DataType *data) {

        const std::string fileName = this->memoizeableFileName(m);

        std::ofstream out;
        out.open(fileName, std::ofstream::binary);
        if(!out.is_open())
            return;

//        std::cout << "Writing out... " << std::endl;

        /* magic string */
        out.write("\xAA\x00\xAA\xFF", 4);

        /* write variable data type */
        out.write((char*)&type, sizeof(type));

        unsigned size = shape.size();
        /* write shape size */
        out.write((char*)&size, sizeof(size));

        unsigned long sum = 1;
        /* write out each size of dimension */
        for(unsigned i = 0; i < shape.size(); i++) {
            unsigned dim = shape[i];
            out.write((char*)&dim, sizeof(dim));
            sum *= shape[i];
        }

//        std::cout << std::dec;
//        std::cout << "Writing data of length: " << sum << ", "
//                  << sum*money::Base::getTypeSize(type) << std::endl;

        out.write((const char*)(data), sum*money::Base::getTypeSize(type));

        std::cout << "[MONEY-MEMO]["<<m.name()<<"] Writing "
                  << shape.size() << " dimensions."
                  << std::endl;


    }

    money::Backend::Header readMemoizeableInfo(const money::Base::Memoizeable & m) {

        const std::string fileName = this->memoizeableFileName(m);

        std::ifstream in;
        in.open(fileName, std::ofstream::binary);

        if(!in.is_open())
            throw money::Exceptions::BackendException("Could not find memoizeable");


        char header[4];
        unsigned size;
        unsigned typeVal;
        unsigned dims;

        in.read(header, 4);

        if(header[0] != char(0xAA)
           || header[1] != char(0x00)
           || header[2] != char(0xAA)
           || header[3] != char(0xFF))
            throw money::Exceptions::BackendException("memo file's header is corrupted.");


        in.read((char*)&typeVal, sizeof(money::Base::TypeEnum));
//        std::cout << "type" << typeVal << int(money::Base::TypeEnum::Int32) << std::endl;

        in.read((char*)&dims, sizeof(size));
//        std::cout << "must read number of dimensions: " << dims << std::endl;

        money::Base::Shape shape;
        shape.resize(dims);

        for(unsigned i = 0; i < dims; i++) {
            in.read((char*)&shape[i], sizeof(shape[i]));
        }

        return money::Backend::Header(money::Base::TypeEnum(typeVal), shape);
    }

    void readMemoizeable(const money::Base::Memoizeable &m,
                         money::Base::TypeEnum &type,
                         money::Base::Shape &shape,
                         DataTypePtr & data) {


        const std::string fileName = this->memoizeableFileName(m);

        std::ifstream in;
        in.open(fileName, std::ofstream::binary);

        if(!in.is_open())
            throw money::Exceptions::BackendException("Could not find memoizeable");


        char header[4];
        unsigned size;
        unsigned typeVal;
        unsigned dims;

        in.read(header, 4);

        if(header[0] != char(0xAA)
           || header[1] != char(0x00)
           || header[2] != char(0xAA)
           || header[3] != char(0xFF))
            throw money::Exceptions::BackendException("memo file's header is corrupted.");


        in.read((char*)&typeVal, sizeof(type));
//        std::cout << "type" << typeVal << int(money::Base::TypeEnum::Int32) << std::endl;

        type = money::Base::TypeEnum(typeVal);

        in.read((char*)&dims, sizeof(size));
//        std::cout << "must read number of dimensions: " << dims << std::endl;

        shape.resize(dims);

        unsigned sum = 1;
//        std::cout << "shape: " << std::endl;
        for(unsigned i = 0; i < dims; i++) {
            in.read((char*)&shape[i], sizeof(shape[i]));
//            std::cout << shape[i] << ",";
            sum *= shape[i];
        }
//        std::cout << std::endl;

        unsigned dataSize = sum*unsigned(money::Base::getTypeSize(type));

//        std::cout << std::dec;
//        std::cout << "sum: " << sum << ", "
//                  << "data size: " << money::Base::getTypeSize(type) << ", "
//                  << "type : " << unsigned(type) << std::endl;
//        std::cout << "reading: " << dataSize << std::endl;

        data = DataTypePtr(new DataType[dataSize]);


        in.read((char*)data.get(), dataSize);
        if(in.fail())
            throw money::Exceptions::BackendException("File is too short.");

        std::cout << "[MONEY-MEMO]["<<m.name()<<"] Reading "
                  << shape.size() << " dimensions."
                  << std::endl;

    }

    std::vector<money::Base::BlockInfo> readBlockInfo() {



        /** @todo extend by context name */
        const std::string fileName = this->blocksFileName();

        std::ifstream in;
        in.open(fileName);

        if(!in.is_open())
            throw money::Exceptions::BackendException("Input file could not be found.");

        std::vector<money::Base::BlockInfo> blocks;

        /* defines a regexp for a header line.
         * complicated macros won't here, of course */
        std::regex lineEntry("([a-zA-Z0-9_]*);([a-zA-Z0-9 \\:]*)");



        std::string linefeed;
        while(!std::getline(in, linefeed).fail()) {
            std::smatch m;

            if(!std::regex_search(linefeed, m, lineEntry))
                continue;

//            std::cout << "found viable line: " << m[1] << ", '" << m[2] << "'" << std::endl;

            blocks.push_back(std::make_tuple(m[1].str(), m[2].str()));
        }

        return blocks;

    }

    void writeBlockInfo(const std::vector<money::Base::BlockInfo> &info) {
        const std::string fileName = this->blocksFileName();

        std::ofstream out;
        out.open(fileName);

        if(!out.is_open())
            return;

        for(auto & b : info) {
            //std::cout << "writing a block: " << std::get<0>(b) << ";" << std::get<1>(b) << std::endl;
            out << std::get<0>(b) << ";" << std::get<1>(b) << std::endl;
        }

    }
};

}

#endif // MONEY_BACKEND_SIMPLE_H

