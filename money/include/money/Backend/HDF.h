#ifndef MONEY_BACKEND_HDF_H
#define MONEY_BACKEND_HDF_H

#include <iostream>
#include <fstream>
#include <regex>
#include <memory>

#include <hdf5.h>
#include <hdf5_hl.h>

#include <money/Base/Exceptions.h>
#include <money/Backend.h>
#include <money/Controller.h>



namespace money {



class HDFBackend : public money::Backend {



    hid_t m_file;

public:

    static hid_t hdfToNative(const hid_t &type) {
        if(H5Tget_class(type) == H5T_INTEGER) {
            if(H5Tget_sign(type) == H5T_SGN_2) {
                if(H5Tget_size(type) == 1)
                    return H5T_NATIVE_CHAR;
                if(H5Tget_size(type) == 4)
                    return H5T_NATIVE_INT32;
                if(H5Tget_size(type) == 8)
                    return H5T_NATIVE_INT64;
            } else {
                if(H5Tget_size(type) == 1)
                    return H5T_NATIVE_UCHAR;
                if(H5Tget_size(type) == 4)
                    return H5T_NATIVE_UINT32;
                if(H5Tget_size(type) == 8)
                    return H5T_NATIVE_UINT64;
            }
        }

        if(H5Tget_class(type) == H5T_FLOAT) {
            if(H5Tget_size(type) == 4)
                return H5T_NATIVE_FLOAT;
            if(H5Tget_size(type) == 8)
                return H5T_NATIVE_DOUBLE;
        }

        throw Exceptions::BackendException("HDFBackend::hdfToNative() type not supported.");
    }

    static money::Base::TypeEnum moneyType(const hid_t &type)
    {
        using money::Base::TypeEnum;

        if(type == H5T_NATIVE_UCHAR)  return TypeEnum::UInt8;
        if(type == H5T_NATIVE_CHAR)   return TypeEnum::Int8;
        if(type == H5T_NATIVE_UINT32) return TypeEnum::UInt32;
        if(type == H5T_NATIVE_INT32 ) return TypeEnum::Int32;
        if(type == H5T_NATIVE_UINT64) return TypeEnum::UInt64;
        if(type == H5T_NATIVE_INT64 ) return TypeEnum::Int64;
        if(type == H5T_NATIVE_FLOAT ) return TypeEnum::Float;
        if(type == H5T_NATIVE_DOUBLE) return TypeEnum::Double;

        throw Exceptions::BackendException("HDFBackend::hdfType() type not supported.");
    }

    static hid_t hdfType(const money::Base::TypeEnum & type)
    {
        using money::Base::TypeEnum;
        switch(type) {
        case TypeEnum::UInt8:  return H5T_NATIVE_UCHAR;
        case TypeEnum::Int8:   return H5T_NATIVE_CHAR;
        case TypeEnum::UInt32: return H5T_NATIVE_UINT32;
        case TypeEnum::Int32:  return H5T_NATIVE_INT32;
        case TypeEnum::UInt64: return H5T_NATIVE_UINT64;
        case TypeEnum::Int64:  return H5T_NATIVE_INT64;
        case TypeEnum::Float:  return H5T_NATIVE_FLOAT;
        case TypeEnum::Double: return H5T_NATIVE_DOUBLE;
        default:
            throw Exceptions::FrontendException("HDFBackend::moneyType() type not supported.");
        }
    }

    HDFBackend()
        : m_file(-1)
    {
        std::stringstream fileName;
        fileName << money::Controller::context()
                 << ".h5";

        m_file = H5Fopen(fileName.str().c_str(), H5F_ACC_RDWR, H5P_DEFAULT);
        if(m_file < 0) {
            m_file = H5Fcreate(fileName.str().c_str(), H5F_ACC_EXCL,H5P_DEFAULT, H5P_DEFAULT);
        }

        H5Fflush(m_file, H5F_SCOPE_GLOBAL);
        if(m_file < 0) {
            throw money::Exceptions::BackendException("[MONEY] HDF Backend could not create or open file.");
        }

    }

    ~HDFBackend() {
        std::cout << "~HDFBACKEND:" << m_file << std::endl;
//        /* this makes troube for some reason ... */
//        if(m_file > 0) {
//            H5Fclose(m_file);
//        }
    }

    void createGroups(const std::string & path) {

        if(m_file < 0) {
            throw money::Exceptions::BackendException("HDF file is not open.");
        }


        std::stringstream ss(path);
        std::string item;
        std::vector<std::string> tokens;
        while (std::getline(ss, item, '/')) {
            if(item.empty())
                continue;
            tokens.push_back(item);
        }

        std::string tmpPath;
        for(auto it = tokens.begin(); it != tokens.end()-1; it++) {
            tmpPath += "/"+*it;
            if(H5Lexists(m_file, tmpPath.c_str(), H5P_DEFAULT)) {
                continue;
            }
            H5Gcreate(m_file, tmpPath.c_str(), H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);

        }
    }



    void writeMemoizeable(const money::Base::Memoizeable &m,
                          const money::Base::TypeEnum &type,
                          const money::Base::Shape &shape,
                          const DataType *data) {



        if(!m.changed())
            return;


        std::string path = m.name();
	if(path.front() != '/')
		path.insert(0,"/");

        if(m_file < 0) {
            throw money::Exceptions::BackendException("HDF file is not open.");
        }

        if(H5LTpath_valid(m_file, path.c_str(), false)) {
            H5Gunlink(m_file, path.c_str());
        }

        auto dims = std::unique_ptr<hsize_t[]>(new hsize_t[shape.size()]);

        for(int i = 0; i < shape.size(); i++) {
            dims.get()[i] = shape[i];
        }

        createGroups(m.name());
        H5LTmake_dataset(m_file, path.c_str(), shape.size(), dims.get(), hdfType(type), data);

        H5Fflush(m_file, H5F_SCOPE_GLOBAL);


        std::cout << "[MONEY-MEMO]["<<m.name()<<"] Writing "
                  << shape.size() << " dimensions."
                  << std::endl;


    }

    void readMemoizeable(const money::Base::Memoizeable &m,
                         money::Base::TypeEnum &type,
                         money::Base::Shape &shape,
                         DataTypePtr & data) {

        if(m_file < 0) {
            throw money::Exceptions::BackendException("File is not open...");
        }

        std::string path = m.name();
        if(path.front() != '/') {
            path.insert(0,"/");
        }

        if(!H5LTpath_valid(m_file, path.c_str(), true)) {
            throw money::Exceptions::BackendException("Dataset does not exist.");
        }

        hid_t dataset = H5Dopen(m_file, path.c_str(), H5P_DEFAULT);
        hid_t dsetType = H5Dget_type(dataset);

        int rank = 0;
        H5LTget_dataset_ndims(m_file, path.c_str(), &rank);

        std::unique_ptr<hsize_t[]> dims(new hsize_t[rank]);
        size_t type_size;

        type = moneyType(hdfToNative(dsetType));

        H5LTget_dataset_info(m_file, path.c_str(), dims.get(),
                             nullptr, &type_size);

        shape.resize(rank);
        for(int i = 0; i < rank; i++) {
            shape[i] = dims.get()[i];
        }

        int numElements = 1;
        for(auto s: shape) numElements *= s;

        data = DataTypePtr(new DataType[numElements*type_size]);
        H5LTread_dataset(m_file, path.c_str(), hdfToNative(dsetType), data.get());

        H5Tclose(dsetType);
        H5Dclose(dataset);


    }

    money::Backend::Header readMemoizeableInfo(const money::Base::Memoizeable & m) {
        if(m_file < 0) {
            throw money::Exceptions::BackendException("File is not open...");
        }

        std::string path = m.name();
        if(path.front() != '/') {
            path.insert(0,"/");
        }

        if(!H5LTpath_valid(m_file, path.c_str(), true)) {
            throw money::Exceptions::BackendException("Dataset does not exist.");
        }

        hid_t dataset = H5Dopen(m_file, path.c_str(), H5P_DEFAULT);
        hid_t dsetType = H5Dget_type(dataset);

        int rank = 0;
        H5LTget_dataset_ndims(m_file, path.c_str(), &rank);

        std::unique_ptr<hsize_t[]> dims(new hsize_t[rank]);
        size_t type_size;

        auto type = moneyType(hdfToNative(dsetType));

        H5LTget_dataset_info(m_file, path.c_str(), dims.get(),
                             nullptr, &type_size);

        money::Base::Shape shape;
        shape.resize(rank);
        for(int i = 0; i < rank; i++) {
            shape[i] = dims.get()[i];
        }

        int numElements = 1;
        for(auto s: shape) numElements *= s;


        H5Tclose(dsetType);
        H5Dclose(dataset);

        return money::Backend::Header(type, shape);
    }

    std::vector<money::Base::BlockInfo> readBlockInfo() {

        /** @todo implement me */
        std::vector<money::Base::BlockInfo> blocks;
        return blocks;
    }

    void writeBlockInfo(const std::vector<money::Base::BlockInfo> &info) {
        /** @todo implement me */
    }
};

}

#endif // MONEY_BACKEND_HDF_H

