#ifndef MONEY_H
#define MONEY_H

#include <money/Controller.h>
#include <money/Memoizeable.h>
#include <money/Block.h>
#include <money/Backend/Simple.h>

namespace money {


/** sets the context of this application.
 *  this is some kind of a project name etc.. */
inline void setContext(const std::string & context) {
    Controller::setContext(context);
}

inline void setBackend(money::Backend * backend) {
    Controller::setBackend(backend);
}

/**
 * @brief init
 * @param context context name of money blocks
 * @param flags defines change detection
 *
 * Call this function to setup money blocks
 */
inline void init(const std::string & context = std::string(),
                 const int & flags = money::NoFlags)
{
    if(!context.empty())
        Controller::setContext(context);
    Controller::setFlags(flags);

    Controller::setBackend(new money::SimpleBackend());
}

/**
 * @brief init with command line arguments.
 *
 * It takes the executeables name as context
 * and parses for flags:
 *  -if : ignore files
 *  -ib : ignore blocks
 *  -i  : ignore all
 *  -r  : recompute in any case
 *
 * @overload
 */
inline void init(int argc = 0, char ** argv = 0) {

    if(argc < 1 )
        return;

    std::string context;

    /* we have a application name provided */
    if(argv[0] != 0) {
        std::string s(argv[0]);
        std::regex fileName(".*[\\\\|\\/](.*)");
        std::smatch m;
        if(std::regex_search(s, m, fileName)) {
            context = m[1].str();
        }
    }

    int flags = money::NoFlags;

    /* walk through list of arguments and check for flags */
    for(int i = 1; i < argc; i++) {
        std::string arg(argv[i]);

        if(arg == "-if")
            flags = flags | money::IgnoreExtern;
        if(arg == "-ib")
            flags = flags | money::IgnoreExtern;
        if(arg == "-i")
            flags = flags | money::IgnoreAll;
        if(arg == "-r")
            flags |= money::Recompute;
    }

    money::init(context, flags);

}


}

#endif // MONEY_H

