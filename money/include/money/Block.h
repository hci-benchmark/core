#ifndef BLOCK_H
#define BLOCK_H


#include <typeinfo>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <functional>
#include <utility>

#include <money/Memoizeable.h>
#include <money/Controller.h>

namespace money {


    /** @brief block of code that will only be executed on demand */
    class Block {


    public:
        typedef std::function<void(void)> BlockFunction;

        enum class Type {
            Class,
            Inline
        };

    private:

        static int countInliners() {
            static int counter = 0;
            return counter++;
        }

        Type m_type;
        int m_inlineId;

    public:

        Block() : m_type(Type::Class), m_inlineId(-1) {}

        virtual ~Block() {}

        bool changed() const {
            try {
                if(m_type == Type::Class) {
                    return money::Controller::checkBlockChanged(typeid(*this).hash_code());
                } else {
                    std::stringstream ss;
                    ss << "inline_"
                       << std::setfill('0') << std::setw(3)
                       << m_inlineId;

                    return money::Controller::checkBlockChanged(ss.str());
                }
            } catch (...) {}

            return true;
        }


        /** @name Inline Blocks
         *
         * yes.. one time this could be done with variadic templates
         * but for now, it is just fine.
         *
         * @{ */

        Block(BlockFunction f)
            : m_type(Type::Inline),
              m_inlineId(countInliners())
        {
            if(changed())
                f();
        }

        template<typename T1>
        Block(const Memoizeable<T1> & mem1, BlockFunction f)
            : m_type(Type::Inline), m_inlineId(countInliners())
        {
            if(changed() || mem1.changed())
                f();
        }

        template<typename T1, typename T2>
        Block(const Memoizeable<T1> & mem1,
              const Memoizeable<T2> & mem2,
              BlockFunction f)
            : m_type(Type::Inline),
              m_inlineId(countInliners())
        {
            if(changed() || mem1.changed() || mem2.changed())
                f();
        }

        template<typename T1, typename T2, typename T3>
        Block(const Memoizeable<T1> & mem1,
              const Memoizeable<T2> & mem2,
              const Memoizeable<T3> & mem3,
              BlockFunction f)
            : m_type(Type::Inline), m_inlineId(countInliners())
        {
            if(changed()
               || mem1.changed()
               || mem2.changed()
               || mem3.changed())
            {
                f();
            }
        }

        /** @} */


    };

}


#endif // BLOCK_H

