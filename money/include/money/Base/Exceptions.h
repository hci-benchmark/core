#ifndef MONEY_BASE_EXCEPTIONS_H
#define MONEY_BASE_EXCEPTIONS_H

#include <string>
#include <exception>
#include <stdexcept>

namespace money {

    namespace Exceptions {
        class FrontendException : public std::runtime_error {
        public:
            FrontendException(const std::string & errString)
                : std::runtime_error(errString) {}
        };

        class BackendException : public std::runtime_error {
        public:
            BackendException(const std::string & errString)
                : std::runtime_error(errString) {}
        };
    }
}

#endif

