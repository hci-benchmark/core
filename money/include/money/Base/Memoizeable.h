#ifndef MONEY_BASE_MEMOIZEABLE_H
#define MONEY_BASE_MEMOIZEABLE_H

#include <exception>
#include <stdexcept>
#include <cstdint>
#include <string>
#include <vector>
#include <iostream>
#include <typeinfo>


namespace money {

enum Flags {
    NoFlags = 0,
    IgnoreExtern = 1,
    IgnoreBlocks = 2,
    Recompute = 4,
    IgnoreAll = IgnoreExtern | IgnoreBlocks,
};


template<typename T> class Frontent;

namespace Base {

    namespace Type {
        typedef uint8_t UInt8;
        typedef int8_t Int8;
        typedef uint32_t UInt32;
        typedef int32_t Int32;
        typedef uint64_t UInt64;
        typedef int64_t Int64;
        typedef double Double;
        typedef float Float;
    }

    enum class TypeEnum {
        UInt8,
        Int8,
        UInt32,
        Int32,
        UInt64,
        Int64,
        Double,
        Float
    };


    template<typename T>
    struct TypeTrait {
        static TypeEnum getType();
    };

    /** @name Returns the Type Enum value for given type as template */
    /** @{ */
    template<typename T> inline
    TypeEnum getType() { return TypeTrait<T>::getType(); }

    template<> inline
    TypeEnum getType<unsigned char>() {
        return TypeEnum::UInt8;
    }

    template<> inline
    TypeEnum getType<char>() {
        return TypeEnum::Int8;
    }

//    template<> inline
//    TypeEnum getType<Type::UInt8>() {
//        return TypeEnum::UInt8;
//    }

//    template<> inline
//    TypeEnum getType<Type::Int8>() {
//        return TypeEnum::Int8;
//    }

    template<> inline
    TypeEnum getType<Type::UInt32>() {
        return TypeEnum::UInt32;
    }

    template<> inline
    TypeEnum getType<Type::Int32>() {
        return TypeEnum::Int32;
    }

    template<> inline
    TypeEnum getType<Type::UInt64>() {
        return TypeEnum::UInt64;
    }

    template<> inline
    TypeEnum getType<Type::Int64>() {
        return TypeEnum::Int64;
    }

    template<> inline
    TypeEnum getType<Type::Float>() {
        return TypeEnum::Float;
    }

    template<> inline
    TypeEnum getType<Type::Double>() {
        return TypeEnum::Double;
    }
    /** @} */

    inline int getTypeSize(const  TypeEnum & type) {
        switch(type) {
        case TypeEnum::UInt8: return sizeof(Type::UInt8);
        case TypeEnum::Int8: return sizeof(Type::Int8);
        case TypeEnum::UInt32: return sizeof(Type::UInt32);
        case TypeEnum::Int32 : return sizeof(Type::Int32);
        case TypeEnum::UInt64: return sizeof(Type::UInt64);
        case TypeEnum::Int64 : return sizeof(Type::Int64);
        case TypeEnum::Float: return sizeof(Type::Float);
        case TypeEnum::Double: return sizeof(Type::Double);
        default: throw std::runtime_error("getTypeSize(): Type not supported.");
        }
        return 0;
    }



    typedef std::tuple<std::string, std::string> BlockInfo;
    typedef std::tuple<std::string, std::string> FileInfo;

    typedef std::vector<unsigned> Shape;


    /** Base Memoizeable */
    class Memoizeable {

        template<typename C> friend class Frontend;

        std::string m_name;
        bool m_changed;

    protected:
        void setUnchanged() {
            m_changed = false;
        }

        void setChanged() {
            m_changed = true;
        }

    public:
        Memoizeable(const std::string & name = std::string())
            : m_name(name),
              m_changed(true)
        {

        }

        bool changed() const {
            return m_changed;
        }

        std::string name() const {
            return m_name;
        }

    };

    template<typename T>
    class Frontend {
    public:
        static void store(const Base::Memoizeable & m,
                          const T & d)
        {
            std::cerr << "[MONEY][WARNING] No Frontend for "
                      << m.name() << " defined. TypeId is" << typeid(d).name() << std::endl;

        }

        static void fetch(Base::Memoizeable & m,
                          T & d)
        {
            std::cerr << "[MONEY][WARNING] No Frontend for "
                      << m.name() << " defined. TypeId is" << typeid(d).name() << std::endl;

        }
    };


}

}

#endif
