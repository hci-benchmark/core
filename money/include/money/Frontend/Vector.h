#ifndef MONEY_FRONTEND_STDVECTOR_H
#define MONEY_FRONTEND_STDVECTOR_H

#include <money/Backend.h>
#include <money/Controller.h>
#include <vector>

namespace money {
namespace Base {

template<typename T>
class Frontend<std::vector<T> > {
public:
    static void store(const Base::Memoizeable &m,
                      const std::vector<T> & d) {

        money::Base::Shape shape;
        shape.resize(1);
        shape[0] = d.size();
        Controller::backend().writeMemoizeable(m, shape,
                                               d.data());

    }

    static void fetch(Base::Memoizeable &m,
                      std::vector<T> & d) {
        Base::Shape shape;

        std::unique_ptr<T[]> data;

        try {
            Controller::backend().readMemoizeable(m, shape, data);

            if(shape.size() != 1)
                throw money::Exceptions::FrontendException("Dimension is wrong.");

            d.resize(shape[0]);

            for(unsigned i = 0; i < shape[0]; i++) {
                d[i] = data[i];
            }

            m.setUnchanged();

        } catch (money::Exceptions::BackendException & e) {
            std::cout << "[MONEY] Fetching for Memo " << m.name() << " failed: "
                      << e.what() << std::endl;
        }

    }

};
}
}

#endif // MONEY_FRONTEND_STDVECTOR_H

