#ifndef MONEY_FRONTEND_VIGRA_H
#define MONEY_FRONTEND_VIGRA_H

#include <money/Base/Memoizeable.h>
#include <money/Memoizeable.h>
#include <money/Backend.h>
#include <money/Controller.h>

#include <vector>
#include <vigra/tinyvector.hxx>
#include <vigra/multi_array.hxx>

namespace money {
namespace Base {

template<typename T, int N>
struct TypeTrait<vigra::TinyVector<T,N>> {
    static Base::TypeEnum getType() {
        return Base::getType<T>();
    }
};

template<typename T, int N>
class Frontend<vigra::TinyVector<T,N>> {

public:
    static void store(const Base::Memoizeable &m,
                      const vigra::TinyVector<T,N> & d) {

        money::Base::Shape shape;
        shape.resize(1);
        shape[0] = N;
        Controller::backend().writeMemoizeable(m, shape,
                                               (T*)d.data());

    }

    static void fetch(Base::Memoizeable &m,
                      vigra::TinyVector<T,N> & d) {
        Base::Shape shape;
        std::unique_ptr<T[]> data;
        try {
            Controller::backend().readMemoizeable(m, shape, data);

            if(shape.size() != 1)
                throw money::Exceptions::FrontendException("Dimension is wrong.");

            if(shape[0] != N)
                throw money::Exceptions::FrontendException("Shape in first dimension does not match.");

            /* copy values from data backend to provided vector */
            d = vigra::TinyVectorView<T,N>(data.get());

            m.setUnchanged();

        } catch (money::Exceptions::BackendException & e) {
            std::cout << "[MONEY] Fetching for Memo " << m.name() << " failed: "
                      << e.what() << std::endl;
        }

    }

};

template<typename T, int N>
class Frontend<std::vector<vigra::TinyVector<T,N>>> {

public:
    static void store(const Base::Memoizeable &m,
                      const std::vector<vigra::TinyVector<T,N>> & d) {

        money::Base::Shape shape;
        shape.resize(2);
        shape[0] = d.size();
        shape[1] = N;
        Controller::backend().writeMemoizeable(m, shape,
                                               reinterpret_cast<const T*>(d.data()));

    }

    static void fetch(Base::Memoizeable &m,
                      std::vector<vigra::TinyVector<T,N>> & d) {
        Base::Shape shape;
        std::unique_ptr<T[]> data;
        try {
            Controller::backend().readMemoizeable(m, shape, data);

            if(shape.size() != 2)
                throw money::Exceptions::FrontendException("Dimension is wrong.");

            if(shape[1] != N)
                throw money::Exceptions::FrontendException("Shape in 2nd dimension does not match.");

            d.resize(shape[0]);

            for(unsigned i = 0; i < shape[0]; i++) {
                d[i] = vigra::TinyVectorView<T,N>(data.get() + i*N);
            }

            m.setUnchanged();

        } catch (money::Exceptions::BackendException & e) {
            std::cout << "[MONEY] Fetching for Memo " << m.name() << " failed: "
                      << e.what() << std::endl;
        }

    }

};

template<unsigned int N, typename T>
class Frontend<vigra::MultiArrayView<N,T>> {

public:
    static void store(const Base::Memoizeable &m,
                      const vigra::MultiArrayView<N,T> d) {

        money::Base::Shape shape;
        shape.resize(d.shape().size());
        for(unsigned i = 0; i < shape.size(); i++) {
            shape[i] = d.shape()[shape.size()-i-1];
        }
        Controller::backend().writeMemoizeable(m, shape,
                                               (T*)d.data());

    }

    static void fetch(Base::Memoizeable &m,
                      vigra::MultiArrayView<N,T> d) {
        Base::Shape shape;
        std::unique_ptr<T[]>data;

        try {
            Controller::backend().readMemoizeable(m, shape, data);

            if(shape.size() != d.shape().size())
                throw money::Exceptions::FrontendException("Dimension is wrong.");

            auto arrayShape = d.shape();

            for(unsigned i = 0; i < shape.size(); i++) {
                if(arrayShape[shape.size()-1-i] != shape[i]) {
                    throw money::Exceptions::FrontendException("Shape mismatch.");
                }
            }

            /* copy the data provided from the backend */
            d = vigra::MultiArrayView<N,T>(arrayShape, data.get());

            m.setUnchanged();

        } catch (money::Exceptions::BackendException & e) {
            std::cout << "[MONEY] Fetching for Memo " << m.name() << " failed: "
                      << e.what() << std::endl;
        }

    }

};

template<unsigned int N, typename T>
class Frontend<vigra::MultiArray<N,T>> {

public:
    static void store(const Base::Memoizeable &m,
                      const vigra::MultiArray<N,T> & d) {

        /* simply forward the call */
        Frontend<vigra::MultiArrayView<N,T>>::store(m, d);
    }

    static void fetch(Base::Memoizeable &m,
                      vigra::MultiArray<N,T> & d) {

        try {
            Backend::Header header = Controller::backend().readMemoizeableInfo(m);


        /* if the provided multi array is not yet initialized,
         * read the shape information and resize it accordingly
         * to the stored array */
            if(!d.data()) {
                auto arrayShape = d.shape();
                for(unsigned i = 0; i < header.shape.size(); i++) {
                    arrayShape[header.shape.size()-1-i] = header.shape[i];
                }

                d.reshape(arrayShape);
            }

            Frontend<vigra::MultiArrayView<N,T>>::fetch(m, d);
        } catch (money::Exceptions::BackendException & e) {
            std::cout << "[MONEY] Fetching for Memo " << m.name() << " failed: "
                      << e.what() << std::endl;
        }

    }

};

template<unsigned int N1, int N2, typename T>
class Frontend<vigra::MultiArray<N1,vigra::TinyVector<T, N2>>> {

    typedef vigra::TinyVector<T, N2> VectorType;

public:
    static void store(const Base::Memoizeable &m,
                      const vigra::MultiArray<N1,VectorType> & d) {

        /* simply forward the call */
        Frontend<vigra::MultiArrayView<N1+1,T>>::store(m, d.expandElements(0));
    }

    static void fetch(Base::Memoizeable &m,
                      vigra::MultiArray<N1, VectorType> & d) {

        try {
            Backend::Header header = Controller::backend().readMemoizeableInfo(m);


            /* if the provided multi array is not yet initialized,
         * read the shape information and resize it accordingly
         * to the stored array */
            if(!d.data()) {
                auto arrayShape = d.shape();
                for(unsigned i = 0; i < header.shape.size()-1; i++) {
                    arrayShape[header.shape.size()-1-i] = header.shape[i];
                }

                d.reshape(arrayShape);
            }

            Frontend<vigra::MultiArrayView<N1+1,T>>::fetch(m, d.expandElements(0));
        } catch (money::Exceptions::BackendException & e) {
            std::cout << "[MONEY] Fetching for Memo " << m.name() << " failed: "
                      << e.what() << std::endl;
        }

    }

};

}
}

#endif // MONEY_FRONTEND_VIGRA_H

