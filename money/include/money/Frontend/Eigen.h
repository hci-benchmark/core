#ifndef MONEY_FRONTEND_EIGEN_H
#define MONEY_FRONTEND_EIGEN_H

#include <money/Base/Memoizeable.h>
#include <money/Memoizeable.h>
#include <money/Backend.h>
#include <money/Controller.h>

#include <vector>
#include <Eigen/Dense>

namespace money {
namespace Base {

template<typename T, int ROWS, int COLS>
struct TypeTrait<Eigen::Matrix<T,ROWS,COLS>> {
    static Base::TypeEnum getType() {
        return Base::getType<T>();
    }
};

template<typename T, int ROWS, int COLS>
class Frontend<Eigen::Matrix<T,ROWS,COLS>> {

public:
    static void store(const Base::Memoizeable &m,
                      const Eigen::Matrix<T,ROWS,COLS> & d) {

        static_assert(ROWS != Eigen::Dynamic);
        static_assert(COLS != Eigen::Dynamic);

        money::Base::Shape shape;
        shape.resize(2);
        shape[0] = ROWS;
        shape[1] = COLS;
        Controller::backend().writeMemoizeable(m, shape,
                                               d.data());

    }

    static void fetch(Base::Memoizeable &m,
                      Eigen::Matrix<T,ROWS,COLS> & d) {

        static_assert(ROWS != Eigen::Dynamic);
        static_assert(COLS != Eigen::Dynamic);

        Base::Shape shape;
        std::unique_ptr<T[]> data;
        try {
            Controller::backend().readMemoizeable(m, shape, data);

            if(shape.size() != 2)
                throw money::Exceptions::FrontendException("Dimension is wrong.");

            if(shape[0] != ROWS || shape[1] != COLS)
                throw money::Exceptions::FrontendException("Shape does not match.");

            /* copy values from data backend to provided vector */
            d = Eigen::Map<Eigen::Matrix<T,ROWS,COLS>>(data.get());

            m.setUnchanged();

        } catch (money::Exceptions::BackendException & e) {
            std::cout << "[MONEY] Fetching for Memo " << m.name() << " failed: "
                      << e.what() << std::endl;
        }

    }

};

template<typename T, int ROWS, int COLS>
class Frontend<std::vector<Eigen::Matrix<T,ROWS,COLS>>> {

public:
    static void store(const Base::Memoizeable &m,
                      const std::vector<Eigen::Matrix<T,ROWS,COLS>> & d) {

        static_assert(ROWS != Eigen::Dynamic);
        static_assert(COLS != Eigen::Dynamic);

        money::Base::Shape shape;
        shape.resize(3);
        shape[0] = d.size();
        shape[1] = ROWS;
        shape[2] = COLS;

        Controller::backend().writeMemoizeable(m, shape,
                                               d.data());

    }

    static void fetch(Base::Memoizeable &m,
                      std::vector<Eigen::Matrix<T,ROWS,COLS>> & d) {

        static_assert(ROWS != Eigen::Dynamic);
        static_assert(COLS != Eigen::Dynamic);

        Base::Shape shape;
        std::unique_ptr<T[]> data;
        try {
            Controller::backend().readMemoizeable(m, shape, data);

            if(shape.size() != 3)
                throw money::Exceptions::FrontendException("Dimension is wrong.");

            if(shape[1] != ROWS  || shape[2] != COLS )
                throw money::Exceptions::FrontendException("Shape does not match.");

            static auto N = ROWS*COLS;

            d.resize(shape[0]);

            /* copy values from data backend to provided vector */
            for(unsigned i = 0; i < shape[0]; i++) {
                d[i] = Eigen::Map<Eigen::Matrix<T,ROWS,COLS>>(data.get()+i*N);
            }

            m.setUnchanged();

        } catch (money::Exceptions::BackendException & e) {
            std::cout << "[MONEY] Fetching for Memo " << m.name() << " failed: "
                      << e.what() << std::endl;
        }

    }

};


}
}

#endif // MONEY_FRONTEND_EIGEN_H

