#ifndef MONEY_FRONTEND_OPENCVMAT_H
#define MONEY_FRONTEND_OPENCVMAT_H

#include <money/Backend.h>
#include <money/Controller.h>
#include <opencv2/core/core.hpp>

namespace money {


namespace Base {

template<>
class Frontend<cv::Mat> {

    static money::Base::TypeEnum cvDepthToType(int type)
    {
        using money::Base::TypeEnum;
        switch(type) {
        case CV_8U: return TypeEnum::UInt8;
        case CV_8S: return TypeEnum::Int8;
        case CV_32S: return TypeEnum::Int32;
        case CV_32F: return TypeEnum::Float;
        case CV_64F: return TypeEnum::Double;
        default:
            throw Exceptions::FrontendException("Frontend<cv::Mat>::cvDepthToType() type not supported.");

        }

        return TypeEnum::UInt8;
    }

    static int TypeToCvDepth(const money::Base::TypeEnum & type)
    {
        using money::Base::TypeEnum;
        switch(type) {
        case TypeEnum::UInt8:  return CV_8U;
        case TypeEnum::Int8:   return CV_8S;
        case TypeEnum::Int32:  return CV_32S;
        case TypeEnum::Float:  return CV_32F;
        case TypeEnum::Double: return CV_64F;
        default:
            throw Exceptions::FrontendException("Frontend<cv::Mat>::TypeToCvDepth() type not supported.");
        }

        return CV_8U;

    }

public:
    static void store(const Base::Memoizeable &m,
                      const cv::Mat & d) {

        /* we don't store empty matricies */
        if(d.empty()) {
            return;
        }

        if(!d.isContinuous()) {
            throw Exceptions::FrontendException("cv::Mat: Could not store non-continous matrices.");
        }

        /* open cv matrix is always seen as 3d object */
        money::Base::Shape shape;
        shape.resize(3);
        shape[0] = d.rows;
        shape[1] = d.cols;
        shape[2] = d.channels();


        Base::TypeEnum type = cvDepthToType(d.depth());


        Controller::backend().writeMemoizeable(m,
                                               type,
                                               shape,
                                               d.ptr<Backend::DataType>());

    }

    static void fetch(Base::Memoizeable &m,
                      cv::Mat & d) {

        Base::Shape shape;
        Backend::DataTypePtr data;
        try {
            Base::TypeEnum type;
            Controller::backend().readMemoizeable(m, type, shape, data);

            if(shape.size() != 3) {
                throw Exceptions::FrontendException("cv::Mat: stored matrix has is not of 3D shape.");
            }

            int rows = shape[0];
            int cols = shape[1];
            int channels = shape[2];

            /* define the matrix type from stored value */
            int cvType = CV_8UC1;
            switch(type) {
                case Base::TypeEnum::UInt8: cvType = CV_8UC(channels); break;
                case Base::TypeEnum::Int8: cvType = CV_8SC(channels); break;
                case Base::TypeEnum::Int32: cvType = CV_32SC(channels); break;
                case Base::TypeEnum::Float: cvType = CV_32FC(channels); break;
                case Base::TypeEnum::Double: cvType = CV_64FC(channels); break;
            default: break;
            }

            /* create a view from the raw data
             * and copy it to the provided matrix */
            cv::Mat(cv::Size(cols, rows), cvType, (char*)data.get()).copyTo(d);

            m.setUnchanged();

        } catch (money::Exceptions::BackendException & e) {
            std::cout << "[MONEY] Fetching for Memo " << m.name() << " failed: "
                      << e.what() << std::endl;
        }

    }

};
}
}

#endif // MONEY_FRONTEND_OPENCVMAT_H

