#ifndef MONEY_MEMOIZEABLE_H
#define MONEY_MEMOIZEABLE_H

#include <iostream>
#include <utility>
#include <cstdint>
#include <vector>
#include <string>
#include <exception>
#include <stdexcept>

#include <money/Base/Exceptions.h>
#include <money/Base/Memoizeable.h>
#include <money/Controller.h>

namespace money {


    template<typename T>
    class Memoizeable : public Base::Memoizeable {

        T * m_data;
        bool m_dataCreated;

    private:
        void store() {
            Base::Frontend<T>::store(*this, *m_data);
        }

        void fetch() {
            try {
                Base::Frontend<T>::fetch(*this, *m_data);
            } catch (const money::Exceptions::FrontendException & e) {
                std::cout << "[MONEY] Fetching for Memo " << name() << " failed: "
                          << e.what() << std::endl;
            }
        }


    public:


        Memoizeable(T & data)
            : m_data(&data),
              m_dataCreated(false)
        {
            throw std::logic_error("Unnamed Memoizeables are currently not implemented.");
        }

        /**
         * constructs a Memoizeable with given name
         * and a data container */
        Memoizeable(const std::string & name, T & data)
            : Base::Memoizeable(name),
              m_data(&data),
              m_dataCreated(false)
        {
            this->fetch();
        }

        Memoizeable(const std::string & name)
            : Base::Memoizeable(name),
              m_data(0),
              m_dataCreated(false)
        {
            m_data = new T();
            m_dataCreated = true;
            this->fetch();
        }

        template<typename ... ArgTypes>
        Memoizeable(const std::string & name, ArgTypes && ... args)
            : Base::Memoizeable(name),
              m_data(0),
              m_dataCreated(0)
        {
            m_data = new T(std::forward<ArgTypes>(args)...);
            m_dataCreated = true;
            this->fetch();
        }

        ~Memoizeable() {
            if(changed())
                this->store();
        }

        bool changed() const {
            return Base::Memoizeable::changed() | Controller::checkMemoChanged();
        }

        const T & get() const {
            return *m_data;
        }

        const T & cget() const {
            return get();
        }


        operator const T&() const {
            return *m_data;
        }

        Memoizeable & operator=(const T & d) {
            setChanged();
            (*m_data) = d;
        }

        Memoizeable & operator=(T & d) {
            setChanged();
            (*m_data) = d;
        }

        T & get() {
            setChanged();
            return *m_data;
        }

        operator T&() {
            setChanged();
            return *m_data;
        }
    };
}

#endif // MONEY_MEMOIZEABLE_H

