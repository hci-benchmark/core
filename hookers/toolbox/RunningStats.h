#ifndef HOOKERS_TOOLBOX_RUNNINGSTATS_H
#define HOOKERS_TOOLBOX_RUNNINGSTATS_H

class RunningStats {

public:
    void clear() {
        sum = 0;
        squaresum = 0;
        n = 0;
    }
    template<class T>
    void push(T value) {
        if (n == 0) {
            min = value;
            max = value;
        }
        else {
            min = std::min(min, (double)value);
            max = std::max(max, (double)value);
        }
        sum += value;
        squaresum += value * value;
        n++;
    }

    double getMean() {
        if (n < 1) {
            return 0;
        }
        return sum / n;
    }
    double getVar() {
        if (n < 2) {
            return 0;
        }
        return 1.0/(n-1) * (squaresum - sum*sum / n);
    }
    double getStddev() {
        return std::sqrt(getVar());
    }
    void print(std::ostream& out) {
        out << getMean() << " +- " << getStddev() << ", " << n << " Samples, range: [" << min << ", " << max << "]";
    }

    std::string print() {
        std::stringstream out;
        print(out);
        return out.str();
    }

    double sum = 0;
    double squaresum = 0;
    double min = 0;
    double max = 0;
    size_t n = 0;
};

#endif // HOOKERS_TOOLBOX_RUNNINGSTATS_H
