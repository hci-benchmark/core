#include <iostream>
#include <string>
#include <sstream>
#include <exception>
#include <stdexcept>
#include <thread>

#include "Context.h"


using namespace hookers::toolbox::opengl;

int Context::contextCounter(0);


Context::Context(unsigned width, unsigned height, bool visible)
    : window(nullptr)
{
    /* initialize glfw.
     * succesive calls are ignored */
    if (glfwInit() != GL_TRUE) {
        throw std::runtime_error("Failed to initialize GLFW\n");
    }

    std::stringstream windowName;
    windowName << "hookers gl (" << int(contextCounter+1) << ")";

    /* Open a window and create its OpenGL context */
    glfwWindowHint(GLFW_VISIBLE, visible);
    window = glfwCreateWindow(width, height, windowName.str().c_str(), NULL, NULL);
    if (window == NULL){
        throw std::runtime_error("Failed to open GLFW window.");
    }

    /* make our context current */
    Locker locker(*this);

    if (glewInit() != GLEW_OK) {
        throw std::runtime_error("Failed to Initialize GLEW");
    }



    /* count the number of active contexts */
    contextCounter++;

}

Context::~Context()
{
    glfwDestroyWindow(window);
}

void Context::makeCurrent()
{
    glfwMakeContextCurrent(window);
}

void Context::release()
{
    glfwMakeContextCurrent(nullptr);
}
