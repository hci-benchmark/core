#ifndef HOOKERS_TOOLBOX_OPENGL_CONTEXT_H
#define HOOKERS_TOOLBOX_OPENGL_CONTEXT_H

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <atomic>

namespace hookers {
namespace toolbox {
namespace opengl {


/**
 * @brief small context wrapper
 *
 * Sets up a opengl context including a window.
 *
 * You can make the given context current using makeCurrent().
 *
 * @warning this class is currently not entirely threadsafe.
 *          Only create contexts in apps' main thread.
 * @warning Creating a new context will make it current in the current thread.
 *          Be aware of side-effects with running computions!
 */
class Context
{
    static int contextCounter;



public:

    /**
     * @brief Helper Class for RIAA of a Context
     *
     * Example:
     * @code
     * Context c;
     *
     * // context is only made current during this scope and
     * // released afterwards
     * {
     *   Context::Locker lock(c);
     *   glDoSomething();
     * }
     * @endcode
     *
     */
    class Locker {

        Context & c;

    public:
        Locker(Context & context)
            : c(context) {
            c.makeCurrent();
        }

        ~Locker() {
            c.release();
        }
    };

public:
    GLFWwindow * window;

public:
    Context(unsigned width, unsigned height, bool visible = false);
    ~Context();

    /** sets this current context as active */
    void makeCurrent();

    /** releases a previously current context */
    void release();
};

} // ns opengl
} // ns toolbox
} // ns hookers

#endif // HOOKERS_TOOLBOX_OPENGL_CONTEXT_H
