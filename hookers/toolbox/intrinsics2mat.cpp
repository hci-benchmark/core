#ifndef HOOKERS_TOOLBOX_INTRINSICS2MAT_H
#define HOOKERS_TOOLBOX_INTRINSICS2MAT_H

#include <vigra/random.hxx>
#include <vigra/unittest.hxx>

#include <assert.h>
#include <iomanip>

#include <opencv2/core/core.hpp>
#include <opencv2/calib3d/calib3d.hpp>


namespace hookers {
namespace toolbox {

template<class Vec>
void intrinsics2mat(const Vec& intrinsics, cv::Mat& mat) {
    mat = cv::Mat(3,3,CV_32F, 0.0);
    mat.at<float>(0,0) = intrinsics[0];
    mat.at<float>(1,1) = intrinsics[1];
    mat.at<float>(2,2) = 1;
    mat.at<float>(0,2) = intrinsics[2];
    mat.at<float>(1,2) = intrinsics[3];
}

}
}

#endif // HOOKERS_TOOLBOX_INTRINSICS2MAT_H
