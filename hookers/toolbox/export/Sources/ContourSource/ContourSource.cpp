
#include <vector>
#include <map>
#include <iomanip>
#include <sstream>

#include <hookers/interface/Sequence.h>

#include "ContourSource.h"

using namespace hookers::interface;
using namespace hookers::toolbox;


std::string zeroPadNumber(int num, int pad_count=6){
    std::ostringstream ss;
    ss << std::setw(pad_count) << std::setfill( '0' ) << num;
    return ss.str();
}


ContourSource::ContourSource()
    :jsonMap({}), jsonPath(""){
    m_id = SourceIdentifier("masks", "dynamic");
}

ExporterSource::SourceIdentifierList ContourSource::provides() const{
    SourceIdentifierList ids;

    // No JSON file has been set
    if(jsonMap.size() == 0)
        return ids;

    ids.push_back(m_id);
    return ids;
}

ExporterSource::SourceMap ContourSource::sources(Frame f){
    SourceMap sources;

    /* Get current frame from list of frames */
    FrameList frames = f.sequence().frames();
    auto f_it = frames.find(f);

    /** @throws runtime_error when provided frame could not be found in sequence */
    if(f_it == frames.end()) {
        throw std::runtime_error("ContourSource: provided frame could not be found in sequence.");
    }

    /* initialize the mask buffer */
    sources[m_id] = cv::Mat(f.size()[1], f.size()[0], CV_32FC1);
    cv::Mat & map = sources[m_id];
    map = 0.0;

    /* find the frame number */
    int frame =  f_it-frames.begin();

    /* Create full label string to check against */
    std::string name = f.sequence().name();
    std::string fullLabel = name + "_" + zeroPadNumber(frame);

    /**
     * @throws runtime_error if no frame mask can be found
     */
    if(!jsonMap.count(fullLabel)) {
        throw std::runtime_error("ContourSource: No masks could be found for given frame.");
    }

    /* fetch contours list from json entry map */
    auto &contours = jsonMap[fullLabel];

    cv::Scalar color(1.0);
    for(auto &el : contours){

        /* we don't need to draw empty contours */
        if(!el.size())
            continue;

        /* Convert float points to integer points */
        std::vector<cv::Point> tempVec;
        cv::Mat(el).copyTo(tempVec);

        /* prepapre required structure for drawContours() */
        std::vector< std::vector<cv::Point> > contourVec;
        contourVec.push_back(tempVec);

        /* draw each contour */
        cv::drawContours(map, contourVec, -1, color, CV_FILLED);
    }


    return sources;
}

void ContourSource::setJsonPath(const std::string &pathname){
    jsonPath = pathname;
    jsonMap.clear();

    if(pathname.empty())
        return;

    addContours(jsonMap, jsonPath.c_str());
}

std::string ContourSource::getJsonPath() const {
    return jsonPath;
}

