#ifndef FRAMENAME_H
#define FRAMENAME_H

#include <string>
#include "basename.h"
#include "split.h"
#include "stringexception.h"
#include "tostringlz.h"

class FrameName {
private:
    std::string filename;

public:

    static std::string purgeFilename(const std::string& filename) {
        std::string allowed;
        for (char s = 'A'; s != 'Z'; ++s) {
            allowed += s;
        }
        for (char s = 'a'; s != 'z'; ++s) {
            allowed += s;
        }
        for (char s = '0'; s != '9'; ++s) {
            allowed += s;
        }
        allowed += "_.-+#";
        return purge(filename, allowed);
    }

    static std::string purge(const std::string& str, const std::string& allowed_chars) {
        std::string result;
        for (auto it : str) {
            if (std::string::npos != allowed_chars.find_first_of(it)) {
                result += it;
            }
        }
        return result;
    }

    static std::string purge (const std::string& str) {
        return purge(str, "_0123456789");
    }

    void setName(const std::string& _filename) {
        filename = _filename;
    }

    /**
     * @brief isStereo checks if a given filename corresponds to the right image of a stereo pair.
     * @param filename
     * @return
     */
    static bool isStereo(const std::string& filename) {
        auto name = basename(filename);
        if (name.length() < 2) {
            return false;
        }
        if ('r' == name[0] && '_' == name[1]) {
            return true;
        }
        return false;
    }

    static bool sameFrame(const std::string& _a, const std::string& _b) {
        const std::string digits = "0123456789";
        auto clean_a = basename(_a);
        auto clean_b = basename(_b);
        if (clean_a[0] != clean_b[0]) {
            return false;
        }
        if ('r' == clean_a[0]) {
            clean_a = clean_a.substr(2);
            clean_b = clean_b.substr(2);
        }
        auto a = split(clean_a, '_');
        auto b = split(clean_b, '_');
        if (a.size() < 3 || b.size() < 3) {
            return false;
        }
        for (size_t ii = 0; ii < 3; ++ii) {
            if (!numbersMatch(a[ii], b[ii])) {
                return false;
            }
        }
        return true;
    }

    static bool sameScene(const std::string& _a, const std::string& _b) {
        auto clean_a = basename(_a);
        auto clean_b = basename(_b);
        if ('r' == clean_a[0]) {
            clean_a = clean_a.substr(2);
        }
        if ('r' == clean_b[0]) {
            clean_b = clean_b.substr(2);
        }
        auto a = split(clean_a, '_');
        auto b = split(clean_b, '_');
        if (a.size() < 3 || b.size() < 3) {
            return false;
        }
        for (size_t ii = 0; ii < 2; ++ii) {
            if (!numbersMatch(a[ii], b[ii])) {
                return false;
            }
        }
        return true;
    }

    static bool numbersMatch(const std::string& a, const std::string& b) {
        std::string digits = "0123456789";
        return std::stoll(purge(a, digits)) == std::stoll(purge(b,digits));
    }

    static std::string getScene(const std::string& filename) {
        auto clean_name = basename(filename);
        if (isStereo(clean_name)) {
            clean_name = clean_name.substr(2);
        }
        auto name = split(basename(filename), '_');
        if (name.size() < 2) {
            throw StringException("Filename does not contain a day and scene ID");
        }
        int day = std::stoi(name[0]);
        int scene = std::stoi(name[1]);
        std::string result = std::to_string(day);
        result += "_";
        result += toStringLZ(scene, 4);
        return result;
    }

    static std::string getFrame(const std::string& filename) {
        auto name = split(basename(filename), '_');
        if (name.size() < 3) {
            throw StringException("Name does not contain a frame name");
        }
        return toStringLZ(std::stoi(name[2]), 6);
    }

    static std::string getName(const std::string& filename) {
        return getScene(filename) + "_" + getFrame(filename);
    }


};

#endif // FRAMENAME_H
