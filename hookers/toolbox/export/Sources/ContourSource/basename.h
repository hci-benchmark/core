#ifndef BASENAME_H
#define BASENAME_H
#include <string>
#include <libgen.h>

inline std::string basename(std::string name) {
    char * _name = (char*)name.c_str();
    char * _bname = basename(_name);
    return std::string(_bname);
}

#endif // BASENAME_H
