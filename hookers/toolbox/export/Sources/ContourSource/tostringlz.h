#ifndef TOSTRINGLZ_H
#define TOSTRINGLZ_H

#include <string>

template<class T>
/**
 * @brief toStringLZ
 * @param val
 * @return
 */
std::string toStringLZ(const T val, const size_t minlength) {
    std::string res = std::to_string(val);
    if (res.size() < minlength) {
        res = std::string(minlength - res.size(), '0') + res;
    }
    return res;
}

#endif // TOSTRINGLZ_H
