#pragma once

#include <vector>
#include <opencv2/opencv.hpp>
#include <opencv2/imgproc/types_c.h>

#include "stdlib.h"
#include "stdio.h"
#include "string.h"
#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"
#include "rapidjson/filereadstream.h"

#include "framename.h"


inline std::string pallas_basename(const std::string& in) {
    return FrameName::getName(in);
}

template<class Map, class Json>
void addContours(Map& map, const Json& doc) {
    int zIndexReadCounter = 0;
    for (size_t ii = 0; ii < doc["project"]["tasks"].Size(); ++ii) {
        const auto& task = doc["project"]["tasks"][ii];
        const std::string frameName = FrameName::getName(task["frame"]["original"].GetString());
        std::map<int, std::vector<std::vector<cv::Point2f>>> frameContours;
        for (size_t jj = 0; jj < task["results"].Size(); ++jj) {
            const auto& result = task["results"][jj];
            for (size_t kk = 0; kk < result["annotations"].Size(); ++kk) {
                const auto& annotation = result["annotations"][kk];
                if (annotation.HasMember("isDeleted")
                        && annotation["isDeleted"].IsBool()
                        && annotation["isDeleted"].GetBool()) {
                    continue;
                }
                for (size_t ll = 0; ll < annotation["points"].Size(); ++ll) {

                    const auto& contourVec = annotation["points"][ll];
                    std::vector<cv::Point2f> contour;
                    const size_t arr1Size = contourVec.Size();
                    contour.reserve(arr1Size);
                    for (size_t mm = 0; mm+1 < arr1Size; mm+=2) {
                        contour.push_back(cv::Point2f(contourVec[mm].GetDouble(), contourVec[mm+1].GetDouble()));
                    }
                    int zIndex = 0;
                    if (annotation.HasMember("meta")
                            && annotation["meta"].HasMember("zIndex")
                            && annotation["meta"]["zIndex"].IsInt()) {
                        zIndex = annotation["meta"]["zIndex"].GetInt();
                    }
                    zIndexReadCounter++;
                    frameContours[zIndex].push_back(contour);
                }
            }
        }
        for (const auto& contourVec: frameContours) {
            for (const auto& contour : contourVec.second) {
                map[frameName].push_back(contour);
            }
        }
    }
}

template<class Map>
void addContours(Map& map, const char* filename) {
    rapidjson::Document doc;

    FILE* fp = fopen(filename, "r"); // non-Windows use "r"
    char readBuffer[1024*1024];
    rapidjson::FileReadStream is(fp, readBuffer, sizeof(readBuffer));
    doc.ParseStream(is);


    addContours(map, doc);

#if 0
    static const char* kTypeNames[] = { "Null", "False", "True", "Object", "Array", "String", "Number" };
    auto tasks = &doc["project"]["tasks"];
    for (size_t ii = 0; ii < 3; ++ii) {
        for (auto itr = doc["project"]["tasks"][ii]["results"][0].MemberBegin();
             itr != doc["project"]["tasks"][ii]["results"][0].MemberEnd(); ++itr) {
            printf("Type of member %s is %s\n",
                   itr->name.GetString(), kTypeNames[itr->value.GetType()]);

        }
        std::cout << "Size of results array: " << doc["project"]["tasks"][ii]["results"].Size() << std::endl;
        std::cout << "Frameindex: " << (*tasks)[ii]["frame"]["frameIndex"].GetInt() << std::endl;
        std::cout << "Original: " << pallas_basename((*tasks)[ii]["frame"]["original"].GetString()) << std::endl;
    }
    std::cout << "Size of array: " << doc["project"]["tasks"].Size() << std::endl;
#endif


    fclose(fp);
}
