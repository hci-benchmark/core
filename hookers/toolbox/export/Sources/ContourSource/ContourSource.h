#ifndef HOOKERS_TOOLS_EXPORTERSOURCE_CONTOUR_H
#define HOOKERS_TOOLS_EXPORTERSOURCE_CONTOUR_H

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>

#include "../ExporterSource.h"
#include "readJson.h"


namespace hookers {
namespace toolbox {

class ContourSource : public ExporterSource
{

    SourceIdentifier m_id;
    std::map< std::string, std::vector< std::vector<cv::Point2f> > > jsonMap;
    std::string jsonPath;
public:
    ContourSource();

    SourceIdentifierList provides() const;
    SourceMap sources(interface::Frame f);

    void setJsonPath(const std::string& pathname);
    std::string getJsonPath() const;
};

}
}

#endif // HOOKERS_TOOLS_EXPORTERSOURCE_CONTOUR_H
