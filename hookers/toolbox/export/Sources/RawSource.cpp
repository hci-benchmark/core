#include "RawSource.h"


RawSource::RawSource(const std::string &directory)
{
    /* this call will throw some exceptions if anything went wrong.
     * we don't need to catch it here but just let it happen */
    m_reader = RawImporter::fromDirectory(directory);
}

ExporterSource::SourceIdentifierList RawSource::provides() const
{

    /* currently we only provide the gray values for left map */
    const ExporterSource::SourceIdentifier RawSourceIdentifier("map", "left");

    SourceIdentifierList ids;
    ids.push_back(RawSourceIdentifier);
    return ids;
}

ExporterSource::SourceMap RawSource::sources(Frame f)
{
    const ExporterSource::SourceIdentifier RawSourceIdentifier("map", "left");

    RawImporter::RawFrame frame = m_reader.nextFrame();

    /* call the function on the importer to get the left view */
    //m_reader.nextFrame(...)


    SourceMap sources;
    frame.base.copyTo(sources[RawSourceIdentifier]);

    return sources;

}

