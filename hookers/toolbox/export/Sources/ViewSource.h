#ifndef VIEWSOURCE_H
#define VIEWSOURCE_H

#include "ExporterSource.h"
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>

#include <hookers/interface/Frame.h>

namespace hookers {
namespace toolbox {


class ViewSource : public ExporterSource
{

    interface::Frame::View m_view;
public:
    ViewSource(const interface::Frame::View & view = interface::Frame::View::Base);

	SourceIdentifierList provides() const;
    SourceMap sources(interface::Frame f);
};

}
}

#endif
