#ifndef HOOKERS_TOOLS_EXPORTERSOURCE_H
#define HOOKERS_TOOLS_EXPORTERSOURCE_H

#include <map>
#include <vector>
#include <string>


#include <hookers/interface/Frame.h>

#include <opencv2/core.hpp>

namespace hookers {
namespace toolbox {

class ExporterSource
{
public:


    struct SourceIdentifier {

        SourceIdentifier() {}
        SourceIdentifier(const std::string & cat,
			const std::string & name)
            : category(cat), name(name){}

        std::string category;
        std::string name;

        bool operator==(const SourceIdentifier & other) const {
            return (category == other.category) && (name == other.name);
        }

        bool operator<(const SourceIdentifier & other) const {
            return (path()) < (other.path());
        }

        bool valid() const {
            return !category.empty() && !name.empty();
        }

        std::string path() const {
            return category+"/"+name;
        }

    };

    typedef std::map<SourceIdentifier, cv::Mat> SourceMap;
    typedef std::vector<SourceIdentifier> SourceIdentifierList;

    virtual ~ExporterSource() {}

    /** list of data this source provides */
    virtual SourceIdentifierList provides() const = 0;

    /** provide the data for the source identifiers given in provides() */
    virtual SourceMap sources(interface::Frame f) = 0;


};

}
}

#endif // HOOKERS_TOOLS_EXPORTERSOURCE_H

