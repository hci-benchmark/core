#include "ViewSource.h"


using namespace hookers::interface;
using namespace hookers::toolbox;


ViewSource::ViewSource(const Frame::View &view)
    : m_view(view)
{}

ExporterSource::SourceIdentifierList ViewSource::provides() const
{
    return SourceIdentifierList{SourceIdentifier("view", std::to_string(m_view))};
}

ExporterSource::SourceMap ViewSource::sources(Frame f)
{
    SourceMap sources;

    /* check if the applied view is correct */
    if(m_view != Frame::View::Base
      && m_view != Frame::View::Stereo)
    {
        return sources;
    }

    cv::Mat view;
    Image mView;

    f.getView(mView, m_view);
    view = cv::Mat(mView.height(), mView.width(), CV_8UC1, mView.data());
    if (!view.data)                              // Check for invalid input
        std::cout << "Could not open or find the image" << std::endl;

    SourceIdentifier id("view", std::to_string(m_view));
    view.copyTo(sources[id]);

	return sources;
}
