#ifndef HOOKERS_TOOLS_EXPORTERSOURCE_RAW_H
#define HOOKERS_TOOLS_EXPORTERSOURCE_RAW_H

#include "ExporterSource.h"


/**
 * raw source is a little bit different from all other sources.
 * It doesn't operatore on the interface but reads data from raw input.
 * Therefor this source needs some special handling in the event loop.
 * The provided frame is always an empty frame and the reader has to advance
 * the raw stream for each call to sources.
 */

namespace hookers {
namespace toolbox {


class RawSource : public ExporterSource
{

public:
    RawSource(const std::string & directory);


    SourceIdentifierList provides() const;
    SourceMap sources(interface::Frame f);


};

}
}

#endif // HOOKERS_TOOLS_EXPORTERSOURCE_RAW_H
