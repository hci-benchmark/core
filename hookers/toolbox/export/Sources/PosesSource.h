#ifndef HOOKERS_TOOLS_EXPORTERSOURCE_POSES_H
#define HOOKERS_TOOLS_EXPORTERSOURCE_POSES_H

#include <iostream>
#include <unordered_set>

#include <hookers/interface/Project.h>
#include <hookers/interface/Track.h>
#include <hookers/toolbox/PoseEstimation/PoseEstimation.h>

#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>


#include "ExporterSource.h"

namespace hookers {
namespace toolbox {

class PosesSource : public ExporterSource
{
    bool m_useTriangulate;
    std::unordered_set<interface::Project::Context> m_triangulateTrackContexts;

    bool m_useRightView;
    bool m_storeVis;

    SourceIdentifier m_id;

    interface::FrameList & m_framesBA;
    interface::FrameList & m_frames;
    interface::FrameList & m_framesExport;

    BundleAdjustmentProblem * m_baPtr;
    std::vector<int> m_poseIndices;

    std::vector<Vector6d> m_ps;
    std::vector<Array2d> m_pcovs;
    std::vector<BundleAdjustmentProblem::ImageParametrization> m_images;
    BundleAdjustmentProblem::ObjectParametrization m_objects;

    double m_annotatedImageUncertainty;
    double m_triangulatedImageUncertainty;
    std::string mask_path;
public:
    PosesSource(interface::FrameList & frames_all, interface::FrameList & frames_adjust, interface::FrameList & frames_export);
    ~PosesSource();

    void setTriangulate(bool argTriangulate);
    void setTrackContext(const std::unordered_set<interface::Project::Context> &             contexts);
    void setUseRightView(bool argUseRightView);
    void setStoreVis(bool storeVis);

    void setAnnotatedImageUncertainties(double unc) {
        m_annotatedImageUncertainty = unc;
    }

    void setTriangulatedImageUncertainties(double unc) {
        m_triangulatedImageUncertainty = unc;
    }

    void setMaskDirectory(std::string path)
    {
        mask_path = path;
    }

    void solve();

    std::vector<Vector6d> & poses();
    std::vector<Array2d> & covariances();

    SourceIdentifierList provides() const;
    
    SourceMap sources(interface::Frame f);

};

}
}

#endif // HOOKERS_TOOLS_EXPORTERSOURCE_MAP_H
