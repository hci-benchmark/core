#include "MapSource.h"

using namespace hookers;
using namespace hookers::interface;
using namespace hookers::toolbox;

MapSource::MapSource(const std::string &mapName)
{
    if(mapName.empty())
        return;
    m_id = SourceIdentifier("map", mapName);
}

ExporterSource::SourceIdentifierList MapSource::provides() const
{

    /* if the name is invalid or anything else, then we don't return anything */
    if(!m_id.valid())
        return SourceIdentifierList();

    SourceIdentifierList ids;
    ids.push_back(m_id);

    return ids;
}

ExporterSource::SourceMap MapSource::sources(Frame f)
{
    SourceMap sources;

    if(!m_id.valid())
        return SourceMap();


    sources[m_id] = cv::Mat();

    cv::Mat map;
	Frame::Map mMap;

	if (m_id.category == "map"&& f.hasMap(m_id.name)){
		f.getMap(m_id.name, mMap);
		map = cv::Mat(mMap.shape(2), mMap.shape(1), CV_32FC(mMap.shape(0)), mMap.data());

		if (!map.data)                              // Check for invalid input
		{
			std::cout << "Could not open or find the image" << std::endl;
		}

	}
	else{
		map.zeros(f.size()[1], f.size()[0], CV_8UC1);
	}
    map.copyTo(sources[m_id]);

    return sources;

}

