#ifndef HOOKERS_TOOLS_EXPORTERSOURCE_MAP_H
#define HOOKERS_TOOLS_EXPORTERSOURCE_MAP_H

#include "ExporterSource.h"
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>

namespace hookers {
namespace toolbox {

class MapSource : public ExporterSource
{

    SourceIdentifier m_id;
public:
    MapSource(const std::string & mapName);

    SourceIdentifierList provides() const;
    SourceMap sources(interface::Frame f);
};

}
}

#endif // HOOKERS_TOOLS_EXPORTERSOURCE_MAP_H
