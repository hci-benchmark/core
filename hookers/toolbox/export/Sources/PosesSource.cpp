#include"PosesSource.h"

using namespace hookers;
using namespace hookers::interface;
using namespace hookers::toolbox;

// Similar to quadratic prior, but the residuals are relative to a second
// parameter, i.e.
//      res = (x1+prior) - x2      
template<int N>
struct binaryPrior
{

    typedef ceres::AutoDiffCostFunction<binaryPrior<N>, N, N, N> AutoDiff;

    double prior[N];
    double weight[N];

    binaryPrior(double * d, double * w)
    {
        for (int ii = 0; ii < N; ++ii)
        {
            weight[ii] = 1.0/w[ii];
            prior[ii] = d[ii];
        }
    }

    template<class T>
    bool operator()(const T* const x1, const T* const x2, T* res) const
    {
        for (int ii = 0; ii < N; ++ii) {
            res[ii] = T(weight[ii]) * (x1[ii] + T(prior[ii]) - x2[ii]);
        }
        return true;
    }
};

void PosesSource::setTriangulate(bool argTriangulate) {
    m_useTriangulate = argTriangulate;
}

void PosesSource::setTrackContext(const std::unordered_set<Project::Context> &contexts)
{
    m_triangulateTrackContexts = contexts;
}

void PosesSource::setUseRightView(bool argUseRightView) {
    m_useRightView = argUseRightView;
}

void PosesSource::setStoreVis(bool storeVis) {
    m_storeVis = storeVis;
}

std::vector<Vector6d> & PosesSource::poses() {
    return m_ps;
}

std::vector<Array2d> & PosesSource::covariances() {
    return m_pcovs;
}

cv::Mat visualizeFrame(
    Frame f,
    const BundleAdjustmentProblem::ImageParametrization & img,
    const BundleAdjustmentProblem::ObjectParametrization & objs,
    const Vector6d & pose = Vector6d(),
    Vector4d intrinsics = Vector4d()) {

    Image imgGray;
    f.getView(imgGray);
    cv::Mat cvImgGray(imgGray.height(), imgGray.width(), CV_8UC1, imgGray.data());


    cv::Mat colorImg;
    cv::cvtColor(cvImgGray, colorImg, CV_GRAY2RGB, 3);

    std::cout << "frame: " << f.id().toString() << ", " << img.size() << std::endl;

    for(auto & i: img) {
        typedef BundleAdjustmentProblem BA;
        typedef BundleAdjustmentProblem::ImageParametrization IP;
        typedef BundleAdjustmentProblem::ObjectParametrization OP;
             
        auto & imgPoint = std::get<IP::POS>(i);
        auto & imgFlags = std::get<IP::FLAGS>(i);

        auto & obj = objs[std::get<IP::TO_OBJECT>(i)];
        auto & objPos = std::get<OP::POS>(obj);
        auto & objFlags = std::get<OP::FLAGS>(obj);

        /* only display actually used points */
        if(objFlags & BA::ObjectUnuseable) {
            continue;
        }

        cv::Scalar pcolor(255,0,0);
        if(objFlags & BA::ObjectUnlinked) {
            pcolor = cv::Scalar(255,255,0);
        }

        cv::circle(colorImg, {int(imgPoint[0]), int(imgPoint[1])}, 5, pcolor, 2);

        if(intrinsics == Vector4d()) {
            intrinsics = f.intrinsics();
        }

        if(pose != Vector6d()) {
            auto projectedPosition = Projection::project3d(objPos, pose, intrinsics);
            cv::circle(colorImg, {int(projectedPosition[0]), int(projectedPosition[1])}, 4, cv::Scalar(0,0,255), 1.0);
        }
    }
    cv::Mat floatImg;
    colorImg.convertTo(floatImg, CV_32F, 1.0/255.0);
    return floatImg; }

ExporterSource::SourceIdentifierList PosesSource::provides() const {

    SourceIdentifierList ids;
    if (m_storeVis) {ids.push_back(m_id);}
    return ids;
}

ExporterSource::SourceMap PosesSource::sources(Frame f){
    SourceMap sources;
    if (m_storeVis){

            int i = m_framesExport.find(f) - m_framesExport.begin();
            if(i < (int)m_poseIndices.size()){
                std::cout << i << " " << m_poseIndices[i] << "\n";
                sources[m_id] = visualizeFrame(f, m_images[m_poseIndices[i]], m_objects, m_ps[i],
                            m_baPtr->intrinsics());
            }
    }
                            
    return sources;

}

PosesSource::PosesSource(FrameList & frames_all, FrameList & frames_adjust, FrameList & frames_export):
    m_useTriangulate(false),
    m_triangulateTrackContexts{Project::ManualContext, Project::PipelineContext},
    m_useRightView(false),
    m_storeVis(false),
    m_id("ba", "tracks"),
    m_framesBA(frames_adjust),
    m_frames(frames_all),
    m_framesExport(frames_export),
    m_annotatedImageUncertainty(2.0),
    m_triangulatedImageUncertainty(2.0)
     {}

PosesSource::~PosesSource() {
        delete m_baPtr;}

void PosesSource::solve(){


    const double defaultImageUncertainty = m_annotatedImageUncertainty;

    /* find all tracks from requested bundle adjustment frames */
    TrackList allTracks;
    {
        TrackSet collectedTracks;
        for(auto f: m_framesBA) {
            collectedTracks |= TrackSet::fromList(f.tracks(Frame::View::Base));
        }
        allTracks = collectedTracks.toList();
    }

    /* these are the per view correspondence information */
    std::vector<Vector6d> poses;

    /* these guys keep information of tracks occourances in frames
     * and some default initialization values */
    std::unordered_map<Track, Frame> lastTrackOccourance;
    std::unordered_map<Frame, Vector3d> defaultLandmarkPosition;

    /* go through each frame and add 2d/3d correspondences */
    for(auto f: m_framesBA) {
        TrackList annotTracks = f.tracks().annotated();
        TrackList unlinkedTracks = (TrackSet::fromList(f.tracks(Frame::View::Base)) - TrackSet::fromList(annotTracks)).toList();

        /* do solvePNP for initial camera pose estimation
         * including outlier detection */
        std::vector<int> inliers;
        Vector6d pose;
        std::cout << " Frame " << f.number() << " ";
        try {
            std::tie(pose, inliers) = PoseEstimation::solvePnPRansac(f, 10,2);
        } catch (std::exception & e) {
            std::stringstream err;
            err << "BundleAdjustment: could not find pose for frame "
                << int(m_frames.find(f)-m_frames.begin())
                << ": " << e.what();
            throw std::runtime_error(err.str());
        }

        /* calculate a feaseable default landmark position that is in front of
         * the camera */
        defaultLandmarkPosition[f] = Projection::reproject3d(Vector3d(0, 0, 6),
                                                             pose, f.intrinsics());

        BundleAdjustmentProblem::ImageParametrization image;


        /* go through list of inliers and place the corresponding
         * index into correspondence array */
        for(auto i: inliers) {
            int pos = allTracks.find(annotTracks[i])-allTracks.begin();
            image.emplace_back(std::make_tuple(annotTracks[i].posInFrame(f),
                                               pos,
                                               BundleAdjustmentProblem::ImageRegular,
                                               defaultImageUncertainty));
        }


        /* now go through unlinked tracks and add 2d-3d correspondences to problem */
        if(m_useTriangulate) {
            int numManual = 0;

            cv::Mat maskForValids;
            cv::Rect maskRc;
            if(!mask_path.empty())
            {
                char tmp_frame_name[256]={0};
                int frame_id_int = f.number();
                sprintf(tmp_frame_name,"/%s_%06i.png",f.sequence().name().c_str(),frame_id_int);
                std::string check_mask = mask_path+tmp_frame_name;
                maskForValids = cv::imread(check_mask,cv::IMREAD_ANYDEPTH);
                if(maskForValids.data
                    && ((maskForValids.type() == CV_16S)
                    || (maskForValids.type() == CV_16U))) //mask is 16bit
                    maskForValids.convertTo(maskForValids,CV_8U, 256.0);
                if(maskForValids.data && maskForValids.type() != CV_8U)
                    maskForValids = cv::Mat();
                if(!maskForValids.data)
                    std::cout << "Warning: could not load valid frame masking from " << check_mask << std::endl;
                else
                    maskRc = cv::Rect(0,0,maskForValids.cols,maskForValids.rows);
            }
            for(Track t : unlinkedTracks) {

                if(!allTracks.contains(t))
                    throw std::runtime_error("Track index out of bounds");

                /* find the track id */
                int objectIndex = allTracks.find(t)-allTracks.begin();

                auto pos2d = t.posInFrame(f);
                auto context_t = t.creationContext();

                if(maskForValids.data) /* check if this point lies on an dynamic object */
                {
                    bool bIsValidPoint = false;
                    cv::Point pntCheck((int)(pos2d[0]+0.5), (int)(pos2d[1]+0.5));
                    if(pntCheck.inside(maskRc))
                        bIsValidPoint = (maskForValids.at<uchar>(pntCheck) != 0);
                    if(!bIsValidPoint)
                        continue;
                }

                bool bIsManual = (Project::ManualContext == context_t);
                if(bIsManual)
                    numManual++;
                /* check if the track should be used for triangulation */
                if(!m_triangulateTrackContexts.count(context_t)) {
                    continue;
                }


                /* mask for tracks on road.
                 * ignore tracks that are outside of this window */
                const vigra::Rect2D trackMask(vigra::Point2D(768, 500), vigra::Point2D(1792, 900));
                if(!bIsManual && !trackMask.contains(vigra::Point2D(pos2d[0], pos2d[1])))
                    continue;

                /* store the last frame in which this track occours */
                lastTrackOccourance[t] = f;

                /* finally add correspondence to frame parametrization */
                image.emplace_back(std::make_tuple(pos2d,
                                                   objectIndex,
                                                   BundleAdjustmentProblem::ImageRegular,
                                                   m_triangulatedImageUncertainty));
                                                   //bIsManual?(m_triangulatedImageUncertainty*0.5):m_triangulatedImageUncertainty));
            }
            std::cout << " Imported " << numManual << " manual tracks " << std::endl;
        }

        poses.push_back(pose);
        m_images.push_back(image);
    }

    /* initialize unknown point positions in a few meters in front of camera */
    m_objects.reserve(allTracks.size());
    int numUnlinkedStart = 0;
    for(const Track & t: allTracks) {

        /* initialize the object as unlinked */
        auto lm = t.linkedLandmark();
        auto lmPos = lm.pos();
        auto flags = BundleAdjustmentProblem::ObjectRegular;

        if(!t.hasLink()) {
            lmPos = defaultLandmarkPosition[lastTrackOccourance[t]];
            numUnlinkedStart++;
            flags = BundleAdjustmentProblem::ObjectUnlinked;
            if(Project::ManualContext == t.creationContext())
                flags = BundleAdjustmentProblem::ObjectFlags(BundleAdjustmentProblem::ObjectFromManual |  BundleAdjustmentProblem::ObjectUnlinked);
        }

        m_objects.push_back(std::make_tuple(lmPos,flags));
    }

    // Now for the right view:
    
    //Default baseline between the cameras and uncertainty
    Vector6d poseOffset = Vector6d(0.0);
    poseOffset[0] = -0.3;
    double baselineUncert[]={ 0.001, 0.001, 0.001, //1mm
                            1e-4, 1e-4, 1e-4};

    // Go through the frames
    for(unsigned frameId=0; frameId<m_framesBA.size(); frameId++) {

        // Get left image for debugging 
        Image viewLeft;
        m_framesBA[frameId].getView(viewLeft, Frame::View::Base);

        // Size of the image
        auto size=cv::Size_<int>((int)viewLeft.width(), (int)viewLeft.height());

        auto cvViewLeft=cv::Mat(size, CV_8UC1, (void*)viewLeft.data());
        cv::equalizeHist( cvViewLeft, cvViewLeft );

        // Get the image of the right camera
        Image viewRight;
        m_framesBA[frameId].getView(viewRight, Frame::View::Stereo);

        // Construct cv Mat from vigra, equalize histogram
        auto cvViewRight=cv::Mat(size, CV_8UC1, (void*)viewRight.data());
        cv::equalizeHist( cvViewRight, cvViewRight );

        // Parameters for harris corner detection 
        double minDist = 6.; //6
        int Ntracks=18000; //6000
        double minQuality=0.002;

        // Find corners
        std::vector<cv::Point2f> tracks;
        cv::goodFeaturesToTrack(cvViewRight, tracks, Ntracks, minQuality, minDist, cvViewRight, 3, false, 0.02 );

        // Pose of the left view + default offset = pose of right view
        Vector6d pose=poses[frameId];
        auto rightPose=pose+poseOffset;

        // Keep track of how close the matches were and how many found
        double distSum=0.;
        int distCount=0;

        // Add a new image to hold the additional tracks
        BundleAdjustmentProblem::ImageParametrization image;

        // Go through all the 3d objects ONLY THE VISIBLE ONES!
        // (otherwise, lots of nonsense in the far distance is going to be added)
        for ( auto tuple: m_images[frameId] ){
            int objectId = std::get<1>(tuple);
            auto trackLeft = std::get<0>(tuple); // the position of the track in the left image

            // Get the data 
            auto & obj=m_objects[objectId];
            auto & objectPos = std::get<BundleAdjustmentProblem::ObjectParametrization::POS>(obj);
            auto & objectFlags = std::get<BundleAdjustmentProblem::ObjectParametrization::FLAGS>(obj);
            
            // Project the point to the right view using the modified pose
            auto projRight = Projection::project3d(objectPos, rightPose, 
                            m_framesBA[frameId].intrinsics());

            // Project to the left view for debugging only
            auto projLeft = Projection::project3d(objectPos, pose, 
                            m_framesBA[frameId].intrinsics());
            
            /* only if the object is a regular object, meaning that is an existing object
             * in the reference geometry, we want to use it */
            if (!(objectFlags & BundleAdjustmentProblem::ObjectUnlinked)){

                // Index of nearest track
                int nearestIndex=-1; 
                double nearestDist=4.;

                // Go through all the tracks, find the closest
                for (unsigned trackId = 0;trackId<=tracks.size(); trackId++){
                
                    // Distance from reprojected position to track     
                    double dist = cv::norm( tracks[trackId] - cv::Point2f(projRight[0], projRight[1]) );
                    if (dist<=nearestDist){
                            nearestIndex = trackId;
                            nearestDist = dist; } 
                }

                // If a match was found:
                if (nearestIndex>-1){
                        // Check the normalized cross correlation!
                        //corr=patchCorr(cv::Point2f(trackLeft[0], trackLeft[1]), tracks[nearestIndex],
                        //    cvViewLeft, cvViewRight);
                        //if (corr<0.6){ 
                                // Add it to the current image
                                auto pos=Vector2d(tracks[nearestIndex].x, tracks[nearestIndex].y);
                                image.emplace_back(std::make_tuple(pos,
                                                           objectId,
                                                           BundleAdjustmentProblem::ImageRegular,
                                                           m_annotatedImageUncertainty));

                                // Count it into the statistic
                                distSum+=nearestDist;
                                distCount++;
                        //}
                } 
            } 
        }

        // Add this image to the list
        if (m_useRightView) {
            m_images.push_back(image);
            poses.push_back(rightPose);
        }

    }

    m_baPtr = new BundleAdjustmentProblem(m_objects,
                               m_images,
                               m_framesBA.front().intrinsics(),
                               poses);


    /* this list contains the indices of export frames within the BA frame list */
    std::vector<std::pair<int,int>> covIndices;

    for(unsigned i = 0; i < m_framesExport.size(); i++) {
        /* find the indices of the frames that should be exported
         * in the potentially different list of BA frames */

        auto baItBase = m_framesBA.find(m_framesExport[i]);
        if(baItBase == m_framesBA.end()) {
            throw std::runtime_error("Bundle Adjustment: Requested export frames are not in list for bundle adjustment.");
        }
        int baIndexBase = baItBase-m_framesBA.begin();
        m_poseIndices.push_back(baIndexBase);

        if(i >= m_framesExport.size()-1)
            continue;

        auto baItTarget = m_framesBA.find(m_framesExport[i+1]);
        if(baItTarget == m_framesBA.end()) {
            throw std::runtime_error("Bundle Adjustment: Requested export frames are not in list for bundle adjustment.");
        }
        int baIndexTarget = baItTarget-m_framesBA.begin();
        covIndices.push_back(std::make_pair(baIndexBase, baIndexTarget));
    }

    m_baPtr->setImageUncertainty(m_annotatedImageUncertainty);

    /* First triangulate the unknown points.
     * These are all points that have been flagged with ObjectUnlinked */
    if(m_useTriangulate) {
        m_baPtr->triangulate();
    }

    /* after triangulating the unknown points, we can setup the problem */
    m_baPtr->setup();


    // Add the offset priors for all frames
    if (m_useRightView){
        // Actual number of frames, not counting right views
        int posesLen=poses.size()/2;
        // Poses to export are now right and left (or not?)
        auto leftIndices = m_poseIndices;

//        for( auto i: leftIndices ) {
//            m_poseIndices.push_back(i+posesLen);
//        }

            for (int i=0; i<posesLen; i++){
                    auto block =  new binaryPrior<6>::AutoDiff(
                                    new binaryPrior<6>( poseOffset.data(), baselineUncert));
                    m_baPtr->problem().AddResidualBlock(block,nullptr,poses[i].data(), poses[i+posesLen].data());
            }
    }
    
    /* solve ... */
    m_baPtr->setConstantObjectPositions(false);
    m_baPtr->setConstantIntrinsics(false);
    m_ps = m_baPtr->poses(m_poseIndices);

    /* ... and fetch BA poses and pairwise frame covariances.
     *
     * In order to avoid rank deficient jacobians
     * we ignore triangulated points from covariance analysis.
     */
    m_baPtr->setConstantTriangulatedObjectPositions(true);
    m_baPtr->setConstantLinkedObjectPositions(false);
    m_baPtr->setConstantIntrinsics(false);

    m_pcovs = m_baPtr->posePairwiseUncertainties(covIndices);
}
