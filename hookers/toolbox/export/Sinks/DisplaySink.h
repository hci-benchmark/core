#ifndef DISPLAYSINK_H
#define DISPLAYSINK_H

/**
* @author     Mohsen Rahimimoghaddam, mohsen.r.moghadam@gmail.com
* @date       2015
* @brief	   Displays views and maps of a frame in the given sequence
*
* This class drived form ExporterSink class.
* @see ExporterSink class
*
*
*/

#include"ExporterSink.h"
#include <opencv2/opencv.hpp>
namespace hookers {
namespace toolbox {

class DisplaySink : public ExporterSink{
	/*export a frame depth map as a frame of avi video
	*m_path is the address for saving video*/
	cv::Size m_size;
	std::map<ExporterSource::SourceIdentifier, cv::VideoWriter> m_videos;

public:
	/**@brief construct a diasplaysinks object*/

	DisplaySink();
    ~DisplaySink();

	/**@brief do nothing
	*@param sources contain list of information about the data requested to convert to videos*/
    void setup(const ExporterSource::SourceIdentifierList & sources);

	/**@brief add frames to the videos
	*add single frame to each video (frames can be a map or views)
	*@param map is standard map of open cv matrix of the images should be added and information about them
	*@param framenumber is the number of frame which maps and views related to it in the sequence*/
    void push(const ExporterSource::SourceMap & map, int frameNumber);
};

}
}

#endif
