#include "HDFExporterSink.h"

#include <vigra/hdf5impex.hxx>

using namespace hookers::toolbox;

/*HDF Exporter*/
HDFExporterSink::HDFExporterSink(){
}

void HDFExporterSink::setup(const ExporterSource::SourceIdentifierList & sources){
	vigra::HDF5File m_file(m_path + "/" + m_sequenceName + ".h5", vigra::HDF5File::New);

}

void HDFExporterSink::push(const ExporterSource::SourceMap & map, int frameNumber) {

    /**
     * @todo We need to take care of different datatypes that the matrices can have.
     *       Namely float, double and uchar. */
    std::runtime_error("This function is not correctly implemented.");

    for (auto  it = map.begin(); it != map.end(); ++it) {
		this->m_file.open(m_path + "/" + m_sequenceName + ".h5", vigra::HDF5File::Open);
		if (!it->second.empty()){
			vigra::MultiArray<3, unsigned char> vMat(vigra::Shape3((int)it->second.channels(), (int)it->second.rows, (int)it->second.cols), (unsigned char*)it->second.data);
			this->m_file.write("/" + it->first.category + "/" + it->first.name + "/" + std::to_string(frameNumber), vMat, 0, 9);
			this->m_file.flushToDisk();
		}
	}
}
