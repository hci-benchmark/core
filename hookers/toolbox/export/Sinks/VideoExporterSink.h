#ifndef VIDEOEXPORTERSINK_H
#define VIDEOEXPORTERSINK_H

/**
 * @author     Mohsen Rahimimoghaddam, mohsen.r.moghadam@gmail.com
 * @date       2015
 * @brief	   Export views and maps of a frame in a sequence to seperate videos
 *
 * This class drived form ExporterSink class. 
 * @see ExporterSink class
 *
 * @warning    image sized for video is fixed (2560x1080) if your images has different size you need to modify it accordingly. 
 *            
 *
 * @todo Due  to the fact that vidoe created before exporter get the frames and information about size of the frame 
 *            currently size of the video is constant and it is defined based on the Bosch ground truth project. 
 * 
 */

#include"ExporterSink.h"
#include <opencv2/opencv.hpp>

namespace hookers {
namespace toolbox {


class VideoExporterSink : public ExporterSink{
	/*export a frame depth map as a frame of avi video
	*m_path is the address for saving video*/
	cv::Size m_size;
	std::map<ExporterSource::SourceIdentifier, cv::VideoWriter> m_videos;

public:
	/**@brief construct a video exporter object*/
	
	VideoExporterSink();

	/**@brief create empty avi videos
	*this function create empty videos in the address that sequence is located.
	*number of videos are based on the reqquested maps and views and named accordingly.
	*@param sources contain list of information about the data requested to convert to videos*/
    void setup(const ExporterSource::SourceIdentifierList & sources);

	/**@brief add frames to the videos
	*add single frame to each video (frames can be a map or views)
	*@param map is standard map of open cv matrix of the images should be added and information about them
	*@param framenumber is the number of frame which maps and views related to it in the sequence*/
    void push(const ExporterSource::SourceMap & map,int frameNumber);
};

}
}

#endif
