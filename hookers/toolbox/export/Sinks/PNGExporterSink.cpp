/*
Copyright (C) 2015 Mohsen Rahimimoghaddam, <mohsen.r.moghadam@gmail.com>

This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This software is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this software.  If not, see <http://www.gnu.org/licenses/>.
*/
#include"PNGExporterSink.h"
#include <iostream>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

using namespace hookers::toolbox;

PNGExporterSink::PNGExporterSink(){
}

void PNGExporterSink::setup(const ExporterSource::SourceIdentifierList & sources){
//	for (ExporterSource::SourceIdentifierList::iterator it = sources.begin(); it != sources.end(); ++it) {
//		this->m_folders[*it] = (m_path + "/" + m_sequenceName + "_PNG/");
//		mkdir(this->m_folders[*it].data());
//		this->m_folders[*it] = (m_path + "/" + m_sequenceName + "_PNG/" + it->category + it->name);
//		mkdir(this->m_folders[*it].data());
//		std::cout << this->m_folders[*it].data()<<std::endl;
//	}
	
}

void PNGExporterSink::push(const ExporterSource::SourceMap & map,int frameNumber){
    for (auto it = map.cbegin(); it != map.cend(); ++it) {
		if (!it->second.empty()){
            if (!(it->second.channels() == 1) && !(it->second.channels() == 3)){
				throw std::runtime_error(it->first.name + it->first.category + "of frame" + std::to_string(frameNumber) + "Does not have match number of channels (3 or 1)");
			}
			if (it->second.channels() == 1 && it->first.category == "map"){ /*Normalize data incase it is grayscale channel map*/
				cv::Mat adjMap, normedMap;
				cv::normalize(it->second, adjMap, 0, 1, cv::NORM_MINMAX);

				double min;
				double max;
				cv::minMaxIdx(adjMap, &min, &max);
				adjMap.convertTo(normedMap, CV_8UC1, 255 / (max - min), -min);
				cv::imwrite(this->m_folders[it->first] + "/" + std::to_string(frameNumber) + ".png", normedMap);
			}
			else{
				cv::imwrite(this->m_folders[it->first] + "/" + std::to_string(frameNumber) + ".png", it->second);
			}
		}
	}
}
