/*
Copyright (C) 2015 Mohsen Rahimimoghaddam, <mohsen.r.moghadam@gmail.com>

This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This software is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this software.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <iostream>
#include "VideoExporterSink.h"

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui_c.h>

using namespace hookers::toolbox;

VideoExporterSink::VideoExporterSink(){

}



void VideoExporterSink::setup(const ExporterSource::SourceIdentifierList & sources){
    for (auto it = sources.cbegin(); it != sources.cend(); ++it) {
			/** @todo implement a method to get real size of the slides */
			m_size = cv::Size(2560, 1080);
			std::cout << "video saved in \" " << m_path + "/" + m_sequenceName + it->category + it->name + ".avi" << "\"\n";
			this->m_videos[*it] = (cv::VideoWriter(m_path + "/" + m_sequenceName + it->category + it->name + ".avi",
				CV_FOURCC('M', 'J', 'P', 'G'), 30,
				m_size));
	}
}

void VideoExporterSink::push(const ExporterSource::SourceMap & map,int frameNumber=0){
	cv::Mat img;
    for (auto it = map.begin(); it != map.end(); ++it) {
        if (!(it->second.channels() == 1) && !(it->second.channels() == 3)){
			throw std::runtime_error(it->first.name + it->first.category + "of frame" + std::to_string(frameNumber) + "Does not have match number of channels (3 or 1)");
		}
		if (it->second.channels() == 1 && it->first.category == "map"){ /*Normalize data incase it is grayscale channel map*/
			cv::Mat adjMap, normedMap;
			cv::normalize(it->second, adjMap, 0, 1, cv::NORM_MINMAX);

			double min;
			double max;
			cv::minMaxIdx(adjMap, &min, &max);
			adjMap.convertTo(normedMap, CV_8UC1, 255 / (max - min), -min);
			cv::cvtColor(normedMap, img, CV_GRAY2RGB);
		}
		else if (it->second.empty()){/*if there is no data add a black frame */
			img = cv::Mat::zeros(this->m_size, CV_8UC3);
		}
		else if (it->second.type() == 0){/*since cv::videowrite is only works with three chanels matrix single chanels should be converted*/
			cv::cvtColor(it->second, img, CV_GRAY2RGB);
		}
		else{
			it->second.convertTo(img, CV_8UC3);
		}
		this->m_videos[it->first].write(img);
	}
}
