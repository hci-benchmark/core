#ifndef HOOKERS_TOOLBOX_MAPEXPORTER_SINK_H
#define HOOKERS_TOOLBOX_MAPEXPORTER_SINK_H

#include <hookers/interface/Sequence.h>

#include"ExporterSink.h"
#include <vigra/hdf5impex.hxx>
#include <vigra/multi_array.hxx>

namespace hookers {
namespace toolbox {

class MapExporterSink : public ExporterSink
{

    interface::FrameList frames;
public:
    MapExporterSink();

    void setFrames(interface::FrameList frames);

    void setup(const ExporterSource::SourceIdentifierList & sources);

    void push(const ExporterSource::SourceMap & map, int);

    void pushMeta(int frame);

    std::string getDataFilePath(const int frame);
	
};

}
}

#endif //HOOKERS_TOOLBOX_MAPEXPORTERSINK_H
