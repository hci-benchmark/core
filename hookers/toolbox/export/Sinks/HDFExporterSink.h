#ifndef HDFEXPORTERSINK_H
#define HDFEXPORTERSINK_H

/**
* @author     Mohsen Rahimimoghaddam, mohsen.r.moghadam@gmail.com
* @date       2015
* @brief	   Export views and maps of a frame in a sequence to seperate videos
*
* This class drived form ExporterSink class.
* @see ExporterSink class
*
* @warning    image sized for video is fixed (2560x1080) if your images has different size you need to modify it accordingly.
*
*
* @todo Due  to the fact that vidoe created before exporter get the frames and information about size of the frame
*            currently size of the video is constant and it is defined based on the Bosch ground truth project.
*
*/

#include"ExporterSink.h"
#include <vigra/hdf5impex.hxx>
#include <vigra/multi_array.hxx>


namespace hookers {
namespace toolbox {

class HDFExporterSink : public ExporterSink
{
	/*copy a frame depth map to the new HDF file
	*m_path is the address for saving HDF file
	*m_file is HDF file*/

	vigra::HDF5File m_file;

public:
	HDFExporterSink();

    void setup(const ExporterSource::SourceIdentifierList & sources);

    void push(const ExporterSource::SourceMap & map, int);
	
};

}
}
#endif
