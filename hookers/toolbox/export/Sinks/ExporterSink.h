#ifndef HOOKERS_TOOLS_EXPORTER_EXPORTERSINK_H
#define HOOKERS_TOOLS_EXPORTER_EXPORTERSINK_H




#include <map>
#include <vector>
#include <string>

#include <hookers/interface/Frame.h>

#include "../Sources/ExporterSource.h"

namespace hookers {
namespace toolbox {


class ExporterSink
{
public:
    ExporterSink() {}
	virtual ~ExporterSink() {}

    static std::string m_path;
    static std::string m_sequenceName;

    static std::string globalPath() {
        return m_path;
    }

    static std::string sequenceName() {
        return m_sequenceName;
    }

    static void setSequenceName(const std::string & name) {
        m_sequenceName = name;
    }

    virtual void setup(const ExporterSource::SourceIdentifierList & sources )=0;
    virtual void push(const ExporterSource::SourceMap & map, int)=0;

    virtual std::string getDataFilePath(const int frameNumber) {return "";}

    /** writes meta information from frame to sink target */
    virtual void pushMeta(int frame) {}
	
};

}
}
#endif // HOOKERS_TOOLS_EXPORTER_EXPORTERSINK_H

