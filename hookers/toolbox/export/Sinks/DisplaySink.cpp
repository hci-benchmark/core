#include "DisplaySink.h"
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/highgui/highgui_c.h>


using namespace hookers::toolbox;

DisplaySink::DisplaySink(){

}

DisplaySink::~DisplaySink()
{
    cv::waitKey(0);
}



void DisplaySink::setup(const ExporterSource::SourceIdentifierList & sources){
}

void DisplaySink::push(const ExporterSource::SourceMap & map, int frameNumber = 0){
    for (auto it = map.begin(); it != map.end(); ++it) {
        cv::namedWindow(it->first.path(), CV_WINDOW_NORMAL);

        if (!it->second.empty()){
            if (it->second.channels() == 1){
                cv::Mat adjMap, normedMap;
                cv::normalize(it->second, adjMap, 0, 255, cv::NORM_MINMAX);
                cv::imshow(it->first.path(), adjMap);
            }
            else if (it->second.channels() == 2) {

                /* two channel image is very likely a flow map.
                 * So we use HSV colorspace to represent it */
                cv::Mat flow;
                it->second.convertTo(flow, CV_32F);


                //extraxt x and y channels
                cv::Mat xy[2]; //X,Y
                cv::split(flow, xy);


                //calculate angle and magnitude
                cv::Mat magnitude, angle;
                cv::cartToPolar(xy[0], xy[1], magnitude, angle, true);

                cv::Mat nanMask = (magnitude!=magnitude) & (angle!=angle);

                //translate magnitude to range [0;1]
                double mag_max=0;
                cv::minMaxLoc(magnitude,
                              nullptr, &mag_max,
                              nullptr, nullptr,
                              ~nanMask);

                if(mag_max == 0)
                    mag_max = 1;

                magnitude *= 1.0 / mag_max;

//                magnitude.convertTo(magnitude, -1, 1.0 / mag_max);


                //build hsv image
                cv::Mat _hsv[3], hsv;

                _hsv[0] = angle.clone();
                _hsv[0].setTo(0.0, nanMask);

                _hsv[1] = magnitude.clone();
                _hsv[1].setTo(0.0, nanMask);

                _hsv[2] = cv::Mat::ones(magnitude.size(), CV_32FC1);
                _hsv[2].setTo(0.0, nanMask);
                cv::merge(_hsv, 3, hsv);

                std::cout << it->first.path() << ": " << mag_max << std::endl;


                double min, max;
                cv::minMaxLoc(magnitude, &min, &max,
                              nullptr, nullptr,
                              ~nanMask);

                std::cout << "mean: " << cv::mean(magnitude,~nanMask) << ", " << min << ", " << max << std::endl;


                //convert to BGR and show
                cv::Mat bgr;//CV_32FC3 matrix
                cv::cvtColor(hsv, bgr, cv::COLOR_HSV2BGR);
                cv::imshow(it->first.path(), bgr);

            }
            else if (it->second.channels() == 3){
                cv::imshow(it->first.path(), it->second);
            }
            else{
                std::cout << "number  of channels" << it->second.channels() << std::endl;
                throw std::runtime_error(it->first.path() +" of frame " + std::to_string(frameNumber)
                                         + " does not have valid number of channels (1-3)");
            }
        }
        cv::waitKey(25);
	}
}
