#include "MapExporterSink.h"

#include <vigra/hdf5impex.hxx>

using namespace hookers::interface;
using namespace hookers::toolbox;

MapExporterSink::MapExporterSink()
{
}

void MapExporterSink::setFrames(FrameList frames)
{
    this->frames = frames;
}

void MapExporterSink::setup(const ExporterSource::SourceIdentifierList & sources){


}

void MapExporterSink::push(const ExporterSource::SourceMap & map, int frameNumber) {


    for (auto it = map.begin(); it != map.end(); ++it) {


        cv::Mat floatCnv;
        it->second.convertTo(floatCnv, CV_32F);


        if (!it->second.empty()){
            vigra::MultiArray<3, float> vMat(vigra::Shape3((int)floatCnv.channels(),
                                                           (int)floatCnv.cols,
                                                           (int)floatCnv.rows
                                                           ),
                                             (float*)floatCnv.data);
            frames[frameNumber].setMap(it->first.category + "/" + it->first.name , vMat);
        }
    }
}


std::string MapExporterSink::getDataFilePath(const int frameNumber)
{
    if(frameNumber >= (int)frames.size())
        return "";
    return frames[frameNumber].getDataFilePath();
}

void MapExporterSink::pushMeta(int frame)
{
    frames[frame].setMap();
}

