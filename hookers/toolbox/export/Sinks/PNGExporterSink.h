#ifndef PNGEXPORTERSINK_H
#define PNGEXPORTERSINK_H

/**
* @author     Mohsen Rahimimoghaddam, mohsen.r.moghadam@gmail.com
* @date       2015
* @brief save views and maps of a frame in a sequence to separte folader as png images.
*
* This class drived form ExporterSink class.
* @see ExporterSink class
*
*/

#include"ExporterSink.h"

#include <cv.h>
#include <highgui.h>

namespace hookers {
namespace toolbox {

class PNGExporterSink : public ExporterSink{
	
	std::map<ExporterSource::SourceIdentifier, std::string >  m_folders;

public:
	/**@brief construct a PNG image exporter object*/
	PNGExporterSink();

	/**@brief create new folders 
	*this function create folders in the address that sequence is located.
	*number of folders are based on the requested maps and views, and named accordingly.
	*@param sources contain list of information about the data requested to convert to videos*/
    void setup(const ExporterSource::SourceIdentifierList & sources);

	/**@brief export a data as an png image
	*@param map is standard map of open cv matrix of the images should be added and information about them
	*@param framenumber is the number of frame which maps and views related to it in the sequence*/
    void push(const ExporterSource::SourceMap & map, int framenumber);
};

}
}

#endif
