#include <iostream>

#include "OpenGLSampling.h"



using namespace hookers::toolbox;

OpenGLSampling::OpenGLSampling(sampling::opengl::Projector &projector, int N)
    : cloudUncertainty(1.0), m_projector(projector),
      C(1), N(N), N_hist(11),
      hasSample(false),
      disableSampling(false)
{
}

void OpenGLSampling::getSamplingDensity(OpenGLSampling::SamplingDensityView target)
{

    if(!target.data() || needsUpdate<NotSampled>(target.shape()))
        throw std::runtime_error("FlowSampling::getFlow(): Has not sampled before");

    target = result.count;
    target /= N;
}


void OpenGLSampling::getFlow(FlowFieldView target)
{

    if(!target.data() || needsUpdate<NotSampled>(target.shape()))
        throw std::runtime_error("FlowSampling::getFlow(): Has not sampled before");

    target = Vector2f(std::numeric_limits<float>::signaling_NaN());

    for(int xx = 0; xx < target.shape(0); xx++) {
        for(int yy = 0; yy < target.shape(1); yy++) {

            if(!hasPixelSamples({xx, yy}))
                    continue;

            auto flow = percentile({xx, yy}, 0.5);

//            FlowEntry::value_type variance;
//            FlowEntry flow;
//            for(int c = 0; c < FlowEntry::static_size; c++) {
//                result.storage.bindOuter(yy).bindOuter(xx).bindOuter(c).meanVariance(&flow[c], &variance);
//            }

            target(xx,yy) = Vector2f(flow[0], flow[1]);
        }
    }


}

void OpenGLSampling::getFlowUncertainty(OpenGLSampling::FlowFieldView target)
{
    if(!target.data() || needsUpdate<NotSampled>(target.shape()))
        throw std::runtime_error("FlowSampling::getFlow(): Has not sampled before");

    target = Vector2f(std::numeric_limits<float>::signaling_NaN());

    for(int xx = 0; xx < target.shape(0); xx++) {
        for(int yy = 0; yy < target.shape(1); yy++) {


            if(!hasPixelSamples({xx, yy}))
                    continue;

//            auto mean25 = percentile({xx, yy}, 0.25);
//            auto mean75 = percentile({xx, yy}, 0.75);
//            auto unc = (mean75-mean25)/2.0;
            auto unc = stddev({xx, yy});
            target(xx,yy) = Vector2f(unc[0], unc[1]);
        }
    }
}

