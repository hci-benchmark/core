#ifndef HOOKERS_TOOLS_GROUNDTRUTHERSOURCE_MAP_H
#define HOOKERS_TOOLS_GROUNDTRUTHERSOURCE_MAP_H


#include <map>
#include <unordered_map>
#include <iostream>

#include <opencv2/core.hpp>
#include <opencv2/highgui/highgui.hpp>

// Hookers Interface
#include <hookers/interface/Geometry.h>

// Hookers Toolbox
#include <hookers/toolbox/export/Sources/ExporterSource.h>
#include <hookers/toolbox/Projection/Projection.h>
#include <hookers/toolbox/Sampling/StereoSampling.h>
#include <hookers/toolbox/Sampling/OpenGLSampling.h>


namespace hookers {
namespace toolbox {


/**
 * Pipeline object that finally creates groundtruth
 * on the fly.
 *
 * The flow steps are by default [1,8] frames from
 * target frame away.
 */
class GroundtruthSource : public ExporterSource
{

    typedef hookers::toolbox::OpenGLSampling Sampling;

    /** stores the number of relative target frames for flow */
    std::vector<int> m_flowTargets;

    /** projection instance */
    hookers::toolbox::Projection * m_projection;

    /** shared projector for OpenGLSampling Objects */
    hookers::toolbox::sampling::opengl::Projector projector;

    /** sampler object for stereo */
    Sampling samplingStereo;

    /** sampler object for flow */
    Sampling samplingFlow;


    /**
     * specifies whether we want to take
     * stored pose or whether we want to
     * estimate per frame */
    bool m_estimatePose = true;


    /** sample count */
    int m_samples = 1;


    interface::Geometry m_g;
    Vector3fArray m_cloud;

    std::unordered_map<interface::Frame, std::tuple<interface::Frame, Vector6d, Vector6d, Array2d>> covariances;



public:
    GroundtruthSource(hookers::toolbox::Projection * p = 0);

    void setProjector(hookers::toolbox::Projection * p) {
        if(!p) {
            throw std::runtime_error("GroundtruthSource::setProjector(): No valid projector instance provided.");
        }
        m_projection = p;
    }

    /** sets the flow targets. Number is given relative to
     * target frame */
    void setFlowTargets(const std::vector<int> &targets) {
        m_flowTargets = targets;
    }

    /** @see setFlowTargets() */
    std::vector<int> flowTargets()
    {
         return m_flowTargets;
    }

    void setSampleCount(int samples) {
        samplingStereo.setSampleCount(samples);
        samplingFlow.setSampleCount(samples);
    }
    
    void setDisableSampling(bool newValue) {
        samplingStereo.setDisableSampling(newValue);
        samplingFlow.setDisableSampling(newValue);
    }

    void enablePoseEstimation(bool estimatePose)
    {
        m_estimatePose = estimatePose;
    }

    bool estimatePose() const
    {
        return m_estimatePose;
    }

    void setHistogramSize(int binCount) {
        samplingStereo.setHistogramSize(binCount);
        samplingFlow.setHistogramSize(binCount);
    }

    /**
     * @brief provide manual poses and covariance matrix for frame pairs
     * @param f1 first frame
     * @param p1 pose for first frame
     * @param f2 second frame
     * @param p2 pose for second frame
     * @param cov covariance matrix. Either 6x6 if !f2.isValid() or 12x12 for both frames
     *            defaults to empty matrix, denoting no uncertainty.
     */
    void addFramePoses(interface::Frame f1, Vector6d p1,
                       interface::Frame f2, Vector6d p2,
                       Array2dView cov = Array2dView());

    /** @overload */
    void addFramePoses(interface::Frame f1, Vector6d p1,
                       Array2dView cov = Array2dView()) {
        addFramePoses(f1, p1, interface::Frame(), Vector6d(), cov);
    }


    SourceIdentifierList provides() const;


    /** calculates the groundtruth maps.
     *
     * It is
     * - disp/map
     * - disp/uncertainty
     * - disp/count
     *
     * - flow/N/map
     * - flow/N/uncertainty
     * - flow/N/count
     *
     * where disp is the stereo disparity map
     * and flow/N/ is the flow map for respective
     * steppings N
     */
    SourceMap sources(interface::Frame f);


private:

    void getDepth(Sampling::FlowFieldView map, Sampling::FlowFieldView uncertainty, Sampling::SamplingDensityView count);
    void getFlow(int step, Sampling::FlowFieldView map, Sampling::FlowFieldView uncertainty, Sampling::SamplingDensityView count);
};

} // ns toolbox
} // ns hookers

#endif // HOOKERS_TOOLS_GROUNDTRUTHERSOURCE_MAP_H
