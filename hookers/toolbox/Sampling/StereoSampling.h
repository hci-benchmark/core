#ifndef HOOKERS_TOOLBOX_STEREOSAMPLING_H
#define HOOKERS_TOOLBOX_STEREOSAMPLING_H

#include <typeinfo>
#include <tuple>
#include <functional>
#include <thread>

#include <assert.h>


#include <vigra/random.hxx>
#include <vigra/linear_algebra.hxx>
#include <vigra/accumulator.hxx>
#include <vigra/linear_algebra.hxx>
#include <vigra/timing.hxx>

// hookers includes
#include <hookers/interface/NumericTypes.h>
#include <hookers/toolbox/Projection/Projection.h>


namespace hookers {
namespace toolbox {


/**
 * @brief The FlowSampling class
 *
 * Calculates dense optical flow from one frame to another using a reference pointcloud.
 * The calculation will be done using monte carlo simulation, thereby taking
 * the uncertainties of camera intrinsics, extrinsics and the pointcloud into account.
 *
 * The cloud will be set during class initialization.
 *
 * The intrinsics and camera poses can be set independently by using setIntrinsics() and setPose().
 *
 * The sampling will be triggered by calling sample() and by this providing a target image size.
 * The different results like depth map, occlusion maps and flow map can then be fetched using
 * the corresponding getter. The sampling can be adjusted by providing the corresponding sampling object.
 * StereoSampler and FlowSampler for this case.
 *
 * The difference of flow calculation will be chosen such that a flow vector points from
 * camera 1 to camera 2.
 *
 */
class FlowSampling
{
public:
    /** left pose, right pose, left intrinsics, right intrinsics */
    typedef std::tuple<Vector6d, Vector6d, Vector4d, Vector4d> SamplerReturn;

    /** helper struct */
    struct NotSampled {};

public:

    const static unsigned N_features = 3;
    typedef vigra::TinyVector<double,N_features> FlowEntry;
    typedef vigra::TinyVector<int,N_features> FlowIndex;




    /**
     * This is the base class for different scalings of BaseHistogram class.
     *
     * The functions physicalToScaled and scaledToPhysical represent the
     * mappings from a real world value to the rescaled version and
     * its respective inverse mapping.
     *
     * scaledToLogical and logicalToScaled are the respective mappings
     * that map from the scaled units to logical. The axis scaler
     * classes are meant to map to histogram bins.
     *
     * So the mapping of scaledToPhysical() is meant to map
     * @f$ \mathbb{R} \arrow [0,1] @f$ where 0 corresponds to
     * the left boundary of the zero's bin and 1 corresponds to the right
     * boundary of the highest histogram bin.
     *
     * @note in order to have a true zero bin, make sure the histogram as an odd number
     *       of bins.
     */
    template<unsigned N, typename T = double>
    struct IdentityAxis {

        typedef vigra::TinyVector<T,N> Feature;

        const static unsigned N_features = N;

        Feature min, max, delta;

        IdentityAxis(const Feature & min, const Feature & max)
            : min(min), max(max), delta(max-min)
        {}

        IdentityAxis(const T & min, const T & max)
            : IdentityAxis(Feature(min), Feature(max))
        {}


        /** computes from physical value the axis scaling */
        const vigra::TinyVector<T,N> physicalToScaled(const vigra::TinyVector<T,N> & value) const
        {
            return value;
        }

        /**
         * computes the inverse axis scaling,
         * as to get back from logical to scaled to physical.
         */
        const vigra::TinyVector<T,N> scaledToPhysical(const vigra::TinyVector<T,N> & forward) const
        {
            return forward;
        }

        /** computes the logical coordinate given the physical value after scaling.
         *
         * @returns logical space coordinates.
         */
        const vigra::TinyVector<T,N> scaledToLogical(const vigra::TinyVector<T,N> & forward) const {
            return (forward-min)/delta;
        }

        /** computes the physical value after scaling given the logical coordinate.
         *
         * @arg logical logical coordinates @f$ logical_i \in [0,1] @f$
         */
        const vigra::TinyVector<T,N> logicalToScaled(const vigra::TinyVector<T,N> & logical) const {
            return min+delta*logical;
        }
    };


    template<int N, typename T>
    static const vigra::TinyVector<T,N> exp(const vigra::TinyVector<T,N> & x) {
        vigra::TinyVector<T,N> res;
        for(unsigned i = 0; i < N; i++) {
            res[i] = std::exp(x[i]);
        }
        return res;
    }

    template<int N, typename T>
    static const vigra::TinyVector<T,N> log(const vigra::TinyVector<T,N> & x) {
        vigra::TinyVector<T,N> res;
        for(unsigned i = 0; i < N; i++) {
            res[i] = std::log(x[i]);
        }
        return res;
    }

    template<int N, typename T>
    static const vigra::TinyVector<T,N> sign(const vigra::TinyVector<T,N> & x) {
        vigra::TinyVector<T,N> res;
        for(unsigned i = 0; i < N; i++) {
            res[i] = vigra::sign(x[i]);
        }
        return res;
    }


    /**
     *
     * This class implements a logarithmic axis scaling for BaseHistogram
     *
     * The scaling is symmetrical around zero with
     *
     * @f$ LogAxis(0) = 0, LogAxis(max) = max @f$ and scales the values logarithmic inbetween.
     */
    template<int N, typename T = double>
    struct LogAxis: public IdentityAxis<N,T> {

        typedef typename IdentityAxis<N,T>::Feature Feature;
        Feature b;

        LogAxis(const T & min = 0, const T & max = 1)
            : LogAxis(Feature(min), Feature(max))
        {}

        LogAxis(const Feature & min, const Feature & max)
            : IdentityAxis<N,T>(min, max)
        {
//            assert(max > min);
            const Feature absmax = vigra::max(vigra::abs(min), vigra::abs(max));
            b = absmax/log(absmax+1.0);
        }

        /** computes the axis scaling in forward direction. Eg. from physical
         * to logical units */
        const vigra::TinyVector<T,N> physicalToScaled(const vigra::TinyVector<T,N> & value) const
        {
            return b*log(vigra::abs(value)+1.0)*sign(value);
        }

        /**
         * computes the axis scaling in backward direction, eg. from logical
         * to physical units */
        const vigra::TinyVector<T,N> scaledToPhysical(const vigra::TinyVector<T,N> & index) const
        {
            typedef vigra::TinyVector<T,N> TV;
            return (exp((1.0/b)*vigra::abs(index))-T(1.0))*sign(index);
        }


    };



    /**
     * Base histogram class.
     *
     * The axis scaling can be adjusted by supplying the
     * Scaler type. See IdentityAxis and LogAxis.
     *
     * The bin count of the histogram can be adjusted by the constructor.
     * There you also can provide the configured scaler instance.
     * The scaler instance defines the histogram boundaries and the axis scales.
     *
     * You can put a new entry in the histogram via place(). Resetting the histogram
     * is possible via reset().
     *
     *
     * Statistics can be retrieved using percentile() and the respective
     * shortcuts median(), inter-quartile-range IQR() and stddev().
     *
     * If the underlying sampled distribution resembles a normal distribution then
     *   mean = median()
     * and
     *   stddev = stddev().
     *
     *
     * The following example
     * @code
     * hist = BaseHistogram<IdentityAxis<3,double>>(1001, IdentityAxis(0,50))
     *
     * // place a new entry in the histogram
     * hist.place({20,30,40});
     * hist.place({40,30,20});
     *
     * std::cout << "median, IQR: " << hist.median() << ", " << hist.stddev() << std::endl;
     * @code
     *
     * creates a Histogram with linear scaling and the lower bin boundary at
     * 0 and upper boundary at 50, with an value type of a three dimensional double valued vector.
     * Two values are added to the histogram and the median and IQR are printed to
     * command line.
     *
     * The rest of the functions are helper functions used internally.
     */
    template<typename Scaler,unsigned N=1>
    struct BaseHistogram {

        static const unsigned N_DDim = 2;



        typedef vigra::MultiArray<N+N_DDim,double> DataType;
        /** this is the Shape of the internal Data storage */
        typedef typename DataType::difference_type DataShape;

        /** this type is used to index the outer shape of the histogram.
         *  eg, if you want to create a pixel-wise histogram, you would
         *  set BaseHistogram Template N=2, so this one would index a pixel. */
        typedef typename vigra::MultiArray<N,double>::difference_type HistShape;

        DataType hist;
        unsigned int size;

        Scaler s;

        HistShape m_shape;

        BaseHistogram()
        {}

        BaseHistogram(int size, const Scaler & s, const HistShape & hshape = HistShape(1))
            : size(size), s(s), m_shape(hshape)
        {
            //hist({N_features, size},0.0);
            DataShape shape;

            shape[0] = N_features;
            shape[1] = size;

            for(unsigned i = DataShape::static_size; i >= N_DDim; i--) {
                shape[i] = hshape[i-N_DDim];
            }

            hist.reshape(shape);
        }

        BaseHistogram(const BaseHistogram & other)
            : size(other.size), hist(other.hist), s(other.s)
        {}


        HistShape shape() const {
            return m_shape;
        }

        void setScaler(const Scaler & scaler ) {
            s = scaler;
        }

        FlowIndex getIndex(const FlowEntry & entry) const {
            auto res = FlowIndex(s.scaledToLogical(s.physicalToScaled(entry))*size);
            return vigra::max(vigra::min(FlowIndex(size-1), res), FlowIndex(0));
        }


        FlowEntry getBoundary(const FlowIndex & index) const {
            return s.scaledToPhysical(s.logicalToScaled(FlowEntry(index)/(size)));
        }


        FlowEntry getCenter(const FlowIndex & index) const {
            auto delta = FlowEntry(1.0/double(size))/2.0;
            return s.scaledToPhysical(s.logicalToScaled(FlowEntry(index)/double(size))+delta);
        }

        void place(const FlowEntry &flow, const HistShape & outerIndex = HistShape()) {
            const FlowIndex & index = getIndex(flow);
            for(unsigned i = 0; i < N_features; i++) {
                hist.bindOuter(outerIndex)(i, index[i])++;
            }
        }

        void reset() {
            hist = 0.0;
        }

        double count(const HistShape &index = HistShape()) const {

            /* hack because some strange compiler error would occour if we directly would apply
             * sum() to the result of the following call */
            const vigra::MultiArrayView<N_DDim-1,double> tmp = hist.bindOuter(index).bindInner(0);
            return tmp.sum<double>();
        }

        inline vigra::MultiArray<2,double> boundaries() const {
            auto boundaries = vigra::MultiArray<2,double>({N_features, size+1},0.0);
            for(int i = 0; i <= size; i++) {
                auto b = getBoundary(i);
                    for(int j = 0; j < N_features; j++) {
                    boundaries(j,i) = b[j];
                }
            }

            return boundaries;
        }

        inline vigra::MultiArray<2,double> centers() const {
            auto centers = vigra::MultiArray<2,double>({N_features, size},0.0);
            for(unsigned i = 0; i < size; i++) {
                const auto b = getCenter(vigra::TinyVector<unsigned,N_features>(i));
                    for(int j = 0; j < N_features; j++) {
                    centers(j,i) = b[j];
                }
            }

            return centers;
        }


        /** calculates median from histogram */
        inline FlowEntry median(const HistShape & index = HistShape()) const {
            return percentile(0.5,index);
        }

        /** inter quartile range */
        FlowEntry IQR(const HistShape & index = HistShape()) const {
            return percentile(0.75,index)-percentile(0.25,index);
        }

        /** returns the standard deviation of
         * the sampled values in histogram
         *
         * @warning this is only correct if the sampled values are normal distributed
         */
        FlowEntry stddev(const HistShape & index = HistShape()) const {
            return (percentile(0.5+0.5*0.683,index)-percentile(0.5-0.5*0.683,index))/2.0;
        }


        /**
         * calculates the pecentile for given histogram.
         */
        FlowEntry percentile(const double & q, const HistShape & index = HistShape()) const {


            /* stores the logical percentile position */
            FlowEntry p(1.0);


            /* go through each feature dimension and
             * find the required percentile */
            for(unsigned f = 0; f < N_features; f++) {

                vigra::MultiArrayView<1,double> lhist = hist.bindOuter(index).bindInner(f);

                double total = lhist.sum<double>();
                double count = 0;

                for(unsigned i = 0; i < size; i++) {
                    count += lhist(i);

                    const auto current_q = count/total;

                    /* first check if the current bin does still not
                     * fall inside the requested quantile */
                    if(current_q < q) {
                        continue;
                    }

                    double b = i/double(size);
                    p[f] = b;
                    break;
                }
            }

            return s.scaledToPhysical(s.logicalToScaled(p));
        }

        std::vector<FlowEntry> percentile(const std::vector<double> & percentiles) {
            return std::vector<FlowEntry>();
        }

    };

    typedef BaseHistogram<LogAxis<3,double>,2> Histogram;

    /**
     * @brief The StereoSampler struct
     *
     * This function samples only the first camera position completly.
     * The second pose will then be
     *
     * p2 = p1^S + (p2-p1) + N(0, \Sigma_2)
     *
     * Both intrinsics will be sampled independently
     */
    struct StereoSampler {

        Vector6d p1;
        Vector6d p2;
        Vector4d intrinsics;

        Array2d poseUncertainty;
        Array2d intrinsicsUncertainty;

        vigra::RandomNumberGenerator<> RNG;

        StereoSampler(const Vector6d & p1,
                      const Vector6d & p2,
                      const Array2d  & p1Uncertainty,
                      const Array2d  & p2Uncertainty,
                      const Vector4d & intrinsics,
                      const Array2d  & intrinsicsUncertainty)
            : p1(p1), p2(p2),
              intrinsics(intrinsics)
        {
            this->intrinsicsUncertainty = decomposeUncertainty(intrinsicsUncertainty);
            this->poseUncertainty = decomposeUncertainty(p1Uncertainty);
        }

        SamplerReturn sample() {
            auto poseLeft = randomVariable(p1, poseUncertainty, RNG);

            /** @todo add baseline uncertainty */
            auto poseRight = poseLeft + (p2-p1);

            auto intrins = randomVariable(intrinsics, intrinsicsUncertainty, RNG);

            return std::make_tuple (
                        poseLeft,
                        poseRight,
                        intrins,
                        intrins
                    );
        }

        SamplerReturn base() {
            auto poseLeft = p1;
            auto poseRight = poseLeft + (p2-p1);
            return std::make_tuple (
                        poseLeft,
                        poseRight,
                        intrinsics,
                        intrinsics
                    );
        }


    };

    /**
     * @brief Samples two camera poses given by correlation
     *
     * This sampler expects a Correlation Matrix for both poses.
     * In this case the correlation matrix has the shape of 12x12,
     * where the upper left 6x6 block corresponds to the unary covariance
     * matrix of the first pose, the lower right 6x6 block to the second pose
     * respectively.
     * The off-diagonal blocks correpond to the covariance between two camera poses.
     * If the these correlations are unknown, they can be neglected by setting them to
     * zero. This leads to two independently sampled camera poses.
     */
    struct CorrelatedSampler {

        Vector6d p1;
        Vector6d p2;
        Vector4d intrinsics;

        Array2d poseUncertainty;
        Array2d intrinsicsUncertainty;

        vigra::RandomNumberGenerator<> RNG;

        /**
         * @brief CorrelatedSampler
         * @param p1 first pose
         * @param p2 second pose
         * @param posesUncertainties 12x12 covariance matrix of poses
         * @param intrinsics camera intrinsics of the form (fx,fy,cx,cy)
         * @param intrinsicsUncertainties 4x4 covariance matrix of intrinsics.
         */
        CorrelatedSampler(const Vector6d & p1,
                          const Vector6d & p2,
                          const Array2d  & posesUncertainties,
                          const Vector4d & intrinsics,
                          const Array2d  & intrinsicsUncertainties)
            : p1(p1), p2(p2), intrinsics(intrinsics)
        {
            /* decompose the covariance matrices */
            poseUncertainty = decomposeUncertainty(posesUncertainties);
            intrinsicsUncertainty = decomposeUncertainty(intrinsicsUncertainties);
        }

        SamplerReturn sample() {
            vigra::TinyVector<double, 12> X;
            X.subarray<0, 6>() = p1;
            X.subarray<6, 12>() = p2;
            auto poses = randomVariable(X, poseUncertainty, RNG);

            auto intr = randomVariable(intrinsics, intrinsicsUncertainty, RNG);

            return std::make_tuple (
                        poses.subarray<0, 6>(),
                        poses.subarray<6, 12>(),
                        intr,
                        intr
                    );
        }

        SamplerReturn base() {
            return std::make_tuple (
                        p1,
                        p2,
                        intrinsics,
                        intrinsics
                    );
        }


    };


public:
    typedef vigra::MultiArray<2,Vector2f> FlowField;
    typedef vigra::MultiArrayView<2,Vector2f> FlowFieldView;

    typedef vigra::MultiArray<2,float> Count;
    typedef vigra::MultiArrayView<2,float> CountView;

    typedef vigra::MultiArray<2,bool> Occlusion;
    typedef vigra::MultiArrayView<2,bool> OcclusionView;

private:

    typedef SamplerReturn(*SamplerCallType)();

    Vector3fArrayView cloud;
    double cloudUncertainty;


    /** number of sampling passes */
    int N;

    /** number of bins in pixel histograms */
    int N_hist;

    /** saves the address to the sampler that was used to
     * samples random numbers. Is required to distinguish
     * whether we need to recalculate */
    bool hasSample;

    /** disable the sampling process altogether */
    bool disableSampling;

    typedef vigra::MultiArray<2, vigra::FindMinMax<Vector3d>> FlowMinMax;
    typedef Histogram FlowHist;

    FlowHist stereoFlowHist;





public:

    FlowSampling(Vector3fArrayView cloud = Vector3fArrayView(), int N = 10);


    void setCloud(Vector3fArrayView cloud) {
        this->cloud.swap(cloud);
        this->resetSampler();
    }

    void setSampleCount(int count) {
        N = count;
        this->resetSampler();
    }
    void setDisableSampling(bool newValue = false) {
        this->disableSampling = newValue;
        this->resetSampler();
    }
    void setHistogramSize(int bincount) {
        N_hist = bincount;
        this->resetSampler();
    }


    /**
     * @brief Handles Projection on callback basis
     *
     * This Projection functor can be used to
     * calculate the projections of a pointcloud.
     * But allows to put in a lambda callback handler
     * for each reprojection.
     *
     * This can be handy to use the same projection code for different
     * accumulators. Because it's templated the compiler
     * can inline the lambda without loss of performance.
     */
    struct ProjectionFunctor {
        Vector6d poseLeft, poseRight;
        Vector4d intrinsicsLeft, intrinsicsRight;

        const std::vector<Vector3fArrayView> clouds;

        ProjectionFunctor(const std::vector<Vector3fArrayView> clouds)
            :clouds(clouds) {}

        template<typename C>
        void operator() ( const vigra::Rect2D & view, const C& f) const {


//            USETICTOC;

//            TIC;
            /* loop over point cloud */
            //for(const auto & wpf: c){
            for(int i = 0; i < clouds[0].size(); i++) {

                const auto &pLeft = clouds[0](i);
                const auto &pRight = clouds[1](i);

                /* and check if point is projected inside view */
                int px = pLeft[0], py = pLeft[1];
                if(!view.contains({px, py}))
                    continue;

                /* calculate flow components */
                const double u = pRight[0] - pLeft[0];
                const double v = pRight[1] - pLeft[1];

                /* store the resulting flow and depth information */
                f(px, py, Vector3d(u, v, pLeft[2]));
            }
//            TOC;


        }
    };


    /**
     * runs the monte-carlo simulation
     * Template paramter is the sampler Type
     * that is used for sampling the different camera parameters.
     *
     * Have look at StereoSampler and CorrelatedSampler
     *
     * required interface:
     *
     * SamplerReturn sample(): returns randomly sampled parameters
     * SamplerReturn base(): returns paramters without random components
     *
     *
     */
    template<typename T>
    void sample(Array2Shape size, T & sampler = T())
    {

        if(!cloud.data())
            throw std::runtime_error("FlowSampling: no cloud set");

        const int WIDTH = size[0];
        const int HEIGHT = size[1];


        /**
         * Before the actual sampling process we estimate the histogram boundaries
         * by calculating all values with unperturbed camera poses. */
        vigra::FindMinMax<double> globalMinMax[3];
        {
            Vector6d p0, p1;
            Vector4d in0, in1;

            std::tie(p0, p1, in0, in1) = sampler.base();

            Vector3fArray leftCloud = cloud;
            Projection::project3d(leftCloud, p0, in0);

            Vector3fArray rightCloud = cloud;
            Projection::project3d(rightCloud, p1, in1);

            std::vector<Vector3fArrayView> clouds{leftCloud, rightCloud};

            ProjectionFunctor proj(clouds);
            proj(vigra::Rect2D(vigra::Size2D{WIDTH, HEIGHT}),
                 [&](const int &x, const int &y, const FlowEntry&p){
                globalMinMax[0](p[0]);
                globalMinMax[1](p[1]);
                globalMinMax[2](p[2]);
            });
        }

        FlowEntry minFlow(globalMinMax[0].min, globalMinMax[1].min, globalMinMax[2].min);
        FlowEntry maxFlow(globalMinMax[0].max, globalMinMax[1].max, globalMinMax[2].max);

        /* make sure that delta is not null.
         * this would make trouble later when setting up the histograms */
        FlowEntry deltaFlow = vigra::abs(maxFlow-minFlow);
        deltaFlow = vigra::max(deltaFlow, FlowEntry(1));

        /**
         * For the boundaries of the histogram bins
         * we assume that the values will not deviate more than 25% of
         * the min and max values found by unperturbed sampling process.
         * So 25% is our crumple zone */
        minFlow -= 0.25*deltaFlow;
        maxFlow += 0.25*deltaFlow;

        LogAxis<3,double> scaler(minFlow, maxFlow);

        if(stereoFlowHist.shape() != size) {
            stereoFlowHist = Histogram(N_hist, scaler,size);
        } else {
            stereoFlowHist.setScaler(scaler);
            stereoFlowHist.reset();
        }


        /* this lambda is used as thread function */
        auto samplingFunctor = [&](const vigra::Rect2D view,
                                   const std::vector<Vector3fArrayView> &clouds) {

            /* create projection instance */
            ProjectionFunctor proj(clouds);

            /* reproject all points.
                 * by this call the point callback that puts results
                 * into histograms */
            proj(view,
                 [&](const int &x, const int &y, const FlowEntry&p){
                stereoFlowHist.place(p,{x,y});
            });
        };

        /* construct a sampler instance that samples the random variable */
//        T sampler(*this);

        /* find number of columns that divide image equidistantly.
         * Worst case is one column and hence no parallelism */
        int N_threads = std::thread::hardware_concurrency();
        for(; N_threads > 1 && WIDTH%N_threads; N_threads--);
        std::cout << "parallel computing: " << N_threads << std::endl;
        const int COL_WIDTH = WIDTH/N_threads;


        Vector3fArray leftCloud = cloud;
        Vector3fArray rightCloud = cloud;


        /* sample loop */
        for(int i = 0; i < N; i++) {

            auto sr = this->disableSampling?sampler.base(): sampler.sample();


            Vector6d poseLeft, poseRight;
            Vector4d intrinsicsLeft, intrinsicsRight;
            std::tie(poseLeft, poseRight, intrinsicsLeft, intrinsicsRight) = sr;

            /* reproject the pointcloud.
             * use two threads for each reprojection */
            leftCloud = cloud;
            rightCloud = cloud;
            std::vector<Vector3fArrayView> clouds{leftCloud, rightCloud};
            {
                auto t1 = std::thread([&]{Projection::project3d(leftCloud, poseLeft, intrinsicsLeft);});
                auto t2 = std::thread([&]{Projection::project3d(rightCloud, poseRight, intrinsicsRight);});
                t1.join();
                t2.join();
            }


            /* now run the sampling. We split up the target array
             * into columns. For each column one thread is responsible
             * using premature exits to speed up binning */
            std::vector<std::thread> threads;
            for(int i = 0; i < N_threads; i++) {
                vigra::Rect2D view(i*COL_WIDTH, 0, (i+1)*COL_WIDTH,HEIGHT);
                threads.push_back(std::thread(samplingFunctor, view, clouds));
            }

            for(size_t i = 0; i < threads.size(); i++) threads[i].join();
        }

        /* saves sampler call */
        hasSample = true;

    }

    void getCount(CountView target);
    void getOcclusionMask(OcclusionView target);

    void getFlow(FlowFieldView target);
    void getFlowUncertainty(FlowFieldView target);



    /**
     * creates a gaussian distributed random vector.
     * The distribution is parametrized via @f$ A @f$ which is the choleski decomposition
     * of the covariance matrix @f$ \Sigma @f$
     */
    template<typename T, int N, class RNG>
    static vigra::TinyVector<T, N> randomVariable(const vigra::TinyVector<T, N> & mu,
                                                  const vigra::MultiArrayView<2,T> A, RNG& rng)
    {

        using namespace vigra::linalg;
        typedef vigra::TinyVector<T, N> RV;

        static_assert(A.actual_dimension == 2, "Given covariance matrix must have dimension of 2!");
        if(A.shape(0) != A.shape(1)) {
            throw std::runtime_error("FlowSampler::randomVariable(): Shape of covariance decomposition A is not quadratic.");
        }

        if(A.shape(0) != N) {
            throw std::runtime_error("FlowSampler::randomVariable(): Shape of covariance decomposition A has not same size as N");
        }

        RV stdNormalX;
        for(auto &it: stdNormalX) {
            it = rng.normal();
        }


        return mu + vigra::linalg::Matrix<T>(A)*stdNormalX;
    }

    template<typename T>
    static vigra::MultiArray<2,T> decomposeUncertainty(vigra::MultiArrayView<2,T> A) {

        Array2d unc_decomposed(A.shape());

        /* calculate cholesky decomposition.
         * This is required for random sampling */
        {
            /* this makes sure that the matrix is symmetric */
            using namespace vigra::multi_math;
            Array2d tmpCov = (A+A.transpose())/2.0;

            if (!vigra::linalg::choleskyDecomposition(tmpCov, unc_decomposed)) {
                throw std::runtime_error("decomposeUncertainty(): Cholesky decomposition could not be calculated");
            }
        }

        return unc_decomposed;
    }

    void resetSampler() {
        hasSample = false;
    }

    template<typename T>
    bool needsUpdate(const vigra::Shape2 & size) {

        if(stereoFlowHist.shape() != size)
            return true;
        return !hasSample;
    }


};


} // ns toolbox
} // ns hookers

#endif // HOOKERS_TOOLBOX_STEREOSAMPLING_H
