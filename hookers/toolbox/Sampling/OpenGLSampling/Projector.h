#ifndef HOOKERS_TOOLBOX_SAMPLING_OPENGLPROJECTIONHELPER_H
#define HOOKERS_TOOLBOX_SAMPLING_OPENGLPROJECTIONHELPER_H

#include <string>
#include <vector>
#include <map>
#include <unordered_map>
#include <unordered_set>

#include <sstream>
#include <fstream>
#include <iostream>

#include <thread>
#include <mutex>

#include <vigra/hdf5impex.hxx>


#include <hookers/interface/NumericTypes.h>

// This is required to be on top due to some include dependencies regarding GL/GLEW/GLFW
#include <hookers/toolbox/opengl/Context.h>

#include <glm/vec3.hpp> // glm::vec3
#include <glm/vec4.hpp> // glm::vec4, glm::ivec4
#include <glm/mat4x4.hpp> // glm::mat4
#include <glm/gtc/matrix_transform.hpp> // glm::translate, glm::rotate, glm::scale, glm::perspective




namespace hookers {
namespace toolbox {
namespace sampling {
namespace opengl {

/**
 * @brief The ShaderLoader class provides singleton-like functionality for
 * loading and compiling shader programs.
 * Simply call ShaderLoader::getShaders() with paths to the vertex and
 * fragmen files. The method search
 */
class ShaderLoader {
public:
    static GLuint getShaders(const char* vertex_file_path, const char* fragment_file_path) {
        const std::string idString = std::string(vertex_file_path) + "\n" + fragment_file_path;
        //if (programmID.end() == programmID.find(idString)) {

            programmID[idString] = LoadShaders(vertex_file_path, fragment_file_path);

         //}
        return programmID[idString];
    }
private:
    static std::map<std::string, GLuint> programmID;
    static GLuint LoadShaders(const char * vertex_file_path, const char * fragment_file_path);
    ShaderLoader() {}
};

class Projector
{
public:
    typedef vigra::MultiArray<2, Vector3f> Vec3fArr2d;
    typedef vigra::MultiArray<2, Vector4i> IdBufferType;

private:
    GLuint VertexArrayID;
    std::vector<GLuint> vertexbuffer;
    std::vector<GLuint> idbuffer;
    GLuint programID;

    std::vector<vigra::MultiArrayView<2, float> > cloud;
    std::vector<vigra::MultiArrayView<2, int> > id;
    std::vector<GLuint> rendertype;


    GLuint P_loc;

    GLuint RT1_loc;
    GLuint RT2_loc;
    GLuint intrin_loc;

    GLuint FramebufferName;
    GLuint renderedTexture;
    GLuint renderedId;
    GLuint depthrenderbuffer;
    hookers::toolbox::opengl::Context context;

    std::mutex globalLock;


    Vec3fArr2d colorTargetBuffer;
    IdBufferType idTargetBuffer;


    Vector6d p1, p2;
    Vector4d intrinsics1, intrinsics2;

private:



    /** this function ensures that all buffers have the correct target size */
    void setupRenderBuffers(Array2Shape size);

public:
    Projector();
    Projector(Array2fView cloud, Vector4f intr);
    Projector(Vector3fArrayView cloud, Vector4f intr);

    void addGeometry(const vigra::MultiArrayView<2, float>  cloudin, const vigra::MultiArrayView<2, int> &idin, GLuint type = GL_POINTS);


    /** acquires a lock on the projector */
    std::unique_lock<std::mutex> acquireLock() {
        return std::unique_lock<std::mutex>(globalLock);
    }

    static void create_p(  const float* intr, float* p,
        const float width = 2560, const float height = 1080,
        const float znear = 0.1,  const float zfar = 500);

    ~Projector();

    void setCloud(Array2fView cloud, Array3iView color);

    void setPose(const Vector6f &p1, const Vector6f &p2);
    void setIntrinsics(const Vector4d &intrinsics1, const Vector4d &intrinsics2);

    void project(Array2iShape shape);
    void getPoints(Array2iView target);
    void getPointList(std::unordered_set<int> &target);
    void getDepth(Array2fView target);
    void getColor(Array3fView target);
    void getPositions(Array3fView target);

};

} // ns opengl
} // ns sampling
} // ns toolbox
} // ns hookers

void decimate_cloud(vigra::MultiArrayView<2, float> cloud,
    vigra::MultiArrayView<2, float> triangles,
    vigra::TinyVectorView<double, 4> intrinsics,
    vigra::TinyVectorView<double, 6> poses,
    vigra::MultiArray<2, float> & cloud_red);



#endif // SAMPLING_OPENGLPROJECTIONHELPER_H
