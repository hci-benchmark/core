#include <sstream>

#include <hookers/toolbox/Projection/Projection.h>
#include "Projector.h"


using namespace hookers::toolbox::sampling::opengl;

std::map<std::string, GLuint> ShaderLoader::programmID;

#define GLSL(src) "#version 410 core\n" #src

const char* samplingFragmentShader = GLSL(

            layout(early_fragment_tests) in;

            uniform mat4 RT1;
            uniform mat4 RT2;
            uniform vec4 intrin[2];

            flat in vec3 pointPos;
            flat in ivec4 pointId;

            layout(location = 0)  out vec3 depthBuffer;
            layout(location = 1)  out ivec4  idBuffer;

            void main(){


                vec4 p = vec4(pointPos, 1.0);

                /* transform the point into each cameras coordinate system */
                vec4 pos[2];
                pos[0] = RT1*p;
                pos[1] = RT2*p;

                /* calculate projection */

                vec4 posp[2];

                for(int i = 0; i < 2; i++) {
                    posp[i] = pos[i]/pos[i].z;
                    posp[i].x = intrin[i][0]*posp[i].x + intrin[i][2];
                    posp[i].y = intrin[i][1]*posp[i].y + intrin[i][3];
                }

                vec2 flow = posp[1].xy-posp[0].xy;

                /* set the flow component and point depth
                 * as well as ID */
                depthBuffer = vec3(flow.x, flow.y, pos[0].z);
                idBuffer = pointId;

            }
);

const char* samplingVertexShader = GLSL(

            uniform mat4 P;
            uniform mat4 RT1;

            layout(location = 0) in vec3 vertexPosition_modelspace;
            layout(location = 1) in int id;

            flat out vec3 pointPos;
            flat out ivec4 pointId;

            void main()
            {

                /* get point position and project it */
                vec4 p = vec4(vertexPosition_modelspace.xyz, 1.0);

                /* update openGLs point position */
                gl_Position = RT1*p;
                gl_Position.z *= -1;
                gl_Position = P*gl_Position;

                pointPos = vertexPosition_modelspace.xyz;
                pointId = ivec4(0, 0, 0, id);
            }
);



GLuint ShaderLoader::LoadShaders(const char * vertex_file_path, const char * fragment_file_path){

    // Create the shaders
    GLuint VertexShaderID = glCreateShader(GL_VERTEX_SHADER);
    GLuint FragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);

    // Read the Vertex Shader code from the file
    std::string VertexShaderCode;
    std::ifstream VertexShaderStream(vertex_file_path, std::ios::in);
    if (VertexShaderStream.is_open())
    {
        std::string Line = "";
        while (getline(VertexShaderStream, Line))
            VertexShaderCode += "\n" + Line;
        VertexShaderStream.close();
    }

    // Read the Fragment Shader code from the file
    std::string FragmentShaderCode;
    std::ifstream FragmentShaderStream(fragment_file_path, std::ios::in);
    if (FragmentShaderStream.is_open()){
        std::string Line = "";
        while (getline(FragmentShaderStream, Line))
            FragmentShaderCode += "\n" + Line;
        FragmentShaderStream.close();
    }

    GLint Result = GL_FALSE;
    int InfoLogLength;

    // Compile Vertex Shader
    printf("Compiling shader : %s\n", vertex_file_path);
    char const * VertexSourcePointer = VertexShaderCode.c_str();
    glShaderSource(VertexShaderID, 1, &samplingVertexShader, NULL);
    glCompileShader(VertexShaderID);

    // Check Vertex Shader
    glGetShaderiv(VertexShaderID, GL_COMPILE_STATUS, &Result);
    glGetShaderiv(VertexShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
    std::vector<char> VertexShaderErrorMessage(InfoLogLength);
    glGetShaderInfoLog(VertexShaderID, InfoLogLength, NULL, &VertexShaderErrorMessage[0]);
    fprintf(stdout, "%s\n", &VertexShaderErrorMessage[0]);

    // Compile Fragment Shader
    printf("Compiling shader : %s\n", fragment_file_path);
    char const * FragmentSourcePointer = FragmentShaderCode.c_str();
    glShaderSource(FragmentShaderID, 1, &samplingFragmentShader, NULL);
    glCompileShader(FragmentShaderID);

    // Check Fragment Shader
    glGetShaderiv(FragmentShaderID, GL_COMPILE_STATUS, &Result);
    glGetShaderiv(FragmentShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
    std::vector<char> FragmentShaderErrorMessage(InfoLogLength);
    glGetShaderInfoLog(FragmentShaderID, InfoLogLength, NULL, &FragmentShaderErrorMessage[0]);
    fprintf(stdout, "%s\n", &FragmentShaderErrorMessage[0]);

    // Link the program
    fprintf(stdout, "Linking program\n");
    GLuint ProgramID = glCreateProgram();
    glAttachShader(ProgramID, VertexShaderID);
    glAttachShader(ProgramID, FragmentShaderID);
    glLinkProgram(ProgramID);

    // Check the program
    glGetProgramiv(ProgramID, GL_LINK_STATUS, &Result);
    glGetProgramiv(ProgramID, GL_INFO_LOG_LENGTH, &InfoLogLength);
    std::vector<char> ProgramErrorMessage(std::max(InfoLogLength, int(1)));
    glGetProgramInfoLog(ProgramID, InfoLogLength, NULL, &ProgramErrorMessage[0]);
    fprintf(stdout, "%s\n", &ProgramErrorMessage[0]);

    glDeleteShader(VertexShaderID);
    glDeleteShader(FragmentShaderID);

    return ProgramID;
}


/**
 * @brief create_p Creates a homogenuous 4x4 matrix.
 * @param[in]  intr Intrinsics. Expects a 4-vector. [0]: focal length x, [1]: focal length y, [2]: center point x, [3]: center point y
 * @param[out] p Homogenuous matrix, mapping P^3 to P^3.
 * @param[in]  width Width of the image.
 * @param[in] height Height of the image.
 * @param[in] znear clipping plane in front
 * @param[in] zfar clipping plane at back
 */
void Projector::create_p(const float* intr, float* p,
                                const float width, const float height,
                                const float znear, const float zfar)
{
    p[0] = 2 * intr[0] / width;
    p[1] = 0;
    p[2] = 0;
    p[3] = 0;

    p[4] = 0;
    p[5] = 2 * intr[1] / height;
    p[6] = 0;
    p[7] = 0;

    p[8] = (width - 2 * intr[2] ) / width;
    p[9] = (height - 2 * intr[3] ) / height;
    p[10] = (-zfar - znear) / (zfar - znear);
    p[11] = -1;

    p[12] = 0;
    p[13] = 0;
    p[14] = -2 * zfar*znear / (zfar - znear);
    p[15] = 0;
}

void Projector::setCloud(Array2fView cloud, Array3iView color)
{
    throw std::logic_error("Not implemented");
}


void Projector::setPose(const hookers::Vector6f &p1, const hookers::Vector6f &p2)
{
    this->p1 = p1;
    this->p2 = p2;
}

void Projector::setIntrinsics(const Vector4d &intrinsics1, const Vector4d &intrinsics2)
{
    this->intrinsics1 = intrinsics1;
    this->intrinsics2 = intrinsics2;
}

void Projector::getPoints(Array2iView target)
{
    target = idTargetBuffer.bindElementChannel(3);
}

void Projector::getPointList(std::unordered_set<int> &target)
{
    using namespace vigra::multi_math;

    if(!idTargetBuffer.data())
        throw Projection::NotProjectedError("has not projected()");

    /* create a view on the 4th element of id color buffer */
    Array2iView idBuffer = idTargetBuffer.bindElementChannel(3);

    /* count how many points have been reprojected */
    auto pointCount = sum<int>(idBuffer>-1);

    /* if there are no points that set the array empty */
    if(!pointCount) {
        return;
    }

    /* resize the array according to the number of points */
    target.reserve(pointCount);


    for(int xx = 0; xx < idBuffer.shape(0); xx++) {
        for(int yy = 0; yy < idBuffer.shape(1); yy++) {

            if(idBuffer(xx, yy) < 0)
                continue;

            target.insert(idBuffer(xx,yy));
        }
    }

}

void Projector::getDepth(Array2fView target)
{
    if(!(target.shape() == idTargetBuffer.shape()))
        throw std::runtime_error("Target shape does not match id shape");

    target = colorTargetBuffer.bindElementChannel(2);
}

void Projector::getColor(Array3fView target)
{
    if(!(target.bindInner(0).shape() == idTargetBuffer.shape()))
        throw std::runtime_error("Target shape does not match id shape");


    /* fill by default with infinity, denoting no point */
    target = std::numeric_limits<float>::signaling_NaN();

    for(int xx = 0; xx < target.shape(1); xx++) {
        for(int yy = 0; yy < target.shape(2); yy++) {
            int &id = idTargetBuffer(xx, yy)[3];
            if(id >= 0) {
                auto &color = colorTargetBuffer(xx, yy);
                target(0, xx, yy) = color[0];
                target(1, xx, yy) = color[1];
                target(2, xx, yy) = color[2];
            }
        }
    }
}

void Projector::getPositions(Array3fView target)
{
    if(!(target.bindInner(0).shape() == idTargetBuffer.shape()))
        throw std::runtime_error("Target shape does not match id shape");


    /* fill by default with infinity, denoting no point */
    target = std::numeric_limits<float>::infinity();

    for(int xx = 0; xx < target.shape(1); xx++) {
        for(int yy = 0; yy < target.shape(2); yy++) {
            int id = idTargetBuffer(xx, yy)[3];
            if(id >= 0) {
                target(0, xx, yy) = cloud[0](0, id);
                target(1, xx, yy) = cloud[0](1, id);
                target(2, xx, yy) = cloud[0](2, id);
            }
        }
    }
}

Projector::Projector()
    : context(2560, 1080)
{
    toolbox::opengl::Context::Locker locker(context);

    glGenVertexArrays(1, &VertexArrayID);
    glBindVertexArray(VertexArrayID);

    /* Create and compile our GLSL program from the shaders */
    programID = ShaderLoader::getShaders("vertex.glsl", "fragment.glsl");;

    glUseProgram(programID);

    /* creates matrices in openGL memory.
     * we use these matrices for projection */
    P_loc = glGetUniformLocation(programID, "P");

    RT1_loc = glGetUniformLocation(programID, "RT1");
    RT2_loc = glGetUniformLocation(programID, "RT2");
    intrin_loc = glGetUniformLocation(programID, "intrin");



    /* create a texture where we can draw the depth buffer into */
    glGenTextures(1, &renderedTexture);
    glBindTexture(GL_TEXTURE_2D, renderedTexture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, 2560, 1080, 0, GL_RGB, GL_FLOAT, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

    /* generate a texture where we draw the "id" color into */
    glGenTextures(1, &renderedId);
    glBindTexture(GL_TEXTURE_2D, renderedId);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32I, 2560, 1080, 0, GL_RGBA_INTEGER, GL_INT, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

    /* setup a frame buffer */
    FramebufferName = 0;
    glGenFramebuffers(1, &FramebufferName);
    glBindFramebuffer(GL_FRAMEBUFFER, FramebufferName);

    glGenRenderbuffers(1, &depthrenderbuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, depthrenderbuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, 2560, 1080);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthrenderbuffer);

    /* Set "renderedTexture" as our colour attachement #0 */
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, renderedTexture, 0);
    /* Set "renderedTexture" as our colour attachement #1 */
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, renderedId, 0);

    /* Set the list of draw buffers. */
    GLenum DrawBuffers[2] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1 };
    glDrawBuffers(2, DrawBuffers); // "2" is the size of DrawBuffers

    /* check the capabilities and throw error in case they don't match */
    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        throw std::runtime_error("Projector: render to texture not supported");

    /* try to load the triangles file, containing backfaces of houses. */
    try {
        /** @todo replace this crap by a clean implementation in geometry class
         * that exposes backface types */
        vigra::MultiArray<2, float> triangles;
        vigra::HDF5File trifile("triangles.h5", vigra::HDF5File::ReadOnly);
        trifile.readAndResize("/triangles", triangles);
        addGeometry(triangles, vigra::MultiArray<2, int>(), GL_TRIANGLES);
    } catch(std::runtime_error & e) {
        std::cout << "OpenGLProjectoin: Could not load trianglese: " << e.what() << std::endl;
    }

}
Projector::~Projector()
{
    toolbox::opengl::Context::Locker locker(context);

    // Cleanup VBO
    for (size_t ii = 0; ii < vertexbuffer.size(); ++ii)
        glDeleteBuffers(1, &vertexbuffer[ii]);
    glDeleteVertexArrays(1, &VertexArrayID);
    glDeleteProgram(programID);

}
void Projector::setupRenderBuffers(Array2Shape size)
{
    if(!(size == Array2dShape{2560, 1080}))
        throw std::runtime_error("Projector: requested framebuffer size does not match 2560x1080");

    if(idTargetBuffer.shape() != size) {
        idTargetBuffer = IdBufferType(size);
    }

    if(colorTargetBuffer.shape() != size) {
        colorTargetBuffer = Vec3fArr2d(size);
    }

}

void Projector::addGeometry(const vigra::MultiArrayView<2, float>  cloudin, const vigra::MultiArrayView<2, int> & idin, GLuint type)
{
    /* make projection context current */
    toolbox::opengl::Context::Locker locker(context);

    cloud.push_back(cloudin);
    id.push_back(idin);
    rendertype.push_back(type);
    vertexbuffer.push_back(0);

    glGenBuffers(1, &vertexbuffer.back());
    glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer.back());
    glBufferData(GL_ARRAY_BUFFER, sizeof(float)*cloudin.size(), cloudin.data(), GL_STATIC_DRAW);
    auto errcod = glGetError();
    if (errcod != GL_NO_ERROR)
    {
        std::stringstream err;
        err << "Projector::addGeometry(): " << gluErrorString(errcod);
        throw std::runtime_error(err.str());
    }

    idbuffer.push_back(0);
    glGenBuffers(1, &idbuffer.back());
    glBindBuffer(GL_ARRAY_BUFFER, idbuffer.back());


    /**
     * If no IDs are provided, then we create fallback ids.
     * For GL_POINTS this corresponds to a positive incrementing number per point.
     * For all other primitives we set this to negative number.
     */
    if(idin.data()) {
        glBufferData(GL_ARRAY_BUFFER, sizeof(int)*idin.size(), idin.data(), GL_STATIC_DRAW);
    } else {

        vigra::MultiArray<2,int> tmpIds(Array2Shape{1,cloudin.shape(1)});

        if(type == GL_POINTS) {
            for(int i = 0; i < cloudin.shape(1); i++) {
                tmpIds(0,i) = i;
            }
        } else {
            tmpIds = -2;
        }

        /* load the data into HW memory */
        glBufferData(GL_ARRAY_BUFFER, sizeof(int)*tmpIds.size(), tmpIds.data(), GL_STATIC_DRAW);
    }

    /* check if everything worked well. */
    if (glGetError() != GL_NO_ERROR)
    {
        std::cerr << gluErrorString(glGetError()) << std::endl;
        std::stringstream err;
        err << "Projector: Adding geometry faild. "
            << gluErrorString(glGetError());
        throw std::runtime_error(err.str());
    }
}

Projector::Projector(Array2fView cloud, Vector4f intr)
    : Projector()
{
    addGeometry(cloud, vigra::MultiArrayView<2,int>(), GL_POINTS);
    setIntrinsics(intr, intr);
}

Projector::Projector(Vector3fArrayView cloud, Vector4f intr)
    : Projector(cloud.expandElements(0), intr) {}

void Projector::project(Array2iShape shape)
{
    this->setupRenderBuffers(shape);

    /* make the given context current */
    toolbox::opengl::Context::Locker locker(context);


    // Clear the screen
    glBindFramebuffer(GL_FRAMEBUFFER, FramebufferName);
    glViewport(0, 0, colorTargetBuffer.shape(0), colorTargetBuffer.shape(1));

    glClearColor(0,0,0,0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    /* this enables depth tests, hence no pixel will be overwritten by a point more distant
     * than the already projected.
     * Comparison will be made by less */
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);

    /* point size */
    glPointSize(1);

    /* set the depth buffer clear color. */
    float depthClearColor[] = {
        std::numeric_limits<float>::infinity(),
        std::numeric_limits<float>::infinity(),
        std::numeric_limits<float>::infinity()
    };
    glClearBufferfv(GL_COLOR, 0, depthClearColor);

    /* set the clear color for id render target */
    int idClearColor[] = {-1, -1, -1, -1};
    glClearBufferiv(GL_COLOR, 1, idClearColor);

    /* setup projection matrices */

    float intrinsicsf[4*2];
    {
        vigra::TinyVectorView<float,4> tmp1(intrinsicsf);
        tmp1 = intrinsics1;
        vigra::TinyVectorView<float,4> tmp2(intrinsicsf+4);
        tmp2 = intrinsics2;
    }

    Array2f P(4,4);
    create_p(Vector4f(intrinsics1).data(), P.data());

    vigra::MultiArray<2, float> RT1 = Projection::matrixFromVector(p1);
    vigra::MultiArray<2, float> RT2 = Projection::matrixFromVector(p2);


    /* and update the opengl uniforms */
    glUniformMatrix4fv(P_loc, 1, GL_FALSE, P.data());

    glUniformMatrix4fv(RT1_loc, 1, GL_FALSE, RT1.data());
    glUniformMatrix4fv(RT2_loc, 1, GL_FALSE, RT2.data());
    glUniform4fv(intrin_loc, 2, intrinsicsf);



    // Use our shader
    glUseProgram(programID);

    // 1rst attribute buffer : vertices
    for (size_t ii = 0; ii < cloud.size(); ++ii)
    {

        /* we are using buffers stored on HW. Here we need to address them */

        /* world point coordinates */
        glEnableVertexAttribArray(0);
        glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer[ii]);
        glVertexAttribPointer(
                    0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
                    3,                  // size
                    GL_FLOAT,           // type
                    GL_FALSE,           // normalized?
                    0,                  // stride
                    (void*)0            // array buffer offset
        );


        /* color id buffer.
         * Note that, if we want to use more than one channel,
         * we need to add another vertex shader */
        glEnableVertexAttribArray(1);
        glBindBuffer(GL_ARRAY_BUFFER, idbuffer[ii]);
        glVertexAttribIPointer(
            1,
            1,        /* size */
            GL_INT,   /* type */
            0,        /* stride */
            (void*)0  /* array buffer offset */
        );


        /* Now draw the primitives.
         * Primitive types are defined by rendertype array
         * Values are GL_POINT, GL_TRIANGLE, ... */
        glDrawArrays(rendertype[ii], 0, cloud[ii].shape(1));
        glDisableVertexAttribArray(0);
        glDisableVertexAttribArray(1);
   }


    /* load the frame buffers from GPU
     * and swap render buffers (back and foreground buffers)
     */
    glFlush();
    glBindTexture(GL_TEXTURE_2D, renderedTexture);
    glGetTexImage(GL_TEXTURE_2D, 0, GL_RGB, GL_FLOAT, colorTargetBuffer.data());
    glBindTexture(GL_TEXTURE_2D, renderedId);
    glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA_INTEGER, GL_INT, idTargetBuffer.data());
}


