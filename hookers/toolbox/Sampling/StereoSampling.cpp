#include <iostream>

#include "StereoSampling.h"



using namespace hookers::toolbox;

FlowSampling::FlowSampling(Vector3fArrayView cloud, int N)
    : cloud(cloud), cloudUncertainty(1.0),
      N(N), N_hist(11),
      hasSample(false),
      disableSampling(false)
{
//    RunningStats X, Y, Z;
//    for (int ii = 0; ii < cloud.size(); ++ii) {
//        X.push(cloud(ii)[0]);
//        Y.push(cloud(ii)[1]);
//        Z.push(cloud(ii)[2]);
//    }
//    std::cout << "X stats: " << X.print() << std::endl;
//    std::cout << "Y stats: " << Y.print() << std::endl;
//    std::cout << "Z stats: " << Z.print() << std::endl;
}

void FlowSampling::getCount(FlowSampling::CountView target)
{

    if(!target.data() || needsUpdate<NotSampled>(target.shape()))
        throw std::runtime_error("FlowSampling::getFlow(): Has not sampled before");

    target = 0;

    for(int xx = 0; xx < target.shape()[0]; xx++) {
        for(int yy = 0; yy < target.shape()[1]; yy++) {
            if(stereoFlowHist.count({xx,yy}) <= 1)
                continue;

            target(xx,yy) = stereoFlowHist.count({xx,yy});
        }
    }
}


void FlowSampling::getFlow(FlowFieldView target)
{

    if(!target.data() || needsUpdate<NotSampled>(target.shape()))
        throw std::runtime_error("FlowSampling::getFlow(): Has not sampled before");

    target = Vector2f(std::numeric_limits<float>::signaling_NaN());

    for(int xx = 0; xx < target.shape(0); xx++) {
        for(int yy = 0; yy < target.shape(1); yy++) {

            if(stereoFlowHist.count({xx,yy}) < 1)
                continue;

            auto mean = stereoFlowHist.median({xx,yy});
            target(xx,yy) = Vector2f(mean[0], mean[1]);
        }
    }
}

void FlowSampling::getFlowUncertainty(FlowSampling::FlowFieldView target)
{
    if(!target.data() || needsUpdate<NotSampled>(target.shape()))
        throw std::runtime_error("FlowSampling::getFlow(): Has not sampled before");

    target = Vector2f(std::numeric_limits<float>::signaling_NaN());

    for(int xx = 0; xx < target.shape(0); xx++) {
        for(int yy = 0; yy < target.shape(1); yy++) {

            if(stereoFlowHist.count({xx,yy}) < 1)
                continue;

            auto var =stereoFlowHist.IQR({xx,yy});
            target(xx,yy) = Vector2f(var[0], var[1]);
        }
    }
}

