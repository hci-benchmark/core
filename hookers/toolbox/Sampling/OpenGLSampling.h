#ifndef HOOKERS_TOOLBOX_OPENGLSAMPLING_H
#define HOOKERS_TOOLBOX_OPENGLSAMPLING_H

#include <typeinfo>
#include <tuple>
#include <functional>

#include <thread>
#include <mutex>

#include <assert.h>


#include <vigra/random.hxx>
#include <vigra/linear_algebra.hxx>
#include <vigra/accumulator.hxx>
#include <vigra/linear_algebra.hxx>
#include <vigra/timing.hxx>

// hookers includes
#include <hookers/interface/NumericTypes.h>
#include <hookers/toolbox/Projection/Projection.h>
#include <hookers/toolbox/Sampling/OpenGLSampling/Projector.h>


namespace hookers {
namespace toolbox {


/**
 * @brief The FlowSampling class
 *
 * Calculates dense optical flow from one frame to another using a reference pointcloud.
 * The calculation will be done using monte carlo simulation, thereby taking
 * the uncertainties of camera intrinsics, extrinsics and the pointcloud into account.
 *
 * The cloud will be set during class initialization.
 *
 * The intrinsics and camera poses can be set independently by using setIntrinsics() and setPose().
 *
 * The sampling will be triggered by calling sample() and by this providing a target image size.
 * The different results like depth map, occlusion maps and flow map can then be fetched using
 * the corresponding getter. The sampling can be adjusted by providing the corresponding sampling object.
 * StereoSampler and FlowSampler for this case.
 *
 * The difference of flow calculation will be chosen such that a flow vector points from
 * camera 1 to camera 2.
 *
 */
class OpenGLSampling
{
public:
    /** left pose, right pose, left intrinsics, right intrinsics */
    typedef std::tuple<Vector6d, Vector6d, Vector4d, Vector4d> SamplerReturn;

    /** helper struct */
    struct NotSampled {};

public:

    const static unsigned N_features = 3;
    typedef vigra::TinyVector<double,N_features> SampleType;



    /**
     * @brief The StereoSampler struct
     *
     * This function samples only the first camera position completly.
     * The second pose will then be
     *
     * p2^S = p1^S + (p2-p1) + N(0, \Sigma_2)
     *
     * Both intrinsics will be sampled independently
     */
    struct StereoSampler {

        Vector6d p1;
        Vector6d p2;
        Vector4d intrinsics;

        Array2d poseUncertainty;
        Array2d intrinsicsUncertainty;

        vigra::RandomNumberGenerator<> RNG;

        StereoSampler(const Vector6d & p1,
                      const Vector6d & p2,
                      const Array2d  & p1Uncertainty,
                      const Array2d  & p2Uncertainty,
                      const Vector4d & intrinsics,
                      const Array2d  & intrinsicsUncertainty)
            : p1(p1), p2(p2),
              intrinsics(intrinsics)
        {
            this->intrinsicsUncertainty = decomposeUncertainty(intrinsicsUncertainty);
            this->poseUncertainty = decomposeUncertainty(p1Uncertainty);
        }

        SamplerReturn sample() {
            auto poseLeft = randomVariable(p1, poseUncertainty, RNG);

            /** @todo add baseline uncertainty */
            auto poseRight = poseLeft + (p2-p1);

            auto intrins = randomVariable(intrinsics, intrinsicsUncertainty, RNG);

            return std::make_tuple (
                        poseLeft,
                        poseRight,
                        intrins,
                        intrins
                    );
        }

        SamplerReturn base() {
            auto poseLeft = p1;
            auto poseRight = poseLeft + (p2-p1);
            return std::make_tuple (
                        poseLeft,
                        poseRight,
                        intrinsics,
                        intrinsics
                    );
        }


    };

    /**
     * @brief Samples two camera poses given by correlation
     *
     * This sampler expects a Correlation Matrix for both poses.
     * In this case the correlation matrix has the shape of 12x12,
     * where the upper left 6x6 block corresponds to the unary covariance
     * matrix of the first pose, the lower right 6x6 block to the second pose
     * respectively.
     * The off-diagonal blocks correpond to the covariance between two camera poses.
     * If the these correlations are unknown, they can be neglected by setting them to
     * zero. This leads to two independently sampled camera poses.
     */
    struct CorrelatedSampler {

        Vector6d p1;
        Vector6d p2;
        Vector4d intrinsics;

        Array2d poseUncertainty;
        Array2d intrinsicsUncertainty;

        vigra::RandomNumberGenerator<> RNG;

        /**
         * @brief CorrelatedSampler
         * @param p1 first pose
         * @param p2 second pose
         * @param posesUncertainties 12x12 covariance matrix of poses
         * @param intrinsics camera intrinsics of the form (fx,fy,cx,cy)
         * @param intrinsicsUncertainties 4x4 covariance matrix of intrinsics.
         */
        CorrelatedSampler(const Vector6d & p1,
                          const Vector6d & p2,
                          const Array2d  & posesUncertainties,
                          const Vector4d & intrinsics,
                          const Array2d  & intrinsicsUncertainties)
            : p1(p1), p2(p2), intrinsics(intrinsics)
        {
            /* decompose the covariance matrices */
            poseUncertainty = decomposeUncertainty(posesUncertainties);
            intrinsicsUncertainty = decomposeUncertainty(intrinsicsUncertainties);
        }

        SamplerReturn sample() {
            vigra::TinyVector<double, 12> X;
            X.subarray<0, 6>() = p1;
            X.subarray<6, 12>() = p2;
            auto poses = randomVariable(X, poseUncertainty, RNG);

            auto intr = randomVariable(intrinsics, intrinsicsUncertainty, RNG);

            return std::make_tuple (
                        poses.subarray<0, 6>(),
                        poses.subarray<6, 12>(),
                        intr,
                        intr
                    );
        }

        SamplerReturn base() {
            return std::make_tuple (
                        p1,
                        p2,
                        intrinsics,
                        intrinsics
                    );
        }


    };


public:
    typedef vigra::MultiArray<2,Vector2f> FlowField;
    typedef vigra::MultiArrayView<2,Vector2f> FlowFieldView;

    typedef vigra::MultiArray<2,float> SamplingDensity;
    typedef vigra::MultiArrayView<2,float> SamplingDensityView;

    typedef vigra::MultiArray<2,bool> Occlusion;
    typedef vigra::MultiArrayView<2,bool> OcclusionView;

    typedef vigra::Shape2 ImageShape;

private:



    double cloudUncertainty;

    /** opengl projection instance. */
    sampling::opengl::Projector &m_projector;

    /** @brief concurrency
     *
     * how many threads this sampler can use in order to parallelize tasks
     */
    unsigned C;

    /** number of sampling passes */
    int N;

    /** number of bins in pixel histograms */
    int N_hist;

    /** saves the address to the sampler that was used to
     * samples random numbers. Is required to distinguish
     * whether we need to recalculate */
    bool hasSample;

    /** disable the sampling process altogether */
    bool disableSampling;

    /** this contains the stacked results from sampler */
    struct ResultStorage {
        vigra::MultiArray<4, SampleType::value_type> storage;
        vigra::MultiArray<2, unsigned> count;
    } result;


    bool hasPixelSamples(const ImageShape & pixel) const {
        return result.count(pixel[0], pixel[1]) > 0;
    }

    /** calculates the percentile for a given pixel index */
    OpenGLSampling::SampleType percentile(const OpenGLSampling::ImageShape &pixel, double q) const
    {
        static_assert(result.storage.actual_dimension == 4, "Dimension of result.storage is not as expected.");

        SampleType ret(std::numeric_limits<SampleType::value_type>::signaling_NaN());

        auto channels = result.storage.bindOuter(pixel[1]).bindOuter(pixel[0]);
        auto & count = result.count(pixel[0], pixel[1]);

        const unsigned loc = q*count;
        for(int c = 0; c < SampleType::static_size; c++) {
            ret[c] = channels(loc, c);
        }

        return ret;
    }

    /**
     * @brief estimates the standard deviation from samples
     *
     * It uses that 68.3%  of all samples fall inside the 1sigma band
     * of a gaussian normal distribution.
     *
     * So the returned value is the inter quantile range of 0.159
     * and 0.841, corresponding to 68.3% of the samples around the median.
     *
     * @warning this is only correct if the sampled values are normal distributed
     */
    SampleType stddev(const ImageShape & index) const {
        return (percentile(index, 0.5+0.5*0.683)-percentile(index, 0.5-0.5*0.683))/2.0;
    }

public:


    /**
     * @brief OpenGLSampling
     * @param projector Projection Instance. The reference can be shared by serveral OpenGLSampling instances in a thread safe manner.
     * @param N number of sample passes
     */
    OpenGLSampling(sampling::opengl::Projector & projector, int N = 10);

    /** @brief defines how many threads can be used for parallelizing tasks */
    void setConcurrency(unsigned N) {
        C = N;
    }

    void setSampleCount(int count) {
        N = count;
        this->resetSampler();
    }
    void setDisableSampling(bool newValue = false) {
        this->disableSampling = newValue;
        this->resetSampler();
    }
    void setHistogramSize(int bincount) {
        N_hist = bincount;
        this->resetSampler();
    }

    /**
     * runs the monte-carlo simulation
     * Template paramter is the sampler Type
     * that is used for sampling the different camera parameters.
     *
     * Have look at StereoSampler and CorrelatedSampler
     *
     * required interface:
     *
     * SamplerReturn sample(): returns randomly sampled parameters
     * SamplerReturn base(): returns paramters without random components
     *
     *
     */
    template<typename T>
    void sample(Array2Shape size, T & sampler = T())
    {

        const int WIDTH = size[0];
        const int HEIGHT = size[1];

        /* reset the result array */
        if(result.count.shape() != size) {
            result.storage.reshape({N, SampleType::static_size, WIDTH, HEIGHT});
            result.storage = 0.0;
            result.count.reshape({WIDTH, HEIGHT});
            result.count = 0;
        } else {
            result.storage = 0.0;
            result.count = 0;
        }


        /* >> sample loop */
        {

//            auto start = std::chrono::high_resolution_clock::now();

            Array2i idTarget(WIDTH, HEIGHT);
            vigra::MultiArray<2,Vector3f> flowTarget(WIDTH,HEIGHT);

            for(int i = 0; i < N; i++) {

                auto sr = this->disableSampling?sampler.base(): sampler.sample();

                Vector6d poseLeft, poseRight;
                Vector4d intrinsicsLeft, intrinsicsRight;
                std::tie(poseLeft, poseRight, intrinsicsLeft, intrinsicsRight) = sr;

                /* project the points given the sampled poses/intrinsics */
                {

                    /* lock guard on the projector.
                     * this ensures that all following calls happen
                     * atomic on the object */
                    auto l = m_projector.acquireLock();

                    m_projector.setPose(poseLeft, poseRight);
                    m_projector.setIntrinsics(intrinsicsLeft, intrinsicsRight);

                    m_projector.project({WIDTH, HEIGHT});
                    m_projector.getPoints(idTarget);
                    m_projector.getColor(flowTarget.expandElements(0));
                }

                {
                    /* sort in the sampled flow values into histogram */
                    for(int x = 0; x < flowTarget.shape(0); x++) {
                        for(int y = 0; y < flowTarget.shape(1); y++) {

                            /* if no point has been projected into that pixel, we
                             * can ignore it */
                            if(idTarget(x,y) < 0)
                                continue;

                            /* place the sample at the head of the list */
                            {
                                const int elemCount = result.count(x,y)++;
                                for(int c = 0; c < SampleType::static_size; c++) {
                                    result.storage(elemCount, c, x, y) = flowTarget(x,y)[c];
                                }
                            }
                        }
                    }
                }
            }

//            auto end = std::chrono::high_resolution_clock::now();
//            std::cout << "sampling took: " << std::chrono::duration<double>(end-start).count() << std::endl;

        }
        /* << sample loop */

        /* >> sorting
         *
         * now run the sampling. We split up the target array
         * into columns. For each column one thread is responsible
         * in order to speedup sorting */
        {

//            auto start = std::chrono::high_resolution_clock::now();

            /* find number of columns that divide image equidistantly.
             * Worst case is one column and hence no parallelism */
            unsigned N_threads = C;
            for(; N_threads > 1 && WIDTH%N_threads; N_threads--);
            const unsigned COL_WIDTH = WIDTH/N_threads;

            std::vector<std::thread> threads;

            /* for each column, spawm a thread */
            for(unsigned i = 0; i < N_threads; i++) {
                auto xStart = i*COL_WIDTH;
                auto xEnd = (i+1)*COL_WIDTH;

                /* function that does the sort magic */
                threads.push_back(std::thread([&](int xStart, int xEnd)
                {
                    /* >> thread function */
                    for(int x = xStart; x < xEnd; x++) {
                        for(int y = 0; y < HEIGHT; y++) {
                            for(int c = 0; c < SampleType::static_size; c++) {

                                /* binds the column for given channel, x and y */
                                auto column = result.storage.bindOuter(y).bindOuter(x).bindOuter(c);

                                /* define start and end iterators.
                                 * the end iterator is not necessarily sample count N */
                                auto begin = column.begin();
                                auto end = begin+result.count(x,y);

                                std::sort(begin, end, [&](const float & a, const float & b){
                                    return a < b;
                                });
                            }
                        }
                    }
                    /* << end thread function */

                }, xStart, xEnd));
            }

            /* wait for the threads to finish */
            for(size_t i = 0; i < threads.size(); i++) threads[i].join();

//            auto end = std::chrono::high_resolution_clock::now();
//            std::cout << "sorting took: " << std::chrono::duration<double>(end-start).count() << std::endl;
        }
        /* << end sorting */

        /* saves sampler call */
        hasSample = true;

    }

    void getSamplingDensity(SamplingDensityView target);
    void getOcclusionMask(OcclusionView target);

    void getFlow(FlowFieldView target);
    void getFlowUncertainty(FlowFieldView target);



    /**
     * creates a gaussian distributed random vector.
     * The distribution is parametrized via @f$ A @f$ which is the choleski decomposition
     * of the covariance matrix @f$ \Sigma @f$
     */
    template<typename T, int N, class RNG>
    static vigra::TinyVector<T, N> randomVariable(const vigra::TinyVector<T, N> & mu,
                                                  const vigra::MultiArrayView<2,T> A, RNG& rng)
    {

        using namespace vigra::linalg;
        typedef vigra::TinyVector<T, N> RV;

        static_assert(A.actual_dimension == 2, "Given covariance matrix must have dimension of 2!");
        if(A.shape(0) != A.shape(1)) {
            throw std::runtime_error("FlowSampler::randomVariable(): Shape of covariance decomposition A is not quadratic.");
        }

        if(A.shape(0) != N) {
            throw std::runtime_error("FlowSampler::randomVariable(): Shape of covariance decomposition A has not same size as N");
        }

        RV stdNormalX;
        for(auto &it: stdNormalX) {
            it = rng.normal();
        }


        return mu + vigra::linalg::Matrix<T>(A)*stdNormalX;
    }

    template<typename T>
    static vigra::MultiArray<2,T> decomposeUncertainty(vigra::MultiArrayView<2,T> A) {

        Array2d unc_decomposed(A.shape());

        /* calculate cholesky decomposition.
         * This is required for random sampling */
        {
            /* this makes sure that the matrix is symmetric */
            using namespace vigra::multi_math;
            Array2d tmpCov = (A+A.transpose())/2.0;

            if (!vigra::linalg::choleskyDecomposition(tmpCov, unc_decomposed)) {
                throw std::runtime_error("decomposeUncertainty(): Cholesky decomposition could not be calculated");
            }
        }

        return unc_decomposed;
    }

    void resetSampler() {
        hasSample = false;
    }

    template<typename T>
    bool needsUpdate(const vigra::Shape2 & size) {

        if(result.count.shape() != size)
            return true;
        return !hasSample;
    }


};


} // ns toolbox
} // ns hookers

#endif // HOOKERS_TOOLBOX_OPENGLSAMPLING_H
