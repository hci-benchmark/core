#include <exception>

// Hookers Interface
#include <hookers/interface/backends/hdf/HDFDataset.h>
#include <hookers/interface/backends/hdf/Vigra.h>

#include <hookers/interface/Frame.h>
#include <hookers/interface/Sequence.h>
#include <hookers/interface/Geometry.h>
#include <hookers/interface/Track.h>

// Hookers Toolbox
#include <hookers/toolbox/Projection/SoftwareProjection.h>
#include <hookers/toolbox/Sampling/StereoSampling.h>
#include <hookers/toolbox/PoseEstimation/PoseEstimation.h>

#include "GroundtruthSource.h"

using namespace hookers;
using namespace hookers::interface;

/**
 * required for covariance analysis.
 * Uncertainties for reprojected points
 */
const double PIXEL_VARIANCE = 4.0;

using namespace hookers::toolbox;

GroundtruthSource::GroundtruthSource(Projection *p)
    : m_projection(p), projector(), samplingStereo(projector), samplingFlow(projector)
{

}

void GroundtruthSource::addFramePoses(Frame f1, Vector6d p1, Frame f2, Vector6d p2, Array2dView cov)
{

    if(!f1.isValid())
        throw std::runtime_error("No Valid frame provided.");


    if(!f2.isValid() && cov.shape() != Array2dShape{6,6})
        throw std::runtime_error("Pose Covariance Matrix has wrong shape.");

    covariances[f1] = std::make_tuple(f2, p1, p2, cov);
}

ExporterSource::SourceIdentifierList GroundtruthSource::provides() const
{

    SourceIdentifierList ids;
    ids.push_back(SourceIdentifier("disp", "density"));
    ids.push_back(SourceIdentifier("disp", "map"));
    ids.push_back(SourceIdentifier("disp", "uncertainty"));

    for(auto i : m_flowTargets) {
        std::stringstream number;
        number << i;

        ids.push_back(SourceIdentifier("flow",number.str()+"/density"));
        ids.push_back(SourceIdentifier("flow",number.str()+"/map"));
        ids.push_back(SourceIdentifier("flow",number.str()+"/uncertainty"));
    }

    return ids;
}


std::tuple<Vector6d, Array2d> solvePnPForFrame(Frame f) {
    if(!f.isValid())
        throw std::runtime_error("PoseEstimation::solvePnPRansac(): Frame invalid.");
    if(f.intrinsics() == Vector4d())
        throw std::runtime_error("PoseEstimation::solvePnPRansac(): Frame has no intrinsics");

    TrackList annotTracks = f.tracks().annotated();
    LandmarkList annotLandmarks = annotTracks.landmarks();
    Vector2fArray trackPositions;
    Vector3fArray landmarkPositions;
    annotTracks.getPositionsInFrame(trackPositions, f);
    annotLandmarks.getPositions(landmarkPositions);

    Array2d pixelUncertainties(1,1);
    pixelUncertainties = PIXEL_VARIANCE;

    SolvePNPRansacProblem pnp(landmarkPositions, trackPositions, f.intrinsics(),
                              10, 10,
                              Vector6d(), pixelUncertainties);

    return std::make_tuple(pnp.pose(), pnp.poseUncertainty());
}


ExporterSource::SourceMap GroundtruthSource::sources(Frame f)
{

    /* check if we need to retrieve the current point cloud */
    if(!(m_g == f.sequence().geometry())) {
        f.sequence().geometry().getPointCloud(m_cloud);

        /* set the pointcloud to the projection instance. */
        projector.addGeometry(m_cloud.expandElements(0), vigra::MultiArrayView<2,int>(), GL_POINTS);

        m_g = f.sequence().geometry();
    }

    if(!f.isValid()) {
        throw std::runtime_error("PartViewer::sampleFrame(): Frame is invalid. ");
    }

    if(f.intrinsics() == Vector4d()) {
        throw std::runtime_error("PartViewer::sampleFrame(): Frame has no valid intrinsics. ");
    }

    Vector4d leftIntrinsics = f.intrinsics();
    Vector4d rightIntrinsics = f.intrinsics();
    Vector4d leftIntrinsicsUncertainty;
    Vector4d rightIntrinsicsUncertainty;
    for(int i = 0; i < 4; i++) {
        leftIntrinsicsUncertainty[i] = f.intrinsicsUncertainty()[i]*f.intrinsicsUncertainty()[i];
        rightIntrinsicsUncertainty[i] = f.intrinsicsUncertainty()[i]*f.intrinsicsUncertainty()[i];
    }



    /* try to get the frame pose and use
     * pose estimatoin in case no valid pose
     * has been set */
    Vector6d currentPose = f.pose();

    /** @todo implement Frame::getUncertainty() as full covariance matrix */
    Array2d currentUncertainty(6,6);

    /* check if the user provided a manual pose for given frame */
    if(covariances.count(f)) {
        Array2d cov;
        std::tie(std::ignore, currentPose, std::ignore, cov) = covariances[f];

        if(cov.data()) {
            currentUncertainty = cov.subarray(Array2dShape(0,0), Array2dShape(6,6));
        }
    }

    /* this one is used as fallback solution in
     * order to find the camera pose and uncertainties. */
    if(currentPose == Vector6d() || m_estimatePose) {
        std::tie(currentPose, currentUncertainty) = solvePnPForFrame(f);
    }


    samplingFlow.setConcurrency(std::thread::hardware_concurrency()/2);
    samplingStereo.setConcurrency(std::thread::hardware_concurrency()/2);

    /* acquire this once, because f.size() is not threadsafe */
    auto SIZE = f.size();

    /* >> sample stereo */
    SourceMap sourcesStereo;
    std::exception_ptr stereoExcpPtr;
    auto stereoSamplingFunctor = [&](){

        try {

            auto & sources = sourcesStereo;
            std::cout << ">> Sampling stereo" << std::endl;

            Vector6d poseLeft = currentPose;
            Vector6d poseRight = currentPose;
            poseRight.subarray<0,3>() += Vector3d(-0.3, 0, 0);

            // Then we can call the main sampling function
            {
                Array2d intrinsicsCovariance(Array2dShape(4,4));
                intrinsicsCovariance = 0;
                intrinsicsCovariance.diagonal() = Array1dView(Array1Shape(4), leftIntrinsicsUncertainty.data());
                Sampling::StereoSampler poseSampler(poseLeft, poseRight,
                                                    currentUncertainty, currentUncertainty,
                                                    leftIntrinsics, intrinsicsCovariance);
                samplingStereo.sample(SIZE, poseSampler);
            }

            SourceIdentifier stereoCountId      ("disp", "density");
            SourceIdentifier stereoMapId        ("disp", "map");
            SourceIdentifier stereoUncertaintyId("disp", "uncertainty");


            sources[stereoMapId] =  cv::Mat(f.size()[1], f.size()[0], CV_32FC2);
            cv::Mat & flowMat = sources[stereoMapId];

            sources[stereoUncertaintyId] = cv::Mat(f.size()[1], f.size()[0], CV_32FC2);
            cv::Mat & flowUncertaintyMat = sources[stereoUncertaintyId];

            sources[stereoCountId] = cv::Mat(f.size()[1], f.size()[0], CV_32FC1);
            cv::Mat & flowCountMat = sources[stereoCountId];

            Sampling::FlowFieldView flow(f.size(), (Sampling::FlowFieldView::pointer) flowMat.data);
            Sampling::FlowFieldView flowVar(f.size(), (Sampling::FlowFieldView::pointer) flowUncertaintyMat.data);
            Sampling::SamplingDensityView count(f.size(), (Sampling::SamplingDensityView::pointer) flowCountMat.data);

            samplingStereo.getFlow(flow);
            samplingStereo.getFlowUncertainty(flowVar);
            samplingStereo.getSamplingDensity(count);

            std::cout << ">> Done stereo" << std::endl;

        } catch (std::exception & e) {
            stereoExcpPtr = std::current_exception();
        }
    };


    /* >> sample flow */
    SourceMap sourcesFlow;
    std::exception_ptr flowExcpPtr;
    auto flowSamplingFunctor = [&]() {
        try {
            auto & sources = sourcesFlow;

            std::cout << ">> Sampling flow" << std::endl;

            FrameList frames = f.sequence().frames();
            auto fit = frames.find(f);

            for(int step: m_flowTargets) {



                /* check if the frame could be found in the frame list */
                if(fit == frames.end())
                    continue;

                /* check if the requested flow frame is out of the sequence */
                if((fit+step)-frames.end() >=0)
                    continue;

                /* get the frame number in the ordered list of sequences frames */
                Frame nf = *(fit+step);
                int f_number = fit -frames.begin();
                int nf_number = (fit+step)-frames.begin();


                /* here we are going to call the sampling function.
             * first we make sure camera poses and covariance matrices are
             * setup properly for the use of correlated pose sampler */
                {

                    Vector6d poseFront = nf.pose();
                    Array2d poseFrontUncertainty(6,6);


                    /* first check for manual poses and covariances */
                    if(covariances.count(nf)) {
                        Array2d cov;
                        std::tie(std::ignore, poseFront, std::ignore, cov) = covariances[nf];

                        if(cov.data()) {
                            poseFrontUncertainty = cov.subarray(Array2dShape(0,0), Array2dShape(6,6));
                        }
                    }

                    /* fallback solution if neither frame has valid pose
                 * nor has it been set manually */
                    if(poseFront == Vector6d() || m_estimatePose) {
                        std::tie(poseFront, poseFrontUncertainty) = solvePnPForFrame(nf);
                    }

                    /* we are using the correlated pose sampler. So we need a covariance matrix
                 * between the two camera poses. Therefor we first construct a handcrafted
                 * default matrix using the unary covariance blocks. */
                    Array2d poseUncertainties(12,12);
                    poseUncertainties.subarray(Array2dShape(0,0), Array2dShape(6,6)) = currentUncertainty;
                    poseUncertainties.subarray(Array2dShape(6,6), Array2dShape(12,12)) = poseFrontUncertainty;

                    /* now check if the user provided a true covariance matrix. If this is
                 * the case then use it and the corresponding poses accordingly. */
                    if(covariances.count(f) && std::get<0>(covariances[f]).isValid()) {
                        std::tie(std::ignore, std::ignore, poseFront, poseUncertainties) = covariances[f];
                    }

                    Array2d intrinsicsCovariance(Array2dShape(4,4));
                    intrinsicsCovariance = 0;
                    intrinsicsCovariance.diagonal() = Array1dView(Array1Shape(4), leftIntrinsicsUncertainty.data());


                    /* construct sampler that is responsible for proper flow sampling */
                    Sampling::CorrelatedSampler poseSampler(currentPose, poseFront, poseUncertainties,
                                                            leftIntrinsics, intrinsicsCovariance);

                    /* finally call the groundtruth sampling function, creating the groundtruth for one
                 * whole frame */
                    samplingFlow.sample(SIZE, poseSampler);
                }


                std::stringstream numberStr;
                numberStr << step;
                SourceIdentifier flowCountId("flow",numberStr.str()+"/density");
                SourceIdentifier flowMapId("flow",numberStr.str()+"/map");
                SourceIdentifier flowUncertaintyId("flow",numberStr.str()+"/uncertainty");


                sources[flowMapId] =  cv::Mat(f.size()[1], f.size()[0], CV_32FC2);
                cv::Mat & flowMat = sources[flowMapId];

                sources[flowUncertaintyId] = cv::Mat(f.size()[1], f.size()[0], CV_32FC2);
                cv::Mat & flowUncertaintyMat = sources[flowUncertaintyId];

                sources[flowCountId] = cv::Mat(f.size()[1], f.size()[0], CV_32FC1);
                cv::Mat & flowCountMat = sources[flowCountId];

                /* create views on the open CV matrices */
                Sampling::FlowFieldView flow(f.size(), (Sampling::FlowField::const_pointer) flowMat.data);
                Sampling::FlowFieldView flowVar(f.size(), (Sampling::FlowField::const_pointer) flowUncertaintyMat.data);
                Sampling::SamplingDensityView count(f.size(), (Sampling::SamplingDensity::const_pointer) flowCountMat.data);

                samplingFlow.getFlow(flow);
                samplingFlow.getFlowUncertainty(flowVar);
                samplingFlow.getSamplingDensity(count);

                std::cout << ">> Done flow" << std::endl;

            }

        } catch (std::exception & e) {
            flowExcpPtr = std::current_exception();
        }

    };

    {
        auto start = std::chrono::high_resolution_clock::now();

        /* run the stereo and flow computation in parallel */
        {
            std::thread stereoThread(stereoSamplingFunctor);
            std::thread flowThread(flowSamplingFunctor);
            stereoThread.join();
            flowThread.join();
        }

        /* handle exceptions in threads */
        if(stereoExcpPtr) std::rethrow_exception(stereoExcpPtr);
        if(flowExcpPtr) std::rethrow_exception(flowExcpPtr);


        auto end = std::chrono::high_resolution_clock::now();
        std::cout << "Frame sampling took: " << std::chrono::duration<double>(end-start).count() << std::endl;
    }

    /* copy each results into the big sources map */
    SourceMap sources;
    for(auto & s: sourcesStereo) {
        sources[s.first] = s.second;
    }
    for(auto & s: sourcesFlow) {
        sources[s.first] = s.second;
    }

    return sources;
}


