#ifndef HOOKERS_TOOLBOX_POSEESTIMATION_H
#define HOOKERS_TOOLBOX_POSEESTIMATION_H
#define GLOG_NO_ABBREVIATED_SEVERITIES

#ifndef _MSC_VER //VS2013 does not have noexcept ):
#define NOEXCEPT noexcept
#else
#define NOEXCEPT
#endif

#include <vector>

#include <hookers/interface/AbstractObjectList.h>
#include <hookers/interface/NumericTypes.h>
#include <hookers/interface/Frame.h>
#include <hookers/interface/LandmarkList.h>

#include <hookers/toolbox/Projection/Projection.h>

#include <ceres/ceres.h>
#include <ceres/rotation.h>


namespace hookers {
namespace toolbox {

/**
 * adds regularization term to pin certain values around initial guess.
 * Prior must be given, standard deviation is optional.
 * If stddev is not provided it will be assumed to be 1.0.
 */
template<int N>
struct QuadraticPrior
{
    typedef ceres::AutoDiffCostFunction<QuadraticPrior<N>, N, N> AutoDiff;

    double prior[N];
    double weight[N];

    /**
     * @brief QuadraticPrior
     * @param d prior values
     * @param w weights given in stddev provided by a c-array of values.
     */
    QuadraticPrior(double * d, double* w)
    {
        for (int ii = 0; ii < N; ++ii)
        {
            weight[ii] = 1.0/w[ii];
            prior[ii] = d[ii];
        }
    }

    /**
     * @brief QuadraticPrior
     * @param d prior values
     * @param w weights given in stddev
     */
    QuadraticPrior(double * d, double w = 1.0)
    {
        for (int ii = 0; ii < N; ++ii)
        {
            weight[ii] = 1.0/w;
            prior[ii] = d[ii];
        }
    }

    template<class T>
    bool operator()(const T* const x, T* res) const
    {
        for (int ii = 0; ii < N; ++ii) {
            res[ii] = T(weight[ii]) * (T(prior[ii]) - x[ii]);
        }
        return true;
    }
};

class PoseEstimation
{
public:

    typedef std::runtime_error Failure;

    class BehindCameraFailure : public std::exception 
    {
        const char * what() const NOEXCEPT
        {
            return "Point lies behind camera after reprojection.";
        }
    };

    template<typename T, unsigned N>
    using Vec = Projection::Vec<T, N>;

    template<typename T, unsigned N>
    using VecView = Projection::VecView<T, N>;



    ceres::Problem m_problem;

    class PoseBlock {
        PoseBlock(const Vector6d & pose);
        Vector6d pose();
    };


    class FrameBlock {
    public:
        FrameBlock(interface::Frame f);
        Vector6d pose();
    };

    PoseEstimation();

    void addPoseBlock(PoseBlock & b);
    void addFrameBlock(FrameBlock & b);


    const ceres::Problem & problem() const {
        return m_problem;
    }
    ceres::Problem & problem() {
        return m_problem;
    }

    template<typename T> inline static
    Vec<T,3> reproject3d(const Vec<T,3> & object,
                         const Vec<T,6> & transform,
                         const Vec<T,4> & intrinsics) {

        return Projection::project3d(object, transform, intrinsics);
    }

    template<typename T> inline static
    Vec<T,2> reproject(const Vec<T,3> & object,
                       const Vec<T,6> & transform,
                       const Vec<T,4> & intrinsics) {


        const Vec<T,3> & repr3d = reproject3d(object, transform, intrinsics);

        if(repr3d[2] <= 0.0)
            throw BehindCameraFailure();

        return Vec<T,2>(repr3d[0], repr3d[1]);
    }


    /**
     * class that encapsulates a solve pnp result.
     * It builds on std::tuple to be used with std tuple functions
     * but contains implicit type conversion for convenience, as well.
     */
    template<typename T>
    class PNPResult : public std::tuple<Vector6d, T> {
    public:
        PNPResult(const Vector6d & pose, const T & inliers = T())
            : std::tuple<Vector6d,T>(pose, inliers) {}

        operator Vector6d() {
            return std::get<0>(*this);
        }

        operator Vector6f() {
            return std::get<0>(*this);
        }

        operator T() const {
            return std::get<1>(*this);
        }
    };


    /**
     * @brief solvePnP
     * @param objects 3d points of objects
     * @param images 2d image points
     * @param intrinsics camera intrinsics
     * @param pose initial pose. optional
     * @return camera pose
     * @throw PoseEstimation::Failure if the numeric part went wrong somehow
     */
    static Vector6d solvePnP(const Vector3fArrayView objects,  const Vector2fArrayView images,
                             const Vector4d &intrinsics, const Vector6d &pose = Vector6d());

    /**
     * try to find the camera pose first using
     * RANSAC algorithm and then refining with usual solvePnP.
     *
     * Any unspecified arguments behave as in solvePnP()
     *
     * @note currently this function uses itertative pose estimation even for
     *       RANSAC step. This might take a little more time...
     *
     * @todo implement the RANSAC step with analytical camera pose estimation
     *
     * @param distance the maximum error for points. bigger values will be considered as outliers
     * @param trials factor that specifies how much trials should be made in RANSAC step
     *
     */
    static PoseEstimation::PNPResult<std::vector<int> > solvePnPRansac(const Vector3fArrayView objects,  const Vector2fArrayView images,
                                   const Vector4d &intrinsics, const float &distance, const int &trials = 1,
                                   const Vector6d &pose = Vector6d());

    static Vector6d solvePnP(interface::Frame f);
    static PoseEstimation::PNPResult<std::vector<int> > solvePnPRansac(interface::Frame f,
                                   const float &distance,
                                   const int & trials = 1,
                                   const Vector6d pose = Vector6d());


    /** calculates full transformation matrix R|t from reduced camera pose vector */
    static vigra::Matrix<double> matrixFromVector(const Vector6d & vec);

    static Vector6d vectorFromMatrix(vigra::MultiArray<2, double> mat);



};

class SolvePNPProblem {

    ceres::Problem problem;

    bool m_solved;
    bool m_covarianced;

    Vector3fArrayView objects;
    Vector2fArrayView images;

    Vector4d intrinsics;
    Vector6d m_pose;
    Array2d m_poseCovar;
    const Array2dView uncertainty2d;


public:
    SolvePNPProblem();

    /**
     * @param uncertainty2d Specify the uncertainties of 2d measurements.
     *
     * uncertainty2d can be:
     * - Zero (Arrar2d()) in which case identity uncertainties are assumed
     * - 1x1 shaped, where all reprojection errors are assume to have this scalar value
     * - 1xN shaped, where each measurement can have it's own uncertainty.
     *
     * Uncertainties should be given as variance
     */
    SolvePNPProblem(const Vector3fArrayView objects,  const Vector2fArrayView images,
                    const Vector4d &intrinsics, const Vector6d &pose = Vector6d(),
                    const Array2dView uncertainty2d = Array2d());

    /** call this function to solve the */
    void solve();
    void covarianceAnalysis();

    Vector6d pose();
    Array2d poseUncertainty();
};


/**
 * @brief The BundleAdjustmentProblem class
 *
 * This class handles a full blown bundle adjustment problem.
 *
 * The basic idea is that you provide
 *
 * - 3D Objects
 * - 2D Images of Objects per View
 * - Relation between 2D images and 3D objects
 *
 * In addition to that objects as well as images can be flagged
 * with certain information. Have a look at BundleAdjustmentProblem::ObjectParametrization
 * and BundleAdjustmentProblem::ImageParametrization for further information.
 *
 * BundleAdjustment in principal has serveral modes on it's own:
 *
 * - Pose Estimation given 3D Objects, 2D Images and Relations
 * - Triangulation of unkown 3D Points given camera poses ("SfM").
 *
 * The latter is required when we mix known 2D-3D correspondences with
 * correspondences that are only known temporaly but not spatially and
 * would be employed in order to get a rough estimate for the yet unknown spatial
 * information. Objects that have to spatial information should be flagged with
 * BundleAdjustmentProblem::ObjectParametrization::Unlinked.
 *
 * In order to ignore some of the images, eg. because they were outliers,
 * you can flag them with BundleAdjustmentProblem::ImageParametrization::Invalid.
 * This way the images index relative to your providing storage can be retained,
 * because they don't need to be removed from the provided lists.
 *
 */
class BundleAdjustmentProblem {

public:

    enum ObjectFlags {
        ObjectRegular = 0,
        ObjectUnlinked = 0x1,
        ObjectUnuseable = 0x2,
        ObjectFromManual = 0x4

    };

    /**
     * Stores point data and additional information
     * specific to each object. */
    struct ObjectParametrization : public std::vector<std::tuple<Vector3d,ObjectFlags>> {

        enum Fields {
            POS = 0,
            FLAGS
        };

        ObjectParametrization(const Vector3fArrayView objectsArray = Vector3fArrayView()) {
            this->resize(objectsArray.size());
            for(int i = 0; i < objectsArray.size(); i++) {
                std::get<POS>((*this)[i]) = objectsArray[i];
                std::get<FLAGS>((*this)[i]) = ObjectRegular;
            }
        }
    };

    enum ImageFlags : int {
        ImageRegular = 0,
        ImageInvalid = 0x1
    };

    struct ImageParametrization : public std::vector<std::tuple<Vector2d,int, ImageFlags, double>> {


        enum Fields : int {
            POS = 0,
            TO_OBJECT,
            FLAGS,
            UNCERTAINTY
        };

        /** constructs empty image object */
        ImageParametrization() {}

        /**
         * stores all required parameters.
         * have a look at Field class.
         */

        ImageParametrization(const Vector2fArrayView imagesArray, const Array1iView imagesToObjects) {

            this->resize(imagesArray.size());
            for(int i = 0; i < imagesArray.size(); i++) {

                std::get<POS>      ((*this)[i]) = imagesArray(i);
                std::get<TO_OBJECT>((*this)[i]) = imagesToObjects(i);
                std::get<FLAGS>    ((*this)[i]) = ImageRegular;

                /* a negative values corresponds to a point that shouldn't be used */
                if(imagesToObjects(i) < 0) {
                    std::get<FLAGS>((*this)[i]) = ImageInvalid;
                }
            }
        }
    };

private:
    ceres::Problem m_problem;
    bool m_problemSetup;


    ObjectParametrization& m_objects;
    double objectsUncertainty;

    std::vector<ImageParametrization>& m_images;
    double imageUncertainty;

    std::vector<Vector6d>& m_poses;

public:
    Vector4d m_intrinsics;
    Vector4d m_intrinsicSigma;

    vigra::TinyVector<double,8> distortion;

    /** stores interal ceres ids to projection residual */
    hookers::interface::AbstractObjectList<ceres::ResidualBlockId> projectionBlocks;

    /** stores interal ceres ids to projection residual for unlinked points*/
    hookers::interface::AbstractObjectList<ceres::ResidualBlockId> projectionUnlinkedBlocks;
    double projectionUnlinkedUncertainty;

    /** stores interal ceres ids to object position priors */
    hookers::interface::AbstractObjectList<ceres::ResidualBlockId> objectBlocks;

private:





public:

    BundleAdjustmentProblem(ObjectParametrization &objectsArray,
                            std::vector<ImageParametrization> &imagesArray,
                            const Vector4d & m_intrinsics, std::vector<Vector6d> &m_poses);

    /** solves bundle adjustment problem for poses and object positions jointly */
    void solve();

    /** solves unknown object positions given camera poses */
    void triangulate();


    ceres::Problem & problem() {
        return m_problem;
    }

    void setup();

    /** retrieves poses for provided images */
    std::vector<Vector6d> poses(const std::vector<int> & indices = std::vector<int>()) {
        this->solve();
        /* if default/empty list provided, return all poses */
        if(indices.empty()) {
            return m_poses;
        }

        std::vector<Vector6d> ps;
        for(int i: indices) {
            ps.push_back(m_poses[i]);
        }

        return ps;
    }

    Vector4d intrinsics() const {
        return m_intrinsics;
    }

    /** get reference to images */
    std::vector<ImageParametrization> & images() {
        return m_images;
    }

    ObjectParametrization & objects() {
        return m_objects;
    }

    void setImageUncertainty(double unc) {
        imageUncertainty = unc;
    }

    /**
     * when optimizing or doing covariance analysis, one can set the object positions
     * to be constant */
    void setConstantObjectPositions(bool constant);

    /** same as setConstantObjectPositions but affects only unlinked objects */
    void setConstantTriangulatedObjectPositions(bool constant);

    /** same as setConstantObjectPositions but affects only unlinked objects */
    void setConstantLinkedObjectPositions(bool constant);

    /** set intrinsics as constant */
    void setConstantIntrinsics(bool constant);
    /**
     * retrieves camera pose uncertainties.
     * @param poses List of poses for which covariance matrices should be calculated.
     *              If the list is empty, for all provided image poses the covariance will
     *              be calculated.
     * @return list of covariance matrices in the same order as provided by poses.
     */
    std::vector<Array2d> poseUncertainties(std::vector<int> poses = std::vector<int>());


    /**
     * @brief poseUncertainties
     * @param poses pairs of poses that the covariances should be computed to.
     *        If list is empty, all successive poses are computed.
     */
    std::vector<Array2d> posePairwiseUncertainties(std::vector<std::pair<int,int>> poses = std::vector<std::pair<int,int>>());


};


class SolvePNPRansacProblem {
    std::unique_ptr<SolvePNPProblem> m_problem;
    std::vector<int> m_inliers;

public:

    /**
     * @brief SolvePNPRansacProblem
     * @param distance RANSAC outlier distance
     * @param trials number of trials as factor of number of objects
     *
     * The rest of the arguments is equal to SolvePNPProblem.
     */
    SolvePNPRansacProblem(const Vector3fArrayView objects,  const Vector2fArrayView images,
                          const Vector4d &intrinsics, const float &distance, const int &trials = 1,
                          const Vector6d &pose = Vector6d(),
                          const Array2dView uncertainty2d = Array2d());

    Vector6d pose();
    Array2d poseUncertainty();

    std::vector<int> inliers() {
        return m_inliers;
    }
};

}
}

#endif // HOOKERS_TOOLBOX_POSEESTIMATION_H
