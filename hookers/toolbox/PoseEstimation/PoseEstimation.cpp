#include <iostream>
#include <thread>

#include <ceres/ceres.h>
#include <ceres/rotation.h>
#include <ceres/covariance.h>

#include <random>

#include <hookers/interface/Frame.h>
#include <hookers/interface/LandmarkList.h>
#include <hookers/interface/TrackList.h>

#include "PoseEstimation.h"

using namespace hookers;
using namespace hookers::interface;
using namespace hookers::toolbox;

PoseEstimation::PoseEstimation()
{

}

Vector6d PoseEstimation::vectorFromMatrix(vigra::MultiArray<2, double> mat) {
    Vector6d iPose;
    iPose[0] = mat(0,3);
    iPose[1] = mat(1,3);
    iPose[2] = mat(2,3);

    vigra::Matrix<double> iR(3,3);
    iR = mat.subarray({0,0}, {3,3});

    ceres::RotationMatrixToAngleAxis(iR.data(), iPose.subarray<3,6>().data());
    return iPose;
}

vigra::Matrix<double> PoseEstimation::matrixFromVector(const Vector6d & vec) {
    vigra::Matrix<double> rotation(3,3);

    auto T = vec.subarray<0,3>();
    auto axis = vec.subarray<3,6>();

    ceres::AngleAxisToRotationMatrix(axis.data(), rotation.data());
    vigra::Matrix<double> result(4,4);

    result.subarray({0,0}, {3,3}) = rotation;
    result(0,3) = T[0];
    result(1,3) = T[1];
    result(2,3) = T[2];
    result(3,3) = 1.0;
    return result;
}

struct ReprojectionErrorTerm
{
    typedef ceres::AutoDiffCostFunction<ReprojectionErrorTerm, 3, 3, 6, 4, 8> BundleAdjustmentDistortion;
    typedef ceres::AutoDiffCostFunction<ReprojectionErrorTerm, 3, 3, 6, 4> BundleAdjustment;
    typedef ceres::AutoDiffCostFunction<ReprojectionErrorTerm, 3, 6> SolvePnP;

    Vector2d m_image;
    Vector3d m_object;
    Vector4d m_intrinsics;

    double scale;

    /**
     * @brief ReprojectionErrorTerm for solvePnP or BundleAdjustment
     * @param object 3d position of tracked object
     * @param image 2d image of the object
     * @param intrinsics camera intrinsics
     * @param uncertainty of image position [px]
     */
    ReprojectionErrorTerm(const Vector3d &object,
                          const Vector2d &image,
                          const Vector4d &intrinsics,
                          const double &uncertainty = 1.0)
        : m_image(image), m_object(object), m_intrinsics(intrinsics), scale(1.0/uncertainty) {}


    template <typename T> bool operator()(const T* const X,
                                          const T* const transform,
                                          const T* const intrinsics,
                                          const T* const distortion,
                                          T* residual) const
    {

        const PoseEstimation::Vec<T,3> &reprojection = Projection::project3dBase<T,8>(X, transform, intrinsics, distortion);
        if(!ceres::IsFinite(reprojection[0]) || !ceres::IsFinite(reprojection[1]))
            return false;

        residual[0] = scale*(reprojection[0] - T(m_image[0]));
        residual[1] = scale*(reprojection[1] - T(m_image[1]));
        /* this term makes sure the points stays in front of the camera */
        residual[2] = (reprojection[2]>0.0?T(0.0):(intrinsics[0]*intrinsics[1])*reprojection[2]);
        return true;

    }

    template <typename T> bool operator()(const T* const X,
                                          const T* const transform,
                                          const T* const intrinsics,
                                          T* residual) const
    {

        const PoseEstimation::Vec<T,3> &reprojection = Projection::project3dBase(X, transform, intrinsics);
        if(!ceres::IsFinite(reprojection[0]) || !ceres::IsFinite(reprojection[1]))
            return false;

        residual[0] = scale*(reprojection[0] - T(m_image[0]));
        residual[1] = scale*(reprojection[1] - T(m_image[1]));
        /* this term makes sure the points stays in front of the camera */
        residual[2] = (reprojection[2]>0.0?T(0.0):(intrinsics[0]*intrinsics[1])*reprojection[2]);
        return true;

    }

    template <typename T> bool operator()(const T*  const transform,
                                          T* residual) const
    {
        const T objects[3] = {
            T(m_object[0]),
            T(m_object[1]),
            T(m_object[2])
        };

        const T intrinsics[4] = {
            T(m_intrinsics[0]),
            T(m_intrinsics[1]),
            T(m_intrinsics[2]),
            T(m_intrinsics[3])
        };

        const PoseEstimation::Vec<T,3> &reprojection = Projection::project3dBase<T>(objects, transform, intrinsics);
        if(!ceres::IsFinite(reprojection[0]) || !ceres::IsFinite(reprojection[1]))
            return false;

        residual[0] = scale*(reprojection[0] - T(m_image[0]));
        residual[1] = scale*(reprojection[1] - T(m_image[1]));
        /* this term makes sure the points stays in front of the camera */
        residual[2] = (reprojection[2]>0.0?T(0.0):(intrinsics[0]*intrinsics[1])*reprojection[2]);
        return true;

    }

};


Vector6d PoseEstimation::solvePnP(const Vector3fArrayView objects, const Vector2fArrayView images,
                                  const Vector4d & intrinsics, const Vector6d & pose)
{
    SolvePNPProblem problem(objects, images, intrinsics, pose);
    return problem.pose();
}

PoseEstimation::PNPResult<std::vector<int>> PoseEstimation::solvePnPRansac(
        const Vector3fArrayView objects, const Vector2fArrayView images,
        const Vector4d &intrinsics,
        const float & distance,
        const int & trials,
        const Vector6d &pose
        )
{

    SolvePNPRansacProblem problem(objects, images, intrinsics, distance, trials, pose);

    return {problem.pose(), problem.inliers()};
}

Vector6d PoseEstimation::solvePnP(Frame f)
{
    if(!f.isValid())
        throw std::runtime_error("PoseEstimation::solvePnPRansac(): Frame invalid.");
    if(f.intrinsics() == Vector4d())
        throw std::runtime_error("PoseEstimation::solvePnPRansac(): Frame has no intrinsics");

    TrackList annotTracks = f.tracks().annotated();
    LandmarkList annotLandmarks = annotTracks.landmarks();
    Vector2fArray trackPositions;
    Vector3fArray landmarkPositions;
    annotTracks.getPositionsInFrame(trackPositions, f);
    annotLandmarks.getPositions(landmarkPositions);


    return hookers::toolbox::PoseEstimation::solvePnP(landmarkPositions,
                                                            trackPositions,
                                                            f.intrinsics());

}

PoseEstimation::PNPResult<std::vector<int>>
PoseEstimation::solvePnPRansac(Frame f, const float &distance, const int &trials, const Vector6d pose)
{
    if(!f.isValid())
        throw std::runtime_error("PoseEstimation::solvePnPRansac(): Frame invalid.");
    if(f.intrinsics() == Vector4d())
        throw std::runtime_error("PoseEstimation::solvePnPRansac(): Frame has no intrinsics");

    TrackList annotTracks = f.tracks().annotated();
    LandmarkList annotLandmarks = annotTracks.landmarks();
    Vector2fArray trackPositions;
    Vector3fArray landmarkPositions;
    annotTracks.getPositionsInFrame(trackPositions, f);
    annotLandmarks.getPositions(landmarkPositions);


    return hookers::toolbox::PoseEstimation::solvePnPRansac(landmarkPositions, trackPositions,
                                                            f.intrinsics(), distance, trials);
}



SolvePNPProblem::SolvePNPProblem()
{
}

SolvePNPProblem::SolvePNPProblem(const Vector3fArrayView objects,
                                 const Vector2fArrayView images,
                                 const Vector4d &intrinsics,
                                 const Vector6d &pose, const Array2dView uncertainty2d)
    : m_solved(false), m_covarianced(false),
      objects(objects), images(images),
      intrinsics(intrinsics), m_pose(pose), m_poseCovar(6,6),
      uncertainty2d(uncertainty2d)

{

    if(objects.size() != images.size())
        throw std::runtime_error("Image and Object list size must match.");

    if(uncertainty2d.size() > 1 && uncertainty2d.shape()[0] != objects.size())
        throw std::runtime_error("Uncertainty2d given but shape doesn not match with object list size.");

    if(uncertainty2d.size() > 1 && uncertainty2d.shape()[1] != 1)
        throw std::runtime_error("Component-wise uncertainty2d given. This is not supported yet!");


    for(int i = 0; i < objects.size(); i++) {

        Vector3d object(objects(i)[0],  objects(i)[1], objects(i)[2]);
        Vector2d image(images(i)[0], images(i)[1]);

        double uncertainty = (uncertainty2d.size() == 1)?uncertainty2d(0,0):1.0;
        if(uncertainty2d.size() > 1)
            uncertainty = uncertainty2d(0,i);

        auto block = new ReprojectionErrorTerm::SolvePnP(new ReprojectionErrorTerm(
                                                             object, image, intrinsics,
                                                             sqrt(uncertainty)));
        problem.AddResidualBlock(block, nullptr, m_pose.data());
    }

}

void SolvePNPProblem::solve()
{
    if(m_solved)
        return;

    ceres::Solver::Options options;
    options.linear_solver_type = ceres::DENSE_SCHUR;
    options.minimizer_progress_to_stdout = false;
    options.max_num_iterations = 100;
    options.minimizer_type = ceres::TRUST_REGION;
    ceres::Solver::Summary summary;
    ceres::Solve(options, &problem, &summary);

    if(!summary.IsSolutionUsable()) {
            throw PoseEstimation::Failure("PoseEstimation::solvePnP failed: Could not estimate camera pose.");
    }

    m_solved = true;

}

void SolvePNPProblem::covarianceAnalysis()
{

    solve();

    if(m_covarianced)
        return;

    ceres::Covariance::Options options;
    ceres::Covariance covar(options);

    std::vector<std::pair<const double*,double const*>> blocks =
    {
      {m_pose.data(), m_pose.data()}
    };

    if(!covar.Compute(blocks, &problem)) {
        throw std::runtime_error("Covariance analysis failed.");
    }

    covar.GetCovarianceBlock(m_pose.data(), m_pose.data(),
                             m_poseCovar.data());





}

Vector6d SolvePNPProblem::pose() {
    if(!m_solved)
        solve();
    return m_pose;
}

Array2d SolvePNPProblem::poseUncertainty()
{
    covarianceAnalysis();
    return m_poseCovar;
}



//BundleAdjustmentProblem::BundleAdjustmentProblem(FrameList frames)
//{

////    if(!f.isValid())
////        throw std::runtime_error("PoseEstimation::BundleAdjustment(): Frame invalid.");
////    if(f.intrinsics() == Vector4d())
////        throw std::runtime_error("PoseEstimation::BundleAdjustment(): Frame has no intrinsis.");


//    /* find all landmarks that should be used for bundle adjustment */
//    {
//        LandmarkSet landmarks;
//        for(const auto& frame : frames) {
//            auto baseLandmarks = frame.tracks(Frame::View::Base).landmarks();
//            landmarks |= LandmarkSet::fromList(baseLandmarks);
//        }

//        /* make the objects a ordered list */
//        objects = landmarks.toList();

//    }

//    images.clear();
//    images.resize(frames.size());

//    imagesToObjects.clear();
//    imagesToObjects.resize(frames.size());

//    for(size_t i = 0; i < frames.size(); i++){

//        Frame f = frames[i];


//        auto tracks = f.tracks(Frame::View::Base);
//        tracks.getPositionsInFrame(images[i], f, Frame::View::Base);

//        auto landmarks = tracks.landmarks();

//        for(auto l: landmarks) {
//            int pos = objects.find(l) - objects.begin();

//            imagesToObjects[i].push_back(pos);
//        }
//    }



//}


SolvePNPRansacProblem::SolvePNPRansacProblem(const Vector3fArrayView objects,  const Vector2fArrayView images,
                                             const Vector4d &intrinsics,
                                             const float &distance, const int &trials,
                                             const Vector6d &pose, const Array2dView uncertainty2d)
{
    const int MIN_POINTS  = 5;
    const float distanceSq = distance*distance;

    if(objects.size() < MIN_POINTS) {
        std::stringstream err;
        err << "solvePnPRansac: must provide at least " << MIN_POINTS;
        throw std::runtime_error(err.str());
    }

    if(objects.size() != images.size()) {
        throw std::runtime_error("objects and images musth have same size");
    }


    /* trial history entry type */
    typedef std::pair<std::pair<Vector6d, double>, std::vector<int>> THE;
    std::vector<THE> trialHistory;

    std::default_random_engine generator;
    std::uniform_int_distribution<int> distribution(0,objects.size()-1);


    Vector3fArray lobjs(MIN_POINTS);
    Vector2fArray limgs(MIN_POINTS);
    for(int i = 0; i < trials*objects.size(); i++) {

        /* generate a set of random samples. */
        std::vector<int> points;
        while(true) {
            int p = distribution(generator);

            if(std::find(points.cbegin(), points.cend(), p) != points.cend())
                continue;
            points.push_back(p);
            if(points.size() >= MIN_POINTS)
                break;
        }

        for(size_t i = 0; i < points.size(); i++) {
            lobjs(i) = objects(points[i]);
            limgs(i) = images(points[i]);
        }

        try {

            Vector6d lpose;
            lpose = PoseEstimation::solvePnP(lobjs, limgs, intrinsics, pose);

            /* create a new entry in the trial buffer */
            trialHistory.emplace_back();
            trialHistory.back().first.first = lpose;

            /* caclulate the error of the reprojection with respect to the
             * measurement. If the error is smaller than specified,
             * push it to the list of points that lie inside.
             */
            double calcReprojRmsSqSum = 0.0;
            for(int i = 0; i < objects.size(); i++) {
                double currentDistanceSq = -1;
                try {
                    Vector3f reprojection3d = PoseEstimation::reproject3d(objects(i),
                                                                      Vector6f(lpose),
                                                                      Vector4f(intrinsics));
                    Vector2f reprojection(reprojection3d[0], reprojection3d[1]);


                    /* the point is behind the camera... no good choice */
                    if(reprojection3d[2] <= 0.0)
                        continue;

                    currentDistanceSq = (reprojection-images(i)).squaredMagnitude();
                    if(currentDistanceSq > distanceSq)
                        continue;
                } catch (...) {
                    continue;
                }

                if( currentDistanceSq >= 0) {
                    calcReprojRmsSqSum += currentDistanceSq;
                    trialHistory.back().second.push_back(i);
                }
            }
            trialHistory.back().first.second = sqrt(calcReprojRmsSqSum/(double)trialHistory.back().second.size());;
        } catch(...) {
            continue;
        }
    }

    if(trialHistory.empty()) {
        throw PoseEstimation::Failure("solvePnPRansac: could not find any viable set of points.");
    }

    /* sort the list according to their sizes */
    std::sort(trialHistory.begin(), trialHistory.end(),
              [&](const THE & a, const THE & b){
        //return (a.second.size() == b.second.size())?(a.first.second > b.first.second):(a.second.size() < b.second.size());
        return a.second.size() < b.second.size();
    });

    m_inliers = trialHistory.back().second;
    const Vector6d & bestPose = trialHistory.back().first.first;

    if(m_inliers.empty()) {
        throw PoseEstimation::Failure("solvePnPRansac: could not find any viable set of points.");
    }

    std::cout << "Best: " << m_inliers.size() << "/" << objects.size() << ", " << bestPose << "; " << trialHistory.back().first.second << std::endl;

    /* create a list of points that are withing the error
     * and use this list to finally estimate the camera pose */
    Vector3fArray bobjs(m_inliers.size());
    Vector2fArray bimgs(m_inliers.size());
    for(size_t i = 0; i < m_inliers.size(); i++) {
        bobjs(i) = objects(m_inliers[i]);
        bimgs(i) = images(m_inliers[i]);
    }

    m_problem = std::unique_ptr<SolvePNPProblem>(new SolvePNPProblem(bobjs, bimgs, intrinsics, bestPose,
                                                                     uncertainty2d));

}

Vector6d SolvePNPRansacProblem::pose()
{
    if(!m_problem)
        throw std::runtime_error("solvePNPRansacProblem: No problem defined.");

    return m_problem->pose();
}

Array2d SolvePNPRansacProblem::poseUncertainty()
{
    if(!m_problem)
        throw std::runtime_error("solvePNPRansacProblem: No problem defined.");

    return m_problem->poseUncertainty();
}




void BundleAdjustmentProblem::setup()
{

    if(m_problemSetup)
        return;


    if(m_images.size() !=  m_poses.size()) {
        throw std::runtime_error("List of images and poses have different size.");
    }

    /* set up object priors */
    for(auto & object: m_objects) {
        auto & pos = std::get<ObjectParametrization::POS>(object);
        auto & flags = std::get<ObjectParametrization::FLAGS>(object);

        /* only if the point has been found useable, add it */
        if(flags & ObjectUnuseable)
            continue;

        m_problem.AddParameterBlock(pos.data(), pos.size());
        m_problem.SetParameterBlockConstant(pos.data());

//        m_problem.SetParameterBlockVariable(pos.data());

        /** object priors are only added for known object positions */
        if(!(flags & ObjectUnlinked)) {
            auto block = new QuadraticPrior<3>::AutoDiff(new QuadraticPrior<3>(pos.data(), objectsUncertainty));
            objectBlocks.push_back(m_problem.AddResidualBlock(block, nullptr, pos.data()));
        } else {
            /** @todo this is a dirty hack in order to avoid rank deficiency */
            auto block = new QuadraticPrior<3>::AutoDiff(new QuadraticPrior<3>(pos.data(), 100.0));
            objectBlocks.push_back(m_problem.AddResidualBlock(block, nullptr, pos.data()));

        }
    }

    projectionUnlinkedUncertainty = 0;
    /* iterate over the list of different frames */
    for(unsigned imageId = 0; imageId < m_images.size(); imageId++) {

        auto & imagePara = m_images[imageId];
        auto & pose = m_poses[imageId];

        for(auto & image: imagePara) {

            /* get referemce to image information */
            auto & imgPos      = std::get<ImageParametrization::POS>(image);
            auto & imgToObject = std::get<ImageParametrization::TO_OBJECT>(image);
            auto & imgFlags     = std::get<ImageParametrization::FLAGS>(image);
            auto & imgUncertainty = std::get<ImageParametrization::UNCERTAINTY>(image);;

            /* get reference to object information */
            auto & objectPos = std::get<ObjectParametrization::POS>(m_objects[imgToObject]);
            auto & objectFlags = std::get<ObjectParametrization::FLAGS>(m_objects[imgToObject]);


            if(imgFlags & ImageInvalid)
                continue;

            /* only if the point has been found useable, add it */
            if(objectFlags & ObjectUnuseable)
                continue;

            if(!m_problem.HasParameterBlock(objectPos.data()))
                throw std::runtime_error("BundleAdjustmentProblem::setup(): adding projection term for unknown object block.");

            auto block = new ReprojectionErrorTerm::BundleAdjustment(new ReprojectionErrorTerm(
                                                                         objectPos, Vector2d(imgPos[0], imgPos[1]),
                                                                         m_intrinsics,
                                                                         imgUncertainty));


            /* add loss function for triangulated objects */
            ceres::LossFunction * loss = nullptr;
//            if(objectFlags & ObjectUnlinked) {
//                loss = new ceres::CauchyLoss(1);
//            }

            auto blockId = m_problem.AddResidualBlock(block, loss,
                                                      objectPos.data(), pose.data(),
                                                      m_intrinsics.data());
            if(!(objectFlags & ObjectUnlinked))
                projectionBlocks.push_back(blockId);
            else
            {
                projectionUnlinkedBlocks.push_back(blockId);
                projectionUnlinkedUncertainty += imgUncertainty;
            }
        }
    }
    if(projectionUnlinkedBlocks.size() > 0)
        projectionUnlinkedUncertainty *= 1.0/(double)projectionUnlinkedBlocks.size();
    /* set up the intrinsics prior.
     * add an intrinsics block for each camera pose in order to weight it correctly */
    for(unsigned i = 0; i < m_poses.size(); i++) {
        auto block = new QuadraticPrior<4>::AutoDiff(new QuadraticPrior<4>(m_intrinsics.data(), m_intrinsicSigma.data()));
        m_problem.AddResidualBlock(block, nullptr, m_intrinsics.data());
    }

    m_problemSetup = true;
}

BundleAdjustmentProblem::BundleAdjustmentProblem(ObjectParametrization &objects,
                                                 std::vector<ImageParametrization> &images,
                                                 const Vector4d &intrinsics,
                                                 std::vector<Vector6d> & poses)
    : m_problemSetup(false),
      m_objects(objects),
      objectsUncertainty(0.035),
      m_images(images),
      imageUncertainty(2.0),
      m_poses(poses),
      m_intrinsics(intrinsics),
      m_intrinsicSigma{2,2,1.4,1.4},
      distortion(0.0)
{

    /* if no poses where provided, initialize with
     * zero poses */
    if(poses.empty()) {
        for(auto & image: images) {
            this->m_poses.push_back(Vector6d());
        }
    }
}

void BundleAdjustmentProblem::setConstantObjectPositions(bool constant)
{
    this->setConstantLinkedObjectPositions(constant);
    this->setConstantTriangulatedObjectPositions(constant);
}

void BundleAdjustmentProblem::setConstantTriangulatedObjectPositions(bool constant)
{
    /* search for all objects that are not unlinked, make them constant */
    for(auto & o: m_objects) {
        auto & pos = std::get<ObjectParametrization::POS>(o);
        auto & flags = std::get<ObjectParametrization::FLAGS>(o);

        /* only apply this function to required objects */
        if((flags & ObjectUnuseable)) {
            continue;
        }

        /* only apply this function to required objects */
        if(!(flags & ObjectUnlinked)) {
            continue;
        }


        if(constant) {
            m_problem.SetParameterBlockConstant(pos.data());
        } else {
            m_problem.SetParameterBlockVariable(pos.data());
        }

    }

}

void BundleAdjustmentProblem::setConstantLinkedObjectPositions(bool constant)
{
    /* search for all objects that are not unlinked, make them constant */
    for(auto & o: m_objects) {
        auto & pos = std::get<ObjectParametrization::POS>(o);
        auto & flags = std::get<ObjectParametrization::FLAGS>(o);

        if((flags & ObjectUnuseable)) {
            continue;
        }

        /* only apply this function to required objects */
        if((flags & ObjectUnlinked)) {
            continue;
        }

        if(constant) {
            m_problem.SetParameterBlockConstant(pos.data());
        } else {
            m_problem.SetParameterBlockVariable(pos.data());
        }
    }
}

void BundleAdjustmentProblem::setConstantIntrinsics(bool constant)
{
    if(constant) {
        m_problem.SetParameterBlockConstant(m_intrinsics.data());
    } else {
        m_problem.SetParameterBlockVariable(m_intrinsics.data());
    }

}


void BundleAdjustmentProblem::triangulate() {

    ceres::Solver::Options options;
    options.linear_solver_type = ceres::DENSE_QR;
    options.minimizer_type = ceres::TRUST_REGION;
    options.num_threads = std::min(std::thread::hardware_concurrency(), unsigned(8));
    options.update_state_every_iteration = false;

    options.max_num_iterations = 15000;
    options.function_tolerance = 10e-10;


//    options.minimizer_progress_to_stdout = true;


    int finallyUsedCount = 0;
    int totalUnlinkedCount = 0;
    double meanReproError = 0;

    for(unsigned objectId = 0; objectId < m_objects.size(); objectId++) {


        auto & o = m_objects[objectId];
        auto & objectPos = std::get<ObjectParametrization::POS>(o);
        auto & objectFlags = std::get<ObjectParametrization::FLAGS>(o);

        /* we are only interested in unlinked tracks */
        if(!(objectFlags & ObjectUnlinked))
            continue;


        totalUnlinkedCount++;

        ceres::Problem problem;

        for(auto & p: m_poses) {
            problem.AddParameterBlock(p.data(), p.size());
            problem.SetParameterBlockConstant(p.data());
        }

        std::vector<ceres::ResidualBlockId> reproBlockIds;

        const double expStd = 1.0;

        for(unsigned imageId = 0; imageId < m_images.size(); imageId++) {
            auto & img = m_images[imageId];
            for(auto & proj: img) {
                auto & imgPos = std::get<ImageParametrization::POS>(proj);
                auto & toObject = std::get<ImageParametrization::TO_OBJECT>(proj);

                if(toObject != int(objectId))
                    continue;

                auto block = new ReprojectionErrorTerm::BundleAdjustment(new ReprojectionErrorTerm(
                                                                             objectPos, Vector2d(imgPos[0], imgPos[1]),
                                                                             m_intrinsics,
                                                                             /*imageUncertainty*/expStd));
                reproBlockIds.push_back(
                            problem.AddResidualBlock(block, nullptr,
                                                     objectPos.data(), m_poses[imageId].data(),
                                                     m_intrinsics.data())
                );
//                std::cout << "images: " << imgPos << std::endl;
            }
        }


        /* check if the object has enough parameter blocks at all */
        if(problem.NumResidualBlocks() < 2) {
            objectFlags = ObjectFlags(objectFlags | ObjectUnuseable);
            continue;
        }


        /* set intrinsics data constant */
        problem.AddParameterBlock(m_intrinsics.data(), m_intrinsics.size());
        problem.SetParameterBlockConstant(m_intrinsics.data());


//        std::cout << "triangulating for object: " << objectId << "," << objectPos << std::endl;

        ceres::Solver::Summary summary;
        ceres::Solve(options, &problem, &summary);
//        std::cout << summary.BriefReport() << std::endl;


        {
            double cost;
            ceres::Problem::EvaluateOptions eval;
            eval.parameter_blocks.clear();
            eval.residual_blocks = reproBlockIds;
            problem.Evaluate(eval, &cost, nullptr, nullptr, nullptr);

            auto std = std::sqrt(cost/double(reproBlockIds.size()))*expStd;
//            std::cout << "repro (cost, rms):"
//                      << cost << ", " << std << std::endl;
            bool bIsManual = (objectFlags & ObjectFromManual);
            double used_uncertainty = bIsManual ? 10.0*imageUncertainty : imageUncertainty;
            if(std > used_uncertainty)  {
                objectFlags = ObjectFlags(objectFlags | ObjectUnuseable);
            } else {
                std::cout << ">>>> " << objectId << ":"  << cost << ", " << std << ", " << objectPos << std::endl;
                finallyUsedCount++;
                meanReproError += std;
            }
        }

    }

    if(finallyUsedCount) {
        meanReproError /= finallyUsedCount;
    }

    std::cout << "================" << std::endl;
    std::cout << " BA Triangulate " << std::endl;
    std::cout << "================" << std::endl;
    std::cout << "finally used: " << finallyUsedCount << " from " << totalUnlinkedCount << std::endl;
    std::cout << "mean repro error: " << meanReproError << "px" << std::endl;

    std::cout << std::endl;
    std::cout << std::endl;

}

void BundleAdjustmentProblem::solve()
{


    this->setup();

//    if(!m_problemSetup)
//        throw std::runtime_error("BundleAdjustmentProblem: solving problem that has not been setup yet.");

    ceres::Solver::Options options;
    options.minimizer_type = ceres::TRUST_REGION;
//    options.minimizer_type = ceres::LINE_SEARCH;
    options.linear_solver_type = ceres::SPARSE_SCHUR;
//    options.linear_solver_type = ceres::ITERATIVE_SCHUR;
//    options.linear_solver_type = ceres::DENSE_SCHUR;

    options.num_threads = std::thread::hardware_concurrency();

    options.max_num_iterations = 15000;
    options.function_tolerance = 1e-12;

    options.minimizer_progress_to_stdout = true;

    /* set camera poses variable */
    for(auto & p : m_poses) {
        m_problem.SetParameterBlockVariable(p.data());
    }

    /* set intrinsics data constant */
//    m_problem.SetParameterBlockVariable(m_intrinsics.data());
//    m_problem.SetParameterBlockConstant(intrinsics.data());


//    /* search for all objects that are going to be used in bundle adjustment and
//      make them variable. If an object is unuseable, then set it constant */
//    for(auto & o: m_objects) {
//        auto & pos = std::get<ObjectParametrization::POS>(o);
//        auto & flags = std::get<ObjectParametrization::FLAGS>(o);

//        if(!(flags & ObjectUnuseable)) {
//            m_problem.SetParameterBlockVariable(pos.data());
//        } else {
//            m_problem.SetParameterBlockConstant(pos.data());
//        }
//    }


    ceres::Solver::Summary summary;
    ceres::Solve(options, &m_problem, &summary);

    std::cout << std::endl;
    std::cout << std::endl;
    std::cout << "=================" << std::endl;
    std::cout << "Bundle Adjustment" << std::endl;
    std::cout << "=================" << std::endl;

    std::cout << "intrinsics: " << m_intrinsics << std::endl;
    std::cout << "distortion: " << distortion << std::endl;

    {
        double cost;
        ceres::Problem::EvaluateOptions eval;
        eval.parameter_blocks.clear();
        eval.residual_blocks = projectionBlocks;
        m_problem.Evaluate(eval, &cost, nullptr, nullptr, nullptr);

        std::cout << "proj. error (cost [px^2], rms [px]):"
                  << cost << ", " << std::sqrt(cost/double(projectionBlocks.size()))*imageUncertainty;
    }

    if(projectionUnlinkedBlocks.size() > 0)
    {
        double cost;
        ceres::Problem::EvaluateOptions eval;
        eval.parameter_blocks.clear();
        eval.residual_blocks = projectionUnlinkedBlocks;
        m_problem.Evaluate(eval, &cost, nullptr, nullptr, nullptr);

        std::cout << " ;unl. "
                  << cost << ", " << std::sqrt(cost/double(projectionUnlinkedBlocks.size()))*projectionUnlinkedUncertainty << std::endl;
    }
    else
        std::cout << std::endl;

    {
        double cost;
        ceres::Problem::EvaluateOptions eval;
        eval.parameter_blocks.clear();
        eval.residual_blocks = objectBlocks;
        m_problem.Evaluate(eval, &cost, nullptr, nullptr, nullptr);

        std::cout << "object (cost [m^2], rms [m]):"
                  << cost << ", " << std::sqrt(cost/double(objectBlocks.size()))*objectsUncertainty << std::endl;
    }

    if(!summary.IsSolutionUsable()) {
            throw PoseEstimation::Failure("PoseEstimation::BundleAdjustmentProblem failed: Could not estimate camera poses.");
    }

    std::cout << std::endl;
    std::cout << std::endl;

}


std::vector<Array2d> BundleAdjustmentProblem::poseUncertainties(std::vector<int> poses)
{

    /* handle default value. fill it with all indices we need */
    if(poses.empty()) {
        for(unsigned i = 0; i < m_poses.size(); i++) {
            poses.push_back(i);
        }
    }

    std::vector<Array2d> covariances(poses.size(), Array2d(6,6));

    ceres::Covariance::Options options;
    ceres::Covariance covar(options);


    std::vector<std::pair<const double*,double const*>> blocks;
    for(unsigned i = 0; i < poses.size(); i++) {
        auto & pose = m_poses[poses[i]];
        blocks.push_back({pose.data(), pose.data()});
    }

    blocks.push_back({m_poses[0].data(), m_poses[1].data()});


    if(!covar.Compute(blocks, &m_problem)) {
        throw std::runtime_error("Covariance analysis failed.");
    }

    for(unsigned i = 0; i < poses.size(); i++) {
        auto & pose = m_poses[poses[i]];
        covar.GetCovarianceBlock(pose.data(), pose.data(),
                                 covariances[i].data());
    }


    return covariances;
}

std::vector<Array2d> BundleAdjustmentProblem::posePairwiseUncertainties(std::vector<std::pair<int, int> > poses)
{


    /* if argument list is empty, fill with default, a list of consecutive frames */
    if(poses.empty()) {
        for(unsigned i = 0; i < m_poses.size()-1; i++) {
            poses.push_back({i, i+1});
        }
    }


    typedef Array2d::difference_type Shape;

    /** @returns list of covariances with shape 12x12 where the first 6x6 diagonal
     *           block corresponds to first pose of pair and second 6x6 diagonal block
     *           corresponds to second pose. Off diagonal elements are covariances
     *           between both poses.
     */
    std::vector<Array2d> covariances(poses.size(), Array2d(12,12));

    std::vector<std::pair<const double*,double const*>> blocks;

    ceres::Covariance::Options options;
    ceres::Covariance covar(options);


    /* define all blocks that are required for one pair of poses.
     * N is the number of blocks per pose pair and is later used
     * to calculate striding */
    const int N = 3;
    for(unsigned i = 0; i < poses.size(); i++) {

        auto & pose1 = m_poses[poses[i].first];
        auto & pose2 = m_poses[poses[i].second];


        /* make sure we add the same covariance block only once */
        std::pair<const double*,double const*> block1 = {pose1.data(), pose1.data()};
        if(std::find(blocks.cbegin(), blocks.cend(), block1) == blocks.cend()) {
            blocks.push_back(block1);
        }

        /* make sure we add the same covariance block only once */
        std::pair<const double*,double const*> block2 = {pose2.data(), pose2.data()};
        if(std::find(blocks.cbegin(), blocks.cend(), block2) == blocks.cend()) {
            blocks.push_back(block2);
        }

        blocks.push_back({pose1.data(), pose2.data()});
    }

    /* compute covariance blocks using ceres */
    if(!covar.Compute(blocks, &m_problem)) {
        throw std::runtime_error("Covariance analysis failed.");
    }

    /* fetch them and assmeble covariance matrices */
    for(unsigned i = 0; i < poses.size(); i++) {


        /* reference to pose vectors */
        auto & pose1 = m_poses[poses[i].first];
        auto & pose2 = m_poses[poses[i].second];

        /* reference to target covariance matrix */
        auto &covm  = covariances[i];

        /* temp buffer to store fetched covariance blocks */
        Array2d tmp(6,6);


        /* diagonal block for first pose */
        covar.GetCovarianceBlock(pose1.data(), pose1.data(),
                                 tmp.data());
        covm.subarray(Shape(0,0),Shape(6,6)) = tmp;


        /* diagonal block for second pose */
        covar.GetCovarianceBlock(pose2.data(), pose2.data(),
                                 tmp.data());
        covm.subarray(Shape(6,6),Shape(12,12)) = tmp;


        /* fill off-diagonal blocks with inter-pose covariances */
        covar.GetCovarianceBlock(pose1.data(), pose2.data(),
                                 tmp.data());
        covm.subarray(Shape(6, 0), Shape(12, 6)) = tmp;
        covm.subarray(Shape(0, 6), Shape(6, 12)) = tmp.transpose();

    }

    return covariances;
}


