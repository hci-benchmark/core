#ifndef HOOKERS_TOOLBOX_PROJECTION_H
#define HOOKERS_TOOLBOX_PROJECTION_H


#include <unordered_set>

#include <ceres/rotation.h>

#include <hookers/interface/NumericTypes.h>

#include <vigra/multi_math.hxx>

namespace hookers {
namespace toolbox {


/**
 * @brief The Projection Base Class
 *
 * This class is a common interface to projection instances.
 *
 *
 */
class Projection
{


public:

public:
    template<typename T, unsigned N>
    using Vec = vigra::TinyVector<T, N>;

    template<typename T, unsigned N>
    using VecView = vigra::TinyVectorView<T, N>;

    template<typename T, unsigned N, typename DERIVED>
    using VecBase = vigra::TinyVectorBase<T, N, T*, DERIVED>;

    class NotProjectedError : public std::runtime_error
    {
        std::string what;
    public:
        NotProjectedError(const std::string & whatElse = std::string())
            : std::runtime_error("Information not available until projection" + whatElse)
        {}

    };

protected:
    int c_pointSize;


public:
    Projection(const hookers::Vector4f & intrinsics = hookers::Vector4f());

    /**
     * defines the point size of each point.
     */
    void setPointSize(int size);

    /** set the point cloud.
     * If color is not provided, then it will be set to the id of the point
     *
     * @warning the cloud must be available for the whole life time of Projection instance
     */
    virtual void setCloud(hookers::Array2fView cloud,
                          hookers::Array3iView color = hookers::Array3iView()) = 0;

    /** sets camera pose */
    virtual void setPose(const hookers::Vector6f & pose) = 0;


    /**
     *
     * @param intrinsics focal length and principal point (f_x, f_y, c_x, c_y)
     * @brief set camera intrinisics.
     */
    void setIntrinsics(const hookers::Vector4f intrinsics);


    /**
     * @brief returns projected IDs of pointcloud
     * @param target 2D array view where the projected id's should be placed.
     *
     * The id is equal to the index array of the pointcloud provided by setCloud()
     *
     */
    virtual void getPoints(hookers::Array2iView target) = 0;

    /**
     * @brief getPointList
     * @param target 1D array of integers. Target array will be reshaped to match the number of points
     * @warning this function can only be called after a projection pass has been made
     */
    virtual void getPointList(std::unordered_set<int> & target) = 0;

    /** get the depth information */
    virtual void getDepth(hookers::Array2fView target)= 0;

    /** get color information */
    virtual void getColor(hookers::Array3fView target) = 0;

    /**
     * @brief get an array of model points
     * @param target
     *
     * This function only returns fragments where points from the pointcloud have been reprojected.
     * Different depths like shaded triangles are omited.
     */
    virtual void getPositions(hookers::Array3fView target) = 0;




    /**
     * software projection implementation for a single 3d point to 2d projection.
     *
     * @param object 3d point that is going to be reprojected
     * @param transform the camera pose given in (t_x, ..., r_x, ... , r_z)
     * @param intrinsics focal lengths and principal point (f_x, f_y, c_x, c_y)
     * @param distortion coefficients k_1...k_DISTORTION as used in standard camera calibration
     */
    template<typename T, int DISTORTION=0> inline static
    Vec<T,3> project3dBase(
            const T* const objectv,
            const T* const transformv,
            const T* const intrinsicsv,
            const T* const distortionv=nullptr) {

        VecView<T,3> object(objectv);
        VecView<T,4> intrinsics(intrinsicsv);
        VecView<T,6> transform(transformv);

        Vec<T,3> reprojection;

        /* perform pointwise transformation */
        Vec<T,3> p;

        ceres::AngleAxisRotatePoint(transform.data()+3, object.data(), p.data());
        p[0] += transform[0];
        p[1] += transform[1];
        p[2] += transform[2];


        /* perspective devide */
        p[0] /= p[2];
        p[1] /= p[2];

        /* if enabled by template parameter */
        if(DISTORTION == 8 && distortionv){

            const T* const k = distortionv;

            const T &x = p[0];
            const T &y = p[1];

            const T r2 = x*x+y*y;
            const T r4 = r2*r2;
            const T r6 = r4*r2;

            const T a1 = T(2)*x*y;
            const T a2 = r2 + T(2)*x*x;
            const T a3 = r2 + T(2)*y*y;

            const T d = (T(1) + T(k[0])*r2 + T(k[1])*r4 + T(k[4])*r6)/(T(1) + T(k[5])*r2 + T(k[6])*r4 + T(k[7])*r6);
            p[0] = x*d + T(k[2])*a1 + T(k[3])*a2;
            p[1] = y*d + T(k[2])*a3 + T(k[3])*a1;

        }

        const T & fx = intrinsics[0];
        const T & fy = intrinsics[1];
        const T & cx = intrinsics[2];
        const T & cy = intrinsics[3];

        /* from normalized camera coordinates to pixel coordinates */
        reprojection[0] = fx * p[0] + cx;
        reprojection[1] = fy * p[1] + cy;
        reprojection[2] = p[2];

        return reprojection;
    }

    template<typename T> inline static
    Vec<T,3> project3d(const Vec<T,3> & object,
                         const Vec<T,6> & transform,
                         const Vec<T,4> & intrinsics) {
        return project3dBase(object.data(),
                             transform.data(),
                             intrinsics.data());
    }



    /**
     * transforms objects in place
     * @overload
     */

    template<typename T> inline static
    void project3d(Vector3fArrayView objects,
                     const Vec<T,6> & transform,
                     const Vec<T,4> & intrinsics) {

        /* rotation */
        vigra::Matrix<float> rotMatLeft = Projection::rotationMatrixFromVector(transform);
        objects.expandElements(0) = vigra::linalg::mmul(rotMatLeft,objects.expandElements(0));

        /* translation */
        for(int i : {0,1,2}){
            objects.bindElementChannel(i) += transform[i];
        }

        const float fx = intrinsics[0];
        const float fy = intrinsics[1];
        const float cx = intrinsics[2];
        const float cy = intrinsics[3];

        /* perspective devide */
        {
            using namespace vigra::multi_math;
            objects.bindElementChannel(0) /= objects.bindElementChannel(2);
            objects.bindElementChannel(0) *= fx;
            objects.bindElementChannel(0) += cx;

            objects.bindElementChannel(1) /= objects.bindElementChannel(2);
            objects.bindElementChannel(1) *= fy;
            objects.bindElementChannel(1) += cy;
        }
    }

    /**
     * @brief inverts project3d
     *
     * this function inverts project3d such that reproject3d(project3d(X)) == X.
     *
     * @param object point in logical camera coordinates (u, v, z) with
     *        z beeing the distance of the point to the camera plane in world
     *        coordinates.
     * @param transform the camera pose given in (t_x, ..., r_x, ... , r_z)
     * @param intrinsics focal lengths and principal point (f_x, f_y, c_x, c_y)
     */
    template<typename T> inline static
    Vec<T,3> reproject3dBase(
            const T* const objectv,
            const T* const transformv,
            const T* const intrinsicsv) {

        VecView<T,3> object(objectv);
        VecView<T,4> intrinsics(intrinsicsv);
        VecView<T,6> transform(transformv);

        const T & fx = intrinsics[0];
        const T & fy = intrinsics[1];
        const T & cx = intrinsics[2];
        const T & cy = intrinsics[3];

        /* pull back from logical coordinates to 3d camera system in world units */
        Vec<T,3> p;
        p[0] = object[2]/fx*(object[0]-cx);
        p[1] = object[2]/fx*(object[1]-cy);
        p[2] = object[2];

        /* now we need to invert the camera extrinsics transformations
         * to get back into world coordinates */
        p[0] -= transform[0];
        p[1] -= transform[1];
        p[2] -= transform[2];

        /* angle axis vector.
         * in order to invert the rotation.
         * we just need to flip the vector itself. */
        Vec<T,3> r{-transform[3],-transform[4],-transform[5]};

        /* calculate the rotated point */
        Vec<T,3> reprojection;
        ceres::AngleAxisRotatePoint(r.data(), p.data(), reprojection.data());

        return reprojection;
    }


    /** @overload */
    template<typename T> inline static
    Vec<T,3> reproject3d(const Vec<T,3> & object,
                         const Vec<T,6> & transform,
                         const Vec<T,4> & intrinsics) {
        return reproject3dBase(object.data(),
                               transform.data(),
                               intrinsics.data());
    }

    /** calculates full transformation matrix R|t from reduced camera pose vector */
    static vigra::Matrix<double> matrixFromVector(const Vector6d & vec);

    /** creates the 3x3 rotation matrix part from given camera pose vector */
    static vigra::Matrix<double> rotationMatrixFromVector(const Vector6d & vec);

    static Vector6d vectorFromMatrix(vigra::MultiArray<2, double> mat);



private:



};

}
}

#endif // HOOKERS_TOOLBOX_PROJECTION_H
