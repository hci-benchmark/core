#include "SoftwareProjection.h"

using namespace hookers::toolbox;

SoftwareProjection::SoftwareProjection(Array2fView cloud, Vector4f intr) :
    cloud(cloud), intr(intr)
{
    RT = Array2f(Array2fShape{4,4});
    for(int i = 0; i < 4; i++)
        RT(i,i) = 1.0;

}

SoftwareProjection::SoftwareProjection(Vector3fArrayView &cloud, Vector4f intr)
    : SoftwareProjection(cloud.expandElements(0), intr) {}


void SoftwareProjection::setCloud(Array2fView cloud, Array3iView color)
{

}

void SoftwareProjection::setPose(const Vector6f &pose)
{
    zBuffer = Vec3fArr2d();
    idBuffer = Array2i();

    RT = matrixFromVector(pose);
}

void SoftwareProjection::getPoints(Array2iView target)
{

}

void SoftwareProjection::getPointList(std::unordered_set<int> &target)
{

    using namespace vigra::multi_math;

    if(!idBuffer.data())
        throw NotProjectedError(" in getPointList()");

    /* count how many points have been reprojected */


    auto pointCount = sum<int>(idBuffer>-1);

    /* if there are no points that set the array empty */
    if(!pointCount) {
        return;
    }

    /* resize the array according to the number of points */
    target.reserve(pointCount);

    for(int xx = 0; xx < idBuffer.shape(0); xx++) {
        for(int yy = 0; yy < idBuffer.shape(1); yy++) {

            if(idBuffer(xx, yy) < 0)
                continue;

            target.insert(idBuffer(xx,yy));
        }
    }
}

void SoftwareProjection::getDepth(Array2fView target)
{

    using namespace vigra::multi_math;

    zBuffer = Vec3fArr2d(target.shape(), Vector3f(std::numeric_limits<float>::infinity()));
    idBuffer = Array2i(target.shape(), -1);


    project(RT, zBuffer.expandElements(0), idBuffer);

    /* check if data has been requested. if not, then we can return here */
    if(!target.data())
        return;

    target = zBuffer.bindElementChannel(2);
}


void SoftwareProjection::getColor(Array3fView target)
{

}

void SoftwareProjection::getPositions(Array3fView target)
{

    if(!(target.bindInner(0).shape() == idBuffer.shape()))
        throw std::runtime_error("Target shape does not match id shape");


    /* fill by default with infinity, denoting no point */
    target = std::numeric_limits<float>::infinity();

    for(int xx = 0; xx < target.shape(1); xx++) {
        for(int yy = 0; yy < target.shape(2); yy++) {
            int id = idBuffer(xx, yy);
            if(id >= 0) {
                target(0, xx, yy) = cloud(0, id);
                target(1, xx, yy) = cloud(1, id);
                target(2, xx, yy) = cloud(2, id);
            }
        }
    }
}
