#ifndef SOFTWAREPROJECTION_H
#define SOFTWAREPROJECTION_H

#include <iostream>
#include <vector>

#include <hookers/toolbox/Projection/Projection.h>

namespace hookers {
namespace toolbox {


/**
 * @brief Software Implementation of Cloud Renderer
 *
 * This version is used for a quick and dirty solution without worrying about openGL contexts.
 * It is very limited however. It can only project points and is not capable of rendering surfaces.
 * If you need this, consider using OpenGLProjection class instead.
 *
 * However, it can be used as fallback replacement for openGL renderer if no hardware is available.
 */
class SoftwareProjection : public Projection
{


public:
    typedef vigra::MultiArray<2, Vector3f> Vec3fArr2d;


private:

    vigra::MultiArray<2, float> cloud;
    Vector4f intr;

    Array2f RT;

    Vec3fArr2d zBuffer;
    Array2i idBuffer;

public:
    SoftwareProjection(Array2fView cloud, Vector4f intr);
    SoftwareProjection(Vector3fArrayView &cloud, Vector4f intr);

    void setCloud(Array2fView cloud, Array3iView color);
    void setPose(const Vector6f &pose);


    void getPoints(Array2iView target);
    void getPointList(std::unordered_set<int> & target);

    /**
     * @brief get depth map
     * @param target
     *
     * pixels that are not covered by any point are marked with std::numeric_limits::infinity()
     */
    void getDepth(Array2fView target);


    void getColor(Array3fView target);

    void getPositions(Array3fView target);

protected:
    template<typename T = float>
    void project(vigra::MultiArrayView<2, T> RT, vigra::MultiArrayView<3, T> zBuffer3D, vigra::MultiArrayView<2, int> iddata) {

        const auto WIDTH = zBuffer3D.shape(1);
        const auto HEIGHT = zBuffer3D.shape(2);


        const auto rt = RT.data();

        T t[3];
        for (int ii = 0; ii < cloud.shape(1); ++ii)
        {
            T* cld = vigra::columnVector(cloud, ii).data();
            t[0] = rt[0] * cld[0] + rt[4] * cld[1] + rt[8] * cld[2] + rt[12];
            t[1] = rt[1] * cld[0] + rt[5] * cld[1] + rt[9] * cld[2] + rt[13];
            t[2] = rt[2] * cld[0] + rt[6] * cld[1] + rt[10] * cld[2] + rt[14];
            const T t_x = intr[0] * t[0] / t[2]+ intr[2];
            const T t_y = intr[1] * t[1] / t[2]+ intr[3];

            if(t[2] < 0.0)
                continue;

            for (int splat_x = -(c_pointSize-1); splat_x <= (c_pointSize-1); splat_x++) {
                for (int splat_y = -(c_pointSize-1); splat_y <= (c_pointSize-1); splat_y++) {
                    const int i_x = t_x + splat_x;
                    const int i_y = t_y + splat_y;
                    if (i_x < 0 || i_y < 0 || i_x >= WIDTH || i_y >= HEIGHT)
                        continue;

                    const T old_z = zBuffer3D(2, i_x, i_y);
                    if (old_z > t[2]) {
                        zBuffer3D(0, i_x, i_y) = t[0];
                        zBuffer3D(1, i_x, i_y) = t[1];
                        zBuffer3D(2, i_x, i_y) = t[2];
                        iddata(i_x, i_y) = ii;
                    }
                }
            }
        }
    }

};

}
}

#endif // SOFTWAREPROJECTION_H
