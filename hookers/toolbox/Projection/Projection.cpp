#include "Projection.h"

using namespace hookers;
using namespace hookers::toolbox;

Projection::Projection(const Vector4f &intrinsics)
    : c_pointSize(1)
{

}

void Projection::setPointSize(int size)
{
    c_pointSize = size;
}

vigra::Matrix<double> Projection::matrixFromVector(const Vector6d &vec)
{

    vigra::Matrix<double> result(4,4);

    result.subarray({0,0}, {3,3}) = rotationMatrixFromVector(vec);

    auto T = vec.subarray<0,3>();
    result(0,3) = T[0];
    result(1,3) = T[1];
    result(2,3) = T[2];
    result(3,3) = 1.0;

    return result;

}

vigra::Matrix<double> Projection::rotationMatrixFromVector(const Vector6d &vec)
{
    vigra::Matrix<double> rotation(3,3);
    auto axis = vec.subarray<3,6>();
    ceres::AngleAxisToRotationMatrix(axis.data(), rotation.data());
    return rotation;
}

Vector6d Projection::vectorFromMatrix(vigra::MultiArray<2, double> mat)
{
    Vector6d iPose;
    iPose[0] = mat(0,3);
    iPose[1] = mat(1,3);
    iPose[2] = mat(2,3);

    vigra::Matrix<double> iR(3,3);
    iR = mat.subarray({0,0}, {3,3});

    ceres::RotationMatrixToAngleAxis(iR.data(), iPose.subarray<3,6>().data());
    return iPose;

}
