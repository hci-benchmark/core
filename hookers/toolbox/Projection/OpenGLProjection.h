#ifndef HOOKERS_TOOLBOX_OPENGLPROJECTION_H
#define HOOKERS_TOOLBOX_OPENGLPROJECTION_H

#include <GL/glew.h>
#include <iostream>
#include <vector>
#include <tclap/CmdLine.h>
#include <string>
#include <map>

#include <ceres/ceres.h>
#include <ceres/rotation.h>

//project designed library
#include <hookers/interface/Project.h>
#include <hookers/interface/ProjectData.h>
#include <hookers/interface/Sequence.h>
#include <hookers/interface/SequenceList.h>
#include <hookers/interface/SequenceData.h>
#include <hookers/interface/Frame.h>
#include <hookers/interface/FrameList.h>
#include <hookers/interface/FrameData.h>
#include <hookers/interface/Track.h>
#include <hookers/interface/TrackList.h>
#include <hookers/interface/TrackData.h>
#include <hookers/interface/Landmark.h>
#include <hookers/interface/LandmarkList.h>
#include <hookers/interface/LandmarkData.h>
#include <hookers/interface/NumericTypes.h>

#include <vigra/hdf5impex.hxx>
#include <vigra/random.hxx>
//I am not sure if it is really needed to add these libraries
#include <vigra/multi_array.hxx>
#include <vigra/linear_algebra.hxx>
#include <vigra/tinyvector.hxx>
#include <unordered_map>
#include <opencv2/opencv.hpp>

#include <GLFW/glfw3.h>
#include <sstream>
#include <fstream>

#include <glm/vec3.hpp> // glm::vec3
#include <glm/vec4.hpp> // glm::vec4, glm::ivec4
#include <glm/mat4x4.hpp> // glm::mat4
#include <glm/gtc/matrix_transform.hpp> // glm::translate, glm::rotate, glm::scale, glm::perspective

#include <hookers/toolbox/opengl/Context.h>
#include <hookers/toolbox/Projection/Projection.h>


namespace hookers {
namespace toolbox {

namespace projection {
/**
 * @brief The ShaderLoader class provides singleton-like functionality for
 * loading and compiling shader programs.
 * Simply call ShaderLoader::getShaders() with paths to the vertex and
 * fragmen files. The method search
 */
class ShaderLoader {
public:
    static GLuint getShaders(const char* vertex_file_path, const char* fragment_file_path) {
        const std::string idString = std::string(vertex_file_path) + "\n" + fragment_file_path;
        //if (programmID.end() == programmID.find(idString)) {

            programmID[idString] = LoadShaders(vertex_file_path, fragment_file_path);

         //}
        return programmID[idString];
    }
private:
    static std::map<std::string, GLuint> programmID;
    static GLuint LoadShaders(const char * vertex_file_path, const char * fragment_file_path);
    ShaderLoader() {}
};
}

class OpenGLProjection : public Projection
{
public:
    typedef vigra::MultiArray<2, Vector3f> Vec3fArr2d;
    typedef vigra::MultiArray<2, Vector4i> IdBufferType;

private:
    GLuint VertexArrayID;
    std::vector<GLuint> vertexbuffer;
    std::vector<GLuint> idbuffer;
    GLuint programID;
    std::vector<vigra::MultiArrayView<2, float> > cloud;
    std::vector<GLuint> rendertype;
    std::vector<vigra::MultiArrayView<2, int> > id;
    vigra::MultiArray<2, float> P;
    GLuint Pid;
    GLuint RTid;
    GLuint FramebufferName;
    GLuint renderedTexture;
    GLuint renderedId;
    GLuint depthrenderbuffer;
    opengl::Context context;

    Vec3fArr2d zBuffer;
    IdBufferType idTargetBuffer;


    Vector6d pose;
    Vector4d intrinsics;

private:

    /**
     * private constructor to setup all the openGL buffers and stuff.
     * Is going to be called from all overloaded constructors.
     */
    OpenGLProjection();

    /** this function ensures that all buffers have the correct target size */
    void setupRenderBuffers(Array2Shape size);

public:
    void add_geometry(const vigra::MultiArrayView<2, float>  cloudin, const vigra::MultiArrayView<2, int> &idin, GLuint type = GL_POINTS);

    OpenGLProjection(Array2fView cloud, Vector4f intr);
    OpenGLProjection(Vector3fArrayView cloud, Vector4f intr);

    void project(vigra::MultiArrayView<2, float>  Rt);


    static void create_p(  const float* intr, float* p,
        const float width = 2560, const float height = 1080,
        const float znear = 0.1,  const float zfar = 500);

    ~OpenGLProjection()
    {
        // Cleanup VBO
        for (size_t ii = 0; ii < vertexbuffer.size(); ++ii)
            glDeleteBuffers(1, &vertexbuffer[ii]);
        glDeleteVertexArrays(1, &VertexArrayID);
        glDeleteProgram(programID);

    }

    void setCloud(Array2fView cloud, Array3iView color);
    void setPose(const Vector6f &pose);
    void getPoints(Array2iView target);
    void getPointList(std::unordered_set<int> &target);
    void getDepth(Array2fView target);
    void getColor(Array3fView target);
    void getPositions(Array3fView target);

};


} // ns toolbox
} // ns hookers

void decimate_cloud(vigra::MultiArrayView<2, float> cloud,
    vigra::MultiArrayView<2, float> triangles,
    vigra::TinyVectorView<double, 4> intrinsics,
    vigra::TinyVectorView<double, 6> poses,
    vigra::MultiArray<2, float> & cloud_red);



#endif // HOOKERS_TOOLBOX_OPENGLPROJECTION_H
