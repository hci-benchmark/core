#ifndef RESEARCH_AUTOANNTOTATOR_CORRELATE_H
#define RESEARCH_AUTOANNTOTATOR_CORRELATE_H

#include <set>
#include <thread>
#include <mutex>

#include <tclap/CmdLine.h>

#include <vigra/timing.hxx>

#include <opengm/graphicalmodel/graphicalmodel.hxx>
#include <opengm/graphicalmodel/space/simplediscretespace.hxx>
#include <opengm/functions/squared_difference.hxx>
#include <opengm/operations/adder.hxx>
#include <opengm/inference/messagepassing/messagepassing.hxx>

#include <hookers/interface/Project.h>
#include <hookers/interface/Sequence.h>
#include <hookers/interface/Frame.h>


#include <money/Money.h>
#include <money/Frontend/OpenCV.h>
#include <money/Frontend/Vector.h>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>


typedef double ValueType;
typedef size_t IndexType;
typedef size_t LabelType;


namespace hookers {
namespace toolbox {
namespace Correlator {


std::vector<LabelType> findPath(const cv::Mat & C);


struct NoOp
{
    /** no op does nothing */
    void operator()(cv::Mat & transform) const {
    }

    virtual bool beforeResize() const { return true; }
};

struct SaturationCutOp : NoOp {
    SaturationCutOp(){}

    bool beforeResize() const { return true; }

    void operator()(cv::Mat & transform) const {
        transform.setTo(0, transform>=254);
    }
};


template<typename Op = NoOp>
void correlate(const interface::FrameList & haystack,
               const interface::FrameList & needle,
               cv::Mat & output, int scale = 1,
               const Op & operation = Op())
{

    using namespace cv;

    CV_Assert(scale >= 1);
    CV_Assert(!haystack.empty());
    CV_Assert(!needle.empty());


    /** @todo replace by Frame::size() */
    const cv::Size IMAGE_SIZE(2560, 1080);

    /* size to which the images should be resized
     * before processing correlation */
    const cv::Size CORRELATION_SIZE(IMAGE_SIZE.width/scale, IMAGE_SIZE.height/scale);

    /* make sure output matrix has right size and type according to input */
    if((size_t)output.size().width != haystack.size()
       || (size_t)output.size().height != needle.size()
       || output.type() != CV_32FC1)
    {
        output = cv::Mat(cv::Size(haystack.size(), needle.size()), CV_32FC1);
    }
    output = 0.0;


    Image vigraRefView;
    Image vigraSeqView;
    cv::Mat refView;
    cv::Mat seqView;
    cv::Mat scaledRef;
    cv::Mat scaledSeq;

    cv::Mat mask(CORRELATION_SIZE, CV_8UC1);


    cv::Mat tmp;
    for(size_t h = 0; h < haystack.size(); h++) {
        std::cout << "ref frame: " << h << std::endl;

        /* get the reference frame */
        interface::Frame refFrame = haystack[h];
        refFrame.getView(vigraRefView);

        refView = cv::Mat(vigraRefView.height(), vigraRefView.width(),
                          CV_8UC1, vigraRefView.data());



        /* check if we have to do operation before */
        if(operation.beforeResize()) {
            operation(refView);
        }

        /* resize haystack frame to correlation size */
        cv::resize(refView, scaledRef, CORRELATION_SIZE);

        /* check if we have to do operation after resize */
        if(!operation.beforeResize()) {
            operation(scaledRef);
        }

        for(size_t n = 0; n < needle.size(); n++) {

//                USETICTOC;
            /* get the view from data base */
//                TIC
            interface::Frame seqFrame = needle[n];

//                TOC

//                TIC;
            seqFrame.getView(vigraSeqView);
//                TOC;


            seqView = cv::Mat(vigraSeqView.height(), vigraSeqView.width(), CV_8UC1, vigraSeqView.data());
            assert(seqView.size() == IMAGE_SIZE);


            if(operation.beforeResize()) {
                operation(seqView);
            }

            cv::resize(seqView, scaledSeq, CORRELATION_SIZE);

            if(!operation.beforeResize()) {
                operation(scaledSeq);
            }

            /* calculate mask. reject everything were either in ref or in seq frame
             * are zeros */
            mask = (scaledRef>0)&(scaledSeq>0);

            cv::multiply(scaledRef, scaledSeq, tmp, 1, CV_32FC1);
//                TOC;
//                TIC
            tmp/= cv::norm(scaledRef, NORM_L2, mask)*cv::norm(scaledSeq, NORM_L2, mask);

            // compute sum of all entries (NORM_L1) is sum of all entries because images only have positive values.
//            output.at<float>(n,h) = cv::norm(tmp, cv::NORM_L1, mask);
            output.at<float>(n,h) = cv::sum(tmp)[0];
//                TOC

        }

        cv::Mat workingView(output);
        cv::imshow("boschgt: ref position", workingView);
        cv::waitKey(1);

    }


}

} // ns Correlator
} // ns toolbox
} // ns hookers

#endif // RESEARCH_AUTOANNTOTATOR_CORRELATE_H
