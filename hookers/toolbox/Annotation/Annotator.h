#ifndef ANNOTATOR_H
#define ANNOTATOR_H

#include <set>
#include <unordered_set>
#include <unordered_map>
#include <algorithm>

#include <tclap/CmdLine.h>

#include <vigra/timing.hxx>


#include <opengm/graphicalmodel/graphicalmodel.hxx>
#include <opengm/graphicalmodel/space/simplediscretespace.hxx>
#include <opengm/functions/squared_difference.hxx>
#include <opengm/functions/pottsn.hxx>
#include <opengm/operations/adder.hxx>
#include <opengm/inference/messagepassing/messagepassing.hxx>
#include <opengm/inference/astar.hxx>
#include <opengm/inference/bruteforce.hxx>

#include <hookers/interface/Project.h>
#include <hookers/interface/Sequence.h>
#include <hookers/interface/Frame.h>
#include <hookers/interface/Track.h>
#include <hookers/interface/Landmark.h>


#include <money/Money.h>
#include <money/Frontend/OpenCV.h>
#include <money/Frontend/Vector.h>

//local files
#include "Correlator.h"
#include "../PoseEstimation/PoseEstimation.h"

#include <unordered_map>
#include <unordered_set>


#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/features2d.hpp>
#include <opencv2/xfeatures2d.hpp>


namespace hookers {
namespace toolbox {
namespace AutoAnnotator {

using namespace hookers::interface;

typedef std::runtime_error AnnotationError;

template<typename K,typename V>
AbstractObjectList<K> map_keys(const std::unordered_map<K,V> & map) {
    AbstractObjectList<K> objs;
    for(auto it = map.begin(); it != map.end(); it++)
        objs.push_back((*it).first);
    return objs;
}

template<typename K,typename V>
AbstractObjectList<V> map_values(const std::unordered_map<K,V> & map) {
    AbstractObjectList<V> objs;
    for(auto it = map.begin(); it != map.end(); it++)
        objs.push_back((*it).second);
    return objs;
}

void drawCorrespondencies(cv::Mat & img,
                          interface::Frame f1, interface::Frame f2,
                          std::unordered_map<Landmark, cv::Scalar> & colors,
                          const std::unordered_map<Track,Track> &tracks = std::unordered_map<Track,Track>());

typedef std::unordered_map<Track, Track>  AnnotationResult;


typedef std::tuple<Frame, Frame, Vector6d> TargetEntry;

/**
 * @f$ (T, R, Pose_T) @f$ ,
 * where T is the target frame, R is the reference frame and  @f$ Pose_T @f$ is
 * an inivial guess to the target pose.
 */
typedef std::vector<TargetEntry> TargetList;

/**
 * @brief auto annotates a set of frames using advanced neighbourhood information
 *
 * If the pose for each Frame in targets has default value, the reference frame pose
 * will be used as estimation */
AnnotationResult annotate(const TargetList & targets, int KNN = 5);


class AssignmentVote {
    static const int MIN_VOTES  = 3;
    static constexpr double MAJORITY = 0.5;

    int nVotes;
    std::unordered_map<Landmark, int> votes;
public:

    AssignmentVote() : nVotes(0) {}

    void vote(const Landmark &l) {
        if(!votes.count(l)){
            votes[l] = 0;
        }
        votes[l]++;
        nVotes++;
    }

    Landmark winner() {
        if(nVotes < MIN_VOTES)
            return Landmark();

        int sumVotes = 0;
        int currentNVote = 0;
        Landmark currentLandmark;

        for(auto it: votes) {
            if(currentNVote >= it.second)
                continue;
            currentLandmark = it.first;
            currentNVote = it.second;
            sumVotes += it.second;
        }

        if(currentNVote/double(sumVotes) < MAJORITY)
            return Landmark();

        return currentLandmark;

    }
};


/** function that could be called with command line arguments */
int auto_annotate(int argc, char ** argv, bool dryRun = false);


inline double assignmentRatio(const AnnotationResult & res) {
    int correct = 0;
    for(auto it = res.cbegin(); it != res.cend(); it++) {
        if(it->first.linkedLandmark() == it->second.linkedLandmark())
            correct++;
    }
    return correct;
}

}
}
}

#endif // ANNOTATOR_H

