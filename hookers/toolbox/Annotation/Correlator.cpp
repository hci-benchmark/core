#include "Correlator.h"


namespace hookers {
namespace toolbox {
namespace Correlator {


/**
 * TimeProgressPotential is a pairwise potential that enforces label differences in positive
 * direction by applying a very high value for negative differences.
 */
template<class T, class I = size_t, class L = size_t>
class TimeProgressPotential : public opengm::FunctionBase<TimeProgressPotential<T, I, L>, T, I, L> {
public:
    typedef T ValueType;
    typedef I IndexType;
    typedef L LabelType;

    TimeProgressPotential(size_t l1, size_t l2, T weight, T offset = 0.0) :
        m_numberOfLabels1(l1), m_numberOfLabels2(l2), m_weight(weight), m_offset(offset)
    {}


    template<class Iterator>
    ValueType operator()(Iterator begin) const {
        T value = begin[1];
        value -= begin[0];
        if(value < 0.0)
            return 1000*m_weight;
        value -= m_offset;
        return value*value*m_weight;
    }
   /** number of labels of the indicated input variable */
    size_t shape(const size_t i ) const {
        OPENGM_ASSERT(i < 2);
        return (i==0 ? m_numberOfLabels1 : m_numberOfLabels2);
    }


   /** number of input variables */
   size_t dimension() const {
       return 2;
   }

   /** number of parameters */
   size_t size() const {
       return m_numberOfLabels1 * m_numberOfLabels2;
   }


private:

    size_t m_numberOfLabels1;
    size_t m_numberOfLabels2;
    ValueType m_weight;
    ValueType m_offset;
};


/**
 * @brief findPath finds path in correlation map
 * @param C the correlation map.
 * @return a vector of positions
 *
 * This function returns the best path along the column axis of C.
 *
 */
std::vector<LabelType> findPath(const cv::Mat & C) {

    using namespace opengm;

    // construct a label space with numberOfVariables many variables,
    // each having numberOfLabels many labels
    const size_t numberOfVariables = C.rows;
    const size_t numberOfLabels = C.cols;

    typedef SimpleDiscreteSpace<size_t, size_t> Space;
    typedef ExplicitFunction<double> SinglesidePotential;
    typedef TimeProgressPotential<double> PairwisePotential;
    typedef OPENGM_TYPELIST_2(SinglesidePotential, PairwisePotential) FunctionTypelist;
    typedef GraphicalModel<double, Adder, FunctionTypelist, Space> Model;


    Space space(numberOfVariables, numberOfLabels);
    Model gm(space);

    /* for each variable, add one 1st order functions and one 1st order factor */
       for(size_t v = 0; v < numberOfVariables; ++v) {
          const size_t shape[] = {numberOfLabels};

          /* assign the cost function for each variable. The label
           * corresponds to the column index of C. */
          ExplicitFunction<double> f(shape, shape + 1);
          for(size_t s = 0; s < numberOfLabels; ++s) {
             f(s) = -C.at<float>(v, s);
          }

          Model::FunctionIdentifier fid = gm.addFunction(f);

          size_t variableIndices[] = {v};
          gm.addFactor(fid, variableIndices, variableIndices + 1);
       }

    /* add one (!) 2nd order Potts function */
    PairwisePotential f(numberOfLabels, numberOfLabels, 0.05, 1);
    Model::FunctionIdentifier fid = gm.addFunction(f);

    /* for each pair of consecutive variables,
     * add one factor that refers to the Potts function */
    for(size_t v = 0; v < numberOfVariables - 1; ++v) {
        size_t variableIndices[] = {v, v + 1};
        gm.addFactor(fid, variableIndices, variableIndices + 2);
    }

    // set up the optimizer (loopy belief propagation)
    typedef BeliefPropagationUpdateRules<Model, Minimizer> UpdateRules;


    const size_t maxNumberOfIterations = numberOfVariables * 2;
    const double convergenceBound = 1e-7;
    const double damping = 0.0;


    // obtain the (approximate) argmin
    std::vector<LabelType> labeling(numberOfVariables);

//    {
//        typedef MessagePassing<Model, Minimizer, UpdateRules, MaxDistance> BeliefPropagation;
//        BeliefPropagation::Parameter parameter(maxNumberOfIterations, convergenceBound, damping);
//        BeliefPropagation bp(gm, parameter);

//        // optimize (approximately)
//        BeliefPropagation::VerboseVisitorType visitor;
//        bp.infer(visitor);

//        bp.arg(labeling);

//    }

    {
        typedef MessagePassing<Model, Minimizer, UpdateRules, MaxDistance> TRBP;
        TRBP::Parameter parameter(maxNumberOfIterations, convergenceBound, damping);
        TRBP trbp(gm, parameter);

        // optimize (approximately)
        TRBP::VerboseVisitorType visitor;
        trbp.infer(visitor);
        trbp.arg(labeling);
    }

    return labeling;
}


} // ns Correlator
} // ns toolbox
} // ns hookers


