#include "Annotator.h"

using namespace cv;


namespace hookers {
namespace toolbox {
namespace AutoAnnotator {



void drawCorrespondencies(cv::Mat & img,
                          Frame f1, Frame f2,
                          std::unordered_map<Landmark, cv::Scalar> & colors,
                          const std::unordered_map<Track,Track> &tracks)
{



    USETICTOC;
    const std::vector<cv::Scalar> colorPool {
        cv::Scalar(255, 0, 0), cv::Scalar(0, 255, 0), cv::Scalar(0, 0, 255),
        cv::Scalar(255, 255, 0), cv::Scalar(255, 0, 255), cv::Scalar(0, 255, 255)
    };
    static int colorsCounter = 0;



    Image vigraFirstView;
    Image vigraSecondView;
    cv::Mat firstView;
    cv::Mat secondView;

    TIC;
    f1.getView(vigraFirstView);
    f2.getView(vigraSecondView);


    firstView = cv::Mat(vigraFirstView.height(), vigraFirstView.width(),
                      CV_8UC1, vigraFirstView.data());
    secondView = cv::Mat(vigraSecondView.height(), vigraSecondView.width(),
                         CV_8UC1, vigraSecondView.data());

    int width = std::max(vigraFirstView.width(), vigraSecondView.width());

    /* create a view where both frames fit in */
    cv::Mat correspondenciesMap(vigraFirstView.height()+vigraSecondView.height(),
                                width,
                                CV_8UC1);


    int offset = firstView.size().height;

    std::cout << "bla: " << tracks.size() << std::endl
              << cv::Rect(0, 0, firstView.size().width, firstView.size().height) << std::endl
              << cv::Rect(0, offset, secondView.size().width, secondView.size().height) << std::endl;


    firstView.copyTo(cv::Mat(correspondenciesMap,
                             cv::Rect(0, 0, firstView.size().width, firstView.size().height)));
    secondView.copyTo(cv::Mat(correspondenciesMap,
                              cv::Rect(0, offset, secondView.size().width, secondView.size().height)));

    cv::cvtColor(correspondenciesMap, img, CV_GRAY2RGB);
    TOC;



    std::unordered_map<Track, Track> correspondencies = tracks;

    /* if no correspondencies have been provided, then take
     * the annotated tracks of both frames. */
    if(correspondencies.empty()) {

        TrackList T1 = f1.tracks().annotated();
        TrackList T2 = f2.tracks().annotated();


        std::unordered_map<Landmark, Track> f2Correspondencies;

        f2Correspondencies.reserve(T2.size());
        for(auto t: T2) {
            f2Correspondencies[t.linkedLandmark()] = t;
        }

        for(auto t: T1) {
            correspondencies[t] = f2Correspondencies[t.linkedLandmark()];
        }
    }


    const int R = 5;

    for(auto it: correspondencies) {
        Track t1 = it.first;
        Landmark l = t1.linkedLandmark();

        Track t2 = it.second;

        if(!t1.isValid() || !t2.isValid() || !l.isValid()) {
            continue;
        }

        cv::Point p1(t1.posInFrame(f1)[0], t1.posInFrame(f1)[1]);
        cv::Point p2(t2.posInFrame(f2)[0], t2.posInFrame(f2)[1]);

        if(!colors.count(l)) {
            colors[l] = colorPool[colorsCounter++];
            if((size_t)colorsCounter >= colorPool.size())
                colorsCounter = 0;
        }

        cv::Scalar color = colors[l];

        if(color == cv::Scalar())
            color = cv::Scalar(255,255,255);

        cv::circle(img , p1, R,
                   color);
        cv::circle(img, p2+cv::Point(0, offset), R,
                   color);

        cv::line(img, p1, p2+cv::Point(0, offset), color);

    }

}

AnnotationResult annotate(const TargetList &targets, int KNN) {



    //
    // ---- constants
    //

    /* number of nearest neighbours to take into account
     * this should be K+1 because the matcher always finds itself as well */
    const unsigned int K_reference = KNN+1;


    /* number of nearest neighbours in needle image */
//    const int K_needle = KNN+1;
    const unsigned int K_target = 3;

    /* threshold of unary term at which we prefer the unassigned label */
    const double GRAPH_UNARY_THRESHOLD = 300;

    /* threshold of binay term at which we prefer the unassigned label */
    const double GRAPH_BINARY_THRESHOLD = 30;


    //
    // ---- code
    //


    if(targets.empty())
        return AnnotationResult();

    Frame target, reference;

    std::tie(target, reference, std::ignore) = targets.front();

    Image targetImgVigra;
    target.getView(targetImgVigra);
    cv::Mat targetImg = cv::Mat(targetImgVigra.height(), targetImgVigra.width(),
                                CV_8UC1, targetImgVigra.data());

    if(targetImg.empty()) {
        throw std::runtime_error("Needle Image could not be read.");
    }

    TrackList targetTracks = target.tracks();


    /*
     *  ----- check if we need to compute the matching
     */
    if(targetTracks.size() < K_target)
       throw AnnotationError("Not enough tracks in target that could be annotated.");


    cv::Mat targetPoints(cv::Size(2,targetTracks.size()), CV_32FC1);
    std::vector<cv::KeyPoint> targetKeypoints(targetTracks.size());
    for(unsigned int i = 0; i < targetTracks.size(); i++) {

        auto center = targetTracks[i].posInFrame(target);

        targetKeypoints[i] = cv::KeyPoint(center[0], center[1], 10.0);

        targetPoints.row(i).at<float>(0) = center[0];
        targetPoints.row(i).at<float>(1) = center[1];
    }

    /*
     * --- get haystak frame information
     */


    TrackList referenceTracks = reference.tracks().annotated();

    if(referenceTracks.size() < K_reference)
        throw AnnotationError("Reference frame has not enough tracks.");

    /* we need the haystack image for the feature desciptors now */
    Image referenceImgVigra;
    reference.getView(referenceImgVigra);

    cv::Mat referenceImg = cv::Mat(referenceImgVigra.height(), referenceImgVigra.width(),
                                CV_8UC1, referenceImgVigra.data());

    /* use tracks from haystack view to setup keypoints */
    std::vector<cv::KeyPoint> referenceKeypoints(referenceTracks.size());
    cv::Mat referencePoints(cv::Size(2,referenceTracks.size()), CV_32FC1);
    for(unsigned i = 0; i < referenceTracks.size(); i++) {
        Vector2d pos2d = referenceTracks[i].posInFrame(reference, Frame::View::Base);
        cv::Point pos(pos2d[0], pos2d[1]);
        referenceKeypoints[i] = cv::KeyPoint(pos2d[0], pos2d[1], 10.0);

        referencePoints.row(i).at<float>(0) = pos2d[0];
        referencePoints.row(i).at<float>(1) = pos2d[1];
    }

    /*
     * ------ search KNN neighbours in needle and haystack
     */

    /* extract features */
    //cv::xfeatures2d::SurfDescriptorExtractor extractor;
//    cv::xfeatures2d::SiftDescriptorExtractor siftExtractor;
    auto siftExtractor = cv::xfeatures2d::SIFT::create();

    Mat referenceDescriptors, targetDescriptors;
//            extractor.compute( haystackImg, haystackKeypoints, haystackDescriptors );
//            extractor.compute( needleImg, needleKeypoints, needleDescriptors);
    siftExtractor->compute( referenceImg, referenceKeypoints, referenceDescriptors );
    siftExtractor->compute( targetImg, targetKeypoints, targetDescriptors );


    cv::FlannBasedMatcher matcher;
    cv::BFMatcher matcherBF;


    std::vector< std::vector<cv::DMatch> > referenceKNNList;
    std::vector<std::vector<cv::DMatch> > targetKNNList;
    matcherBF.knnMatch(referencePoints, referencePoints, referenceKNNList, K_reference);
    matcherBF.knnMatch(referenceDescriptors, targetDescriptors, targetKNNList, K_target);


    /*
     * --- construct the feature matcher graph *
     */


    using namespace opengm;

    // construct a label space with numberOfVariables many variables,
    // each having numberOfLabels many labels
    const size_t numberOfVariables = referenceTracks.size();
    const size_t numberOfLabels = K_target+1;

    typedef SimpleDiscreteSpace<size_t, size_t> Space;
    typedef ExplicitFunction<double> SinglesidePotential;
    typedef ExplicitFunction<double> PairwisePotential;
    typedef OPENGM_TYPELIST_1(SinglesidePotential) FunctionTypeList;
    typedef GraphicalModel<double, Adder, FunctionTypeList, Space> Model;


    Space space(numberOfVariables, numberOfLabels);
    Model gm(space);

    /* helper class for permuations of indices,
     * that always returnes the smaller index first */
    struct permutation_index {
        size_t i, j;

        permutation_index() : i(-1), j(-1) {}
        permutation_index(size_t i, size_t j) : i(i), j(j){}

        bool operator==(const permutation_index & other) const {
            return ((other.i == i) && (other.j == j))
                    || ((other.i == j) && (other.j == i));
        }

        std::size_t operator()(const permutation_index & k) const {
            return i;
        }

        size_t first() const {
            return (i<j)?i:j;
        }

        size_t second() const {
            return (i<j)?j:i;
        }
    };

    /* stores all already computed pairwise terms */
    std::unordered_set<permutation_index, permutation_index> pairwise_map;

    /* add all the single-side and pairwise terms */
    for(size_t v = 0; v < numberOfVariables; ++v)
    {

      /* construct the single side potential. it is the difference of the
       * feature vector.
       * The last label (K+1) is always a high value. this corresponds to
       * the fact that we want to avoid unassigned values */
      {

          const size_t shapeUnary[] = {numberOfLabels};
        ExplicitFunction<double> f(shapeUnary, shapeUnary+1, 1.0);

        const std::vector<DMatch> & targetKNN = targetKNNList[v];
        for(size_t s = 0; s < targetKNN.size(); ++s) {

            /* reference track */
            const int refId = v;

            /* target track number */
            const int tarId = targetKNN[s].trainIdx;

            f(s) = cv::norm(referenceDescriptors.row(refId)-targetDescriptors.row(tarId));
            f(s) = (referenceTracks[refId].posInFrame(reference)-targetTracks[tarId].posInFrame(target)).squaredMagnitude();
            f(s) /= GRAPH_UNARY_THRESHOLD*GRAPH_UNARY_THRESHOLD;
            //std::cout << "differences in feature vectors..." << f(s) << std::endl;
        }

        Model::FunctionIdentifier fid = gm.addFunction(f);
        size_t variableIndices[] = {v};
        gm.addFactor(fid, variableIndices, variableIndices + 1);
      }

      /* construction  pairwise potentials
       * We need to construct it for each KNN in haystack */
      {

          const std::vector<DMatch> & referenceKNN = referenceKNNList[v];
          for(size_t n = 0; n < referenceKNN.size(); n++) {

              /* point id of neighbour in haystack image */
              int n_idx = referenceKNN[n].trainIdx;

              /* ignore identical match */
              if((size_t)n_idx == v)
                  continue;

              permutation_index ie(v, n_idx);

              /* check if this factor has already been added */
              if(pairwise_map.count(permutation_index(v, n_idx)))
                  continue;


              /* construct binary function */
              const size_t shape[] = {numberOfLabels, numberOfLabels};
              ExplicitFunction<double> f(shape, shape + 2, 1.0);


              /* list of matches for first variable in needle image */
              const std::vector<DMatch> & first_knn = targetKNNList[ie.first()];

              /* matches in needle image for neighbour in haystack image */
              const std::vector<DMatch> & second_knn = targetKNNList[ie.second()];

              /* get the direction difference between the both neighbours from haystack */
              cv::Mat haystackDifference = referencePoints.row(ie.first())-referencePoints.row(ie.second());


              /* double the threshold in case both nodes are unassigned */
              f(first_knn.size(), second_knn.size()) = 2.0;

              /* calculate the squared normed differences */
              for(size_t first_n = 0; first_n < first_knn.size(); first_n++) {
                  int first_idx = first_knn[first_n].trainIdx;

                  for(size_t second_n = 0; second_n < second_knn.size(); second_n++) {
                      int second_idx = second_knn[second_n].trainIdx;

                      /* take the directional difference of both points that correspond to given labels */
                      cv::Mat needleDifference = targetPoints.row(first_idx)-targetPoints.row(second_idx);
                      f(first_n, second_n) = std::pow(cv::norm(haystackDifference-needleDifference)/GRAPH_BINARY_THRESHOLD,2.0);

                      /* if the label values correspond to the same feature point in
                       * needle view, this should get a high value. we don't want
                       * multi assignment */
                      if(first_idx == second_idx) {
                          f(first_n, second_n) = 10e3*10e3;
//                                  std::cout << "pairwise: " << f(first_n, second_n) << "," << first_idx << "," << second_idx
//                                            << cv::Mat(haystackDifference-needleDifference)
//                                            << std::endl;
                      }


                  }
              }


            Model::FunctionIdentifier fid = gm.addFunction(f);

            pairwise_map.insert(ie);

            size_t variableIndices[] = {ie.first(), ie.second()};
            gm.addFactor(fid, variableIndices, variableIndices + 2);


            }

        } // haystack neighbour scope
    } // variable scope




    /*
     * ---- solve the graph matching problem
     */

    std::vector<LabelType> labeling;
    {
        // set up the optimizer (loopy belief propagation)
        typedef BeliefPropagationUpdateRules<Model, Minimizer> UpdateRules;


        const size_t maxNumberOfIterations = numberOfVariables;
        const double convergenceBound = 1e-2;
        const double damping = 0.0;

        typedef MessagePassing<Model, Minimizer, UpdateRules, MaxDistance> TRBP;
        TRBP::Parameter parameter(maxNumberOfIterations, convergenceBound, damping);
        TRBP trbp(gm, parameter);

        // optimize (approximately)
        //TRBP::VerboseVisitorType visitor;
        //trbp.infer(visitor);
        trbp.infer();
        trbp.arg(labeling);
    }


    /*
     * ---- find outliers using RANSAC
     */

    AnnotationResult result;
    LandmarkList landmarks;
    TrackList tracks;
    TrackList tracksAtHaystack;


    std::cout << " --- correspondencies: ";

    for(unsigned int i = 0; i < labeling.size(); i++) {

        if(labeling[i] >= targetKNNList[i].size()) {
            std::cout << "-" << " ";
            continue;
        }


        int haystackIndex = i;
        int needleIndex = targetKNNList[i][labeling[i]].trainIdx;

        std::cout << needleIndex << " ";

        /* setup solvePNP stuff */
        Landmark landmark = referenceTracks[i].linkedLandmark();

        result[referenceTracks[i]] = targetTracks[needleIndex];

        landmarks.push_back(landmark);
        tracks.push_back(targetTracks[needleIndex]);
        tracksAtHaystack.push_back(referenceTracks[i]);

    }
    std::cout << std::endl;

    Vector2fArray trackPositions;
    Vector3fArray landmarkPositions;
    landmarks.getPositions(landmarkPositions);
    tracks.getPositionsInFrame(trackPositions,target);


    /* check if we have enough points to do the solvePNP stuff */
    if(landmarks.size() < 5 || tracks.size() < 5) {
        throw AnnotationError("Found no not enough correspondencies.");
    }


    /* setup solve PnP such that at least 75% of the points are considered as inliers */
    Vector6d pose;
    std::vector<int> inliers;

    try {

        Vector4d intrinsics = target.intrinsics();
        /** @todo hack... if the intrinsics are not available, then assume some default values.
         */
        if(intrinsics == Vector4d(-1) || intrinsics == Vector4d()) {
            std::cerr << "@@@@@@ [ERROR]: frame has no intrinsics..." << std::endl;
            // Intentionally wrong intrinsics for debugging
            intrinsics = {185.0, 185.0, 130.0, 55.0};
        }

        std::tie(pose, inliers) = PoseEstimation::solvePnPRansac(landmarkPositions, trackPositions,
                                                        intrinsics, 7.0, 1,
                                                        reference.pose());
        TrackSet inliersSet;
        for(int i: inliers) {
            inliersSet.insert(tracksAtHaystack[i]);
        }

        AnnotationResult cleanedResult;
        for(auto it: result) {
            if(!inliersSet.contains(it.first))
                continue;
            cleanedResult[it.first] = it.second;
        }

        result = cleanedResult;


    } catch (std::exception & e) {
        std::stringstream err;
        err << "[INFO] Error at solvePnP for frame: " << e.what();
        throw AnnotationError(err.str());
    }


    std::cout << " --- ref pose:" << reference.pose() << std::endl;
    std::cout << " --- camera Pose:" << pose << std::endl;
    std::cout << " --- inliers: " << inliers.size() << ", " << tracks.size() << std::endl;
    std::cout << " --- result: " << result.size() << "," << tracks.size() << std::endl;


    /* if there are too less matches, we should discard it here */
    if(inliers.size()< 7) {
        throw AnnotationError("Too few inlier");
    }

    return result;

}




//
//---------------------------------------------------------------------------------------------------
//

int auto_annotate(int argc, char ** argv, bool dryRun) {

    TCLAP::CmdLine cmd("boschgt auto landmark annotator", ' ', "0.1");

    TCLAP::ValueArg<std::string>
        referenceNameArg(std::string(),
                     "reference",
                     "reference sequence name (eg. 20130101_0000)",
                     true,
                     std::string(),
                     "string",
                     cmd);

    TCLAP::ValueArg<std::string>
        sequenceNameArg("s",
                 "sequence",
                 "sequence to analyze (eg. 20130101_0000)",
                 true,
                 std::string(),
                 "string",
                 cmd);

    TCLAP::ValueArg<int>
        pickFramesArg(std::string(),
                     "pick-frames",
                     "set the numer of frames that should be compared for each sequence",
                     false,
                     250,
                     "int",
                     cmd);

    TCLAP::UnlabeledMultiArg<std::string>
            valueArg("values",
                     "arguments to command, like sequence name etc... Use 'help' for further reference.",
                     false,
                     "string",
                     cmd);

    TCLAP::ValueArg<int>
        startArg(std::string(), "start",
                        "starting frame. default ist first frame in sequence",
                        false,
                        0,
                        "int",
                        cmd);


    TCLAP::SwitchArg onlyVisualizeArg(std::string(),
                                         "v", "only visualize",
                                         cmd, false);


    TCLAP::SwitchArg moneyIgnoreFilesArg(std::string(),
                                         "if", "MONEY: ignore changed files",
                                         cmd, false);
    TCLAP::SwitchArg moneyIgnoreBlocksArg(std::string(),
                                          "ib", "MONEY: ignore changed blocks",
                                          cmd, false);

    TCLAP::SwitchArg moneyIgnoreAllArg(std::string(),
                                       "ia", "MONEY: ignore all changes",
                                       cmd, false);

    TCLAP::SwitchArg moneyRecomputeArg(std::string(),
                                       "r", "MONEY: recompute in any case",
                                       cmd, false);


    try {
        cmd.parse(argc, argv);

    } catch (TCLAP::ArgException &e)  {
        std::cerr << "[ERROR] " << e.error() << " for arg " << e.argId() << std::endl;
        return 0;
    }

    int money_flags = money::NoFlags;
    if(moneyIgnoreFilesArg.isSet())
        money_flags |= money::IgnoreExtern;
    if(moneyIgnoreBlocksArg.isSet())
        money_flags |= money::IgnoreBlocks;
    if(moneyIgnoreAllArg.isSet())
        money_flags |= money::IgnoreAll;
    if(moneyRecomputeArg.isSet())
        money_flags |= money::Recompute;

    std::stringstream context;
    context << "autoannot"
            << "_"
            << referenceNameArg.getValue()
            << "_"
            << sequenceNameArg.getValue()
            << "_"
            << pickFramesArg.getValue();

    money::init(context.str(), money_flags);

    Project project;
    Sequence reference;
    Sequence sequence;

//    try {
        USETICTOC;
        TIC;
        project = Project::open();
        TOC;
        TIC;
        SequenceList sequences = project.sequences();
        TOC;

        TIC;
        reference = sequences.byLabel(referenceNameArg.getValue());
        sequence = sequences.byLabel(sequenceNameArg.getValue());
        TOC;

        if(!reference.isValid() || !sequence.isValid()) {
            throw std::runtime_error("Could not open sequences.");
        }




        /* get frames in scene and start tracking */
        FrameList seqFrames = sequence.frames();
        FrameList refFrames = reference.frames();



        const int pickFrames = pickFramesArg.getValue();
        const int seqDelta = cv::max(seqFrames.size()/pickFrames, (size_t)1);
        const int refDelta = cv::max(refFrames.size()/pickFrames, (size_t)1);


        std::cout << refDelta << ", " << seqDelta << std::endl;

        //FrameListPair frameListPair;

        FrameList needleFrames;// = frameListPair.needleFrames;
        FrameList haystackFrames;// = frameListPair.haystackFrames;



        /* setup picked frames list */
        for(size_t i = 0; i < refFrames.size(); i += refDelta) {
            haystackFrames.push_back(refFrames[i]);
        }
        for(size_t i = 0; i < seqFrames.size(); i += seqDelta) {
            needleFrames.push_back(seqFrames[i]);
        }



//
//----- CORRELATION -------------------------------------------------------------------------------
//
        money::Memoizeable<cv::Mat> correlationMap("correlation");
        money::Block(correlationMap, [&]{

            /* hold reference to frames, so their caches won't go out of scope during computation */
            std::set<Frame> frameLocker;
            for(unsigned i = 0; i < haystackFrames.size(); i++) {
                frameLocker.insert(haystackFrames[i]);
            }

            std::set<Frame> needleFrameLocker;
            for(auto f: needleFrames)
                needleFrameLocker.insert(f);

            Correlator::correlate(haystackFrames, needleFrames, correlationMap, 4);

            cv::Mat tmp;
            cv::normalize(correlationMap.get(), tmp, 0, 1.0, cv::NORM_MINMAX);
            tmp.copyTo(correlationMap.get());

            //cv::imshow("boschgt: correlation map", correlationMap.get());
            //cv::setMouseCallback("boschgt: correlation map", compareImages, &frameListPair);

            //cv::waitKey(0);
        });


        std::stringstream corrName;
        corrName << context.str()
                 << "_vis_correlation.png";
        cv::Mat tmp;
        cv::normalize(correlationMap.cget(), tmp, 0, 255.0, cv::NORM_MINMAX);
        cv::imwrite(corrName.str(), tmp);

//
//----- FRAME CORRESPONDENCE ---------------------------------------------------------------------
//
        money::Memoizeable<std::vector<LabelType> > path("path");
        money::Block(correlationMap, path, [&]{

            cv::Mat tmp;
            correlationMap.cget().copyTo(tmp);

            path.get() = Correlator::findPath(tmp);

            for(unsigned int i = 0; i < path.cget().size(); i++) {
                tmp.at<float>(i, path.cget()[i]) = 0.0;
            }

            cv::imshow("boschgt: correlation map with path", tmp);
            //cv::waitKey(0);
        });

        std::cout << "Path is: ";
        for(unsigned int i = 0; i < path.cget().size(); i++) {
            std::cout << path.cget()[i] << " ";
        }
        std::cout << std::endl;

//
//----- FLANN Matching ----------------------------------------------------------------------------
//


        //money/::Block(path, [&] {




            typedef std::unordered_map<Landmark, int> LandmarkAccumulator;
            typedef std::tuple<Track, LandmarkAccumulator> FeatureInfo;
            TrackSet currentFeatures;
            std::unordered_map<Track, FeatureInfo> currentFeatureInfos;

            std::unordered_map<Track, Landmark> annotations;

            const std::vector<LabelType> & p = path.cget();

            double statTrackNum = 0.0;
            double statTrackLength = 0.0;

            //
            // --- VISUALIZE
            //

            if(onlyVisualizeArg.getValue()) {

                PartList parts = project.parts();
                std::unordered_map<Landmark,cv::Scalar> colors;
                for(size_t i = 0; i < seqFrames.size(); i += 20) {

                    int npos = i/seqDelta;
                    int hpos = p[npos];

                    Frame needle = seqFrames[i];
                    Frame haystack = haystackFrames[hpos];
                    cv::Mat img;
                    drawCorrespondencies(img, haystack, needle, colors);

                    cv::namedWindow("bla", CV_NORMAL);
                    cv::imshow("bla", img);

                    std::stringstream name;
                    name << context.str()
                         << "_vis_"
                         << std::setw(5) << std::setfill('0') << i
                         << ".png";
//                    cv::imwrite(name.str(), img);

                    cv::waitKey(2);

                }

                exit(0);
            }


            /* keeps the color of a landmark persistent */
            std::unordered_map<Landmark, cv::Scalar> colors;

            for(size_t i = startArg.getValue(); i < seqFrames.size(); i += seqDelta/2) {



                std::cout << "[STATUS] Tracking frame " << i << std::endl;
                TIC;

                int npos = i/seqDelta;
                int hpos = p[npos];

                Frame needle = seqFrames[i];
                Frame haystack = haystackFrames[hpos];

                /*
                 * --- handle track insertion etc...
                 */
                TrackList needleTracks = needle.tracks();
                std::cout << "tracks in frame: " << needleTracks.size() << std::endl;

                /* now handle tracks that are disposed */
                TrackSet deletedTracks = currentFeatures-TrackSet::fromList(needleTracks);
                for(const auto & t : deletedTracks) {


                    /* get the landmark accumulator for the given featuretrack that is going to be removed */
                    LandmarkAccumulator & la = std::get<1>(currentFeatureInfos[t]);

                    /* check if the track has any corresponding landmark entries.
                     * if this is the case, we have to check for the votings */
                    if(la.size()) {


                        Landmark maxLandmark;
                        double maxCount = 0;
                        double count = 0;
                        for(auto it = la.begin(); it != la.end(); it++) {
                            auto & key = (*it).first;
                            auto & value = (*it).second;
                            if(value > maxCount) {
                                maxCount = value;
                                maxLandmark = key;
                            }
                            count += value;
                        }

//                        std::cout << "---- REMOVING track: "
//                                  << la.size() << ","
//                                  << maxCount << ","
//                                  << count << ","
//                                  << maxLandmark.isValid()
//                                  << std::endl;


                        /* check for simple majority.
                         * we expect that our track has at least been voted 5 times for maxLandmark.
                         * In addition, it should hold simple majority of votes. */
                        if(maxLandmark.isValid()
                                && maxCount > 3
                                && (maxCount/count > 1.0/2.0)) {
                            std::cout << "--linking to a landmark: " <<  maxCount << count << std::endl;
                            annotations[t] = maxLandmark;
                        }

                    }

                    /* finally remove the entry */
                    currentFeatureInfos.erase(t);
                }

                /* update the set */
                currentFeatures = TrackSet::fromList(needleTracks);

                std::cout << "[STATUS] Searching for track correspondence in pair: "
                          << npos << ", " << hpos
                          << std::endl;


                TargetList target;
                target.push_back(std::make_tuple(needle, haystack, Vector6d()));


                AnnotationResult result;

                try {
                    result = annotate(target);
                } catch(AnnotationError & e) {
                    std::cout << "Annotation for frame " << i
                              << " failed: " << e.what() << std::endl;
                }

                /* display the result */
                {
                    cv::Mat disp;
                    drawCorrespondencies(disp, haystack, needle, colors, result);
                    cv::namedWindow("correspondencies",CV_NORMAL);
                    cv::imshow("correspondencies", disp);
                    cv::waitKey(10);
                }

                std::unordered_map<Track,Landmark> annotatedLandmarks;
                for(auto t: haystack.tracks().annotated()) {
                    annotatedLandmarks[t] = t.linkedLandmark();
                }

                /*
                 * --- put votes to currentTrackInfos
                 */
                for(auto r: result) {
                    /* get all relevant informations and objects */
                    Landmark l = annotatedLandmarks[r.first];
                    Track t = r.second;
                    FeatureInfo & info = currentFeatureInfos[t];

                    /* if no assignment has happend for the landmark in question,
                     * initialize the counter to zero
                     */
                    if(!std::get<1>(info).count(l))
                        std::get<1>(info)[l] = 0;

                    /* increase the vote count (++) for the given feature */
                    std::get<1>(info)[l]++;

                }

                /* anything from here on saves things persistently.
                 * check, if we want this */
                if(dryRun)
                    continue;

                /* finally link the landmarks */
                for(auto & it: annotations) {
                    Track t = it.first;
                    Landmark & l = it.second;

                    t.linkToLandmark(l);
                }

                std::cout << " [STATUS] Finished with correspondence search." << std::endl;


            } // -- frame loop

            std::cout << "number of tracks: " << statTrackNum << std::endl
                      << "mean track length:" << statTrackLength/statTrackNum << std::endl
                      << "---- finished annotating." << std::endl;


        //});




//
//-------------------------------------------------------------------------------------------------
//




//    } catch (std::exception & e) {
//        std::cout << "[ERROR]: " << e.what() << std::endl;
//        return -1;
//    }


    return 0;


}

}
}
}


