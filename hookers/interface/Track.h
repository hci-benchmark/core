#ifndef BOSCHGT_PROJECT_TRACK_H
#define BOSCHGT_PROJECT_TRACK_H

#include <string>
#include <memory>

#include <hookers/interface/NumericTypes.h>

#include "Project.h"
#include "Frame.h"
#include "Geometry.h"
#include "TrackData.h"

namespace hookers {
namespace interface {


class Sequence;
class Geometry;
class Landmark;
class TrackData;

/**
 * @brief holds the path of a tracked feature along frames
 */
class Track
{

	friend class Sequence;
	friend class TrackData;
    friend class Frame;
    friend class FrameData;

public:

	/** @brief type of track */
	enum Type {
		INVALID = 0, /*!< invalid track, eg. has been deleted */
        VALID, /*!< track is valid */
	};

    /** defines a default value for an invalid position in a frame */
    static const Vector2d INVALID_FRAME_POS;


private:


  SharedDataPointer<TrackData> d;


private:
	
	
	/** creates an object based on the offset in /tracks  */
	Track(TrackData * data);


public:
	
	/** constructs an invalid Track */
	Track();

    Object::Id id() const;

	/** @returns the project this track belongs to */
	Sequence sequence() const;


	/** @brief position of Track in a Frame
	 * if This track is not present in given Frame,
	 * a default constructed value should be returned.
	 * @returns the position in given frame or (-1, -1) otherwise
	 */
	Vector2d posInFrame(const Frame & f,
                const Frame::View & view = FrameData::View::Base) const;
	

	/** returns the linked position in 3d space */
	Vector3d position() const;
	void setPosition(const Vector3d & pos);
	
	/**  @returns true if this track is linked to a Landmark */
    bool hasLink(const Geometry & g = Geometry()) const;

	/** @return the Landmark this track is linked to
	 *  @note if Track is not linked, the Landmark will be invalid
	 */
    Landmark linkedLandmark(const Geometry & g = Geometry()) const;

	/** links this track to a Landmark
	 *  if the given Landmark is invalid, the Track will be unlinked
	 *  @param f Landmark that this track will be linked to
	 */
    bool linkToLandmark(const Landmark & l);

    /** unlinks any linked landmark from this track */
    void unlinkLandmark(const Geometry &g = Geometry());

	/** @returns true if this instance references a valid track in the project, false otherwise */
	bool isValid() const;


    /** returns the context in which the track has been created */
    Project::Context creationContext() const;

    /** returns the context in which the track has been modified last */
    Project::Context modificationContext() const;


    bool operator==(const Track & other) const {
        return (d == other.d);
    }
	
    bool operator<(const Track &other) const {
        return d < other.d;
    }

    int hash() const {
        return std::hash<TrackData*>()(d.data());
    }
	

};

} // ns interface
} // ns hookers

namespace std {
  template <> struct hash<hookers::interface::Track>
  {
    size_t operator()(const hookers::interface::Track & x) const
    {
        return x.hash();
    }
  };
}

#endif // BOSCHGT_PROJECT_TRACK_H

#include "TrackList.h"
