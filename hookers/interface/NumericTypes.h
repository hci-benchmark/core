#ifndef NUMERICTYPES_H
#define NUMERICTYPES_H

#include <vigra/multi_array.hxx>
#include <vigra/tinyvector.hxx>
#include <vigra/multi_math.hxx>
#include <vigra/linear_algebra.hxx>

namespace hookers {

typedef vigra::MultiArrayView<2, vigra::UInt8> ImageView;
typedef vigra::MultiArray<2, vigra::UInt8> Image;
typedef Image::difference_type ImageShape;

typedef vigra::MultiArrayView<3, vigra::UInt8> RGBImageView;
typedef vigra::MultiArray<3, vigra::UInt8> RGBImage;
typedef RGBImage::difference_type RGBImageShape;


typedef vigra::TinyVector<int,2> Vector2i;
typedef vigra::TinyVector<int,3> Vector3i;
typedef vigra::TinyVector<int,4> Vector4i;
typedef vigra::TinyVector<int,6> Vector6i;


typedef vigra::TinyVector<double,2> Vector2d;
typedef vigra::TinyVector<double,3> Vector3d;
typedef vigra::TinyVector<double,4> Vector4d;
typedef vigra::TinyVector<double,6> Vector6d;

typedef vigra::TinyVector<float,2> Vector2f;
typedef vigra::TinyVector<float,3> Vector3f;
typedef vigra::TinyVector<float,4> Vector4f;
typedef vigra::TinyVector<float,6> Vector6f;


typedef vigra::MultiArray<1, Vector2f>::difference_type Vector2ArrayShape;

typedef vigra::MultiArray<1, Vector2d> Vector2dArray;
typedef vigra::MultiArrayView<1, Vector2d> Vector2dArrayView;
typedef Vector2ArrayShape Vector2dArrayShape;
typedef vigra::MultiArray<1, Vector2f> Vector2fArray;
typedef vigra::MultiArrayView<1, Vector2f> Vector2fArrayView;
typedef Vector2ArrayShape Vector2fArrayShape;

typedef vigra::MultiArray<1, Vector3d>::difference_type Vector3ArrayShape;

typedef vigra::MultiArray<1, Vector3d> Vector3dArray;
typedef vigra::MultiArrayView<1, Vector3d> Vector3dArrayView;
typedef Vector3ArrayShape Vector3dArrayShape;

typedef vigra::MultiArray<1, Vector3f> Vector3fArray;
typedef vigra::MultiArrayView<1, Vector3f> Vector3fArrayView;
typedef Vector3ArrayShape Vector3fArrayShape;

typedef vigra::MultiArray<1, Vector4d> Vector4dArray;
typedef vigra::MultiArrayView<1, Vector3d> Vector4dArrayView;
typedef vigra::MultiArray<1, Vector3d>::difference_type Vector4dArrayShape;

typedef vigra::MultiArray<1, Vector6d> Vector6dArray;
typedef vigra::MultiArrayView<1, Vector6d> Vector6dArrayView;
typedef vigra::MultiArray<1, Vector6d>::difference_type Vector6dArrayShape;

typedef vigra::Shape1 Array1Shape;
typedef vigra::Shape2 Array2Shape;
typedef vigra::Shape3 Array3Shape;

typedef vigra::MultiArray<1,     double> Array1d;
typedef vigra::MultiArrayView<1, double> Array1dView;
typedef vigra::MultiArray<1,     double>::difference_type Array1dShape;

typedef vigra::MultiArray<1,     float> Array1f;
typedef vigra::MultiArrayView<1, float> Array1fView;
typedef vigra::MultiArray<1,     float>::difference_type Array1fShape;

typedef vigra::MultiArray<1,     int>                  Array1i;
typedef vigra::MultiArrayView<1, int>                  Array1iView;
typedef vigra::MultiArray<1,     int>::difference_type Array1iShape;

typedef vigra::MultiArray<2,     double> Array2d;
typedef vigra::MultiArrayView<2, double> Array2dView;
typedef vigra::MultiArray<2,     double>::difference_type Array2dShape;

typedef vigra::MultiArray<2,     float>                  Array2f;
typedef vigra::MultiArrayView<2, float>                  Array2fView;
typedef vigra::MultiArray<2,     float>::difference_type Array2fShape;

typedef vigra::MultiArray<2,     int>                  Array2i;
typedef vigra::MultiArrayView<2, int>                  Array2iView;
typedef vigra::MultiArray<2,     int>::difference_type Array2iShape;

typedef vigra::MultiArray<3,     double> Array3d;
typedef vigra::MultiArrayView<3, double> Array3dView;
typedef vigra::MultiArray<3,     double>::difference_type Array3dShape;

typedef vigra::MultiArray<3,     float> Array3f;
typedef vigra::MultiArrayView<3, float> Array3fView;
typedef vigra::MultiArray<3,     float>::difference_type Array3fShape;

typedef vigra::MultiArray<3,     int>                  Array3i;
typedef vigra::MultiArrayView<3, int>                  Array3iView;
typedef vigra::MultiArray<3,     int>::difference_type Array3iShape;

} // ns hookers

#endif // NUMERICTYPES_H

