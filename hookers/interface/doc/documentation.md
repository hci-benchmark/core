
Documentation {#documentation}
=============
This page is about how to document specific parts and their changes.


Document the Project File Definition {#documentation_hdf}
====================================

Objects
-------

when you want to add object definitions to @ref hdf use the keyword

  \@page hdf

To make the definition visible.

For a object directly located under /project use
> \@section hdf_objectName objectName/

For a objects on second level or below use
> \@subsection hdf_objectName_childGroup objectName/childGroup/

> \@subsection hdf_objectName_child_dataset objectName/child/dataset

Paths that end with a '/' are groups, paths ending without are datasets.



Attributes
----------
In order to document attributes of a hdf object use
\@h5attr{<name>,<type>} and \@h5oattr{<name>,<type>}
to define a mandatory or optional attribute.

If you wan to reference attributes later in the document use the following scheme:

> \@ref hdf_path_to_object__attrib

where \c path_to_object is the object-path (@em /project/path/to/object) inside a hdf file and \c attrib the the attribute name of the given object. Note the doubled underscores in order to distinguish attributes from groups or datasets.




@todo write a python script for INPUT_FILTER to automaticly
 - replace \@ref hdf_XXX__attrib by more readable link name
 - rearrange the \@h5attr and \@h5oattr such that they will be placed at the bottom
   on each comment block and \@anchor is set properly for each attribute.



Document changes to HDF backend {#documentation_hdf_changes}
===============================

This section describes how you should document your changes to the HDF backend and API.

TODO!