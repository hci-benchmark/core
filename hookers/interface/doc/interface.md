Project File Interface {#interface}
======================

This page describes the c++ interface that is used to access project files.
The file definition can be found on page @ref hdf.


The motivations and basic usage of the API can be found on this page.

Requirements {#interface_requirements}
============
We will discuss the requirements here in short.

GUI
---

When implementing GUIs, one wants fine-grained object-oriented interface.
This makes sense as we want to divide the backend-logic away from the GUI logic.
Backend internals could then be changed without touching GUI logic. This especially implies avoiding
the usage of backend's internal identifiers within the GUI logic.

Additional to that it would be benificial to use *shared datatypes*:
 - No pointers and hence no segfaults.
 - Lists of objects could be passed around *by-value* through the whole application
   without copying one byte of payload. This leads to quite clean interfaces 
   without *by-ref* constructions.
 - As the frontend to *shared datatypes* are just kind of pointers to the actual
   payload
   * memory management will be automated by atomic ref counter.
   * script bindings can be realized quite easy because script-objects
     don't own any memory. Memory management is completely resided on c++-part.
   * a *shared datatype* is an intermediate object that can notify about changes.
     This is important for asynchronous GUI operations.
   

Postprocessing
--------------

Postprocessing has some orthogonal requirements to GUIs.
We usually process huge bunch of data and hence need the payload
to be contigous in memory. Simply by the argument of speed.
Reading and writing these bunches of data from the project file directly would beneficial.

Data Integrity
--------------
We have to assure that changes to the datasets do not break any linkage. Additional
on-the-fly information for caching purposes should be generated. 
This must be consistent through every processing step.

Thread-Safety
-------------
In the long term working with GUI and doing postprocessing should occour within the same application because of 
 - Instant Tracking while the user is annotating
 - Estimation of Camerapose given the annotated data
 - ...

We have to use threads in order to have a non-blocking GUI. So the interface should at least be thread-agnostic. Beneficial would be to be thread-safe.


Conclusion
----------
*Data Integrity* and *thread-safety* requires single point of access. Especially with *Data Integrity*, file revision management and upstream conversion comes into mind. This can only be done with a central library, agnostic of all these changes. Small hacks here and there will very likely introduce broken files through out the processing pipeline.

In order to get fine grained access the library will offer most of the structure in the file through lists of objects, like Tracks, Landmark, ..., Frames.

Special care will be taken for the lists of objects. These lists should offer memory-contigous views of all the objects in a list, even though the list entries might not be contigous in the backend file. See AbstractObjectList for further reference.


Usage {#interface_usage}
========================

To be completed.


TODO
====

 - Frames besitzen Views (left/right)
 - Tracks können Views zugeordnet werden.
