General Description of the whole project

@todo add stuff from paper here

Data Aquisition
===============

We drive with a car through a scene and capture it in two different ways:

  1. Stereo Camera Setup
  2. Laser Distance Scanner

The first ones gives as a pair of images on which stereo depth estimation algorithmns can work on.

The second gives a pointcloud which acts as a reference to the depth estimation algorithms.


Workflow
========

The basic workflow consists of

  1. Find features in images and track them through frames
     We call this set of positions a *track*
  2. We annotate tracks with specific points from the 3d pointcloud, called landmarks.
  3. We do postprocessing like estimating camera pose for each frame or
     calculate depthmaps as well as providing estimations on errors.


Projects
========

In order to store all these different kind of datasets we use a project file.
The definition on the project file can be found on @ref hdf.
To access elements in the project file there exists a c++ interface.
Basic usage is explained on @ref interface. Further reference can be found there as well.
