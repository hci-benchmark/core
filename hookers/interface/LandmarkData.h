#ifndef BOSCHGT_PROJECT_LANDMARKDATA_H
#define BOSCHGT_PROJECT_LANDMARKDATA_H

#include <string>
#include <memory>

#include <hookers/interface/NumericTypes.h>
#include <hookers/interface/SharedType.h>


namespace hookers {
namespace interface {

class Landmark;

class LandmarkData : public SharedData {

public:


    virtual void invalidate() = 0;

	virtual ~LandmarkData() {}

	virtual Vector3d pos() const = 0;

	operator Landmark();

};

} // ns interface
} // ns hookers

#endif //BOSCHGT_PROJECT_LANDMARKDATA_H
