#ifndef BOSCHGT_PROJECT_FRAMEEDATA_H
#define BOSCHGT_PROJECT_FRAMEEDATA_H

#include <memory>
#include <hookers/interface/SharedType.h>
#include <hookers/interface/NumericTypes.h>
#include <hookers/interface/Objects.h>

namespace hookers {
namespace interface {

class Frame;
class Sequence;
class TrackData;
class TrackList;

class FrameData : public SharedData
{
public:

	/**
	 * @brief The View enum
	 * Defines the view position of a frame.
	 * We are dealing with stereo setups, so
	 * there are always two views associated to one Frame.
	 *
	 * Most of the calculations will be done on the so called Base view.
	 * So to most functions this will be the standard argument.
	 * However, to explicitly retrieve information of the corresponding
	 * stereo view, you can specify the view type Stereo.
	 *
	 */
    enum View { All = -1,
                Base = 0, /*!< Base view */
				Stereo /*!< Stereo view. */
			  };

    typedef Frame Frontend;

	typedef vigra::MultiArray<3, float> Map;
	typedef vigra::MultiArray<2, float> List;
    typedef vigra::MultiArrayView<3, float> MapView;
    typedef vigra::MultiArrayView<2, float> ListView;
    typedef vigra::TinyVector<int, 2> Size;


public:

    inline FrameData() {
        /* initialize the external usage counter */
        m_externalUsage = 0;
    }

    virtual Object::Id id() const = 0;
    virtual Sequence sequence() const = 0;

    virtual void create() = 0;
    virtual void load() = 0;
    virtual void save() = 0;

    virtual void externalReferenced() = 0;
    virtual void externalUnreferenced() = 0;


	virtual ~FrameData() {}
    virtual bool isValid() const = 0;
	virtual void setView(const ImageView & image, const View  & view= Base) = 0;
    virtual void getView(Image & image, const View & view = Base) const = 0;

    virtual void linkTrack(TrackData * td, const Vector2d & pos, const View & view) = 0;
    virtual void unlinkTrack(TrackData * td, const View & view) = 0;
    virtual Vector2d trackPosition(TrackData * td, const View & view) const = 0;
    virtual TrackList tracks(const View & view) const = 0;


    /**
     * pose is always returned in world frame. That means, it describes a transformation from
     * camera coordinated into world coordinates.
     *
     * For further information refer to Frame::pose().
     */
    virtual Vector6d pose() const = 0;

    /**
     * @see pose() for information on how to set the pose.
     */
    virtual void setPose(const Vector6d & pose) = 0;

    virtual Vector6d poseUncertainty() const = 0;
    virtual void setPoseUncertainty(const Vector6d & pose) = 0;

    virtual double time() const = 0;
    virtual void setTime(const double & time) = 0;

    virtual Vector4d intrinsics(const FrameData::View& view) const = 0;
    virtual void setIntrinsics(const Vector4d & intrinsics, const FrameData::View & view) = 0;

    virtual Vector4d intrinsicsUncertainty(const FrameData::View & view) const = 0;
    virtual void setIntrinsicsUncertainty(const Vector4d & intrinsics, const FrameData::View & view) = 0;

    virtual Vector6d viewTransformation(const FrameData::View & view) const = 0;
    virtual void setViewTransformation(const Vector6d & trans, const FrameData::View & view) = 0;

    virtual Vector6d viewTransformationUncertainty(const FrameData::View & view) const = 0;
    virtual void setViewTransformationUncertainty(const Vector6d & trans, const FrameData::View & view) = 0;


    virtual void setMap(const std::string & name, const MapView & map = MapView()) = 0;
    virtual void getMap(const std::string & name, Map & map) const = 0;
	virtual bool hasMap(const std::string & name) = 0;
    virtual std::string getDataFilePath() {return "";}

    virtual void setList(const std::string & name, const ListView & list = ListView()) = 0;
    virtual void getList(const std::string & name, List & list) const = 0;
	virtual bool hasList(const std::string & name) = 0;

    operator Frame() const;

private:
    std::atomic_int m_externalUsage;
public:
    template<class T> void ref();
    template<class T> void deref();



};

template<>
inline void FrameData::ref<Frame>()
{
    if(!m_externalUsage.fetch_add(1)) {
        this->externalReferenced();
    }
}

template<>
inline void FrameData::deref<Frame>()
{
    /* if the frame usage counter drops to zero,
     * we can safely remove any required caches and clear
     * lists associated with frames */
    if(m_externalUsage.fetch_sub(1) == 1) {
        this->externalUnreferenced();
    }
}

template<> inline void FrameData::ref<void>() {}
template<> inline void FrameData::deref<void>() {}

} // ns interface
} // ns hookers


#endif //BOSCHGT_PROJECT_FRAMEEDATA_H
