#ifndef LANDMARK_H
#define LANDMARK_H

#include <memory>
#include <hookers/interface/NumericTypes.h>
#include <hookers/interface/SharedType.h>

#include "LandmarkData.h"


namespace hookers {
namespace interface {

class TrackList;


/** @brief describes one dominant Landmark in a Part
 * This class describes one Landmark you can select.
 */
class Landmark
{

    friend class Track;
    friend class Part;
    friend struct std::hash<Landmark>;
    friend class Geometry;

private:

	SharedDataPointer<LandmarkData> d;

public:

	enum Type {
		INVALID = 0,
		MANUAL
	};
	
	/**
	 * @warning NEVER ever use this function!
	 * This constructor is only publicaly exposed due to bad interface design. It is only meant for backend usage.
	 */
	Landmark(LandmarkData * data);

public:


	/** @brief constructs invalid Landmark
	 *  An invalid can never turn into a valid.
	 */
	Landmark();


	/**
	 * @return position of the Landmark in absolute coordinates
	 */
	Vector3d pos() const;
	
	/** @brief object validity
	 *  Tells whether the object is a valid one.
	 *  It can change for instance if it has been deleted.
	 */
	bool isValid() const;


	/** @brief compare against other instance */
	bool operator==(const Landmark & other) const;

    bool operator!=(const Landmark & other) const {
        return !(*this == other);
    }

	TrackList tracks() const;
	

};

} // ns interface
} // ns hookers

namespace std {
  template <> struct hash<hookers::interface::Landmark>
  {
    size_t operator()(const hookers::interface::Landmark & x) const
    {
        return std::hash<hookers::interface::LandmarkData*>()(x.d.data());
    }
  };
}


#endif // LANDMARK_H

// include the header of Landmark list as well
#include "LandmarkList.h"
