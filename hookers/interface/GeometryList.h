#ifndef HOOKERS_PROJECT_GEOMETRYLIST_H
#define HOOKERS_PROJECT_GEOMETRYLIST_H

#include <vigra/tinyvector.hxx>
#include <vigra/multi_array.hxx>

#include <hookers/interface/AbstractObjectList.h>
#include <hookers/interface/GeometryData.h>
#include <hookers/interface/Geometry.h>

namespace hookers {
namespace interface {


/** 
 * Abstract class for object lists that handle contigous
 * objects
 */
class GeometryList: public AbstractSharedTypeList<GeometryData>
{
public:


public:

    /** searches for a sequence with given label.
     * @returns a reference to the sequence or an invalid sequence if not found
     */
    Geometry byLabel(const std::string & label);
};

} // ns interface
} // ns hookers

#endif // HOOKERS_PROJECT_GEOMETRYLIST_H
