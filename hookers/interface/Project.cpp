#include <cstdlib>
#include <cassert>
#include <memory>

#include "Project.h"
#include "ProjectData.h"
#include "backends/hdf/ProjectDataHDF.h"

using namespace hookers::interface;

std::string Project::s_basePath;
Project::Context Project::s_context = Project::PipelineContext;

Project::Project(const ProjectData *data)
	: d(data)
{

}

std::string Project::findProjectName(const std::string &name)
{

	/** If name is empty, it will be checked for the enviroment variable
     *  BOSCHGT_PROJECT to be set. */
	if(name.empty()) {
        if(const char* env_p = std::getenv("BOSCHGT_PROJECT"))
			return std::string(env_p);

        return std::string();
	}

	return name;
}

Project::Project()
{

}

Project::~Project()
{

}


bool Project::isModified()
{

	if(!d)
		return false;

	return d->isModified();
}

bool Project::isValid() const
{
	if(!d)
		return false;

	return d->isValid();
}

void Project::save()
{
	if(!d)
		return;

	d->save();
}

PartList Project::parts() const
{
	if(!d)
		return PartList();

    PartList parts;
    for(Geometry g : geometries()) {
        for(Part p : g.parts()) {
            parts.push_back(p);
        }
    }

    return parts;
}

GeometryList Project::geometries() const
{
    if(!d)
        return GeometryList();
    return d->geometries();
}

Geometry Project::addGeometry(const std::string &name)
{
    if(!d)
        return Geometry();
    return d->addGeometry(name);
}

Sequence Project::addSequence(const std::string &name)
{
	if(!d)
		return Sequence();

	return d->addSequence(name);
}

SequenceList Project::sequences() const
{
	if(!d)
		return SequenceList();
	return d->sequences();
}

std::string Project::base()
{

    if(!s_basePath.empty())
        return s_basePath;

	/**
     * You can set this base path via the enviroment variable BOSCHGT_BASE.
	 * If this is unset, a sane default backend and location will be chosen, if possible.
	 */
    if(const char* env_p = std::getenv("BOSCHGT_BASE")) {
            std::cout << "Your BOSCHGT_BASE is: " << env_p << '\n';
			return std::string(env_p);
	}

	return std::string();
}

void Project::setBase(std::string &url)
{
    s_basePath = url;
}

std::string Project::dataBase()
{
    if(!s_basePath.empty())
        return s_basePath;

    /**
     * You can set this base path via the enviroment variable BOSCHGT_BASE.
     * If this is unset, a sane default backend and location will be chosen, if possible.
     */
    if(const char* env_p = std::getenv("BOSCHGT_DATA")) {
            std::cout << "Your BOSCHGT_DATA is: " << env_p << '\n';
            return std::string(env_p);
    }

    /** if we cannot find enviroment variable, then fall back to base() */
    return base();
}

Project Project::create(const std::string &name)
{
    std::string path = base();
	return ProjectDataHDF::create(path, findProjectName(name));
}

Project Project::open(const std::string &name)
{
    std::string path = base();
    std::string finalName = findProjectName(name);

    if(finalName.empty()) {
        std::cout << "[ERROR] Opening project: no name provided. " << std::endl;
        return Project();
    }

    Project project = ProjectDataHDF::open(path, finalName);

    return project;
}

ProjectData::operator Project() const
{
	return Project(this);
}

