#ifndef BOSCHGT_PROJECT_ABSTRACTOBJECTLIST_H
#define BOSCHGT_PROJECT_ABSTRACTOBJECTLIST_H

#include <iterator>
#include <vector>
#include <list>
#include <set>
#include <unordered_set>
#include <queue>

#include <hookers/interface/SharedType.h>

namespace hookers {
namespace interface {

/**
 * AbstractSharedTypeList is a special list that internally holds
 * references to objects by SharedDataPointer.
 *
 * It is a Template Class and hence can hold any shared types.
 * The only requirements on T are the compability with SharedDataPointer
 * and that T has a type cast operator implemented that casts to
 * T::Frontend.
 *
 * By providing the optional template argument S, you can specify the source
 * of reference (look at SharedDataPointer for further reference)
 * and hence have more precise control over reference counting.
 *
 * This can be useful for types where lists of references shouldn't count as
 * external references.
 *
 */
template <typename T, typename S=void>
class AbstractSharedTypeList {

public:
    typedef typename T::Frontend Frontend;
    typedef SharedDataPointer<T,S> Ptr;
    typedef std::vector<Ptr> container;
    typedef Frontend value_type;



    class iterator {
    public:
        typedef typename container::iterator list_it;
        typedef typename container::const_iterator const_list_it;

        typedef int difference_type;
        typedef Frontend value_type;
        typedef Ptr pointer;
        typedef Frontend& reference;
        typedef std::random_access_iterator_tag iterator_category;
//        typedef std::bidirectional_iterator_tag  iterator_category;

    private:
        list_it it;
    public:

        iterator() {}

        iterator(list_it it) : it(it) {}

        iterator(const_list_it & it) : it(it) {}

        inline Frontend operator->() const {
            return *(it->data());

        }

        inline Frontend operator*() const {
            return *(it->data());
        }

        inline iterator & operator++() {
            ++it;
            return *this;
        }

        inline iterator operator++(int) {
            return iterator(it++);
        }

        inline iterator & operator--() {
            --it;
            return *this;
        }

        inline iterator operator--(int) {
            return iterator(it--);
        }

        inline int operator-(const iterator & other) {
            return this->it - other.it;
        }

        inline int operator+(const iterator & other) {
            return this->it + other.it;
        }

        inline iterator  operator+(int i) {
            return it+i;
        }

        inline iterator operator-(int i) {
            return it-i;
        }

        inline iterator & operator+=(int i) {
            it += i;
            return *this;
        }


        bool operator==(const iterator & other) {
            return it == other.it;
        }

        bool operator!=(const iterator & other) {
            return it != other.it;
        }

        const list_it & toBackendIterator() const {
            return it;
        }

        inline operator list_it() {
            return it;
        }

        inline operator const_list_it() const {
            return it;
        }



    };

    typedef iterator const_iterator;

private:
    container list;


protected:
    container & pointers() {
        return list;
    }

public:

    AbstractSharedTypeList() {}

    inline AbstractSharedTypeList(const std::initializer_list<typename T::Frontend> & list) {
        for(auto f: list) {
            this->push_back(f);
        }
    }

    inline size_t size() const {
        return list.size();
    }

    inline void push_back(T * data) {
        list.push_back(data);
    }

    inline void push_back(const Frontend &f) {
        T * d = static_cast<T*>(f.d.data());
        list.push_back(d);
    }

    inline void append(const Frontend & f) {
        push_back(f);
    }

    inline void remove(const Frontend & f) {
//        T * d = static_cast<T*>(f.d.data());
//        list.remove(d);
        iterator rmIt = this->find(f);
        list.erase(rmIt);
    }

    inline Frontend front() {
        return *(list.front());
    }

    inline const Frontend front() const {
        return *(list.front());
    }

    inline Frontend back() {
        return *(list.back());
    }

    inline const Frontend back() const {
        return *(list.back());
    }

    inline iterator begin() {
        return iterator(list.begin());
    }

    inline iterator end() {
        return iterator(list.end());

    }

    inline const_iterator cbegin() {
        return const_iterator(list.begin());
    }

    inline const_iterator cend() {
        return const_iterator(list.end());
    }


    /**
     * get a reference to element at position i.
     * @warning does not check if i is a valid index.
     */
    inline Frontend at(int i) {
        auto it = list.begin();
        std::advance(it, i);
        return *((*it).data());
    }

    /** @overload */
    inline const Frontend at(int i) const {
        auto ilist = list.begin();
        std::advance(ilist, i);
        return *(*ilist);
    }

    inline Frontend operator[](const int & i) {
        return at(i);
    }

    inline Frontend operator[](const int & i) const {
        return at(i);
    }


    /** checks if t is contained in list */
    inline bool contains(const Frontend & t) const {
        return (this->find(t) != this->end());
    }

    inline iterator find(const Frontend & t) {
        auto it = std::find_if(list.begin(), list.end(), [&](const Ptr & p){
            return (p.data() == t.d.data());
        });
        return iterator(it);
    }

    inline const_iterator find(const Frontend & t) const {
        auto it = std::find_if(list.begin(), list.end(), [&](const Ptr & p){
            return (p.data() == t.d.data());
        });
        return const_iterator(it);
    }

    inline bool empty() const {
        return list.empty();
    }

    /** get value at position i.
     * if position is out of bounds,
     * default constructed value will be returned.
     */
    inline Frontend value(int i, const Frontend & def = Frontend()) {
        if(i < 0 || i >= int(this->size()))
            return def;
        return at(i);
    }

    void reserve(int size) {
        list.reserve(size);
    }

    void resize(int size) {
        list.resize(size);
    }

    /**
     * @brief subsample the list with given step size.
     *
     * Only every step-th entry will be taken and returned
     */
    AbstractSharedTypeList<T,S> subsample(unsigned step)  {
        AbstractSharedTypeList<T,S> subsampled;
        subsampled.reserve(list.size()/step+1);

        for(int i = 0; i < list.size(); i += step) {
            subsampled.push_back(list[i].data());
        }
        return subsampled;
    }


};


/** @brief list of contigous project objects 
 * 
 * Lists derived from AbstractObjectList basicly are a wrapper around std::list.
 * Additonally it offers:
 *  - special filter functions
 *  - temporary views on objects data as contigous arrays
 * 
 * @see TrackList, LandmarkList, PartList, FrameList
 * 
 * ===============
 * Derived Classes
 * ===============
 * 
 * Although this abstract class does not offer any views nor filtering, every derived class
 * should behave as described here:
 * 
 * **Example usage:**
 * @code
 * // get a list of objects
 * const DerivedList list = project.objects();
 * 
 * // request the view
 *  MultiArray view;
 *  list.getData(view);
 * 
 * for(int i = 0, i < list.size(); i++) {
 *
 * 	// fancy optimization code
 * 	if(list[i].hasProperty())) {
 * 		view(0,i) = 300.0+i;
 * 	}
 * }
 * 
 * // write the changes
 * list.setData(view);
 * 
 * @endcode
 * 
 * Data Views
 * ==========
 * Data views offer a temporary view on on objects contained in the list.
 * These data views are created on demand and offer a contigous region in memory.
 * Views on data could be, eg. the positions of all objects.
 * 
 * In order to achieve fast assembly of these contigous arrays, 
 * this list also stores contigous ranges of objects inside the list.
 *
 * Indexing
 * --------
 * 
 * The last index (x,y,z,...) of those arrays corresponds to the index of a object
 * in the list, the former indices are the components of data views.
 * 
 * Note that vigra is column-major and therefore the indices are reversed with
 * regard to project file's @ref hdf_doc_memoryOrder.
 * 
 * 
 * 
 * Interface 
 * ---------
 * 
 * The interface should look similar to this for each dataview you want to offer:
 * 
 * @code
 * // Matrix2d is a vigra::MultiArrayView or cv::Mat
 * void getData(Vector2dArray & arr) const;
 * void setData(const Vector2dArrayView & view);
 * @endcode
 *
 * Each dataview getter should sets up the provided matrix type (resize etc..) and fill it
 * with the data from all list entries.
 *
 * 
 * Changing List Entries
 * ---------------------
 * Special care needs to be taken when requesting a tabular view on the list.
 * It is possible that the list might change during operations on the view.
 *
 *
 * List Entries
 * ============
 * 
 * If you query individual objects from the list, they should not reflect the changes made
 * in the data views as long as they aren't set back via the setXXXX() members.
 * 
 * @code
 *
 * Vector3dArray view;
 * list.getPositions(view);
 * 
 * cout << list[1].position(); // (0,0,0)
 * view(1)[0] = 1.0;
 * cout << list[1].position(); // (0,0,0)
 * list.setPositions();
 * cout << list[1].position(); // (1.0,0,0)
 *
 * @endcode
 * 
 * 
 */
template <typename T,typename S=void>
class AbstractObjectList : public std::vector<T>
{

public:

    typedef typename std::vector<T>::iterator iterator;
    typedef typename std::vector<T>::const_iterator const_iterator;
	
    AbstractObjectList() {}

    AbstractObjectList(const std::initializer_list<T> & list)
        : std::vector<T>(list)
    {}

    /**
     * index access operator.
     * @see at()
     * @warning does not check if i is a valid index.
	 */
    inline T & operator[](int i) {
        return this->at(i);
	}

    inline const T & operator[](int i) const {
        return this->at(i);
    }
	
    /**
     * get a reference to element at position i.
     * @warning does not check if i is a valid index.
     */
    inline T & at(int i) {
//        auto ilist = this->begin();
//        std::advance(ilist, i);
//        return *ilist;
        return std::vector<T>::operator [](i);
    }

    inline const T & at(int i) const {
//        auto ilist = this->begin();
//        std::advance(ilist, i);
//        return *ilist;
        return std::vector<T>::operator [](i);
    }

    /** get value at position i.
	 * if position is out of bounds,
	 * default constructed value will be returned.
	 */ 
    inline T value(int i, const T & def = T()) {
        if(i < 0 || i >= int(this->size()))
            return def;
        return at(i);
    }

    /** checks if t is contained in list */
    inline bool contains(const T & t) const {
        return (this->find(t) != this->cend());
    }

    inline iterator find(const T & t) {
        for(auto it = this->begin(); it != this->end(); it++) {
            if(t == *it)
                return it;
        }
        return this->end();
    }

    inline const_iterator find(const T & t) const {
        for(auto it = this->cbegin(); it != this->cend(); it++) {
            if(t == *it)
                return it;
        }
        return this->cend();
    }

	
public:
	
};

/**
 * @brief A abstract Set of objects. Can be used for convenient set operations.
 */
template <typename T>
class AbstractObjectSet: public std::unordered_set<T> {
public:


public:


    inline void remove(const T & t) {
        this->erase(t);
    }

    inline bool contains(const T & t) const {
        return this->count(t);
    }

    inline AbstractObjectSet<T> & operator&=(const AbstractObjectSet<T> & other) {
        for(auto it = this->begin(); it != this->end();) {
            if(other.find(*it) != other.end()) {
                it++;
                continue;
            }
            it = this->erase(it);
        }
        return *this;
    }

    inline AbstractObjectSet<T> operator&(const AbstractObjectSet & other) const {
        AbstractObjectSet<T> newSet;
        for(auto it = this->cbegin(); it != this->cend(); it++) {
            if(other.find(*it) == other.cend())
                continue;
            newSet.insert(*it);
        }
        return newSet;
    }

    inline AbstractObjectSet<T> & operator|=(const AbstractObjectSet<T> & other) {
        for(auto it = other.cbegin(); it != other.cend(); it++)
            this->insert(*it);
        return *this;
    }

    inline AbstractObjectSet<T> operator|(const AbstractObjectSet & other) const {
        AbstractObjectSet<T> newSet;
        for(auto it = other.cbegin(); it != other.cend(); it++) {
            newSet.insert(*it);
        }
        for(auto it = this->cbegin(); it != this->cend(); it++) {
            newSet.insert(*it);
        }
        return newSet;
    }

    inline AbstractObjectSet<T> & operator-=(const AbstractObjectSet<T> & other) {
        for(auto it = this->begin(); it != this->end(); ) {
            if(other.find(*it) == other.end()) {
                it++;
                continue;
            }
            it = this->erase(it);
        }
        return *this;
    }

    inline AbstractObjectSet<T> operator-(const AbstractObjectSet & other) const {
        AbstractObjectSet<T> newSet;
        for(auto it = this->cbegin(); it != this->cend(); it++) {
            if(other.find(*it) != other.cend())
                continue;
            newSet.insert(*it);
        }
        return newSet;
    }

    inline AbstractObjectList<T> toList() const {
        AbstractObjectList<T> list;
        for(auto it = this->cbegin(); it != this->cend(); it++)
            list.push_back(*it);
        return list;
    }

    inline static AbstractObjectSet<T> fromList(const AbstractObjectList<T> & list) {
        AbstractObjectSet<T> set;
        for(auto it = list.cbegin(); it != list.cend(); it++)
            set.insert(*it);
        return set;
    }


};

template<typename T, int N>
class ObjectCache {

	std::set<T> m_set;
    std::list<T> m_queue;


public:

    ~ObjectCache() {
        clear();
    }

	inline void add(const T & obj) {
	
    		/* if object is in cache, we don't need to care */
		if(m_set.count(obj))
			return;

		/* object has not been inserted so far, so add it */
		m_set.insert(obj);
        m_queue.push_back(obj);

		/* check if number of objects is too large.
		 * in this case delete the oldest object */
		if(m_queue.size() >= N) {
			m_set.erase(m_queue.front());
            m_queue.pop_front();
		}
	}

	inline void clear() {
		m_set.clear();
        m_queue.clear();
	}
	
	
};

} // ns interface
} // ns hookers


#endif // BOSCHGT_PROJECT_ABSTRACTOBJECTLIST_H
