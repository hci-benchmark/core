#include "Landmark.h"
#include "LandmarkData.h"
#include "TrackList.h"

using namespace hookers;
using namespace hookers::interface;

Landmark::Landmark(LandmarkData *data)
	: d(data)
{

}

TrackList Landmark::tracks() const {
    throw std::string("Landmark::tracks() not implemented");
}

Landmark::Landmark()
{

}

Vector3d Landmark::pos() const
{
	if(!d)
		return Vector3d();
	return d->pos();
}

bool Landmark::isValid() const
{
	if(!d)
		return false;
	return true;
}

LandmarkData::operator Landmark()
{
	return Landmark(this);
}

bool Landmark::operator ==(const Landmark & other) const
{
    return (this->d == other.d);
}


void LandmarkList::getPositions(Vector3fArray &view) const
{
    if(empty())
        return;

    Vector3fArrayShape shape;
    shape[0] = size();

    view.reshape(shape);

    int i = 0;
    for(auto it = this->cbegin(); it != this->cend(); it++) {
        view(i) = (*it).pos();
        i++;
    }
}


LandmarkList LandmarkList::knn(const Vector3f &p, unsigned k) const {
    LandmarkList nearest(*this);
    std::sort(nearest.begin(), nearest.end(), [&](const Landmark & a, const Landmark & b){
        return (a.pos()-p).magnitude() < (b.pos()-p).magnitude();
    });

    /* if there are too many items in the list, erase them */
    if(nearest.size() > k) {
        nearest.erase(nearest.begin()+k, nearest.end());
    }

    return nearest;
}

LandmarkList LandmarkList::radius(const Vector3f &p, float radius) const {
    throw std::runtime_error("LandmarkList::radius() is not implemented.");
}

LandmarkList::Box LandmarkList::boundingBox() const {
    Vector3fArray positions;
    getPositions(positions);

    Box box;
    const auto posView = positions.expandElements(0);

    for(int i = 0; i < 3; i++) {
        float min, max;
        posView.bindInner(i).minmax(&min, &max);

        /* calculate the center and the half of the edge length */
        std::get<0>(box)[i] = (min+max)/2.0;
        std::get<1>(box)[i] = (max-min)/2.0;
    }

    return box;

}

LandmarkList::Sphere LandmarkList::perimeter() const {
    const Box & box = this->boundingBox();
    return Sphere(std::get<0>(box), std::get<1>(box).magnitude());
}
