#ifndef PROJECT_H
#define PROJECT_H

#include <string>

#include <hookers/interface/SharedType.h>
#include <hookers/interface/Part.h>
#include <hookers/interface/Geometry.h>
#include <hookers/interface/Sequence.h>

#include "ProjectData.h"

namespace hookers {
namespace interface {

class Project
{

	friend class ProjectData;
public:

    /** @brief revision number of project file this API serves for
     *
     *  If you change the number here, change it in the hdf part as well!
     */
    static const unsigned int REVISION = 1;

    /**
     * @brief The context in which the interface operates on a project
     *
     * The context is meant to distinguish between manual and automatic results.
     *
     */
    enum Context {
        NoContext = 0, /*!< no context referes to a state where it has not been processed so far */
        PipelineContext, /*!< a object or property has been altered by a pipeline step */
        ManualContext /*!< a object or property has been altered by manual (human) interaction */
    };


private:

	SharedDataPointer<ProjectData> d;

    static std::string s_basePath;
    static std::string s_dataBasePath;

    static Context s_context;


private:

	/**
	 * @param data pointer to the ProjectData instance
	 */
    Project(const ProjectData * data);

	/**
	 * @brief findProjectName
	 * @return a project name or a default value
	 */
	static std::string findProjectName(const std::string & name);

public:


	Project();
	~Project();


	/**
	 * @brief tests whether the project has been modified
	 */
	bool isModified();

	/**
	 * @brief tests whether the project is valid
	 */
    bool isValid() const;

	/**
	 * @brief save project
	 * saves any still temporary changes to file
	 */
	void save();


    /** @name Part Management */
    /** @{ */

    /**
     * @returns a list of all parts
     * @deprecated
     */
	PartList parts() const;

	/** @brief creates a new part in the project */
	Part addPart(const std::string & name);

    /** @} */

    /** @name Geometry Management */
    /** @{ */

    /** @returns a list of all parts */
    GeometryList geometries() const;

    /** @brief creates a new part in the project */
    Geometry addGeometry(const std::string & name);

    /** removes a given part from the project.
     *  @note that given part will be invalid afterwards
     */
    void removeGeometry(Geometry part);

    /** @} */


    /** @name Sequence Management */
    /** @{ */

	/**
	 * @brief creates a new sequences in the project
	 * @param name label of the sequence
	 * @returns a reference to the newly created sequence
	 */
	Sequence addSequence(const std::string & name);

	/**
	 * @brief lists all available sequences
	 */
	SequenceList sequences() const;

    /** @} */



public:


    /** @name Project Parameters */
    /** @{ */


	/**
	 *
	 * @returns the backends base path.
	 *
	 * The base path specifies the backend to use and the location where to find data.
     * For hdf-backend this will lead to a  file url pointing to the root folder,
	 * eg. hdf:///var/boschgt.
	 *
	 * But it can also be the host and database name for a DB backend, as in
	 * pgsql://gaperon/boschgt.
     *
     * You can ommit the protocl specifier. In this case the hdf-backend will be
     * choosen.
     *
     * By default this value will be taken from the enviroment variable
     * BOSCHGT_BASE.
     *
     * If BOSCHGT_BASE is empty, the hdf backend will be choosen by default
     * with the current working directory as working folder.
     *
	 *
     * @see setBase()
	 */
    static std::string base();

	/**
	 * @brief manually overwrites base path
	 */
    static void setBase(std::string & url);

    /** reads the directory where map files should be written to */
    static std::string dataBase();

    /** @returns the context of the interface.
     *  @see Context
     */
    inline static const Context & context() {
        return s_context;
    }

    /** sets context */
    static void setContext(Context context) {
        switch (context) {
            case PipelineContext:
            case ManualContext:
                s_context = context;
            break;
        default:
            break;
        }
    }

    /** @} */

    /** @name Project Management */
    /** @{ */

	/** @brief creates an empty project
     *
     *  @param name name of the project
     *  @warning any existing project with that name will be overwritten
     *  @see base() for specifying the backend
	 */
	static Project create(const std::string & name);

	/**
	 * @brief opens a existing project
	 *
	 * If the project does not exist or could not be
	 * opened, a invalid project will be returned.
	 *
     * @param name Name of the project. If name is an empty string,
     *             the BOSCHGT_PROJECT enviroment variable will be taken.
	 */
    static Project open(const std::string & name = std::string());

    /** @} */

};

} // ns interface
} // ns hookers

#endif // PROJECT_H
