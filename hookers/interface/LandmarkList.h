#ifndef BOSCHGT_PROJECT_LANDMARKLIST_H
#define BOSCHGT_PROJECT_LANDMARKLIST_H

#include <hookers/interface/NumericTypes.h>

#include "AbstractObjectList.h"

#include "Landmark.h"
#include "Part.h"

namespace hookers {
namespace interface {

/** 
 * Abstract class for object lists that handle contigous
 * objects
 */
class LandmarkList: public AbstractObjectList<Landmark>
{
public:
    typedef std::tuple<Vector3f, float> Sphere;
    typedef std::tuple<Vector3f, Vector3f> Box;

public:

    LandmarkList(){}

    LandmarkList(const std::initializer_list<Landmark> & list) :
        AbstractObjectList<Landmark>(list) {}

    LandmarkList(const AbstractObjectList<Landmark> & other)
        : AbstractObjectList<Landmark>(other) {}
    LandmarkList(const LandmarkList & other)
        : AbstractObjectList<Landmark>(other) {}

    /** move constructor */
    LandmarkList(AbstractObjectList<Landmark> && other) { swap(other);}
    /** move constructor */
    LandmarkList(LandmarkList & other) { swap(other); }

    /** move assign */
    LandmarkList & operator=(AbstractObjectList<Landmark> && other) {
        swap(other);
        return *this;
    }

    /** move assign */
    LandmarkList & operator=(LandmarkList && other) {
        swap(other);
        return *this;
    }

    LandmarkList & operator=(const AbstractObjectList<Landmark> & other) {
        AbstractObjectList<Landmark>::operator =(other);
        return *this;
    }

    LandmarkList & operator=(const LandmarkList & other) {
        AbstractObjectList<Landmark>::operator =(other);
        return *this;
    }


	/** returns all positions of Landmarks in this list.
	 * Array shape is (3xN) where N is the number of Landmarks in list.
	 */
    void getPositions(Vector3fArray & view) const;
	
	/** @brief writes back the positions to all Landmarks.
	 */
    void setPositions(const Array2f & view);

    /** nearest neighbour search over all items in the list */
    LandmarkList knn(const Vector3f & p, unsigned k) const;

    LandmarkList radius(const Vector3f & p, float radius) const;



    /**
     * returns the smallest ball that contains
     * all landmarks of this list are c.
     */
    Sphere perimeter() const;


    /**
     * @brief boundingBox calculates the bounding box
     * @return a tuple where the first entry is the center
     *         and the second entry corresponds to the half
     *         edge length of each dimension.
     */
    Box boundingBox() const;
	
};

typedef AbstractObjectSet<Landmark> LandmarkSet;

} // ns interface
} // ns hookers

#endif // BOSCHGT_PROJECT_LANDMARKLIST_H
