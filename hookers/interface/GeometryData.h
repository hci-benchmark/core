#ifndef BOSCHGT_PROJECT_GEOMETRYDATA_H
#define BOSCHGT_PROJECT_GEOMETRYDATA_H

#include <string>
#include <memory>


#include "SharedType.h"
#include "Objects.h"
#include <hookers/interface/NumericTypes.h>


namespace hookers {
namespace interface {

class Project;
class Geometry;
class Part;
class PartList;
class Landmark;
class LandmarkList;


class GeometryData : public SharedData, public StructuredObject
{
public:
    typedef Geometry Frontend;

private:
//  /**
//   * @brief open
//   * @param path
//   * @return an instance to a project
//   * This is a sequence factory that creates projects
//   * according to the current backend.
//   */
//  static SequenceData * open(const std::string &path);
//  static SequenceData * create(const std::string & path);

public:

    virtual ~GeometryData() {}
    virtual bool isValid() const = 0;

    virtual Project project() const = 0;

	virtual void save() = 0;

    virtual Id id() const = 0;
    virtual std::string name() const = 0;



    virtual void addLandmarks(Vector3fArrayView pos) = 0;
    virtual void removeLandmarks(LandmarkList landmarks) = 0;

    virtual LandmarkList radiusSearch(const Vector3f & center, double radius) const = 0;

    virtual std::vector<LandmarkList> nearestNeighbourSearches(const std::vector<Vector3f> & center, int k) const = 0;

    virtual PartList parts() const = 0;
    virtual Part addPart(const std::string & name) = 0;
    operator Geometry() const;

    virtual void getPointCloud(Vector3fArray &cloud) = 0;
};

} // ns interface
} // ns hookers


#endif // BOSCHGT_PROJECT_SEQUENCEDATA_H
