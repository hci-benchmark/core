#ifndef BOSCHGT_PROJECT_FRAME_H
#define BOSCHGT_PROJECT_FRAME_H

#include <string>
#include <list>


#include <hookers/interface/NumericTypes.h>
#include <hookers/interface/Objects.h>

#include <hookers/interface/FrameData.h>


namespace hookers {
namespace interface {

class Sequence;
class TrackList;
class Track;


/**
 * @brief Smallest Part in a Sequence
 *
 * This class offers access to the frame views as
 * well as to additional information.
 *
 * One frame is the smallest unit of a sequence.
 * For each frame there are two views associated
 * as we are dealing with stereo setups.
 *
 * Most of the information (like camera pose etc..) are related to
 * the Base view. Some special information, like stereo translation,
 * are available specific to the stereo view.
 *
 * Other information is shared between the two views, like
 * the associated Tracks. In order to distringuish between
 * the two views, all functions exposing such shared data
 * are usgin a argument @em view that will by default be set to Base view.
 *
 * 
 */

class Frame
{

public:
	friend class FrameData;
    friend class Track;

    template <typename T, typename S>
    friend class AbstractSharedTypeList;

    friend struct std::hash<Frame>;

    enum class CameraPoseFrame {
        ObjectFrame, /** !<  interprets the given camera pose as a transformation from camera coordinates
                             into object coordinates. In other words: Translation equals camera position and
                             rotation equals looking direction of camera */
        CameraFrame  /** !< interprets a camera pose as transformation from object frame into camera frame.
                            This is exactly the inverse transformation of ObjectFrame */
    };


private:


    SharedDataPointer<FrameData, Frame> d;

public:
	typedef FrameData::Map Map;
	typedef FrameData::List List;
    typedef FrameData::View View;
    typedef FrameData::Size Size;

private:



	/** constructs a Frame object from a backend data type */
    Frame(const FrameData * data);

public:
	
	/** @brief creates invalid Frame object */
	Frame();
	
	/** number of frame in the sequence */
	int number() const;

    Object::Id id() const;
	
	/** returns the file system path of the view image file, relative to project file */
	std::string path(View view = View::Base) const;
	
	/** @returns true if this is a valid reference to a frame in the project */
    bool isValid() const;

	/** @brief returns the size (H,W) of views.
	 * @toDo currently size call getview each time you call it. it would be faster if it save size one time and call that vector only.
	 */
    Size size() const;
	
    /** Gets image for either base or stereo view of given frame.
     *  If provided array has not the right shape, it will be resized.
     */
    void getView(Image & image, const View & view = View::Base) const;
    //void getView(cv::Mat & image, const View & view = View::Base ) const;

	/**
	 * @brief sets the image for a given view
	 * @param image the image to set for the given view.
	 *
     * The frame size will derived from the image set by View::Base.
     * Setting different views that are larger than Base-View is not supported and will fail.
	 *
	 */
	void setView(const ImageView & image, const View  & view= View::Base);
    //void setView(const cv::Mat & image, const View  & view= View::Base);
		
    /** @returns the camera pose for given frame.
     *
     *  The vector is layed out as follows:
     *  @f$ (T, R) @f$ where @f$ T @f$ corresponds to translation of the camera pose
     *  and @f$ R @f$ corresponds to the rotational part expressed as a rotation vector.
     *  Where @f$ |R| @f$ is the rotation angle around the normalized rotation axis @f$ \frac{R}{|R|} @f$.
     *
     *  If the frame has no pose set, then the default value of @f$ (0, \dots, 0) @f$ will be returned.
     *
     * Depending on the argument @em frame the transformation is either wordl to camera or camera to world.
     *
     *
     */
    Vector6d pose(const CameraPoseFrame & frame = CameraPoseFrame::CameraFrame) const;

    /**
     * @see pose() for information about the layout of the pose
     *
     * If you want to invalidate the camera pose, set it to
     * @f$ (0, \dots, 0) @f$.
     */
    void setPose(const Vector6d & t, const CameraPoseFrame & frame = CameraPoseFrame::CameraFrame);

    Vector6d poseUncertainty(const CameraPoseFrame & frame = CameraPoseFrame::CameraFrame) const;
    void setPoseUncertainty(const Vector6d & pose ,
                            const CameraPoseFrame & frame = CameraPoseFrame::CameraFrame);


    /** returns the absolute time of the frame.
     *  If the frame has no time set, the backend should implement some number
     *  that still uniquely identifies the frame.
     *  @note operator<() will use the frame time as well.
     */
    double time() const;

    /**
     * Sets time for given frame.
     * @see time()
     */
    void setTime(const double & time);

    /**
     * @return camera intrinsics of the given frame
     *
     * the intrinsics are given by @f$ (f_x, f_y, c_x, c_y) @f$.
     *
     * If the intrinsics are not known, @f$ (0, \ldots, 0) @f$ will be returned.
     *
     */
    Vector4d intrinsics(const Frame::View& view = Frame::View::Stereo) const;

    /**
     * Set the camera intrinsics for base view on given frame.
     * Refer to intrinsics() for the vector entries.
     */
    void setIntrinsics(const Vector4d & intrinsics, const Frame::View& view = Frame::View::Stereo);


    /**
     * reads the uncertainty for intrinsics calibration
     * @see intrinsics() 
     */
    Vector4d intrinsicsUncertainty(const View &view = Frame::View::Stereo);

    /** sets the intrinsics calibration uncertainty.
     * @see instrinicsUncertainty()
     */
    void setIntrinsicsUncertainty(const Vector4d & intrinsics, const View &view = Frame::View::Stereo);



	/** returns the translation needed to get from base view to stereo view.
     *
     */
    Vector6d viewTransformation(const Frame::View & view = Frame::View::Stereo) const;

    /** sets the stereo translation from base to stereo view */
    void setViewTransformation(const Vector6d & translation, const View &view = Frame::View::Stereo);


    /** returns stereo translation uncertainty */
    Vector6d viewTransformationUncertainty(const Frame::View & view = Frame::View::Stereo) const;
    void setViewTransformationUncertainty(const Vector6d & uncert, const Frame::View & view = Frame::View::Stereo);


	/** 
	 * @brief accesses a map with given name
	 * 
	 * The goal of this "map" interface is a flexible way to store further information that were generated.
	 *
	 * You can request a map with this function. The provied argument map
	 * will be used to to store the requested view. If the size of the array
	 * does not match with the map size, it will be readjusted.
	 * 
	 * Maps is information that is mapped pixelwise, like depth. Therefore a map
	 * has dimensionality of three where the first index is the channel index.
	 * The last two are defined via Frame::size().
	 * Be aware of @ref hdf_doc_memoryOrder "memory order"!
	 * 
	 * If the name contains the char '/' it will be translated to groups in the project file.
	 */
    void getMap(const std::string & name, Map & map);
	
	
	/** @brief sets a map
	 *  If there is no record with given name, a new one will be created.
	 *  Old ones will be overwritten. Especially even if the first dimension is different.
	 *  By calling this function, this frame is flagged to beeing changed.
	 *  If you want to write the changes to disk, call Project::save().
	 * 
	 *  If the parameter map is a null-instance, the corresponding entry will be deleted.
	 * 
	 */
    void setMap(const std::string & name = std::string(), const Map & map = Map());
	

	/** @brief checks whether a map with given name exists */
	bool hasMap(const std::string & name);
	
	
	/** @brief accesses a list with given name
	 * Lists behave very similar to maps. The only difference is that the dimensionality is two.
	 * This storage format is meant for irregular information that is not closely related to pixel
	 * information.
	 */
	void getList(const std::string & name,List & list);
	
	/** @brief sets a list
	 *  @see setMap()
	 */
	void setList(const std::string & name, const List & list = List());
	
	/** @brief checks whether a list with given name exists
	 * @see hasMap()
	 */
	bool hasList(const std::string & name);
	
	
    /**
     *  Adds the given track to frame at given position.
     *  If the track is already linked to the frame,
     *  the position will be overwritten.
     */
    void linkTrack(const Track & t, const Vector2d & pos, const View & view = View::Base);

    /** unlinks the frame from given track */
    void unlinkTrack(const Track & t, const View & view = View::Base);

    /**
     * @returns a list of all tracks that are linked to this frame.
     *
     * If you provide a view id, then only the tracks registered with the ID are returned.
     */
    TrackList tracks(const View & view = View::All) const;

    /** @returns the position of given track in view */
    Vector2d trackPosition(const Track & t, const View & view = View::Base) const;
	
    /** @returns the sequence this frame belongs to */
    Sequence sequence() const;
	
	/** casts the frame to a string using the frame id */
	operator std::string () const;

    /** saves temporary changes */
    void save();

    void close();
	
    std::string getDataFilePath();

    /** comparison will be made against internal data pointer.
     * For this reason, this operation hence is very fast. */
	bool operator==(const Frame & other) const;

    /** @see time() */
	bool operator<(const Frame & other) const;
	
};

} // ns interface
} // ns hookers

namespace std {
  template <> struct hash<hookers::interface::Frame>
  {
    size_t operator()(const hookers::interface::Frame & x) const
    {
        return std::hash<hookers::interface::FrameData*>()(x.d.data());
    }
  };
}


#endif //  BOSCHGT_PROJECT_FRAME_H

#include "FrameList.h"
