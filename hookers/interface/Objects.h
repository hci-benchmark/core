#ifndef OBJECTS_H
#define OBJECTS_H

#include <sstream>
#include <string>
#include <random>
#include <iomanip>
#include <iomanip>
#include <vigra/tinyvector.hxx>
#include <arpa/inet.h>

namespace hookers {
namespace interface {

class Object
{

public:

    /** thin object to resemble something like a UUID */
    class Id {
    public:
        typedef vigra::TinyVector<vigra::UInt8, 16> IdType;
    private:
        vigra::TinyVector<vigra::UInt8, 16> m_id;

        static std::default_random_engine m_generator;

    public:

        bool valid() {
            return m_id.any();
        }

        const IdType & data() const {
            return m_id;
        }

        Id(const u_int32_t & project,
                  const u_int32_t & r1,
                  const u_int32_t & r2,
                  const u_int32_t & object) {

            *((u_int32_t*)&m_id[0])  = htonl(project);
            *((u_int32_t*)&m_id[4])  = htonl(r1);
            *((u_int32_t*)&m_id[8])  = htonl(r2);
            *((u_int32_t*)&m_id[12]) = htonl(object);
        }


        Id(const IdType & id = IdType(0)) : m_id(id) {}


        std::string toUuid() const {
            std::stringstream ids;
            ids << std::hex << std::setfill('0')
               << "{"
               << std::setw(2) << static_cast<unsigned>(m_id[0])
               << std::setw(2) << static_cast<unsigned>(m_id[1])
               << std::setw(2) << static_cast<unsigned>(m_id[2])
               << std::setw(2) << static_cast<unsigned>(m_id[3])
               << "-"
               << std::setw(2) << static_cast<unsigned>(m_id[4])
               << std::setw(2) << static_cast<unsigned>(m_id[5])
               << "-"
               << std::setw(2) << static_cast<unsigned>(m_id[6])
               << std::setw(2) << static_cast<unsigned>(m_id[7])
               << "-"
               << std::setw(2) << static_cast<unsigned>(m_id[8])
               << std::setw(2) << static_cast<unsigned>(m_id[9])
               << "-"
               << std::setw(2) << static_cast<unsigned>(m_id[10])
               << std::setw(2) << static_cast<unsigned>(m_id[11])
               << std::setw(2) << static_cast<unsigned>(m_id[12])
               << std::setw(2) << static_cast<unsigned>(m_id[13])
               << std::setw(2) << static_cast<unsigned>(m_id[14])
               << std::setw(2) << static_cast<unsigned>(m_id[15])
               << "}";

            return ids.str();
        }

        std::string toString() const {
            std::stringstream ids;
            ids << std::hex << std::setfill('0')
               << "{"
               << std::setw(2) << static_cast<unsigned>(m_id[0])
               << std::setw(2) << static_cast<unsigned>(m_id[1])
               << std::setw(2) << static_cast<unsigned>(m_id[2])
               << std::setw(2) << static_cast<unsigned>(m_id[3])
               << "-"
               << std::setw(2) << static_cast<unsigned>(m_id[4])
               << std::setw(2) << static_cast<unsigned>(m_id[5])
               << std::setw(2) << static_cast<unsigned>(m_id[6])
               << std::setw(2) << static_cast<unsigned>(m_id[7])
               << "-"
               << std::setw(2) << static_cast<unsigned>(m_id[8])
               << std::setw(2) << static_cast<unsigned>(m_id[9])
               << std::setw(2) << static_cast<unsigned>(m_id[10])
               << std::setw(2) << static_cast<unsigned>(m_id[11])
               << "-"
               << std::setw(2) << static_cast<unsigned>(m_id[12])
               << std::setw(2) << static_cast<unsigned>(m_id[13])
               << std::setw(2) << static_cast<unsigned>(m_id[14])
               << std::setw(2) << static_cast<unsigned>(m_id[15])
               << "}";

            return ids.str();
        }

        u_int32_t project() const {
            return ntohl(*((u_int32_t*)&m_id[0]));
        }

        u_int32_t r1() const {
            return ntohl(*((u_int32_t*)&m_id[4]));
        }

        u_int32_t r2() const {
            return ntohl(*((u_int32_t*)&m_id[8]));
        }

        u_int32_t object() const {
            return ntohl(*((u_int32_t*)&m_id[12]));
        }

        /** casts id to printable string */
        operator std::string() const {
            return this->toString();
        }

        bool operator==(const Id & other) {
            return this->m_id == other.m_id;
        }

        bool operator!=(const Id & other) {
            return !(*this == other);
        }

        /** creates a new random id */
        static Id create() {
            Id id;
            std::uniform_int_distribution<vigra::Int16> distribution(0,255);
            for(int i = 0; i < 16; i++)
                id.m_id[i] = distribution(m_generator);
            return id;
        }

    };

private:


public:


};


class StructuredObject  : public Object
{

};

class AtomicObject : public Object
{

};

} // ns interface
} // ns hookers

#endif // OBJECTS_H

