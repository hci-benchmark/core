#include "Track.h"
#include "TrackData.h"

#include <hookers/interface/Project.h>
#include <hookers/interface/Sequence.h>
#include <hookers/interface/PartList.h>

using namespace hookers;
using namespace hookers::interface;


const Vector2d Track::INVALID_FRAME_POS = Vector2d(-1, -1);

Track::Track(TrackData *data)
	: d(data)
{

}

Track::Track()
{
}

Object::Id Track::id() const {
    if(!d)
        return Object::Id();
    return d->id();
}

Sequence Track::sequence() const
{
    if(!d)
        return Sequence();
    return d->sequence();
}

Vector2d Track::posInFrame(const Frame &f, const Frame::View &view) const
{
    if(!d)
        return Track::INVALID_FRAME_POS;

    return f.trackPosition(*this, view);
}

Vector3d Track::position() const
{
	if(!d)
		return Vector3d();

	return d->position();
}

void Track::setPosition(const Vector3d &pos)
{
	if(!d)
		return;
    d->setPosition(pos);
}

Landmark Track::linkedLandmark(const Geometry &g) const
{
    if(!d)
        return Landmark();
    if(g.isValid())
        return d->linkedLandmark(g.d.data());
    return d->linkedLandmark(sequence().geometry().d.data());

}

bool Track::linkToLandmark(const Landmark &l)
{
    if(!d)
        return false;
    /* we can pass a direct pointer of ld because we still hold a reference to LandmarkData here
     * within this scope */
    return d->linkToLandmark(l.d.data());
}

void Track::unlinkLandmark(const Geometry & g)
{
    if(!d)
        return;
    if(g.isValid())
        d->unlinkLandmark(g.d.data());
    d->unlinkLandmark(sequence().geometry().d.data());
}

bool Track::isValid() const
{
	if(!d)
		return false;

    return d->isValid();
}

Project::Context Track::creationContext() const
{
    if(!d)
        return Project::NoContext;
    return d->creationContext();
}

Project::Context Track::modificationContext() const
{
    if(!d)
        return Project::NoContext;
    return d->modificationContext();

}

bool Track::hasLink(const Geometry &g) const {
    if(!d)
        return false;
    if(g.isValid())
        return d->hasLink(g.d.data());
    return d->hasLink(sequence().geometry().d.data());
}

TrackData::operator Track()
{
	return Track(this);
}



/* **TrackList implementations
 *
 *
 *
 */



void TrackList::getPositionsInFrame(Vector2fArray &pos,
                                    const Frame &frame,
                                    const Frame::View &view) const
{
    if(empty())
        return;

    /* Set up array such that all entries can be contained.
     * We are storing 2d positions per Track, so the resulting shape is
     * 2xN */

    Vector2ArrayShape shape;
    shape[0] = size();
    pos.reshape(shape);

    int i = 0;
    for(auto it = cbegin(); it != cend(); it++, i++) {
        pos(i) = frame.trackPosition(this->at(i), view);
    }
}

void TrackList::setPositionsInFrame(const Vector2fArrayView &pos,
                                    Frame &frame,
                                    const Frame::View &view)
{

    Vector2ArrayShape shape = pos.shape();

    /* check if the number of entries is equal
     * to the number of items in current list
     * and that the feature number is correct */
    if(shape[0] != int(size()))
        return;

    /* we still have to use iterators because std::list has no constant time
     * index access operator */
    int i = 0;
    for(auto it = begin(); it != end(); it++, i++) {

        frame.linkTrack(*it, pos(i), view);
    }

}

TrackList TrackList::tracksByFrame(const Frame & f) const {
    return (TrackSet::fromList(f.tracks()) & TrackSet::fromList(*this)).toList();
}

TrackList TrackList::tracksByLandmark(const Landmark &l) const
{
    TrackList filtered;
    filtered.reserve(this->size());

    for(auto t: *this) {
        if(t.linkedLandmark() != l)
            continue;

        filtered.push_back(t);
    }

    return filtered;
}

TrackList TrackList::tracksByCreationContext(const Project::Context &context)
{
    TrackList filtered;
    filtered.reserve(this->size());

    for(auto t: *this) {
        if(t.creationContext() != context)
            continue;

        filtered.push_back(t);
    }

    return filtered;

}

TrackList TrackList::annotated(const Geometry & g) const
{
    TrackList filtered;
    std::unordered_set<Geometry> geometries;
    if (g.isValid())
    {
        geometries.insert(g);
    }
    else
    {
        std::set<Sequence> sequences;
        for (auto it = this->begin(); it != this->end(); it++)
            sequences.insert((*it).sequence());
        for (auto it = sequences.begin(); it != sequences.end(); ++it)
            geometries.insert((*it).geometry());
    }
    for(auto it = this->begin(); it != this->end(); it++) 
    {
        for (auto git = geometries.begin(); git != geometries.end(); ++git)
        if ((*it).hasLink(*git))
        {
                filtered.push_back(*it);
                break;
        }
    }

    return filtered;

}

LandmarkList TrackList::landmarks() const
{

    TrackList annoted = this->annotated();
    LandmarkList landmarksList;

    for(Track & t : annoted) {
        landmarksList.push_back(t.linkedLandmark());
    }

    return landmarksList;
}

LandmarkList TrackList::unfilteredLandmarks() const
{

    LandmarkList landmarksList;
    landmarksList.reserve(this->size());

    for(const Track & t : (*this)) {
        landmarksList.push_back(t.linkedLandmark());
    }

    return landmarksList;

}

void TrackList::getPositions(Vector3fArray &pos) const
{
    if(empty())
        return;

    Vector3ArrayShape shape;
    shape[0] = size();

    pos.reshape(shape);

    int i = 0;
    for(auto it = this->cbegin(); it != this->cend(); it++, i++) {
        pos(i) = (*it).position();
    }

}

void TrackList::setPositions(const Vector3fArrayView &pos)
{

    if(empty())
        return;

    Vector3ArrayShape shape = pos.shape();

    /* check if the number of entries is equal
     * to the number of items in current list
     * and that the feature number is correct */
    if(shape[0] != int(size()))
        return;

    /* iterate over all items in list and set their corresponding features
     * from array view */
    int i = 0;
    for(auto it = begin(); it != end(); it++, i++) {
        (*it).setPosition(pos(i));
    }


}
