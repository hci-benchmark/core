#ifndef BOSCHGT_PROJECT_PARTLIST_H
#define BOSCHGT_PROJECT_PARTLIST_H

#include "AbstractObjectList.h"
#include "Part.h"

namespace hookers {
namespace interface {


/**
 * Abstract class for object lists that handle contigous
 * objects
 */
class PartList: public AbstractObjectList<Part>
{

public:
    /** searches for a sequence with given label.
     * @returns a reference to the sequence or an invalid sequence if not found
     */
    Part byLabel(const std::string & label);

};

} // ns interface
} // ns hookers

#endif // BOSCHGT_PROJECT_FRAMELIST_H
