#ifndef BOSCHGT_PROJECT_PARTDATA_H
#define BOSCHGT_PROJECT_PARTDATA_H

#include <string>
#include <memory>

#include "NumericTypes.h"
#include "SharedType.h"

#include "Landmark.h"

namespace hookers {
namespace interface {


class LandmarkList;
class Part;

class PartData : public SharedData {


public:
	virtual ~PartData() {}

	virtual void save() = 0;
	virtual void close() = 0;
	virtual std::string name() const = 0;
	virtual bool isValid() const = 0;
    virtual void addLandmark(const LandmarkData * where) = 0;
    virtual LandmarkList landmarks() const= 0;

	/**
	 * Casts to a part.
	 * We need this operator in order to keep the Part(PartData*) ctor private.
	 *
	 * You can use this conversation technique anywhere:
	 *
	 * @code
	 * PartDataDerived * pd =  new PartDataDerived();
	 * Part p = *pd;
	 * @endcode
	 */
	operator Part();

};

} // ns interface
} // ns hookers

#endif //BOSCHGT_PROJECT_PARTDATA_H
