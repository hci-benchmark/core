#include <iostream>

#include <hookers/interface/Sequence.h>
#include <hookers/interface/Project.h>
#include <hookers/interface/Track.h>

using namespace hookers::interface;

Sequence::Sequence(const SequenceData *data)
	: d(data)
{

}

Sequence::Sequence()
{

}

Sequence::~Sequence()
{

}


void Sequence::save()
{
	if(!d)
		return;
    d->save();
}

Project Sequence::project() const
{
    if(!d)
        return Project();
    return d->project();
}

Geometry Sequence::geometry() const
{
    GeometryList gs = project().geometries();
    if(gs.empty())
        return Geometry();
    return gs[0];
}

double Sequence::time() const
{
    if(!d)
        return 0.0;
    return d->time();
}

void Sequence::setTime(const double &time)
{
    if(!d)
        return;
    d->setTime(time);
}


Object::Id Sequence::id() const
{
    if(!d)
        return Object::Id();
    return d->id();
}


bool Sequence::isModified()
{

	if(!d)
		return false;

	return false;
}

std::string Sequence::name() const
{
	if(!d)
		return std::string();
	return d->name();
}

TrackList Sequence::tracks() const
{
	if(!d)
		return TrackList();
	return d->tracks();
}

Track Sequence::addTrack()
{
	if(!d)
		return Track();

    return d->addTrack();
}

void Sequence::removeTrack(const Track &t)
{

}

FrameList Sequence::frames() const
{
	if(!d)
		return FrameList();

	return d->frames();
}

Frame Sequence::addFrame()
{
	if(!d)
		return Frame();

	return d->addFrame();
}

bool Sequence::isValid() const
{
	if(!d)
		return false;
	return d->isValid();
}

bool Sequence::operator==(const Sequence & other) const
{
	return this->d == other.d;
}


bool Sequence::operator<(const Sequence & other) const
{
	return this->d < other.d;
}


SequenceData::operator Sequence() const
{
	return Sequence(this);
}


Sequence SequenceList::byLabel(const std::string &label) {

    /* iterate over all sequences in the list and try to find the given label */
    auto it = pointers().begin();
    auto end = pointers().end();
    for(; it != end; it++) {

        /* check if labels match.
         * only if this is true, make an frontend instance.
         * this avoids loading and deloading etc... */
        if((*it)->name() == label)
            return *(*it);

    }

    /* return a invalid sequence if none sequence with given label could be found */
    return Sequence();
}

