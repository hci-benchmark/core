#include "Part.h"
#include "PartList.h"
#include "PartData.h"

using namespace hookers::interface;

Part::Part(const PartData *data)
	: d(data)
{

}

Part::Part()
{

}

void Part::save()
{
	if(!d)
		return;
	d->save();
}

void Part::close()
{
	if(!d)
		return;
	d->close();
}

std::string Part::name() const
{
	if(!d)
		return std::string();
	return d->name();
}

void Part::addLandmark(const Landmark &l)
{
	if(!d)
        return;

    if(!l.isValid())
        return;

    d->addLandmark(l.d.data());
}

LandmarkList Part::landmarks() const
{
	if(!d)
		return LandmarkList();

    return d->landmarks();
}


bool Part::isValid() const
{
	if(!d)
		return false;
	return d->isValid();
}

PartData::operator Part ()
{
	return Part(this);
}

Part PartList::byLabel(const std::string &label) {

    /* iterate over all sequences in the list and try to find the given label */
    auto it = this->cbegin();
    for(; it != this->cend(); it++) {

        /* check if labels match */
        if((*it).name() == label)
            return *it;

    }

    /* return a invalid sequence if none sequence with given label could be found */
    return Part();
}

