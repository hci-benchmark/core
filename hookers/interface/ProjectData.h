#ifndef BOSCHGT_PROJECT_PROJECTDATA_H
#define BOSCHGT_PROJECT_PROJECTDATA_H

#include <string>
#include <memory>

#include "SharedType.h"
#include "Objects.h"

#include "Geometry.h"
#include "GeometryList.h"


#include "Part.h"
#include "PartList.h"

#include "Sequence.h"

namespace hookers {
namespace interface {


class Project;

class ProjectData : public SharedData, public StructuredObject
{
public:

private:

public:

	virtual ~ProjectData() {}

	virtual bool isValid() const = 0;
	virtual bool isModified() const = 0;

    virtual GeometryList geometries() const = 0;
    virtual Geometry addGeometry(const std::string & name) = 0;

    virtual SequenceList sequences() const = 0;
	virtual Sequence addSequence(const std::string & name) = 0;


	virtual void save() = 0;

    operator Project() const;



};

} // ns interface
} // ns hookers

#endif // BOSCHGT_PROJECT_PROJECTDATA_H

