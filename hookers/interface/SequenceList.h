#ifndef BOSCHGT_PROJECT_SEQUENCELIST_H
#define BOSCHGT_PROJECT_SEQUENCELIST_H

#include <vigra/tinyvector.hxx>
#include <vigra/multi_array.hxx>

#include <hookers/interface/AbstractObjectList.h>
#include <hookers/interface/SequenceData.h>
#include <hookers/interface/Sequence.h>

namespace hookers {
namespace interface {


/** 
 * Abstract class for object lists that handle contigous
 * objects
 */
class SequenceList: public AbstractSharedTypeList<SequenceData>
{
public:


public:

    SequenceList() {}
    SequenceList(const std::initializer_list<Sequence> & list)
        : AbstractSharedTypeList<SequenceData>(list) {}
    SequenceList(const AbstractSharedTypeList<SequenceData> & other)
        : AbstractSharedTypeList<SequenceData>(other) {}

    /** searches for a sequence with given label.
     * @returns a reference to the sequence or an invalid sequence if not found
     */
    Sequence byLabel(const std::string & label);
};

} // ns interface
} // ns hookers

#endif // BOSCHGT_PROJECT_SEQUENCELIST_H
