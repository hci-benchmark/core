#ifndef BOSCHGT_PROJECT_PART_H
#define BOSCHGT_PROJECT_PART_H

#include <memory>

#include <hookers/interface/NumericTypes.h>

#include "Landmark.h"
#include "PartData.h"


namespace hookers {
namespace interface {

class Sequence;
class LandmarkList;
class PartData;



/** @brief compound of fixed landmarks in absolute 3d-space
 * A Part is a subset of all 3d cloud points. It holds, for instance, all points belonging to a wall.
 * From here you can select dominant landmarks that will be tracked in the stereo setup.
 * These dominant landmarks can be linked to tracks.
 */
class Part
{

	friend class Sequence;
	friend class PartData;

public:

	
private:

	SharedDataPointer<PartData> d;

private:
	/**
	 * @warning NEVER ever use this function!
	 * This constructor is only publicaly exposed due to bad interface design. It is only meant for backend usage.
	 */
    Part(const PartData * data);

public:




	/** contructs an invalid Part */
	Part();


	/** saves all changes to part to file */
	void save();

	/**
	 * Closes the reference.
	 * References will be invalid afterwards.
	 */
	void close();

	/** returns the name of the part */
	std::string name() const;

	/** adds a new Landmark to this part
	 * @returns a reference to newly created Landmark
	 */
    void addLandmark(const Landmark &l);

	/** @returns the list of Landmarks */
	LandmarkList landmarks() const;
 
	/**
	 * The associated 3d mesh should be a ply-File
	 * containing solid faces that visualize the part
	 * in 3d space. 
	 * @returns associated 3d mesh path for part
	 */
	std::string meshPath() const;

	/** 
	 * @brief sets mesh path
	 * @see meshPath()
	 */
	void setMeshPath(const std::string & path);
	
	bool isValid() const;

	/** @brief compare two parts beeing equal */
	bool operator== (const Part & other) const;


};

} // ns interface
} // ns hookers

//#include "PartList.h"


#endif // BOSCHGT_PROJECT_PART_H
