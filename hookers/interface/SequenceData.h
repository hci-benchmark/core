#ifndef BOSCHGT_PROJECT_SEQUENCEDATA_H
#define BOSCHGT_PROJECT_SEQUENCEDATA_H

#include <string>
#include <memory>


#include "SharedType.h"
#include "Objects.h"

namespace hookers {
namespace interface {



class Sequence;
class Frame;
class FrameList;
class Track;
class Project;
class TrackList;
class TrackData;

class SequenceData : public SharedData, public StructuredObject
{
public:
    typedef Sequence Frontend;

private:
//  /**
//   * @brief open
//   * @param path
//   * @return an instance to a project
//   * This is a sequence factory that creates projects
//   * according to the current backend.
//   */
//  static SequenceData * open(const std::string &path);
//  static SequenceData * create(const std::string & path);

    std::atomic_int m_externalUsage;

public:

    inline SequenceData() {
        m_externalUsage = 0;
    }

	virtual ~SequenceData() {}
    virtual bool isValid() const = 0;

    virtual Project project() const = 0;

	virtual void save() = 0;

    virtual Id id() const = 0;
    virtual std::string name() const = 0;

    virtual double time() const = 0;
    virtual void setTime(const double & time) = 0;


    virtual FrameList frames() const = 0;
	virtual Frame addFrame() = 0;

    virtual TrackList tracks() const = 0;
	virtual Track addTrack() = 0;
    virtual void removeTrack(TrackData * td) = 0;

    operator Sequence() const;

    template<class T> void ref();
    template<class T> void deref();

    virtual void load() = 0;
    virtual void unload() = 0;



};

template<>
inline void SequenceData::ref<Sequence>()
{
    if(!m_externalUsage.fetch_add(1)) {
        this->load();
    }
}

template<>
inline void SequenceData::deref<Sequence>()
{
    /* if the frame usage counter drops to zero,
     * we can safely remove any required caches and clear
     * lists associated with frames */
    if(m_externalUsage.fetch_sub(1) == 1) {
        this->unload();
    }
}

template<> inline void SequenceData::ref<void>() {}
template<> inline void SequenceData::deref<void>() {}

} // ns interface
} // ns hookers

#endif // BOSCHGT_PROJECT_SEQUENCEDATA_H
