#ifndef BOSCHGT_PROJECT_SEQUENCE_H
#define BOSCHGT_PROJECT_SEQUENCE_H

#include <string>
#include <list>

#include "Frame.h"

#include "AbstractObjectList.h"
#include "SequenceData.h"

namespace hookers {
namespace interface {

class Project;
class Geometry;
class Track;

/**
 * @brief Containment for a list of (sequential) frames and meta data.
 *
 * This class is used to manage sequence wise data such as frames,
 * tracks etc.
 *
 * @see Frame, Track
 *
 */
class Sequence {

	friend class SequenceData;

    template <typename T, typename S>
    friend class AbstractSharedTypeList;

public:
	
	friend class Part;
	friend class Track;
	friend class Frame;



private:

    SharedDataPointer<SequenceData, Sequence> d;

    Sequence(const SequenceData * data);

public:
	
	
	/**
	 * @brief creates invalid project instances
	 */
	Sequence();
	
	
	/**
	 * @brief Destructor
	 */
	~Sequence();
	
	

	/**
	 * @brief save project 
	 * saves any still temporary changes to file
	 */
	void save();

    /** @returns the project this sequence belongs to */
    Project project() const;


    /** returns the default associated geometry */
    Geometry geometry() const;


    /** @name Meta Information */
    /** @{ */

    /**
     * @brief time
     * @return  the time of the sequence as unix timestamp.
     *          if value is not set, then 0.0 will be returned.
     */
    double time() const;

    /**
     * @brief sets sequence time
     * @param time time of sequence in unix time stamp
     * @see time()
     */
    void setTime(const double & time);

    /** @} */


    /** @name Status Getter */
    /** @{ */


    Object::Id id() const;

    /** @returns true if this is a valid reference to a sequence */
    bool isValid() const;

	/** 
	 * @returns true if project has been modified, false otherwise
	 */
	bool isModified();

    /** name of the sequence */
    std::string name() const;

    /** @note not implemented */
    void setName(const std::string & name);

    /** @} */
	


    /** @name Track Management */
    /** @{ */
	
	/** @returns a list of all tracks in this sequence */
    TrackList tracks() const;

	/** adds a new track to sequence and returns reference.
	 *  You can then modify the track to fit your needs */
	Track addTrack();

	/** removes the track and all linkages from the sequence.
     *  The reference will be invalid afterwards.
     *  @note not implemented */
	void removeTrack(const Track & t);


    /** @} */



    /** @name Frame Management */
    /** @{ */

	/**
     * @returns a list of all frames in creation order
	 */
    FrameList frames() const;

    /**
     * @brief adds a frame to the sequence
     * @returns a valid Frame if successfull, an invalid otherwise
     */
	Frame addFrame();

    /** removes the given frame from sequence.
     *  @note not implemented */
    void removeFrame(const Frame & f);

    /** @} */
	
	

    /** @name Operators
     *  @{ */
	
	
    /**
     * @brief checks if both instances are the same
     *
     * Two Sequence instances are equal if they reference the
     * same storage object.
     *
     * @code
     * //a valid sequence reference
     * Sequence seq1 = ...;
     *
     * // shares the object, no copy!
     * Sequence seq2 = seq2;
     *
     * seq1 == seq2; // true
     *
     * @endcode
     */

	bool operator==(const Sequence & other) const;

    /**
     * @brief needed for maps etc.
     *
     * The order is defined by some internal means which
     * could be easily computed. It is not of real use for external
     * applications.
     */
	bool operator<(const Sequence & other) const;

    /** @} */
};

} // ns interface
} // ns hookers

#include "SequenceList.h"

#endif // BOSCHGT_PROJECT_SEQUENCE_H
