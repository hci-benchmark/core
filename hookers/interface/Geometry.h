#ifndef HOOKERS_PROJECT_GEOMETRY_H
#define HOOKERS_PROJECT_GEOMETRY_H

#include <string>
#include <list>

#include <hookers/interface/Part.h>
#include <hookers/interface/Landmark.h>


#include "GeometryData.h"
#include "GeometryList.h"

namespace hookers {
namespace interface {


class Project;

/**
 * @brief Containment for a list of (sequential) frames and meta data.
 *
 * This class is used to manage sequence wise data such as frames,
 * tracks etc.
 *
 * @see Frame, Track
 *
 */
class Geometry {

    friend class GeometryData;

    template <typename T, typename S>
    friend class AbstractSharedTypeList;

public:
	
	friend class Part;
	friend class Track;
	friend class Frame;



private:

    SharedDataPointer<GeometryData> d;

    Geometry(const GeometryData * data);

public:
	
	
	/**
	 * @brief creates invalid project instances
	 */
    Geometry();
	
	
	/**
	 * @brief Destructor
	 */
    ~Geometry();
	
    /** @returns the project this sequence belongs to */
    Project project() const;


    /** @name Status Getter */
    /** @{ */



    /**
     * @brief add a bunch of points to the cloud */
    void addLandmarks(Vector3fArrayView pos);

    /**
     * @brief removes landmarks from geometry object
     * @param landmarks list of landmarks that should be deleted
     *
     * @note removing these objects is instant. Any frontent view
     *       will reflect this directly.
     */
    void removeLandmarks(LandmarkList landmarks);

    /**
     * @brief searches for points inside a given radius
     */
    LandmarkList radiusSearch(const Vector3f & center, double radius = 0.0);

    /**
     * @brief searches for nearest neighbours
     * @param center the center around the nearest neighbours should be searched
     * @param N how many neighbours
     * @return a list of points fullfilling the criterions
     */
    LandmarkList nearestNeighbourSearch(const Vector3f & center, int k = 1);
    std::vector<LandmarkList> nearestNeighbourSearches(const std::vector<Vector3f> & center, int k=1) const;

    PartList parts() const;
    Part addPart(const std::string & name);

    inline void getPointCloud(Vector3fArray & cloud) {
        if(!d)
            return;
        d->getPointCloud(cloud);
    }

    Object::Id id() const;

    /** @returns true if this is a valid reference to a sequence */
    bool isValid() const;

	/** 
	 * @returns true if project has been modified, false otherwise
	 */
    bool isModified() const;

    /** name of the sequence */
    std::string name() const;

    /** @note not implemented */
    void setName(const std::string & name);

    /** @} */
	

    /** @name Operators
     *  @{ */
	
	
    /**
     * @brief checks if both instances are the same
     *
     * Two Sequence instances are equal if they reference the
     * same storage object.
     *
     */

    bool operator==(const Geometry & other) const;

    /**
     * @brief needed for maps etc.
     *
     * The order is defined by some internal means which
     * could be easily computed. It is not of real use for external
     * applications.
     */
    bool operator<(const Geometry & other) const;

    int hash() const {
        return d;
    }
    /** @} */
};

} // ns interface
} // ns hookers


namespace std {
  template <> struct hash<hookers::interface::Geometry>
  {
    size_t operator()(const hookers::interface::Geometry & x) const
    {
        return x.hash();
    }
  };
}

#include <hookers/interface/SequenceList.h>

#endif // BOSCHGT_PROJECT_SEQUENCE_H
