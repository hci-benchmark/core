#ifndef SHAREDTYPE_H
#define SHAREDTYPE_H

#include <atomic>
#include <memory>

namespace hookers {
namespace interface {


/**
 * @brief The SharedData class
 * @warning have a look into Section Pitfalls
 *
 * A shared class has the purpose to automaticly delete itself when it is not used anymore.
 * This is realized by means of an internal reference counter.
 *
 * This shared object framework implements some features that are not possible
 * with std::shared_ptr and std::weak_ptr alone:
 *
 * You can pass derived SharedData types safely around by only using a pointer.
 * It is not required to always pass the same shared_ptr around, which in the other case
 * would lead to double freed corruptions. The following code snippet should
 * give a better understanding:
 *
 * @code
 * DerivedData * d = new DerivedData();
 * SharedDataPointer<DerivedData> sp1 = d;
 *
 * DerivedData *d2 = sp1->data();
 * SharedDataPointer<DerivedData> sp2 = d2;
 *
 * sp.reset();
 * @endcode
 *
 * In this example the instance of DerivedData would exist, even after resetting sp1.
 *
 * In addition to that, the existence of an instance can be monitored with
 * a WeakSharedDataPointer. If such a SharedData class goes out of scope,
 * all WeakSharedDataPointer will know about this.
 *
 * If you need a more fine-grained reference counting, then have a look into
 * SharedDataPointer on how to archieve this. The basic idea is to implement templated
 * functions ref() and deref() on the derived ShareData class.
 *
 *
 * Pitfalls
 * ========
 *
 * This implementation assumes that the used Type T is derived from SharedData.
 * It further more requires T to be static down-castable for performance sake.
 * So avoid multi inheritance as this will very likely lead to undefineable behaviour.
 *
 */
class SharedData {

public:
    mutable std::atomic_int __ref;
    mutable std::shared_ptr<SharedData> * __ptr;

private:
	 SharedData & operator=(SharedData & other);
     SharedData(const SharedData & other) {}

public:

     SharedData() : __ptr(new std::shared_ptr<SharedData>(this, [](SharedData*d){
         std::shared_ptr<SharedData> * ptr = d->__ptr;
         delete d;
         delete ptr;
    }))
    {
        __ref = 0;
    }

    virtual ~SharedData() {}


    inline bool _ref() { return static_cast<bool>(__ref.fetch_add(1)+1);  }
    inline bool _deref() { return static_cast<bool>(__ref.fetch_sub(1)-1); }

    /** default implementation for specialized reference counting */
    template<class S=void, class C=void>
    inline void ref(C* caller = nullptr) {}

    /** default implementation for specialized reference counting */
    template<class S=void, class C=void>
    inline void deref(C * caller = nullptr) {}


public:



};

/**
 * SharedDataPointer
 *
 *  If you need fine-grained reference counting, on a derived shared type,
 *  you can specify the Template-Paramter S
 *  in order to set the "source" type of the reference.
 *
 *  @code
 *  SharedDataPointer<OtherData, MyClassName> p;
 *  @code
 *
 *  Now, whenever p gains or looses a reference,
 *  OtherData::ref<MyClassName>() and OtherData::ref<MyClassName>()
 *  will be called. By providing specialized member functions for ref/deref,
 *  you can count the references of arbitrary "sources".
 *
 *  You have to implement two templated member functions , ref() and deref(),
 *  on your derived SharedData class, that need to have the following signatures:
 *
 *  @code
 *  class OtherData {
 *      int counterA;
 *
 *      void template<class T> ref();
 *      void template<class T> deref();
 *  }
 *
 *  template<>
 *  inline void OtherData::ref<MyClassName>() {
 *
 *      if(!counterA)
 *        loadData();
 *
 *      counterA++;
 *  }
 *
 *  template<>
 *  inline void OtherData::deref<MyClassName>() {
 *
 *      counterA--;
 *
 *      if(!counterA)
 *        doCleanUp();
 *  }
 * @endcode
 *
 * As this is completely solved by templates, no overhead by virtual functions
 * etc... will be induced.
 *
 */
template<class T, typename S=void, typename C=void>
class SharedDataPointer {

private:

    T * d;
public:

    inline T & operator*() { return *d; }
    inline const T & operator*() const { return *d; }

    inline T * operator->() { return d; }
    inline const T * operator->() const { return d; }

    inline T * data() const { return d; }
    inline const T * constData() const { return d; }

    inline operator bool () const { return d != 0; }
    inline bool operator!() const { return !d; }
    inline bool operator==(const SharedDataPointer<T,S,C> &other) const { return d == other.d; }
    inline bool operator!=(const SharedDataPointer<T,S,C> &other) const { return d != other.d; }
    inline bool operator==(const T *ptr) const { return d == ptr; }
    inline bool operator!=(const T *ptr) const { return d != ptr; }

    inline bool operator<(const SharedDataPointer<T,S> &other) const { return d < other.d; }

    inline SharedDataPointer<T,S,C> & operator=(const SharedDataPointer<T,S,C> &o) {
        if (o.d != d) {
            if (o.d)
				ref(o.d);

            T *old = d;
            d = o.d;
            deref(old);
        }
        return *this;
    }

    inline SharedDataPointer &operator=(T *o) {
        if (o != d) {

            ref(o);

            T *old = d;
            d = o;

            deref(old);
        }

        return *this;
    }

    inline SharedDataPointer() { d = 0; }
    inline ~SharedDataPointer() { deref(d); }

    inline SharedDataPointer(const T * data) : d(const_cast<T*>(data)) { ref(d); }
    inline SharedDataPointer(const SharedDataPointer<T,S,C> & other) : d(other.d) {ref(d);}

    inline SharedDataPointer(const std::shared_ptr<SharedData> & ptr) {
        d = static_cast<T*>(ptr.get());
        ref(d);
    }

    inline SharedDataPointer(const std::weak_ptr<SharedData> & wptr)
        : d(0)
    {

        std::shared_ptr<SharedData> ptr = wptr.lock();
        if(!ptr)
            return;
        d = static_cast<T*>(ptr.get());
        ref(d);
    }




    inline void reset() {
        deref(d);
        d = 0;
    }

    static void ref(T * d) {
        if(!d)
            return;

        if(d) d->_ref();

        if(typeid(S) != typeid(void)) {
            d->template ref<S>();
        }
    }

    static void deref(T * d) {

        if(!d)
            return;

        if(typeid(S) != typeid(void)) {
            d->template deref<S>();
        }


        if(!d->_deref()) {
            /* delete the internal shared reference such that
             *
             * our object will go out of scope */
            d->__ptr->reset();
        }
    }
};

template <class T, class S=void, class C=void>
class WeakSharedDataPointer
{
    std::weak_ptr<SharedData> d;
public:


    SharedDataPointer<T> lock() const
    {
        return SharedDataPointer<T>(d);
    }

    WeakSharedDataPointer(const SharedDataPointer<T,S,C> & sptr) {
        d = *(sptr.data()->__ptr);
    }

    /** @warning assumes the object ptr exists.
     *  dont' use this function ever if you have given away
     *  ownership */
    WeakSharedDataPointer(T * ptr) {
        d = *(ptr->__ptr);
    }

    WeakSharedDataPointer() {}

    inline bool expired() const { return d.expired(); }

    inline operator bool () const { return !expired(); }
    inline bool operator!() const { return expired(); }

};

} // ns interface
} // ns hookers


#endif // SHAREDTYPE_H
