#include <cassert>

#include <sstream>

#include <hookers/interface/Project.h>

#include "ProjectDataHDF.h"
#include "SequenceDataHDF.h"
#include "GeometryDataHDF.h"
#include "FrameDataHDF.h"
#include "TrackDataHDF.h"

using namespace hookers::hdf5;
using namespace hookers::interface;

const std::string PROJECT_SEQUENCE_TIME_ATTR("time");

SequenceDataHDF::SequenceDataHDF(const ProjectDataHDF *project, int id, const std::string &name)
    : p(project), m_id(id), m_valid(false), m_label(name), m_time(0.0), m_modified(false), m_cfileIsOpen(false)
{
    assert(id >= 0);

    /* set the ref counters to zero */
    m_cacheUsage = 0;
    m_trackUsage = 0;
    m_frameUsage = 0;

    m_trackInsertionHint = 0;
    m_frameInsertionHint = 0;
}

SequenceDataHDF::SequenceDataHDF()
    : p(0), m_valid(false)
{

}

SequenceDataHDF::~SequenceDataHDF()
{
    //std::cout << "SequenceDataHDF::~SequenceDataHDF()" << std::endl;
	this->save();
}


bool SequenceDataHDF::isValid() const
{
    return m_valid;
}

Project SequenceDataHDF::project() const
{
    return *p;
}

template<unsigned int N>
using Shape = typename vigra::MultiArrayShape<N>::type;

void SequenceDataHDF::create()
{

//    SequenceCacheObject::Ptr sco = d();
    SequenceDataHDF * sco = this;


    this->setModified();

    /* we are creating a new sequence. so make sure that any old sequence file
     * will be overwritten */
    sco->m_sfile.open(this->sequenceFilePath(), hdf5::File::OpenMode::New);
    /* create new unique id and save it along with the project id */
    m_uuid = Id::create();

    const std::string sequencePathProject = "/project/sequences/" + idNumberToString(m_id);
    const std::string sequencePathSequence = "/sequence";

    /* Get the project-HDF's sequence group.
     * DON'T MESS this with the sequence hdf file groups !! */
    HDFGroup pSequencesGroup = p->m_root.group("project/sequences");
    if(!pSequencesGroup.contains(idNumberToString(m_id))) {


        HDFGroup pSequenceGroup = pSequencesGroup.mkdir(idNumberToString(m_id));
        pSequenceGroup.setAttribute("label", m_label);

        /* write out the uuid of the sequence into the project file */
        IdAttributeType idData(1);
        idData(0) = m_uuid.data();
        p->m_pfile.writeAttribute(sequencePathProject, "uuid", idData);

        p->m_pfile.flush();
    }


    /* here we are going to create our cache structure as we would
     * expect it when loaded from a file */
    {

        m_geometriesHeader = GeometriesHeader(1,1);

        m_trackHeader = TrackHeader(0);
        m_trackAnnotations = TrackAnnotations(Shape<3>(1, 0, 1));
        m_trackPositions = TrackPositions(0);

        const int frameInitSize = 0;
        m_frameHeader = FrameHeader(frameInitSize);
        m_frameTime = FrameTime(1, frameInitSize);

        /** @todo fix this constant here */
        /* we need to initialize this with 2 views, in order to not crash... */
        int frameViewInitSize = 2;
        m_frameViewHeader = FrameViewHeader(frameViewInitSize, frameInitSize);
        m_frameIntrinsics = FrameIntrinsics(frameViewInitSize, frameInitSize);
        m_frameIntrinsicsUncertainty = FrameIntrinsics(frameViewInitSize, frameInitSize);
        m_frameViewTransformation = FrameStereoTranslations(frameViewInitSize, frameInitSize);
        m_frameViewTransformationUncertainty = FrameStereoTranslations(frameViewInitSize, frameInitSize);


        m_framePose = FramePose(frameInitSize,1);
        m_framePoseUncertainty = FramePose(frameInitSize, 1);


        /* create mandatory entries */
        HDFGroup root(sco->m_sfile.getGroupHandle("/"));
        root.mkdir("sequence");
        HDFGroup sequence = root.group("sequence");



        /* initialize the geometry registry */
        HDFGroup geometries = sequence.mkdir("geometries");
        {

            /* registry header */
            {
                typedef Shape<2> Shape;
                Shape max(H5S_UNLIMITED);
                Shape shape(1);
                Shape chunking(1);
                chunking[0] = 8;
                chunking[1] = GEOMETRY_CHUNK_SIZE;
                m_sfile.createDataset<hdf5::UInt32, 2>("/sequence/geometries/geometries",
                                                           shape, 0, chunking, max,
                                                           0 /* high compression */);
            }
        }

        /* create mandatory track related tables */
        sequence.mkdir("tracks");
        {

            enum { D = 0, T, G };

            HDFGroup gt = geometries.mkdir("tracks");
            /* registry header */
            {


                typedef TrackAnnotations::difference_type Shape;
                typedef TrackAnnotations::value_type Type;
                Shape max(H5S_UNLIMITED);
                max[T] = TRACK_MAX_NUMBER;

                Shape shape(1);
                shape[D] = hdf5::TypeTraits<TrackAnnotations::value_type>::numberOfBands();
                shape[T] = 0;
                shape[G] = 1;

                Shape chunking(1);
                chunking[D] = hdf5::TypeTraits<TrackAnnotations::value_type>::numberOfBands();
                chunking[T] = TRACK_CHUNK_SIZE;
                chunking[G] = GEOMETRY_CHUNK_SIZE;

                m_sfile.createDataset<Type, 3>(gt.path()+"/annotations",
                                               shape, 0, chunking, max,
                                               0);
            }

            /* create trackheader */
            {

                typedef TrackHeader::value_type::value_type Type;
                auto shape = m_trackHeader.expandElements(0).shape();

                auto max = shape;
                max[1] = TRACK_MAX_NUMBER;

                auto chunking(shape);
                chunking[1] = TRACK_CHUNK_SIZE;
				

                const int dim = shape.static_size;

                m_sfile.createDataset<Type, dim>("/sequence/tracks/tracks",
                                                 shape, 0, chunking, max,
                                                 0);
            }

            /* create trackheader */
            {

                typedef TrackPositions::value_type::value_type Type;
                auto shape = m_trackPositions.expandElements(0).shape();

                auto max = shape;
                max[1] = TRACK_MAX_NUMBER;

                auto chunking(shape);
                chunking[1] = TRACK_CHUNK_SIZE;


				const int dim = shape.static_size;

                m_sfile.createDataset<Type, dim>("/sequence/tracks/positions",
                                                 shape, 0, chunking, max,
                                                 0);
            }


        }


        /* create all mandatory frame related tables */
        HDFGroup frames = sequence.mkdir("frames");
        {
            enum { D = 0, F, G };

            /* create the pose table. this is wrt to a geometry */
            geometries.mkdir("frames");
            {

                typedef FramePose::value_type::value_type Type;
                auto shape = m_framePose.expandElements(0).shape();

                auto max(shape);
                max[F] = OBJECT_MAX_NUMBER;
                max[G] = H5S_UNLIMITED;

                auto chunking(shape);
                chunking[F] = FRAME_CHUNK_SIZE;
                chunking[G] = GEOMETRY_CHUNK_SIZE;


				const int dim = shape.static_size;

                m_sfile.createDataset<Type, dim>("/sequence/geometries/frames/pose",
                                                           shape, 0, chunking, max,
                                                           0);
                m_sfile.createDataset<Type, dim>("/sequence/geometries/frames/uncertainty",
                                                           shape, 0, chunking, max,
                                                           0);

            }


            /* create frame header table */
            {

                typedef FrameHeader::value_type::value_type Type;
                auto shape = m_frameHeader.expandElements(0).shape();

                auto max = shape;
                max[1] = OBJECT_MAX_NUMBER;

                auto chunking(shape);
                chunking[1] = FRAME_CHUNK_SIZE;


				const int dim = shape.static_size;

                m_sfile.createDataset<Type, dim>("/sequence/frames/frames",
                                                 shape, 0, chunking, max,
                                                 0);
            }

            /* create frame time table */
            {

                typedef FrameTime::value_type Type;
                auto shape = m_frameTime.shape();

                auto max = shape;
                max[1] = OBJECT_MAX_NUMBER;

                auto chunking(shape);
                chunking[1] = FRAME_CHUNK_SIZE;


                const int dim = shape.static_size;

                m_sfile.createDataset<Type, dim>("/sequence/frames/time",
                                                 shape, 0, chunking, max,
                                                 0);
            }


            frames.mkdir("views");

            /* create intrinsics tables */
            {

                typedef FrameViewHeaderDimensions FVHD;
                typedef FrameViewHeader::value_type::value_type Type;
                auto shape = m_frameViewHeader.expandElements(0).shape();
                shape[FVHD::V] = 0;

                auto max = shape;
                max[FVHD::V] = FRAME_VIEWS_MAX_NUMBER;
                max[FVHD::N] = OBJECT_MAX_NUMBER;

                auto chunking(shape);
                chunking[FVHD::D] = 2;
                chunking[FVHD::V] = 2;
                chunking[FVHD::N] = FRAME_CHUNK_SIZE;


				const int dim = shape.static_size;

                m_sfile.createDataset<Type, dim>("/sequence/frames/views/views",
                                                 shape, 0, chunking, max,
                                                 0);
            }


            /* create intrinsics tables */
            {

                typedef FrameIntrinsics::value_type::value_type Type;
                auto shape = m_frameIntrinsics.expandElements(0).shape();

                auto max = shape;
                max[1] = FRAME_VIEWS_MAX_NUMBER;
                max[2] = OBJECT_MAX_NUMBER;

                auto chunking(shape);
                chunking[1] = 2;
                chunking[2] = FRAME_CHUNK_SIZE;


				const int dim = shape.static_size;

                m_sfile.createDataset<Type, dim>("/sequence/frames/views/intrinsics",
                                                 shape, 0, chunking, max,
                                                 0);
                m_sfile.createDataset<Type, dim>("/sequence/frames/views/intrinsicsUncertainty",
                                                 shape, 0, chunking, max,
                                                 0);

            }

            /* create stereo translation tables */
            {

                typedef FrameStereoTranslations::value_type::value_type Type;
                auto shape = m_frameViewTransformation.expandElements(0).shape();

                auto max = shape;
                max[1] = FRAME_VIEWS_MAX_NUMBER;
                max[2] = OBJECT_MAX_NUMBER;

                auto chunking(shape);
                chunking[1] = 2;
                chunking[2] = FRAME_CHUNK_SIZE;

				const int dim = shape.static_size;

                m_sfile.createDataset<Type, dim>("/sequence/frames/views/transformation",
                                                 shape, 0, chunking, max,
                                                 0);
                m_sfile.createDataset<Type, dim>("/sequence/frames/views/transformationUncertainty",
                                                 shape, 0, chunking, max,
                                                 0);

            }



            /* create the sequence frame view table.
             * It is extensible, so we have to create it once */
            {
                typedef SequenceDataHDF::FrameViewDimension D;

                /* initialize the maximum dataset dimensions such that
                 * channels could be up to 3, and views can be 2. */
                FrameViewShape max(H5S_UNLIMITED);
                max[D::N] = OBJECT_MAX_NUMBER;
                max[D::C] = 7;
                max[D::V] = FRAME_VIEWS_MAX_NUMBER;
                max[D::H] = 0xFFFF;
                max[D::W] = 0xFFFF;

                FrameViewShape chunking(1);
                chunking[D::N] = 1;
                chunking[D::V] = 1;
                chunking[D::H] = 360;
                chunking[D::W] = 512;

                FrameViewShape shape(0);
                shape[D::C] = 1;
                shape[D::V] = 0;

                m_sfile.createDataset<hdf5::UInt8, D::MAX>("/sequence/frames/views/data",
                                                           shape, 0, chunking, max,
                                                           4);

            }
            /* create the track registry tables */
            {
                typedef SequenceDataHDF::FrameTrackDimensions D;
                typedef SequenceDataHDF::FrameTrackHeaderFields THF;
                typedef SequenceDataHDF::FrameTrackPositionFields TPF;

                typedef SequenceDataHDF::FrameTrackPositions::value_type::value_type PositionType;
                typedef SequenceDataHDF::FrameTrackHeader::value_type::value_type HeaderType;


                SequenceDataHDF::FrameTrackShape maxHeader(H5S_UNLIMITED);
                maxHeader[D::T] = 0xFFFF;

                SequenceDataHDF::FrameTrackShape maxPositions(H5S_UNLIMITED);
                maxPositions[D::T] = 0xFFFF;
                maxPositions[D::D] = TPF::MAX;

                SequenceDataHDF::FrameTrackShape chunkingHeader(1);
                chunkingHeader[D::N] = 32;
                chunkingHeader[D::T] = 2048;
                chunkingHeader[D::D] = 2;


                SequenceDataHDF::FrameTrackShape chunkingPositions(1);
                chunkingPositions[D::N] = 32;
                chunkingPositions[D::T] = 2048;
                chunkingPositions[D::D] = 2;

                SequenceDataHDF::FrameTrackShape shapeHeader(0);
                shapeHeader[D::D] = THF::MAX;

                SequenceDataHDF::FrameTrackShape shapePositions(0);
                shapePositions[D::D] = TPF::MAX;

                m_sfile.createDataset<HeaderType, D::MAX>("/sequence/frames/views/tracks",
                                                          shapeHeader, -1,
                                                          chunkingHeader, maxHeader,
                                                           0);
                m_sfile.createDataset<PositionType, D::MAX>("/sequence/frames/views/tracksPosition",
                                                            shapePositions, 0.0,
                                                            chunkingPositions, maxPositions,
                                                            0);

            }
        }



        IdAttributeType idData(1);
        idData(0) = m_uuid.data();
        sco->m_sfile.writeAttribute("/sequence", "uuid", idData);
        idData(0) = p->m_id.data();
        sco->m_sfile.writeAttribute("/sequence", "projectUuid", idData);


        /* save interface revision */
        sco->m_sfile.writeAttribute("/sequence", "revision", vigra::UInt32(Project::REVISION));

        sco->m_sfile.flush();

        /* finally make sure we are saving the tables for tracks etc.. */
        this->saveCache();

        /* and clean up things, for instance close the hdf file again */
        this->dropCache();
    }

    m_valid = true;

}

void SequenceDataHDF::load()
{


    /* check if the file exist, before we open it with hdf api.
     * This should avoid file creation, because we would open it
     * for read/write */
    try {
        std::ifstream testFile;
        testFile.open(this->sequenceFilePath());
        if(!testFile.is_open())
            throw std::runtime_error("File does not exist.");
    } catch (...) {
        return;
    }


    /* opening a sequence must be possible from within many processes
     * without interfering. We ensure this by opening the sequence read-only.
     * Besides this, we use RAII here if anything goes wrong. */
    hookers::hdf5::File file;
    file.open(this->sequenceFilePath(), hdf5::File::OpenMode::ReadOnly);

    /* now do some checks whether the sequence file is a valid one */
    HDFGroup root(file.getGroupHandle("/"));

	if(!root.contains("sequence"))
		return;

    /* get the sequence id and the project id and check if they match and are valid */
    {
        IdAttributeType idData(1);
        idData(0) = 0;
        try {
            file.readAttribute("/sequence", "uuid", idData);
        } catch(...) {
            std::stringstream err;
            err << "Sequence's (" << m_label << ") id could not be found in hdf file." << std::endl;
            throw std::runtime_error(err.str());
        }

        m_uuid = Id(idData(0));

        if(!m_uuid.valid()) {
            std::stringstream err;
            err << "Sequence's (" << m_label << ") id is not valid: " << m_uuid.toString() << std::endl;
            throw std::runtime_error(err.str());
        }

        idData(0) = 0;
        try {
            file.readAttribute("/sequence", "projectUuid", idData);
        } catch(...) {
            std::stringstream err;
            err << "Sequence's (" << m_label << ") projectUuid could not be found in hdf file." << std::endl;
            throw std::runtime_error(err.str());
        }

        Id projectId = Id(idData(0));
        if(!projectId.valid() || !(projectId == p->m_id)) {
            std::stringstream err;
            err << "Sequence's (" << m_label << ") projectUuid doesn't match with current projct:' " << projectId.toString() << std::endl;
            throw std::runtime_error(err.str());
        }

        vigra::UInt32 revision = 0;
        try {
            file.readAttribute("/sequence", "revision", revision);
        } catch (...) {
            std::stringstream err;
            err << "Sequence's (" << m_label << ") revision could not be found in hdf file." << std::endl;
            throw std::runtime_error(err.str());
        }

        if(revision != Project::REVISION) {
            std::stringstream err;
            err << "Sequence's (" << m_label << ") revision (" << revision << ")"
                << "is not equal to Project::REVISION(" << Project::REVISION << ") Upgrade it!"
                << std::endl;
            throw std::runtime_error(err.str());
        }

        /* get reference to sequence group */
        HDFGroup sequence = root.group("sequence");


        for(auto s: {"tracks", "frames"}) {
            if(!sequence.contains("tracks"))
                throw std::runtime_error(std::string("Sequence doesn't contain group for ") + s);
        }
    }

    /* yes, we are available */
    m_valid = true;
}

void SequenceDataHDF::loadMeta()
{
    std::string sequencePath= "/project/sequences/"+idNumberToString(m_id);
    HDFGroup pSequenceGroup = p->m_root.group(sequencePath);
    pSequenceGroup.getAttribute("label", m_label);

    m_time = 0.0;
    if(p->m_pfile.existsAttribute(sequencePath, PROJECT_SEQUENCE_TIME_ATTR))
        p->m_pfile.readAttribute(sequencePath, PROJECT_SEQUENCE_TIME_ATTR, m_time);
}

void SequenceDataHDF::loadCache()
{

    SequenceDataHDF * sco = this;

    try {

        /* check if the file exist, before we open it with hdf api.
         * This should avoid file creation, because we would open it
         * for read/write */
        {
            std::ifstream testFile;
            testFile.open(this->sequenceFilePath());
            if(!testFile.is_open())
                throw std::runtime_error("File does not exist.");
        }

        /* open the hdf file */
        sco->m_sfile.open(this->sequenceFilePath(), hdf5::File::OpenMode::ReadWrite);

        HDFGroup root(sco->m_sfile.getGroupHandle("/"));

        if(!root.contains("sequence"))
            return;

        /* get reference to sequence group */
        HDFGroup sequence = root.group("sequence");
        HDFGroup tracks = sequence.group("tracks");
        HDFGroup frames = sequence.group("frames");
        HDFGroup geometries = sequence.group("geometries");

        /* read geometry tables */
        sco->m_sfile.readAndResize(geometries.path()+"/geometries", sco->m_geometriesHeader);

        /* read track tables */
        sco->m_sfile.readAndResize(tracks.path()+"/tracks", sco->m_trackHeader);
        sco->m_sfile.readAndResize(tracks.path()+"/positions", sco->m_trackPositions);
        sco->m_sfile.readAndResize(geometries.path()+"/tracks/annotations", sco->m_trackAnnotations);

        /* read frame tables */
        sco->m_sfile.readAndResize(frames.path()+"/frames", sco->m_frameHeader);
        sco->m_sfile.readAndResize(frames.path()+"/time", sco->m_frameTime);

        sco->m_sfile.readAndResize(frames.path()+"/views/views", sco->m_frameViewHeader);
        sco->m_sfile.readAndResize(frames.path()+"/views/intrinsics", sco->m_frameIntrinsics);
        sco->m_sfile.readAndResize(frames.path()+"/views/intrinsicsUncertainty", sco->m_frameIntrinsicsUncertainty);
        sco->m_sfile.readAndResize(frames.path()+"/views/transformation", sco->m_frameViewTransformation);
        sco->m_sfile.readAndResize(frames.path()+"/views/transformationUncertainty", sco->m_frameViewTransformationUncertainty);
        sco->m_sfile.readAndResize(geometries.path()+"/frames/pose", sco->m_framePose);
        sco->m_sfile.readAndResize(geometries.path()+"/frames/uncertainty", sco->m_framePoseUncertainty);



    } catch (std::exception & e) {
        std::cerr << "[SequenceDataHDF] Loading Track and Header tables failed:" << e.what() << std::endl;
        m_sfile.close();
    }

}

void SequenceDataHDF::saveCache()
{

    if(!m_modified)
        return;

    SequenceDataHDF * sco = this;


    /* try to get a valid group handle and save data */
    HDFGroup geometriesGroup = HDFGroup(sco->m_sfile.getGroupHandle("/sequence/geometries"));
    {
        sco->m_sfile.write(geometriesGroup.path()+"/geometries", Shape<2>(), m_geometriesHeader);
    }


    /* try to get a valid group handle and save data */
    HDFGroup tracksGroup = HDFGroup(sco->m_sfile.getGroupHandle("/sequence/tracks"));
    if(tracksGroup.isValid()) {

        /* write datasets to hdf file */
        auto offset = sco->m_trackHeader.expandElements(0).shape();
        offset = 0;
        sco->m_sfile.write(tracksGroup.path()+"/tracks",
                           offset, sco->m_trackHeader.expandElements(0));
        sco->m_sfile.write(tracksGroup.path()+"/positions",
                           offset, sco->m_trackPositions.expandElements(0));
        sco->m_sfile.write(geometriesGroup.path()+"/tracks/annotations",
                           Shape<3>(), m_trackAnnotations);
    }

    /* try to get a valid group handle and save data */
    HDFGroup frameGroup = HDFGroup(sco->m_sfile.getGroupHandle("/sequence/frames"));
    if(frameGroup.isValid()) {
        auto offset =  sco->m_frameHeader.expandElements(0).shape();
        offset = 0;
        auto viewOffset = sco->m_frameIntrinsics.expandElements(0).shape();
        viewOffset = 0;

        /* write datasets to hdf file */
        sco->m_sfile.write(frameGroup.path()+"/frames",
                          offset, sco->m_frameHeader.expandElements(0));
        sco->m_sfile.write(frameGroup.path()+"/time",
                          offset, sco->m_frameTime);

        sco->m_sfile.write(frameGroup.path()+"/views/views",
                          viewOffset, sco->m_frameViewHeader.expandElements(0));
        sco->m_sfile.write(frameGroup.path()+"/views/intrinsics",
                          viewOffset, sco->m_frameIntrinsics.expandElements(0));
        sco->m_sfile.write(frameGroup.path()+"/views/intrinsicsUncertainty",
                          viewOffset,sco->m_frameIntrinsicsUncertainty.expandElements(0));
        sco->m_sfile.write(frameGroup.path()+"/views/transformation",
                          viewOffset, sco->m_frameViewTransformation.expandElements(0));
        sco->m_sfile.write(frameGroup.path()+"/views/transformationUncertainty",
                          viewOffset, sco->m_frameViewTransformationUncertainty.expandElements(0));

        /* these entries need little different treatment, as they belong to the
         * geometry table */
        auto poseOffset = m_framePose.expandElements(0).shape();
        poseOffset = 0;
        sco->m_sfile.write(geometriesGroup.path()+"/frames/pose",
                           poseOffset, m_framePose.expandElements(0));
        sco->m_sfile.write(geometriesGroup.path()+"/frames/uncertainty",
                           poseOffset, m_framePoseUncertainty.expandElements(0));


        /* finally make sure, that these datasets are in sync with the shape to all the
         * other */
        if(m_frameHeader.size()) {

            /* we are doing some trick here: resizing by writing an empty array with the
             * correct dimension set */
            {
                FrameViewShape fvs(0);
                fvs[FrameViewDimension::N] = m_frameHeader.size();
                sco->m_sfile.write(frameGroup.path()+"/views/data",
                                   FrameViewShape(), FrameViewView(fvs,nullptr));
            }
            {
                FrameTrackShape shape(0);
                FrameTrackShape offset(0);
                shape[FrameTrackDimensions::N] = m_frameHeader.size();
                vigra::MultiArrayView<FrameTrackDimensions::MAX, FrameTrackHeader::value_type::value_type>
                        fthv(shape, nullptr);
                vigra::MultiArrayView<FrameTrackDimensions::MAX, FrameTrackPositions::value_type::value_type>
                        ftpv(shape, nullptr);

                sco->m_sfile.write(frameGroup.path()+"/views/tracks",
                                   offset, fthv);
                sco->m_sfile.write(frameGroup.path()+"/views/tracksPosition",
                                   offset, ftpv);
            }
        }
    }

    /* make sure we flush file buffers */
    sco->m_sfile.flush();

}

void SequenceDataHDF::dropCache()
{

    /* clear track tables */
    m_trackHeader = TrackHeader();
    m_trackAnnotations = TrackAnnotations();
    m_trackPositions = TrackPositions();

    m_frameHeader = FrameHeader();
    m_frameTime = FrameTime();
    m_framePose = FramePose();
    m_framePoseUncertainty = FramePose();
    m_frameIntrinsics = FrameIntrinsics();
    m_frameIntrinsicsUncertainty = FrameIntrinsics();
    m_frameViewTransformation = FrameStereoTranslations();
    m_frameViewTransformationUncertainty = FrameStereoTranslations();
    m_framePointer.clear();

    m_sfile.close();
}

Object::Id SequenceDataHDF::id() const
{
    return Object::Id(p->m_id.project(), 0x0, 0x0, m_id);
}

std::string SequenceDataHDF::fileName() const
{
    return p->m_name + "_"+ idNumberToString(m_id);
}

std::string SequenceDataHDF::sequenceFilePath() const
{
    std::string filePath = p->m_path;

    if(!filePath.empty())
        filePath += "/";
    filePath += this->fileName() + ".hs";

    return filePath;

}

std::string SequenceDataHDF::dataFilePath() const
{


    std::string comfilePath = Project::dataBase();

	if (!comfilePath.empty())
		comfilePath += "/";
    comfilePath += this->fileName() + ".hd";

	return comfilePath;

}

void SequenceDataHDF::loadData()
{
    if (this->m_cfileIsOpen)
        return;

    this->m_cfile.open(this->dataFilePath(), hdf5::File::OpenMode::ReadWrite);
    this->m_cfileIsOpen = true;
}

Track SequenceDataHDF::getTrack(int id) const
{
    std::list<int> ids;
    ids.push_back(id);

    return getTracks(ids)[0];
}

TrackList SequenceDataHDF::getTracks(const std::list<int> & ids) const
{
    TrackList list;

    if(ids.empty()) {
        return TrackList();
    }

    for(auto & i: ids) {

        SharedDataPointer<TrackDataHDF> d = m_trackPointer[i].lock();
        if(!d) {
            d = new TrackDataHDF(this, i);

            /* note that d currently has not been shared so far.
             * so we are still owner of the object */
            m_trackPointer[i] = WeakSharedDataPointer<TrackDataHDF>(d);
        }

        /* now the list entry is first shared owner of TrackDataHDF */
        list.push_back(*d);

    }

    return list;
}

Frame SequenceDataHDF::getFrame(int id)
{
    SharedDataPointer<FrameDataHDF> d = this->getFramePtr(id);
    if(!d)
        return Frame();
    return *(d.data());
}

SharedDataPointer<FrameDataHDF> SequenceDataHDF::getFramePtr(int id) const
{
    SharedDataPointer<FrameDataHDF> d = m_framePointer[id].lock();

    if(!d) {
        d = new FrameDataHDF(this, id);
        d->load();
        m_framePointer[id] = WeakSharedDataPointer<FrameDataHDF>(d);
    }

    return d;

}

unsigned SequenceDataHDF::geometryRegistry(const GeometryDataHDF *gd) const
{
    if(!gd)
        return 0;

    for(int i = 1; i < m_geometriesHeader.height(); i++) {
        if((int)m_geometriesHeader(0, i) == gd->m_id) {
            return i;
        }
    }

    return 0;
}

unsigned SequenceDataHDF::geometryRegistry(GeometryDataHDF *gd)
{

    unsigned gid = 0;

    /* use the static function to check first, whether
     * the given geometry is already registered */
    if(gd)
        gid = geometryRegistry(static_cast<const GeometryDataHDF*>(gd));


    /* if so, then we are done here */
    if(gid > 0) {
        return gid;
    }

    /* seems like the given geometry was not registered */
    resizeArray(m_geometriesHeader, 1, 1, 0);
    gid = m_geometriesHeader.height()-1;

    static_assert(GeometriesHeader::actual_dimension == 2,
                  "Dimensions don't match.");
    m_geometriesHeader(0, gid) = gd->m_id;

    /* resize the arrays that refer to geometries */
    static_assert(FramePose::actual_dimension == 2,
                  "Dimensions don;t match.");
    resizeArray(m_framePose, 1, 1);
    resizeArray(m_framePoseUncertainty, 1, 1);

    static_assert(TrackAnnotations::actual_dimension == 3,
                  "Dimensions must match.");
    resizeArray(m_trackAnnotations, 2, 1);

    this->setModified();

    return gid;

}

//SequenceCacheObject::Ptr SequenceDataHDF::d()
//{


//	/* try to aquire a lock on the existing cache object */
//	SequenceCacheObject::Ptr sco = m_d.lock();
//	if(sco)
//		return sco;

//	/* lock the mutex to make sure only one new instance of
//	 * the cache object will be created at a time
//	 * and generally to be thread-safe here. */
//	std::lock_guard<std::mutex> lock(p->m_cacheMutex);

//	/* Check if, while we have waited on the locked mutex, a cache instance already has been created. */
//	sco = m_d.lock();
//	if(sco)
//		return sco;

////	if(p->m_cachedSequences.find(*this) != p->m_cachedSequences.cend())
////		return p->m_cachedSequences[*this];

//	/* create a new cache object */
//	sco = std::make_shared<SequenceCacheObject>();
//    p->m_cachedSequencesQueue.push_back(sco);

//    /* We have created a new sequence cache object. In order to make sure
//     * we don't have too many of them in memory, we delete the
//     * oldest one.
//     *
//     * Note that we only need to destroy the shared_ptr in order to delete
//     * the SCO.
//     */
//    if(p->m_cachedSequencesQueue.size() > 5) {
//        p->m_cachedSequencesQueue.pop_front();
//    }

//    /* This line needs to be called before loadCache() which
//	 * would lead into a recursive mutex locking (dead-lock) otherwise. */
//	m_d = sco;

//    this->loadCache();
//	return sco;
//}

void SequenceDataHDF::save()
{

//	SequenceCacheObject::Ptr sco = m_d.lock();
//	/* if there is no data available, then we don't have to save anything */
//	if(!sco)
//		return;

    if(!m_modified)
        return;

	/* Get the project-HDF's sequence group. */
    HDFGroup pSequencesGroup = p->m_root.group("project/sequences");
    HDFGroup pSequenceGroup = pSequencesGroup.group(idNumberToString(m_id));
    std::string pSequencePath = pSequenceGroup.path();
    pSequenceGroup.setAttribute("label", m_label);

    /* write the time attribute on project file */
    p->m_pfile.writeAttribute(pSequencePath, PROJECT_SEQUENCE_TIME_ATTR, m_time);
    p->m_pfile.flush();


    /* only if the cache is in use, we can save something */
    if(m_cacheUsage)
        this->saveCache();

}

std::string SequenceDataHDF::name() const
{
    return m_label;
}

double SequenceDataHDF::time() const
{
    return m_time;
}

void SequenceDataHDF::setTime(const double &time)
{

    if(m_time == time)
        return;

    m_time = time;
    m_modified = true;
}

FrameList SequenceDataHDF::frames() const
{

    /* make sure the frame headers are available */
    CacheLock cacheLock(this);

    int frameSize = this->m_frameHeader.size();

    FrameList list;
    list.reserve(frameSize);
    m_framePointer.reserve(frameSize);

    for(int i = 0; i < frameSize; i++) {
        if(this->m_frameHeader(i)[0] == 0)
            continue;
        SharedDataPointer<FrameDataHDF> d = this->getFramePtr(i);

        if(!d)
            continue;

        list.push_back(d.data());
    }

	return list;

}

Frame SequenceDataHDF::addFrame()
{
    using namespace vigra::multi_math;

    CacheLock cacheLock(this);
    SequenceDataHDF * sco = this;

    int frameSize = sco->m_frameHeader.size();

    /* use the insertion hint to speed up insertions of tracks */
    for(int i = m_frameInsertionHint; i <= m_frameHeader.size(); i++) {


        /* there are no free slots, so enlarge the table */
        if(i == frameSize) {

            SequenceDataHDF::resizeArray(sco->m_frameHeader, 0, FRAME_CHUNK_SIZE);

            SequenceDataHDF::resizeArray(sco->m_frameViewHeader, 1, FRAME_CHUNK_SIZE);
            SequenceDataHDF::resizeArray(sco->m_frameIntrinsics, 1, FRAME_CHUNK_SIZE);
            SequenceDataHDF::resizeArray(sco->m_frameIntrinsicsUncertainty, 1, FRAME_CHUNK_SIZE);
            SequenceDataHDF::resizeArray(sco->m_frameViewTransformation, 1, FRAME_CHUNK_SIZE);
            SequenceDataHDF::resizeArray(sco->m_frameViewTransformationUncertainty, 1, FRAME_CHUNK_SIZE);

            /* these arrays belong to the geomerty group */
            SequenceDataHDF::resizeArray(sco->m_framePose, 0, FRAME_CHUNK_SIZE);
            SequenceDataHDF::resizeArray(sco->m_framePoseUncertainty, 0, FRAME_CHUNK_SIZE);


            /* this array needs to be expanded in second dimension, because
             * it doesn't use TinyVector as value type but a scalar. */
            SequenceDataHDF::resizeArray(sco->m_frameTime, 1, FRAME_CHUNK_SIZE);

        }

        if(sco->m_frameHeader(i)[0] != 0)
            continue;

        sco->m_frameHeader(i) = 0;
        sco->m_frameHeader(i)[0] = 1;
        sco->m_frameHeader(i)[SequenceDataHDF::FrameHeaderFields::Views] = 2;
        sco->m_frameTime(0,i) = 0.0;

//        sco->m_frameIntrinsics(i) = Vector4d(0);
//        sco->m_frameIntrinsicsUncertainty(i) = Vector4d(0);
//        sco->m_frameStereoTranslation(i) = Vector6d(0);
//        sco->m_frameStereoTranslationUncertainty(i) = Vector6d(0);

        FrameDataHDF * fd = new FrameDataHDF(this, i);
        fd->create();

        m_framePointer.insert(std::pair<int, WeakSharedDataPointer<FrameDataHDF> >(i, fd));

        sco->m_modified = true;
        sco->m_frameInsertionHint = i;

        return *fd;

    }

    return Frame();
}

TrackList SequenceDataHDF::tracks() const
{

    CacheLock cacheLock(this);

    TrackList list;

    const SequenceDataHDF * sco = this;

    int trackSize = sco->m_trackHeader.size();

    for(int i = 0; i < trackSize; i++) {
        if(sco->m_trackHeader(i)[0] == Track::INVALID)
            continue;
        list.push_back(this->getTrack(i));
    }

    return list;
}

Track SequenceDataHDF::addTrack()
{

    CacheLock cacheLock(this);

    SequenceDataHDF * sco = this;

    int trackSize = sco->m_trackHeader.size();


    /* use the insertion hint to speed up insertions of tracks */
    for(int i = m_trackInsertionHint; i <= trackSize; i++) {

        /* if i equals to trackSize, no more space is left in dataset,
         * so grow it */
        if(i == trackSize ) {
            SequenceDataHDF::resizeArray(sco->m_trackHeader, 0, TRACK_CHUNK_SIZE);
            SequenceDataHDF::resizeArray(sco->m_trackPositions, 0, TRACK_CHUNK_SIZE);

            /* expand this table in the second dimension,
             * because we are not using TinyVector as value_type */
            SequenceDataHDF::resizeArray(sco->m_trackAnnotations, 1, TRACK_CHUNK_SIZE);
        }

        if(sco->m_trackHeader(i)[0] != Track::INVALID)
			continue;

        sco->m_trackHeader(i)[TrackHeaderFields::Type] = Track::VALID;
        sco->m_trackHeader(i)[TrackHeaderFields::CreationContext] = Project::context();
        sco->m_trackPositions(i)[0] = 0.0;
        sco->m_trackPositions(i)[1] = 0.0;
        sco->m_trackPositions(i)[2] = 0.0;

        sco->m_modified = true;
        m_trackInsertionHint = i;


        return this->getTrack(i);

	}

    /* something went wrong here... */
	return Track();

}

void SequenceDataHDF::removeTrack(TrackData *td)
{
    TrackDataHDF * tdh = static_cast<TrackDataHDF*>(td);

    /* set the track data to invalid.
     * note, because it's an shared object, this
     * is true for all references */
    m_trackHeader(tdh->m_id)[0] = Track::VALID;
}
