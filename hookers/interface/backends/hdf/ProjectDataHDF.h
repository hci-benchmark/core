#ifndef PROJECTDATAHDF_H
#define PROJECTDATAHDF_H

#include <unordered_map>
#include <list>
#include <memory>
#include <map>
#include <mutex>

#include <hookers/interface/ProjectData.h>
#include <vigra/hdf5impex.hxx>

#include "HDFFile.h"
#include "HDFGroup.h"

#include "PartDataHDF.h"


namespace hookers {
namespace interface {

class SequenceDataHDF;
class GeometryDataHDF;

/**
 * @page hdf
 *
 *
 * ********
 * ********
 * @section hdf_project_project /project/
 *
 * @anchor hdf_project_project__uuid
 * @h5attr{uuid,v16uint8} unique project id
 * @h5attr{revision,uint} revision of c++ interface
 *
 * This is the root group of a project.
 *
 * The project filename should be
 *
 * `{project}.bgt`
 *
 * where \e {project} is the name of the project.
 *
 * If the revision number of the c++ interface doesn't match
 * with stored revision, then the project instance should be invalid.
 *
 * ********
 * ********
 * @subsection hdf_project_sequences /project/sequences/
 *
 * This group contains all registered sequences.
 *
 * ********
 * @subsection hdf_project_sequences_sequence /project/sequences/nnnnn/
 *
 * @anchor hdf_project_sequences_sequence__uuid
 * @anchor hdf_project_sequences_sequence__label
 * @anchor hdf_project_sequences_sequence__timestamp
 * @h5attr{uuid,v16uint8}
 * @h5attr{label,string} sequence label (eg. 0_0074)
 * @h5oattr{time,double} unix timestamp of sequence
 * @h5oattr{tags,string} space separated tags for the sequence \n
 *                       like "cloudy", "sunny", ...
 *
 * Each sequence has a 5 digit zero padded number in the sequence.
 * This number should be used to locate the sequence file.
 * See @ref hdf_sequences_sequence on how to construct the filename.
 *
 * ********
 * ********
 * @subsection hdf_project_geometry /project/geometries/
 * @h5attr{label,string} label of the part (eg. a house name)
 *
 * This group contains a list of @link Part Parts @endlink.
 * It is only allowed to have @f$ 2^{16} @f$ number of parts.
 * Each part is only allowed to have @f$ 2^{32} @f$ number of landmarks.
 *
 * ********
 * @subsection hdf_project_geometries_geometry /project/geometries/hhhhhhhh/
 *
 *
 * ********

 */

class ProjectDataHDF : public ProjectData
{

public:
	friend class TrackDataHDF;
	friend class PartDataHDF;
	friend class SequenceDataHDF;
    friend class GeometryDataHDF;
    friend class FrameDataHDF;


    enum FileType {
        PROJECT_FILE = 0, /*!< project file  */
        SEQUENCE_FILE, /*!< sequence file */
        DATA_FILE /*!< auxiliary sequence data file */
    };


private:
    typedef vigra::MultiArray<1,Id::IdType> IdAttributeType;

private:

    Id m_id;

	bool m_valid;
	bool m_modified;

    hookers::hdf5::File m_pfile;
	std::string m_path;
	std::string m_name;


    mutable std::unordered_map<int, WeakSharedDataPointer<GeometryDataHDF> > m_geometryPointer;
    mutable std::unordered_map<int, WeakSharedDataPointer<SequenceDataHDF> > m_sequencePointer;

    hookers::hdf5::HDFGroup m_root;


private:
	ProjectDataHDF();

	/**
	 * Handle any loading of objects from project file
	 */
	void loadFromFile();

	/** loads all parts from file */
	void loadParts();

	/** loads all available sequences from file. */
	void loadSequences();

	/**
	 * creates all mandatory file structures in a new project file
	 */
	void createFileStructure();

	/** @brief returns a sequence instance for given id
	 * Before creating a new instance, this function checks the cache
	 * for any existing instance.
	 */
	Sequence getSequence(int id);
    Geometry getGeometry(int id) const;
	Part getPart(int id);
    SharedDataPointer<SequenceDataHDF> getSequencePtr(int id) const;
    SharedDataPointer<GeometryDataHDF> getGeometryPtr(int id) const;

public:

	~ProjectDataHDF();

	/**
	 * @returns the absolute path to the sequence file
	 */
	std::string fileName() const;

	/**
	 * @brief project folder in filesystem
	 * @return a string pointing to the project path
	 */
	std::string projectPath() const;


	bool isValid() const;
	bool isModified() const;

    GeometryList geometries() const;
    Geometry addGeometry(const std::string & name);

    SequenceList sequences() const;
	Sequence addSequence(const std::string & name);


	void close();
	void save();


public:

	/**
	 * @brief creates a new project file on given path
	 * @param path the path to new file name
	 * @return an instance of the newly created project
	 *
	 * If the file does exist, it will be deleted beforehand and a new file will be created.
	 */
	static Project create(const std::string & path, const std::string & name);

	/**
	 * @brief opens a project file
	 * @param path path to project file
	 * @return a instance of the opened project
	 *
	 * If the project does not exist, a invalid instance will be returned.
	 *
	 */
	static Project open(const std::string & path, const std::string & name);

};

} // ns interface
} // ns hookers

#endif // PROJECTDATAHDF_H
