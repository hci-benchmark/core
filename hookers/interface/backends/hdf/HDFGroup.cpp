#include "HDFGroup.h"
#include "HDFAttribute.h"

using namespace hookers::hdf5;

const int HDF_MAX_NAME_LENGTH = 255;

HDFGroup::HDFGroup(vigra::HDF5Handle handle)
	: m_handle(HDFHandle(handle, &H5Gclose))
{
}

HDFGroup::HDFGroup(HDFHandle handle)
	: m_handle(handle)
{

}

HDFGroup::HDFGroup()
{
}

HDFGroup::~HDFGroup()
{

}

std::string HDFGroup::path()
{
	ssize_t len;
	char group_name[HDF_MAX_NAME_LENGTH];

	/* retrieve the name of the current group */
	len = H5Iget_name (m_handle, group_name, HDF_MAX_NAME_LENGTH);

	return std::string(group_name, len);
}

std::string HDFGroup::name()
{
	std::string path = this->path();

	int pos = path.rfind("/");

	if(pos < 0)
		return std::string();

	return path.substr(pos+1, path.size()-pos);

}

std::list<std::string> HDFGroup::entries()
{
	hid_t gid = m_handle;

	std::list<std::string> objects;

	if(!isValid())
		return objects;

	hsize_t nobj;
	herr_t err = H5Gget_num_objs(gid, &nobj);

	if(err > 0)
		return objects;

	char obj_name[HDF_MAX_NAME_LENGTH];

	for (unsigned int i = 0; i < nobj; i++) {
//		/* get the type of the current entry in group and continue f type is not a group */
//		//otype =  H5Gget_objtype_by_idx(gid, (size_t)i );

		ssize_t len = H5Lget_name_by_idx(gid, ".", H5_INDEX_NAME, H5_ITER_INC, i,
										 obj_name, HDF_MAX_NAME_LENGTH, H5P_DEFAULT);

		/* if the name is as long as the field value, then we discard this one */
		if(len >= HDF_MAX_NAME_LENGTH)
			continue;

		objects.push_back(std::string(obj_name, len));
	}


	return objects;
}

bool HDFGroup::contains(const std::string &name)
{

	if(!isValid())
		return false;

	/* get the entries */
	std::list<std::string> names = this->entries();

	/* and check if the list contains the name */
	if(std::find(names.begin(), names.end(), name) == names.end()) {
		return false;
	}

	return true;
}

HDFGroup HDFGroup::mkdir(const std::string &name)
{
	hid_t grp = H5Gcreate(m_handle, name.data(), H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);

	if(grp < 0)
			return HDFGroup();

	HDFHandle grp_handle(grp, &H5Gclose);

	return HDFGroup(grp_handle);
}

bool HDFGroup::isValid()
{
	return m_handle.isValid();
}

HDFGroup HDFGroup::group(const std::string &name) const
{

	hid_t child = H5Gopen(m_handle, name.data(), H5P_DEFAULT);

	/* if the id is invalid, return an invalid group object */
	if(child < 0)
			return HDFGroup();

	vigra::HDF5Handle child_handle(child, &H5Gclose,
							  "Error message.");


	return HDFGroup(child_handle);
}

void HDFGroup::setAttribute(const std::string &attribute, const std::string &value)
{
	HDFAttribute attr(m_handle, attribute);
	attr.setAttribute(value);
}

void HDFGroup::getAttribute(const std::string &attribute, std::string &value)
{
	HDFAttribute attr(m_handle, attribute);
	attr.getAttribute(value);
}
