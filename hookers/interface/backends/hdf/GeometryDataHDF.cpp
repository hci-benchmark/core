#include <cassert>
#include <sstream>

#include <vigra/unsupervised_decomposition.hxx>
#include <vigra/linear_algebra.hxx>


#include <hookers/interface/Project.h>

#include "ProjectDataHDF.h"
#include "GeometryDataHDF.h"
#include "LandmarkDataHDF.h"
#include "GeometryChunkDataHDF.h"
#include "Vigra.h"

using namespace hookers;
using namespace hookers::hdf5;
using namespace hookers::interface;

const std::string PROJECT_GEOMETRY_BASE_PATH("/project/geometries/");
const std::string GEOMETRY_BASE_PATH("/geometry");

typedef Vector3f vec3;

GeometryDataHDF::GeometryDataHDF(const ProjectDataHDF *project, int id, const std::string &name)
    : p(project), m_id(id), m_valid(false), m_label(name), m_modified(false)
{
    assert(id >= 0);

    /* set the ref counters to zero */
    m_cacheUsage = 0;

}

GeometryDataHDF::GeometryDataHDF()
	: p(0), m_valid(false)
{

}

GeometryDataHDF::~GeometryDataHDF()
{
//	std::cout << "SequenceDataHDF::~SequenceDataHDF()" << std::endl;
	this->save();
}


bool GeometryDataHDF::isValid() const
{
    return m_valid;
}

Project GeometryDataHDF::project() const
{
    return *p;
}


void GeometryDataHDF::create()
{

//    SequenceCacheObject::Ptr sco = d();
    GeometryDataHDF * sco = this;


    /* we are creating a new sequence. so make sure that any old sequence file
     * will be overwritten */
    sco->m_gfile.open(this->filePath(), hookers::hdf5::File::OpenMode::New);

    /* create new unique id and save it along with the project id */
    m_uuid = Id::create();
    void getPointCloud(vigra::MultiArray<1, Vector3f> &cloud);

    const std::string pGeometryPath = PROJECT_GEOMETRY_BASE_PATH + idNumberToString(m_id);

    /* Get the project-HDF's sequence group.
     * DON'T MESS this with the sequence hdf file groups !! */
    HDFGroup pGeometryGroup = p->m_root.group("project/geometries");
    if(!pGeometryGroup.contains(idNumberToString(m_id))) {


        HDFGroup pSequenceGroup = pGeometryGroup.mkdir(idNumberToString(m_id));
        pSequenceGroup.setAttribute("label", m_label);

        /* write out the uuid of the sequence into the project file */
        IdAttributeType idData(1);
        idData(0) = m_uuid.data();
        p->m_pfile.writeAttribute(pGeometryPath, "uuid", idData);

        p->m_pfile.flush();
    }


    /* here we are going to create our cache structure as we would
     * expect it when loaded from a file */
    {

        /* create mandatory entries */
        HDFGroup root(sco->m_gfile.getGroupHandle("/"));
        HDFGroup geometry = root.mkdir("geometry");

        IdAttributeType idData(1);
        idData(0) = m_uuid.data();
        sco->m_gfile.writeAttribute(GEOMETRY_BASE_PATH, "uuid", idData);
        idData(0) = p->m_id.data();
        sco->m_gfile.writeAttribute(GEOMETRY_BASE_PATH, "projectUuid", idData);


        /* save interface revision */
        sco->m_gfile.writeAttribute(GEOMETRY_BASE_PATH, "revision", vigra::UInt32(Project::REVISION));

        sco->m_gfile.flush();


        HDFGroup points = geometry.mkdir("points");
        {
            LandmarkHeader headerTmpl(0);
            LandmarkPositions positionTmpl(0);

            auto headerShape = headerTmpl.expandElements(0).shape();
            auto headerMax = headerShape;
            auto headerChunking = headerShape;
            headerMax = H5S_UNLIMITED;
            headerChunking[1] = LANDMARK_CHUNK_SIZE;

            auto positionsShape = positionTmpl.expandElements(0).shape();
            auto positionsMax = positionsShape;
            auto positionsChunkin = positionsShape;
            positionsMax[1] = H5S_UNLIMITED;
            positionsChunkin[1] = LANDMARK_CHUNK_SIZE;


            static_assert(std::is_scalar<LandmarkHeader::value_type::value_type>::value, "Must be a scalar value");
            static_assert(std::is_scalar<LandmarkPositions::value_type::value_type>::value, "Must be a scalar value");

            m_gfile.createDataset<LandmarkHeader::value_type::value_type, 2>(points.path()+"/points",
                                                                headerShape, 0,
                                                                headerChunking,
                                                                headerMax);
            m_gfile.createDataset<LandmarkPositions::value_type::value_type, 2>(points.path()+"/xyz",
                                                                   positionsShape, 0,
                                                                   positionsChunkin,
                                                                   positionsMax);

        }

        HDFGroup tree = geometry.mkdir("chunks");
        {
            m_chunkHeader = ChunkHeader(2, 0);
            auto headerShape = m_chunkHeader.shape();
            auto headerMax = headerShape;
            headerMax[1] = H5S_UNLIMITED;
            auto headerChunking = headerShape;
            headerChunking[1] = CHUNK_CHUNK_SIZE;

            m_chunkState = ChunkState(6, 0);
            auto stateShape = m_chunkState.shape();
            auto stateMax = stateShape;
            stateMax[1] = H5S_UNLIMITED;
            auto stateChunking = stateShape;
            stateChunking[1] = CHUNK_CHUNK_SIZE;

            static_assert(std::is_scalar<ChunkHeader::value_type>::value, "Must be a scalar value");
            static_assert(std::is_scalar<ChunkState::value_type>::value, "Must be a scalar value");

            m_gfile.createDataset<ChunkHeader::value_type, 2>(tree.path()+"/chunks",
                                                            headerShape, 0,
                                                            headerChunking,
                                                            headerMax);
            m_gfile.createDataset<ChunkState::value_type, 2>(tree.path()+"/state",
                                                           stateShape, 0.0,
                                                           stateChunking,
                                                           stateMax);




        }


        HDFGroup collections = geometry.mkdir("collections");
        {

        }


        m_gfile.flush();
        m_modified = true;

        /* and clean up things, for instance close the hdf file again */
        this->dropCache();
    }

    m_valid = true;

}

void GeometryDataHDF::load()
{


    std::string geometryPath= "/project/geometries/"+idNumberToString(m_id);
    HDFGroup pGeometryGroup = p->m_root.group(geometryPath);
    pGeometryGroup.getAttribute("label", m_label);

//    GeometryCacheObject::Ptr sco = d();
    GeometryDataHDF * sco = this;


    /* Make sure the cache is available.
     * This includes the open hdf file. */
    CacheLock cacheLock(this);

    /* now do some checks whether the Geometry file is a valid one */
    HDFGroup root(sco->m_gfile.getGroupHandle("/"));

    if(!root.contains("geometry"))
        throw std::runtime_error("Geometry has no 'geometry; root group.");

    /* get the Geometry id and the project id and check if they match and are valid */
    {
        IdAttributeType idData(1);
        idData(0) = 0;
        try {
            sco->m_gfile.readAttribute(GEOMETRY_BASE_PATH, "uuid", idData);
        } catch(...) {
            std::stringstream err;
            err << "[ERROR] Geometries's ("
                << m_label
                << ") id could not be found in hdf file." << std::endl;
            throw std::runtime_error(err.str());
        }

        m_uuid = Id(idData(0));

        if(!m_uuid.valid()) {
            std::stringstream err;
            err << "[ERROR] Geometry's ("
                << m_label
                << ") id is not valid: "
                << m_uuid.toString() << std::endl;

            throw std::runtime_error(err.str());
        }

        idData(0) = 0;
        try {
            sco->m_gfile.readAttribute(GEOMETRY_BASE_PATH, "projectUuid", idData);
        } catch(...) {
            std::stringstream err;
            err << "[ERROR] Geometry's ("
                << m_label
                << ") projectUuid could not be found in hdf file."
                << std::endl;
            throw std::runtime_error(err.str());
        }

        Id projectId = Id(idData(0));
        if(!projectId.valid() || !(projectId == p->m_id)) {
            std::cout << "[ERROR] Geometry's (" << m_label << ") projectUuid doesn't match with current projct:' " << projectId.toString() << std::endl;
            return;
        }

        vigra::UInt32 revision = 0;
        try {
            sco->m_gfile.readAttribute(GEOMETRY_BASE_PATH, "revision", revision);
        } catch (...) {
            std::stringstream err;
            err << "[ERROR] Geometry's ("
                << m_label
                << ") revision could not be found in hdf file."
                << std::endl;
            throw std::runtime_error(err.str());
        }

        if(revision != Project::REVISION) {
            std::stringstream err;
            err << "[ERROR] Geometry's (" << m_label << ") revision (" << revision << ")"
                << "is not equal to Project::REVISION(" << Project::REVISION << ") Upgrade it!"
                << std::endl;
            throw std::runtime_error(err.str());
        }
    }

    /* get reference to Geometry group */
    HDFGroup geometry = root.group("geometry");

    if(!geometry.contains("points"))
        throw std::runtime_error("Geometry contains no 'points'' group.");

    if(!geometry.contains("collections"))
        throw std::runtime_error("Geometry contains no 'parts'' group.");

    if(!geometry.contains("chunks"))
        throw std::runtime_error("Geometry contains no 'tree' group.");


    /* yes, we are available */
    m_valid = true;
}

void GeometryDataHDF::loadCache()
{

    GeometryDataHDF * sco = this;



    /* check if the file exist, before we open it with hdf api.
     * This should avoid file creation, because we would open it
     * for read/write */
    {
        std::ifstream testFile;
        testFile.open(this->filePath());
        if(!testFile.is_open())
            throw std::runtime_error("File does not exist.");
    }


    /* open the hdf file */
    sco->m_gfile.open(this->filePath(), hookers::hdf5::File::OpenMode::ReadWrite);

    HDFGroup root(sco->m_gfile.getGroupHandle("/"));

    if(!root.contains("geometry"))
        return;

    /* get reference to sequence group */
    HDFGroup sequence = root.group("geometry");

    HDFGroup chunks = sequence.group("chunks");
    try {
        sco->m_gfile.readAndResize(chunks.path()+"/chunks", m_chunkHeader);
        sco->m_gfile.readAndResize(chunks.path()+"/state", m_chunkState);

    } catch (std::exception & e) {
        std::cerr << "[GeometryDataHDF] Loading Tree failed" << e.what() << std::endl;
    }

}

void GeometryDataHDF::saveCache()
{
    GeometryDataHDF * sco = this;

    HDFGroup chunks = HDFGroup(sco->m_gfile.getGroupHandle("/geometry/chunks"));
    {
        auto headerOffset = m_chunkHeader.shape();
        headerOffset = 0;
        auto stateOffset = m_chunkState.shape();
        stateOffset = 0;

        m_gfile.write(chunks.path()+"/chunks",
                     headerOffset,
                     m_chunkHeader);
        m_gfile.write(chunks.path()+"/state",
                     stateOffset,
                     m_chunkState);


    }

    /* make sure we flush file buffers */
    sco->m_gfile.flush();

}

void GeometryDataHDF::dropCache()
{

    m_gfile.close();
    m_chunkHeader = ChunkHeader();
    m_chunkState = ChunkState();

}

Object::Id GeometryDataHDF::id() const
{
    return m_uuid;
}

std::string GeometryDataHDF::fileName() const
{
    return p->m_name + "_"+ idNumberToString(m_id);
}

std::string GeometryDataHDF::filePath() const
{
    std::string filePath = p->m_path;

    if(!filePath.empty())
        filePath += "/";
    filePath += this->fileName() + ".hg";

    return filePath;

}

Landmark GeometryDataHDF::getLandmark(int id) const
{
    std::list<int> ids;
    ids.push_back(id);

    return getLandmarks(ids)[0];
}

LandmarkList GeometryDataHDF::getLandmarks(const std::list<int> & ids) const
{
    LandmarkList list;

    for(auto & i: ids) {
        SharedDataPointer<LandmarkDataHDF> d = this->getLandmarkPtr(i);
        /* now the list entry is first shared owner of TrackDataHDF */
        list.push_back(*d);
    }

    return list;
}


SharedDataPointer<LandmarkDataHDF> GeometryDataHDF::getLandmarkPtr(int id) const
{
    GeometryChunkDataHDF::Ptr chunk = this->getChunk(GeometryChunkDataHDF::chunkFromLandmarkId(id));
    return chunk->getLandmarkPtr(id);
}

SharedDataPointer<PartDataHDF> GeometryDataHDF::getPartPtr(int id) const
{
    PartDataHDF::Ptr d = m_partPointer[id].lock();
    if(!d) {
        d = new PartDataHDF(this, id);
        d->load();
    }
    return d;
}

//SequenceCacheObject::Ptr SequenceDataHDF::d()
//{


//	/* try to aquire a lock on the existing cache object */
//	SequenceCacheObject::Ptr sco = m_d.lock();
//	if(sco)
//		return sco;

//	/* lock the mutex to make sure only one new instance of
//	 * the cache object will be created at a time
//	 * and generally to be thread-safe here. */
//	std::lock_guard<std::mutex> lock(p->m_cacheMutex);

//	/* Check if, while we have waited on the locked mutex, a cache instance already has been created. */
//	sco = m_d.lock();
//	if(sco)
//		return sco;

////	if(p->m_cachedSequences.find(*this) != p->m_cachedSequences.cend())
////		return p->m_cachedSequences[*this];

//	/* create a new cache object */
//	sco = std::make_shared<SequenceCacheObject>();
//    p->m_cachedSequencesQueue.push_back(sco);

//    /* We have created a new sequence cache object. In order to make sure
//     * we don't have too many of them in memory, we delete the
//     * oldest one.
//     *
//     * Note that we only need to destroy the shared_ptr in order to delete
//     * the SCO.
//     */
//    if(p->m_cachedSequencesQueue.size() > 5) {
//        p->m_cachedSequencesQueue.pop_front();
//    }

//    /* This line needs to be called before loadCache() which
//	 * would lead into a recursive mutex locking (dead-lock) otherwise. */
//	m_d = sco;

//    this->loadCache();
//	return sco;
//}

void GeometryDataHDF::save()
{

    if(!m_modified)
        return;

	/* Get the project-HDF's sequence group. */
    HDFGroup pSequencesGroup = p->m_root.group("project/geometries");
    HDFGroup pSequenceGroup = pSequencesGroup.group(idNumberToString(m_id));
    std::string pSequencePath = pSequenceGroup.path();
    pSequenceGroup.setAttribute("label", m_label);

}

std::string GeometryDataHDF::name() const
{
    return m_label;
}


PartList GeometryDataHDF::parts() const
{

    CacheLock cacheLock(this);


    std::list<std::string> parts = HDFGroup(m_gfile.getGroupHandle("/geometry/collections")).entries();

    PartList list;
    for(auto & entry : parts) {
        int id = PartDataHDF::idStringToNumber(entry);
        list.push_back(*this->getPartPtr(id));
    }
    return list;
}

Part GeometryDataHDF::addPart(const std::string &name)
{

    CacheLock cacheLock(this);

    HDFGroup parts(m_gfile.getGroupHandle(PartDataHDF::HDF_PART_PATH_PREFIX));
    std::list<std::string> partEntries = parts.entries();

    /* get the highest possible id */
    unsigned newId = 0;
    for(auto it = partEntries.cbegin();
        it != partEntries.cend(); it++) {

        /* get id from part name */
        unsigned id = PartDataHDF::idStringToNumber(*it);
        newId = std::max(newId, id+1);
    }

    /* part data */
    PartDataHDF * pd = new PartDataHDF(this, newId, name);
    pd->create();

    /* add part into own cache and return newly created part reference */
    m_partPointer.insert(std::pair<int,WeakSharedDataPointer<PartDataHDF> >(newId, pd));
    return *pd;

}

SharedDataPointer<GeometryChunkDataHDF> GeometryDataHDF::getChunk(unsigned id) const
{
    GeometryChunkDataHDF::Ptr d = m_chunkPointer[id].lock();
    if(!d) {
        d = new GeometryChunkDataHDF(this, id);
        m_chunkPointer[id] = WeakSharedDataPointer<GeometryChunkDataHDF>(d);
    }

    if(!d) {
        std::stringstream err;
        err << "GeometryDataHDF: Loading Chunk with ID "
            << id << " failed.";
        throw std::runtime_error(err.str());
    }

    return d;
}

std::vector<SharedDataPointer<GeometryChunkDataHDF> > GeometryDataHDF::getChunks() const
{
    static_assert(ChunkHeader::actual_dimension == 2, "Dimensions must match.");


    CacheLock lock(this);

    std::vector<GeometryChunkDataHDF::Ptr> chunks;
    for(int i = 0; i < m_chunkHeader.height(); i++) {
        if(m_chunkHeader(0, i) != 0)
            chunks.push_back(this->getChunk(i));
    }

    return chunks;
}

SharedDataPointer<GeometryChunkDataHDF> GeometryDataHDF::addChunk()
{

    /* the following code only works under given assumptions.
     * escpecially the array indices etc are dependant of this */
    static_assert(std::is_same<ChunkHeader, decltype(m_chunkHeader)>::value, "Types must match");
    static_assert(ChunkHeader::actual_dimension == 2, "Dimensions is not correct");


    for(int i = 0; i <= m_chunkHeader.height(); i++) {

        if(i >= m_chunkHeader.height()) {
            resizeArray(m_chunkHeader, 1, CHUNK_CHUNK_SIZE, 0);
            resizeArray(m_chunkState, 1, CHUNK_CHUNK_SIZE, 0);
        }
        auto header = m_chunkHeader.bindOuter(i);

        if(header(ChunkHeaderFields::Type) > 0)
            continue;

        header(ChunkHeaderFields::Type) = 1;
        GeometryChunkDataHDF::Ptr d = new GeometryChunkDataHDF(this, i);

        if(!d) {
            std::stringstream err;
            err << "GeometryDataHDF: Creating Chunk with ID "
                << i << " failed.";
            throw std::runtime_error(err.str());
        }

        d->create();
        m_chunkPointer[i] = SharedDataPointer<GeometryChunkDataHDF>(d);
        return d;
    }
    return nullptr;
}




void GeometryDataHDF::addLandmarks(Vector3fArrayView posInput)
{

    using namespace vigra;

    Vector3fArray pos(posInput);
//    Vector3fArray meanFree(pos.size());
//    Array2fView posView(pos.expandElements(0));
//    Array2fView meanView(meanFree.expandElements(0));

//    vigra::linalg::prepareRows(posView, meanView, ZeroMean);
//    int numFeatures = Vector3f::static_size;
//    int numSamples = pos.size();
//    int numComponents = numFeatures;

//    vigra::Matrix<float> fz(numFeatures, numComponents);
//    vigra::Matrix<float> zv(numComponents, numSamples);
//    vigra::principleComponents(meanView, fz, zv);

//    Vector3fArray pca(numComponents);
//    for(int i = 0; i < numComponents; i++) {
//        pca.expandElements(0).bindOuter(i) = fz.bindOuter(i);
//        std::cout << "PCA: " << i << ", "  << pca(i) << std::endl;
//    }


    CacheLock lock(this);
    addLandmarkChunk(pos, 0);


}

void GeometryDataHDF::removeLandmarks(LandmarkList landmarks)
{

}

LandmarkList GeometryDataHDF::radiusSearch(const Vector3f &center, double radius) const
{
    throw std::runtime_error("GeometryDataHDF::radiusSearch(): Not implemented.");
    return LandmarkList();
}

std::vector<LandmarkList> GeometryDataHDF::nearestNeighbourSearches(const std::vector<Vector3f> &centers, int k) const
{
    typedef GeometryChunkDataHDF::Ptr GCP;
    std::vector<GCP> chunks = this->getChunks();

    std::vector<LandmarkList> knnsList;

    for(auto center : centers) {

        /* sort the chunks according to their boundary distance to
         * the search center. */
        std::sort(chunks.begin(), chunks.end(), [&](const GCP & a, const GCP & b) {
            /* a negative value here is perfectly okay and indicates that
             * the center point lies even inside the perimeter */
            auto da = (a->center()-center).magnitude()-a->boundingBox().magnitude();
            auto db = (b->center()-center).magnitude()-b->boundingBox().magnitude();
            return da < db;
        });


        /* initialize the ball with an infinite radius */
        LandmarkList::Sphere searchRegion(center, -1);

        LandmarkList knns;
        for(auto it = chunks.begin(); it != chunks.end(); it++) {
            GCP chunk = *it;

            /* check if the nearest of the remaining chunks intersects with
             * the search radius.
             * If this is not the case, then we can stop here.
             * This has the following rational:
             * Suppose that a point in a more distant chunk is nearer to
             * the search center as all so far found points.
             * Then the perimeter would for sure intersect with this chunk.
             * The first chunk that doesn't intersect anymore thus cannot
             * hold any points nearer then the ones we have found so far.
             * This is true at least, if you keep in mind that the
             * list of chunks is sorted according to the distance of the boundary
             * from the search center.
             */
            if((int)knns.size() >= k && !chunk->intersecs(searchRegion))
                break;


            /* get the KNNs from each chunk in question.
             * finally get the KNNs from the merged list */
            const LandmarkList & tmp = chunk->knnSearch(center, k);
            knns.insert(knns.end(), tmp.begin(), tmp.end());
            knns = knns.knn(center, k);

            /** @todo Replace by bounding boxes. This will reduce the
             *        amount of needed comparisons.
             *        The currently used perimeters will do this most of the time.
             *        because the chunks are near by each other
             */
            /* calculate the new search region by the following scheme:
             * The new search region is the perimeter of
             * the search center and all points found so far.
             */
            LandmarkList::Sphere tmpBall = knns.perimeter();
            std::get<1>(searchRegion) = (center-std::get<0>(tmpBall)).magnitude()+std::get<1>(tmpBall);

        }
        knnsList.push_back(knns.knn(center, k));
    }

    return knnsList;

}

void GeometryDataHDF::addLandmarkChunk(Vector3fArrayView pos, int dir)
{

    /* if there are no points left, than we can stop here */
    if(pos.size() <= 0)
        return;


    /* go round robin through the axis */
    dir = dir % 3;

    Vector3f r(0);

    /* calculate the center of mass for each dimension of all points */
    for(size_t i = 0; i < r.size(); i++) {
        r[i] = pos.expandElements(0).bindInner(i).sum<float>();
    }
    r /= float(pos.size());


    /* distance transform */
    auto scalar = [&](const Vector3f & v) -> float {
        return v[dir]-r[dir];
    };


    /* Sort the subset of the array along the axis of the current node.
     * We are doing it in place here, so we can write out the data serialized... */
    std::sort(pos.begin(), pos.end(), [&](const Vector3f & a, const Vector3f & b){
        return scalar(a) < scalar(b);
    });



    if(pos.size() <= LANDMARK_CHUNK_SIZE) {
    /* if the number of points in the list is smaller than or equal to the chunk size, we can sort it in
     * and quit here */

        GeometryChunkDataHDF::Ptr chunk = this->addChunk();
        GeometryChunkDataHDF::CacheLock lock(chunk.data());

        typedef vigra::MultiArrayShape<1>::type Shape1;
        chunk->xyz().subarray(Shape1(0), pos.shape()) = pos;
        chunk->points().subarray(Shape1(0), pos.shape()) = {1,0};

        std::cout << " leaf node: " << dir << ", " << pos.size() << ", "
                  << r
                  << std::endl;

    } else {
    /* if not, we have to create a further node and iterate as long as we have splitted up the data into chunks
     * smaller than chunk size */



        /* the split index should be selected such that the points
         * are roughly distributed evenly to both new nodes.
         * However, ultimate goal is to fill up all chunks except
         * the last one.
         * This means, that the left branch must always contain
         * more points then the right.
         *
         * By first dividing by two yields the even distribution.
         * Secondly, by dividing by the the chunksize, we find the number
         * of chunks needed to hold the half of the points.
         * Integer division floors the value.
         * We have to add a 1 at the and due to that reason.
         * What we end up with is the chunk index of the first chunk
         * that should appear on the right branch.
         */
        unsigned requiredChunks = pos.size()/LANDMARK_CHUNK_SIZE;
        requiredChunks += (pos.size()%LANDMARK_CHUNK_SIZE)?1:0;

        assert(requiredChunks > 1);

        unsigned splitIndex = (pos.size())/LANDMARK_CHUNK_SIZE/2 + 1;

        //splitIndex = std::max(unsigned(1), splitIndex);

        assert(splitIndex > 0);

        splitIndex = std::min(splitIndex, requiredChunks-1);
        //splitIndex = std::max(unsigned(1), std::min(unsigned(requiredChunks-1), splitIndex));

        std::cout << "--" << pos.size() << ","
                  << splitIndex << ", "
                  << requiredChunks <<  ", "
                  << pos.size()-splitIndex*LANDMARK_CHUNK_SIZE << ", "
                  << pos.size()%LANDMARK_CHUNK_SIZE
                  << std::endl;

        /* Finally we get the points index back by multiplying the chunk index again
         * by the chunk size. */
        splitIndex *= LANDMARK_CHUNK_SIZE;

        /* here we get the new subarrays that we pass down to the lower nodes */
        Vector3fArray::difference_type startFirst, endFirst, startSecond, endSecond;

        /* the first half is exclusive the splitindex */
        startFirst[0] = 0;
        endFirst[0] = splitIndex;

        startSecond[0] = splitIndex;
        endSecond[0] = pos.size();

        std::cout << "split node: " << "[" << 0 << "," << splitIndex << ", " << pos.size() << ") "
                  <<  dir << ", "
                  << pos(0)[dir] << ","
                  << pos(splitIndex)[dir] << ","
                  << pos(pos.size()-1)[dir] << ","
                  << std::endl;

        addLandmarkChunk(pos.subarray(startFirst, endFirst), dir+1);
        addLandmarkChunk(pos.subarray(startSecond, endSecond), dir+1);

    }
}

void GeometryDataHDF::getPointCloud(Vector3fArray &cloud)
{

    /* cache lock. requires chunk headers to be loaded in memory */
    CacheLock lock(this);

    /* find the total number of children per chunks */
    /** @bug this is not consistent anymore when a chunk has been altered
     *       without beeing saved. */
    unsigned numLandmarks = m_chunkHeader.bind<0>(1).sum<unsigned>();
    auto chunks = this->getChunks();

    /* now reshape the provided array to the number of landmarks in geoemtry */
    cloud.reshape(Vector3fArrayShape(numLandmarks));


    /* keeps track of last free position in cloud array */
    int currentPos = 0;

    /* iterate over all chunks.
     * and read the points */
    for(auto & c: chunks) {

        auto numLandmarks = c->numberChildren();
        c->getPoints(cloud.subarray(Vector3fArrayShape(currentPos),
                                    Vector3fArrayShape(currentPos+numLandmarks)));
        currentPos += numLandmarks;
    }
}
