#ifndef HOOKERS_PROJECT_GEOMETRYCHUNKDATAHDF_H
#define HOOKERS_PROJECT_GEOMETRYCHUNKDATAHDF_H

#include <unordered_map>
#include <string>

#include <hookers/interface/SharedType.h>

#include "GeometryDataHDF.h"
#include "LandmarkDataHDF.h"

namespace hookers {
namespace interface {

class Landmark;
class LandmarkList;

class GeometryChunkDataHDF : public SharedData
{
    friend class LandmarkDataHDF;
    friend class GeometryDataHDF;
    friend class TrackDataHDF;

public:
    typedef SharedDataPointer<GeometryChunkDataHDF, hookers::hdf5::File> CacheLock;
    typedef SharedDataPointer<GeometryChunkDataHDF> Ptr;
    typedef GeometryDataHDF::LandmarkHeader PointsHeader;
    typedef GeometryDataHDF::LandmarkPositions PointsXYZ;

    typedef std::tuple<Vector3f, Vector3f> Box;
    typedef std::tuple<Vector3f, float> Sphere;

private:

    SharedDataPointer<GeometryDataHDF, hookers::hdf5::File> g;
    unsigned id;

    mutable std::unordered_map<unsigned, WeakSharedDataPointer<LandmarkDataHDF>> m_pointsPointers;

    PointsHeader m_points;
    PointsXYZ m_xyz;

    bool m_modified;

public:

    GeometryChunkDataHDF(const GeometryDataHDF * g, unsigned id);
    ~GeometryChunkDataHDF();


    const PointsHeader & points() const { return m_points; }
    PointsHeader & points() { m_modified = true; return m_points; }

    const PointsXYZ & xyz() const { return m_xyz; }
    PointsXYZ & xyz() { m_modified = true; return m_xyz; }


    void create();

    std::atomic_int_fast32_t m_cacheUsage;

    void loadCache();
    void saveCache();
    void dropCache();

    unsigned numberChildren() const;

    Vector3f center() const;
    Vector3f boundingBox() const;

    bool contains(const Vector3f & p) const {
        vigra::TinyVector<bool,3> result;
        Vector3f lower = center()-boundingBox();
        Vector3f upper = center()+boundingBox();

        for(int i = 0; i < 3; i++) {
            result[i] = (lower[i] <= p[i]) && (p[i] <= upper[i]);
        }
        return result.all();
    }

    bool intersecs(const Sphere & b) const;
    bool intersecs(const Box & b) const;

    void getPoints(Vector3fArrayView view);

    LandmarkList landmarks() const;
    LandmarkList radiusSearch(const Vector3d & c, double r) const;
    LandmarkList knnSearch(const Vector3d & c, unsigned k) const;

    SharedDataPointer<LandmarkDataHDF> getLandmarkPtr(unsigned id) const;

    inline static unsigned chunkFromLandmarkId(unsigned id) {
        return id >> 16;
    }

    inline static unsigned rowFromLandmarkId(unsigned id) {
        return id & 0xffff;
    }

    template<class T> void ref() {}
    template<class T> void deref() {}



};

template<>
inline void GeometryChunkDataHDF::ref<hookers::hdf5::File>()
{
    if(!m_cacheUsage.fetch_add(1)) {
        this->loadCache();
    }
}

template<>
inline void GeometryChunkDataHDF::deref<hookers::hdf5::File>()
{
    if(m_cacheUsage.fetch_sub(1) == 1) {
        this->saveCache();
        this->dropCache();
    }
}

} // ns interface
} // ns hookers



#endif //HOOKERS_PROJECT_GEOMETRYCHUNKDATAHDF_H
