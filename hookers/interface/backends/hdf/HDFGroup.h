#ifndef HDFGROUP_H
#define HDFGROUP_H


#include <list>
#include <vector>
#include <string>

#include <vigra/hdf5impex.hxx>

#include "HDFHandle.h"

namespace hookers {
namespace hdf5 {

class HDFGroup
{
    mutable HDFHandle m_handle;

public:

	HDFGroup(vigra::HDF5Handle handle);
	HDFGroup(HDFHandle handle);
	HDFGroup();
	~HDFGroup();


	/** @returns absolute path to group */
	std::string path();

	/** @returns name of the group within parent group */
	std::string name();

	/** lists all entries in given group */
	std::list<std::string> entries();

	/** checks whether given entry exists */
	bool contains(const std::string & name);

	/** creates new group */
	HDFGroup mkdir(const std::string & name);

	/** removes entry in file */
	bool rm(const std::string & name);

	bool isValid();

	/** retrieves a child group */
    HDFGroup group(const std::string & name) const;

	operator hid_t() {
		return m_handle;
	}


	void setAttribute(const std::string & attribute, int value);
	void getAttribute(const std::string & attribute, int & value);

	void setAttribute(const std::string & attribute, float value);
	void getAttribute(const std::string & attribute, float & value);

	void setAttribute(const std::string & attribute, double value);
	void getAttribute(const std::string & attribute, double & value);

	void setAttribute(const std::string & attribute, const std::string & value);
	void getAttribute(const std::string & attribute, std::string & value);

};

} // ns hdf5
} // ns hookers

#endif // HDFGROUP_H
