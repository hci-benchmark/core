#ifndef FRAMEDATAHDF_H
#define FRAMEDATAHDF_H

#include <hookers/interface/FrameData.h>
#include "SequenceDataHDF.h"

namespace hookers {
namespace interface {

/**
 * @page hdf
 *
 * @latexonly
 * \newpage
 * @endlatexonly
 *
 * ********
 * @subsection hdf_sequence_frames /sequence/frames/
 *
 * @anchor hdf_frames__maps
 * @h5oattr{maps,uchar} auxiliary data files for maps and lists.\n
 *                      If not set, the project file should be used.
 *
 * Contains all frames used in the projects.
 *
 * ***********
 *
 * @subsection hdf_sequence_frames_header_header /sequence/frames/frames
 * Shape: @f$ F \times (\text{status}, \text{views}) @f$ \n
 * Type: uint16, expandable in F \n
 * Chunking: @f$ (1024,2) @f$\n
 *
 * Status: 0x1 bit is set, if the frame is valid.
 * views: number of views in the frame.
 *
 *
 * ***********
 * @subsection hdf_sequence_frames_header_time /sequence/frames/time
 * Shape: @f$ F \times (\text{time}) @f$ \n
 * Type: double, expandable in F \n
 * Chunking: @f$ (1024,1) @f$\n
 *
 * Specfies the timestamp given in unix time. The values can be fractional.
 *
 * Default value corresponds to 0.0, which means that there is no time specified.
 *
 * ***********
 * @subsection hdf_sequence_frames_frame /sequence/frames/views/
 *
 * All datasets in this group have a common shape prefix of @f$ N \times V \times \ldots @f$
 * where @f$ N @f$ is the frame dimension and @f$ V @f$ is the corresponding view dimension.
 *
 * ***********
 * @subsection hdf_sequence_frames_views_header /sequence/frames/views/views
 * Shape: @f$ F \times V \times (\text{status}, \text{width}, \text{height}, \text{tracks}) @f$ \n
 * Type: uint16, expandable in F,V \n
 * Chunking: @f$ (1024, 2, 2) @f$ \n
 * Max: @f$(2^{32}-1, 2^{16}-1, 4)@f$,
 *
 *
 * Status:
 * Contains the status of a frame. The following bitmasks are used for each field:
 *
 *   They share the following bitmasks:
 *   - 0x000f: How many channels are present.
 *             0x0: view is not set
 *             0xf: 7 channels are used
 *   - 0xfff0: Currently not used, initialize with 0x000.
 *
 * width/height: dimensions of given view
 * tracks: number of registered tracks of the view
 *
 * ***********
 * @subsection hdf_sequence_frames_views /sequence/frames/views/data
 * Shape: @f$ F \times V \times C \times H \times W @f$ \n
 * Type: uint8, compressed \n
 * Chunking: @f$ (1, 1, 1, 360, 512) @f$ \n
 * Max: @f$(\infty, 7, 2, 2^{16}, 2^{16})@f$\n
 *
 *
 * Contains the phographic information for all frames.
 * F is the frame index, C is the channel, V is the view.
 * W and H are width and height dimensions respectively.
 * Note that this is row major, hence
 * the last dimension is H and not W.
 *
 * ***********
 * @subsection hdf_sequence_frames_frame_track_header /sequence/frames/views/tracks
 * Shape: @f$ F \times V \times T \times (\text{trackid}, \text{context}) @f$ \n
 * Type: int32, chunksize: 1024, expandable \n
 * Chunking: @f$ (32, 1, 2048, 2) @f$ \n
 *
 *
 * This dataset holds meta information about track correspondence to given frame in base view.
 *
 * A deleted entry must be flagged with *trackid* set to @f$ -1 @f$.
 *
 *
 * For *context* see Project::Context.
 *
 * The rows should be ordered according to *trackid*,
 * deleted entries should be placed at the end.
 * The ordering is __optional__ as it only might increase cache-friendlyness.
 *
 * Always fill unassigned rows before resizing the array.
 *
 * ***********
 * @subsection hdf_sequence_frames_frame_track_positions /sequence/frames/views/tracksPositions
 * Shape: @f$ N \times V \times T \times (x, y) @f$ \n
 * Type: double, expandable \n
 * Chunking: @f$ (32, 1, 2048, 2) @f$ \n
 *
 * This dataset holds the track positions of N tracks in @ref hdf_sequence_frames_frame_track_header,
 * where @f$(x, y) @f$ is the corresponding position of the track in current frame's view @f$ V @f$.
 *
 *
 * ***********
 * @subsection hdf_sequence_frames_header_intrinsics /sequence/frames/views/intrinsics
 * Type: double \n
 * Shape: @f$ F \times V \times (\vec{f}, \vec{c}) @f$ \n
 * Chunking: @f$ (1024, 2, 4) @f$ \n
 *
 *
 * Contains the camera intrinsics for each frame. \n
 * First two entries are @f$ \vec{f} := (f_x,f_y)@f$, which are the focal lengths.
 * The last two entries are @f$ \vec{c} := (c_x,c_y)@f$,
 * which is the principal point.
 *
 * Values should be initialized with @f$ (0, \dots, 0) @f$.
 *
 * ***********
 * @subsection hdf_sequence_frames_header_intrinsicUncertainties /sequence/frames/views/intrinsicsUncertainty
 * Type: double \n
 * Shape: @f$ F \times V \times (\vec{f}, \vec{c}) @f$
 * Chunking: @f$ (1024, 2, 4) @f$ \n
 *
 *
 * Contains the camera intrinsics for each frame. \n
 * First two entries are @f$ \vec{F} := (f_x,f_y)@f$, which are the focal lengths.
 * The last two entries are @f$ \vec{C} := (c_x,c_y)@f$,
 * which is the principal point.
 *
 * Values should be initialized with @f$ (0, \dots, 0) @f$.
 *
 * ***********
 * @subsection hdf_sequence_frames_header_stereoTranslations /sequence/frames/views/transformation
 * Type: double \n
 * Shape: @f$ F \times V \times (\vec{t}, \vec{r}) @f$
 * Chunking: @f$ (1024, 2, 6) @f$ \n
 *
 * Contains the transformation of view relative to frame pose, where
 * @f$ \vec{t} = (t_x, t_y, t_z) @f$ is the translational part and
 * @f$ \vec{r} = (r_x, r_y, r_z) @f$ is the rotational part given as angle-axis vector.
 *
 * Values should be initialized with @f$ (0, \ldots, 0) @f$.
 *
 * ***********
 * @subsection hdf_sequence_frames_header_stereoTranslationUncertainties /sequence/frames/views/transformationUncertainty
 * Shape: @f$ F \times V \times (\vec{t}, \vec{r}) @f$ \n
 * Type: double \n
 * Chunking: @f$ (1024, 2, 6) @f$ \n
 *
 *
 * Contains the transformation uncertainties of view relative to frame pose, where
 * @f$ \vec{t} = (t_x, t_y, t_z) @f$ is the translational part and
 * @f$ \vec{r} = (r_x, r_y, r_z) @f$ is the rotational part given as angle-axis vector.
 * This equals the diagonal entries of the covariance matrix.
 *
 * Values should be initialized with @f$ (0, \ldots, 0) @f$.
 *
 * ***********
 * ***********
 * @subsection hdf_sequence_frames_header_pose /sequence/geometries/frames/pose
 * Type: double \n
 * Shape: @f$ G \times F \times (\vec{X}, \vec{R}) @f$
 * Chunking: @f$ (1, 1024, 6) @f$ \n
 *
 *
 * Contains the camera pose for all frames. \n
 * First three entries are @f$ \vec{X} := (x,y,z)@f$,
 * last three entries are @f$ \vec{R} := (r_1,r_2,r_3)@f$,
 * where @f$ \vec{R} @f$ is the rotation axis
 * and @f$|\vec{R}|@f$ is the rotation angle.
 *
 * Values should be initialized with @f$ (0, \dots, 0) @f$.
 *
 * The camera pose should be given in __world coordinate system__.
 * This ensures that we can search for near-by frames very quickly.
 *
 * @warning The default value @f$ (0,\dots ,0) @f$ refers to a missing camera pose.
 * However, this value is a valid camera pose. It corresponds to zero translation and a identity rotation
 * matrix. If you need to set exactly this transformation, set the rotation vector components to
 * @f$ r_i = \sqrt{\frac{1}{3} \cdot (2 \pi)^2} @f$ to achieve an equivalent transformation.
 *
 * ***********
 * @subsection hdf_sequence_frames_header_poseUncertainties /sequence/geometries/frames/poseUncertainty
 * Shape: @f$ N \times (\vec{X}, \vec{R}) @f$ \n
 * Type: double \n
 * Chunking: @f$ (1, 1024, 6) @f$ \n
 *
 *
 * Contains the camera pose uncertainties for frames. \n
 * First three entries are @f$ \vec{X} := (x,y,z)@f$,
 * last three entries are @f$ \vec{R} := (r_1,r_2,r_3)@f$,
 * where @f$ \vec{R} @f$ is the rotation axis
 * and @f$|\vec{R}|@f$ is the rotation angle.
 *
 * Values should be initialized with @f$ (0, \dots, 0) @f$.
 *
 * The camera pose should be given in __world coordinate system__.
 * This ensures that we can search for near-by frames very quickly.
 *
 * ***********
 * ***********
 *
 * @subsection hdf_sequence_frames_frame_maps /sequence/frames/nnnnnn/maps/
 *
 * All datasets within this group or subgroups must have the shape @f$ H \times W \times Z @f$.
 * This group holds optional maps that were created by postprocessing, for instance the depthmap
 * per frame.
 *
 * As maps are meant pixelwise, each dataset in this group should have a dimensionality of 3
 * where the first dimensions @f$ H,W @f$ are the frame view's width and height.
 * The last dimension @f$ Z @f$ is the number of channels for this map.
 *
 * This maps will be distributed to a auxiliary data file.
 *
 *
 * ***********
 * @subsection hdf_sequence_frames_frame_lists /sequence/frames/nnnnnn/lists/
 * Shape: @f$ N \times M @f$ \n
 * This group holds optional data that could be stored in form of lists.
 *
 * This datasets are meant for less structured data like point clouds etc.
 * Therefor the dimensionality is fixed to two, but the shape is not specified.
 *
 * This maps will be distributed to a auxiliary data file.
 *
 * *** */

class FrameDataHDF : public FrameData
{

    friend class SequenceDataHDF;

public:
    static const int TRACKS_CHUNK_SIZE = 2048;

    static const std::string FRAME_BASE_PATH;

    typedef SequenceDataHDF::FrameTrackHeader TrackHeader;
    typedef SequenceDataHDF::FrameTrackPositions TrackPositions;
    typedef SequenceDataHDF::FrameTrackDimensions FTD;
    typedef SequenceDataHDF::FrameTrackHeaderFields FTHF;
    typedef SequenceDataHDF::FrameHeaderFields FHF;
    typedef SequenceDataHDF::FrameViewHeaderFields FVHF;
    typedef SequenceDataHDF::FrameViewHeader::value_type::value_type FVHT;

    /** define a Cache object type. only used for namespaces */
    struct Cache {};
    typedef SharedDataPointer<FrameDataHDF, Cache> CacheLock;

private:
    SharedDataPointer<SequenceDataHDF, FrameDataHDF> s;

    int m_id;
    bool m_modified;
    bool m_valid;

    Vector6d m_pose;

    TrackHeader trackHeader;
    TrackPositions trackPositions;

    mutable Image m_cachedView;
    mutable Image m_cachedStereoView;


    inline FVHT getMaxTracks() const {

        auto boundTrackNums = s->m_frameViewHeader.bindOuter(m_id).expandElements(0).bind<0>(FVHF::Tracks);
        FVHT tracks = 0, NOT_USED = 0;
        boundTrackNums.minmax(&NOT_USED, &tracks);
        return tracks;
    }



public:
    FrameDataHDF(const SequenceDataHDF * sequence, int id);
	FrameDataHDF();
    ~FrameDataHDF();


    inline Object::Id id() const {
        return Object::Id(s->p->m_id.project(),
                          s->m_id,
                          0x0,
                          m_id);
    }

    inline bool isValid() const {
        return m_valid;
    }

    inline bool isModified() const {
        return m_modified;
    }

    Sequence sequence() const;

    void create();
    void load();
	void save();

    void loadCache();
    void saveCache();
    void dropCache();


	void setView(const ImageView &image, const View &view);
    void getView(Image &image, const View &view) const;

    void setMap(const std::string & name, const MapView & map);
    void getMap(const std::string & name, Map & map) const;
	bool hasMap(const std::string & name);


    void setList(const std::string & name, const ListView & list);
    void getList(const std::string & name, List & list) const;
	bool hasList(const std::string & name);

    void writeMapMeta();
    std::string getDataFilePath();

    void linkTrack(TrackData * td, const Vector2d & pos, const View & view);
    void unlinkTrack(TrackData * td, const View & view);
    Vector2d trackPosition(TrackData * td, const View &view) const;
    TrackList tracks(const View &view) const;


    Vector6d pose() const;
    void setPose(const Vector6d & pose);

    Vector6d poseUncertainty() const;
    void setPoseUncertainty(const Vector6d & pose);

    double time() const;
    void setTime(const double &time);

    Vector4d intrinsics(const FrameData::View& view) const;
    void setIntrinsics(const Vector4d & intrinsics, const FrameData::View & view);

    Vector4d intrinsicsUncertainty(const FrameData::View & view) const;
    void setIntrinsicsUncertainty(const Vector4d & intrinsics, const FrameData::View & view);

    Vector6d viewTransformation(const FrameData::View & view) const;
    void setViewTransformation(const Vector6d & trans, const FrameData::View & view);

    Vector6d viewTransformationUncertainty(const FrameData::View & view) const;
    void setViewTransformationUncertainty(const Vector6d & trans, const FrameData::View & view);

    static inline std::string zeroPadId(int num) {
		std::ostringstream ss;
		ss << std::setw(5) << std::setfill( '0' ) << num;
		return ss.str();
	}

    CacheLock m_extRefCacheLock;
    inline void externalReferenced() {
        m_extRefCacheLock = CacheLock(this);
    }
    inline void externalUnreferenced() {
        m_extRefCacheLock.reset();
    }

    std::atomic_int m_cacheUsage;
    template<class T> void ref() {}
    template<class T> void deref() {}


};

template<>
inline void FrameDataHDF::ref<FrameDataHDF::Cache>()
{
    if(!m_cacheUsage.fetch_add(1)) {
        this->loadCache();
    }
}

template<>
inline void FrameDataHDF::deref<FrameDataHDF::Cache>()
{
    /* if the frame usage counter drops to zero,
     * we can safely remove any required caches and clear
     * lists associated with frames */
    if(m_cacheUsage.fetch_sub(1) == 1) {
        this->saveCache();
        this->dropCache();
    }
}

} // ns interface
} // ns hookers


#endif // FRAMEDATAHDF_H
