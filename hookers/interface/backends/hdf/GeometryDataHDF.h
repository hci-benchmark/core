#ifndef HOOKERS_PROJECT_GEOMETRYDATAHDF_H
#define HOOKERS_PROJECT_GEOMETRYDATAHDF_H

#include <unordered_map>
#include <string>

#include <hookers/interface/GeometryData.h>

#include "ProjectDataHDF.h"
#include "HDFFile.h"


namespace hookers {
namespace interface {


class Geometry;
class LandmarkDataHDF;
class GeometryChunkDataHDF;


/**
 * @page hdf
 *
 * ***********
 * ***********
 *
 * @section hdf_geometry /geometry/
 *
 *
 * @anchor hdf_geometries__revision
 * @anchor hdf_geometries__id
 * @h5attr{revision,uint} Revision number of the sequence file.
 * @h5attr{uuid,v16uint8} Unique id of the sequence. \n
 *   Must be unique throughout a project. Used to check against uuid in project file.
 * @h5attr{projectUuid,v16uint8} Unique id of project. \n
 *
 * @revision{1}
 *
 * Geometry File that contains all reference data
 *
 * ***********
 * @subsection hdf_geometry_points /geometry/points/
 *
 * This group contains all datasets that specifiy properties of points in the cloud.
 * All datasets in this group must have the following properties:
 *
 *  - Shape: @f$ N \times A @f$ , where N is the same for all datasets. A is arbitrary.
 *  - Chunking: 2^16
 *  - expandable in N
 *
 *
 */

class GeometryDataHDF : public GeometryData
{

    friend class SequenceDataHDF;
    friend class GeometryChunkDataHDF;
    friend class LandmarkDataHDF;
    friend class PartDataHDF;

public:


    /**
     * @page hdf
     *
     * ***********
     * @subsection hdf_geometry_points_points /geometry/points/points
     * Type: uint8 \n
     * Shape: @f$ N \times (type, NOTUSED) @f$
     *
     * This dataset contains all landmarks belonging to a @ref hdf_geometry.
     *
     * For *type* see Landmark::Type. N is a variable number in multiples of chunk size.
     *
     * The second entry @em NOTUSED is currently not used, but might be in the feature. Initialize
     * these entries to zero!
     *
     * The unique id of a point is a 64bit unsigned int that is computed by
     * @code (geometryId<<32)+rowNumber @endcode
     * where geometryId is the number of the geometry and rowNumber is the row in this dataset.
     * An unique ID equal to zero refers to an invalid landmark, because parts should have ids greater
     * than zero (@ref hdf_geometry).
     *
     */
    typedef vigra::MultiArray<1, vigra::TinyVector<vigra::UInt8, 2> > LandmarkHeader;


    /**
     * @page hdf
     *
     * ***********
     * @subsection hdf_geometry_points_xyz /geometry/points/xyz
     * Type: float \n
     * Shape: @f$ N \times (x, y, z) @f$
     *
     * This dataset contains positions of all landmarks belonging
     * to a @ref hdf_geometry.
     * N is a variable number in multiples of chunk size.
     *
     * @f$(x, y, z)@f$ is the position vector.
     *
     * *** */
    typedef vigra::MultiArray<1, Vector3f> LandmarkPositions;


    static const unsigned int LANDMARK_CHUNK_SIZE = 1<<16;
    static const int CHUNK_CHUNK_SIZE = 1024;


    typedef SharedDataPointer<GeometryDataHDF> Ptr;

    /**
     * @page hdf
     *
     * ***********
     * @subsection hdf_geometry_chunks_chunks /geometry/chunks/chunks
     * Type: uint32 \n
     * Shape: @f$ N \times (\text{type}, \text{children}) @f$
     * Chunking: @f$ (1024, 2) @f$ \n
     *
     *
     * This dataset contains a list of contigous chunks.
     * This can be used for faster lookups.
     *
     * @todo Describe the data structure of the tree
     *
     */
    typedef vigra::MultiArray<2, vigra::UInt32> ChunkHeader;

    /**
     * @page hdf
     *
     * ***********
     * @subsection hdf_geometry_chunks_chunks /geometry/chunks/state
     * Type: double \n
     * Shape: @f$ N \times (r_1, r_2, r_3, b_1, b_2, b_3) @f$
     * Chunking: @f$ (1024, 6) @f$ \n
     *
     * For each chunk tree entry, @f$ \vec{r} @f$ specifies the center of the bounding box
     * with edge lengths @f$ \vec{b} @f$. This bounding box encloses all points within the chunk.
     *
     */
    typedef vigra::MultiArray<2, double> ChunkState;


    typedef vigra::MultiArray<1,Id::IdType> IdAttributeType;

    /** defines a shared pointer on this object such that the specific referece counter
     * for the cache will be tracked */
    typedef SharedDataPointer<GeometryDataHDF, hookers::hdf5::File> CacheLock;


private:
	SharedDataPointer<ProjectDataHDF> p;

    int m_id;

    Id m_uuid;

	bool m_valid;
	std::string m_label;

    std::atomic_int m_cacheUsage;
    hookers::hdf5::File m_gfile;
    bool m_modified;


    mutable std::unordered_map<int, WeakSharedDataPointer<PartDataHDF> > m_partPointer;

    /** @defgroup Chunk and Landmark Management Variables */
    /** @{ */
    mutable std::unordered_map<int, WeakSharedDataPointer<GeometryChunkDataHDF>> m_chunkPointer;

    /** @} */

    /** @name Tree Management */
    /** @{ */
    ChunkHeader m_chunkHeader;
    ChunkState m_chunkState;
    class ChunkHeaderFields { public: enum {Type, Children};};
    class ChunkStateFields { public: enum {R1, R2, R3, B1, B2, B3};};
    /** @} */



private:



//	/**
//	 *
//	 * @return a cache data
//	 *
//	 * If you want to access the CacheData, always retrieve the date with this getter
//	 * as it makes sure the cache entry is valid.
//	 *
//	 * If you require the existence within a whole scope, consider a shared copy:
//	 * @code
//	 * SequenceCacheObject::Ptr data = d();
//	 * @endcode
//	 *
//	 * @note function is thread-safe
//	 *
//	 *
//	 */
//	SequenceCacheObject::Ptr d();


    std::string fileName() const;
    std::string filePath() const;
    SharedDataPointer<GeometryChunkDataHDF> addChunk();

public:
    SharedDataPointer<GeometryChunkDataHDF> getChunk(unsigned id) const;
    std::vector<SharedDataPointer<GeometryChunkDataHDF>> getChunks() const;

    Landmark getLandmark(int id) const;
    LandmarkList getLandmarks(const std::list<int> &ids) const;
    SharedDataPointer<LandmarkDataHDF> getLandmarkPtr(int id) const;

    SharedDataPointer<PartDataHDF> getPartPtr(int id) const;


private:

    /** loads data from HDF file when needed */
    void loadCache();

    /** saves recent changes in cache to HDF file */
    void saveCache();

    /** drops the cache data when not needed anymore */
    void dropCache();

    void addLandmarkChunk(Vector3fArrayView pos, int dir = 0);

public:

	/** creates an empty sequence without an existing hdf file */
    GeometryDataHDF(const ProjectDataHDF * project, int id, const std::string & name = std::string());

    GeometryDataHDF();

    ~GeometryDataHDF();



    template<class T> void ref();
    template<class T> void deref();


    bool isValid() const;


    Project project() const;

    inline void getPointCloud(Vector3fArray &cloud);

    void close();
    void save();
    void create();
    void load();

    Id id() const;
    std::string name() const;


    void addLandmarks(Vector3fArrayView posInput);
    void removeLandmarks(LandmarkList landmarks);

    LandmarkList radiusSearch(const Vector3f & center, double radius) const;
    std::vector<LandmarkList> nearestNeighbourSearches(const std::vector<Vector3f> & center, int k) const;

    PartList parts() const;
    Part addPart(const std::string & name);


    static std::string idNumberToString(int num) {
        std::ostringstream ss;
        ss << std::setw(8) << std::setfill( '0' ) << std::hex << num;
        return ss.str();
    }

    static int idStringToNumber(const std::string & idString) {
        int id;
        std::stringstream(idString) >> std::hex >> id;
        return id;
    }

    template<typename T>
    static void resizeArray(T & array, int dim, int increment, typename T::value_type init = typename T::value_type()) {
        typename T::difference_type newShape = array.shape();
        newShape[dim] += increment;
        T newArray(newShape);
        newArray = init;

        newArray.subarray(typename T::difference_type(0), array.shape()) = array;

        array.swap(newArray);

    }
};

template<>
inline void GeometryDataHDF::ref<hookers::hdf5::File>()
{
    if(!m_cacheUsage.fetch_add(1)) {
        this->loadCache();
    }
}

template<>
inline void GeometryDataHDF::deref<hookers::hdf5::File>()
{
    if(m_cacheUsage.fetch_sub(1) == 1) {
        this->saveCache();
        this->dropCache();
    }
}

template<> inline void GeometryDataHDF::ref<void>() {}
template<> inline void GeometryDataHDF::deref<void>() {}

} // ns interface
} // ns hookers

#endif // HOOKERS_PROJECT_GEOMETRYDATAHDF_H
