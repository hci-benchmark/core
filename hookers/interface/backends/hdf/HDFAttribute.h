#ifndef HDFATTRIBUTE_H
#define HDFATTRIBUTE_H

#include <string>
#include <list>
#include <vigra/hdf5impex.hxx>


namespace hookers {
namespace hdf5 {

/** reads attributes from arbitrary object in hdf */
class HDFAttribute
{

	hid_t m_handle;
	std::string m_name;

	hid_t m_attr;
	hid_t m_type;
	hid_t m_space;
	H5T_class_t m_dataclass;

	int m_ndims;
	hsize_t * m_dims;

	bool m_isValid;


public:
	HDFAttribute(hid_t object, const std::string & name);
	HDFAttribute();
	~HDFAttribute();

	bool isValid() const {
		return m_isValid;
	}

	bool setAttribute(int value);
	bool getAttribute(int & value,
					  int def = 0);

	bool setAttribute(float value);
	bool getAttribute(float & value,
					  float def = 0.0);

	bool setAttribute(double value);
	bool getAttribute(double & value,
					  double def = 0.0);

	bool setAttribute(const std::string & value);
	bool getAttribute(std::string & value,
					  const std::string & def =  std::string());

};

}
}

#endif // HDFATTRIBUTE_H
