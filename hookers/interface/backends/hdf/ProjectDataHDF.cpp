#include <string>
#include <sstream>
#include <list>
#include <vector>
#include <algorithm>

#include <vigra/hdf5impex.hxx>

#include <hookers/interface/Project.h>
#include <hookers/interface/Sequence.h>

#include "ProjectDataHDF.h"
#include "GeometryDataHDF.h"
#include "PartDataHDF.h"
#include "SequenceDataHDF.h"

#include "HDFGroup.h"

using namespace vigra;
using namespace hookers::interface;
using namespace hookers::hdf5;

ProjectDataHDF::ProjectDataHDF()
	: m_valid(false), m_modified(false)
{

}

void ProjectDataHDF::loadFromFile()
{


    HDFGroup root(HDFHandle(m_pfile.getGroupHandle("/"), &H5Gclose));

	/* check if the project root exists */
	if(!root.contains("project")) {
		std::cout << "ProjectDataHDF(): no project group." << std::endl;
		return;
	}

	HDFGroup project = root.group("project");

    IdAttributeType idData(1);
    try {
        m_pfile.readAttribute("/project", "uuid", idData);
    } catch(...) {
        std::cout << "[ERROR] Project's id could not be found in hdf file." << std::endl;
        return;
    }

    m_id = Id(idData(0));

    if(!m_id.valid()) {
        std::cout << "[ERROR] Project's id is not valid: " << m_id.toString() << std::endl;
        return;
    }

    vigra::UInt32 revision = 0;
    try {
        m_pfile.readAttribute("/project", "revision", revision);
    } catch (...) {
        std::cout << "[ERROR] Projects's (" << m_name << ") revision could not be found in hdf file." << std::endl;
        return;
    }

    if(revision != Project::REVISION) {
        std::cout << "[ERROR] Project's (" << m_name << ") revision (" << revision << ")"
                  << "is not equal to Project::REVISION(" << Project::REVISION << ") Upgrade it!"
                  << std::endl;
        return;
    }



	this->loadParts();
	this->loadSequences();

	m_valid = true;
}

void ProjectDataHDF::loadParts()
{
//	HDFGroup parts(HDFHandle(m_pfile.getGroupHandle("/project/parts"), &H5Gclose));
//	std::list<std::string> partNames = parts.entries();

//	std::list<std::string>::const_iterator it = partNames.cbegin();
//	for(; it != partNames.cend(); it++) {
//		HDFGroup partGroup = parts.group(*it);

//		PartDataHDF * pd = new PartDataHDF(this, partGroup);


////		m_parts.insert(std::pair<int,PartDataHDF::Ptr>(pd->id(), PartDataHDF::Ptr(pd)));

//	}
}

void ProjectDataHDF::loadSequences()
{
    HDFGroup sequences(HDFHandle(m_pfile.getGroupHandle("/project/sequences"), &H5Gclose));
	std::list<std::string> sequenceNames = sequences.entries();

	for(auto pn : sequenceNames) {

		int id;
		std::stringstream(pn) >> id;

		HDFGroup sequenceGroup = sequences.group(pn);

		std::string sequenceLabel;
		sequenceGroup.getAttribute("label", sequenceLabel);

//		SequenceDataHDF * sd = new SequenceDataHDF(this, id, sequenceLabel);
//        sd->load();

//		m_sequences.insert(std::pair<int, Sequence>(id, *sd));
	}

}

void ProjectDataHDF::createFileStructure()
{
    HDFGroup root(m_pfile.getGroupHandle("/"));
	root.mkdir("project");
	root.mkdir("project/sequences");
    root.mkdir("project/geometries");

    IdAttributeType idData(1);
    idData(0) = m_id.data();
    m_pfile.writeAttribute("/project", "uuid", idData);

    /* save interface revision */
    m_pfile.writeAttribute("/project", "revision", vigra::UInt32(Project::REVISION));

}

Sequence ProjectDataHDF::getSequence(int id)
{

    SharedDataPointer<SequenceDataHDF> d = this->getSequencePtr(id);
    if(!d)
        return Sequence();
    return *(d.data());

}

SharedDataPointer<SequenceDataHDF> ProjectDataHDF::getSequencePtr(int id) const
{
    SharedDataPointer<SequenceDataHDF> d = m_sequencePointer[id].lock();

    /* we couldn't get a lock. so create a new instance */
    if(!d) {
        d = new SequenceDataHDF(this, id);
        d->loadMeta();
        m_sequencePointer[id] = WeakSharedDataPointer<SequenceDataHDF>(d);
    }
    return d;
}

SharedDataPointer<GeometryDataHDF> ProjectDataHDF::getGeometryPtr(int id) const
{
    SharedDataPointer<GeometryDataHDF> d = m_geometryPointer[id].lock();

    /* we couldn't get a lock. so create a new instance */
    if(!d) {
        d = new GeometryDataHDF(this, id);
        try {
            d->load();
        } catch (...) {
            /* something has happend here. */
        }
        m_geometryPointer[id] = WeakSharedDataPointer<GeometryDataHDF>(d);
    }

    return d;
}

Geometry ProjectDataHDF::getGeometry(int id) const
{

    SharedDataPointer<GeometryDataHDF> d = this->getGeometryPtr(id);
    if(!d)
        return Geometry();
    return *d;
}

Part ProjectDataHDF::getPart(int id)
{
//	SharedDataPointer<PartDataHDF> d = m_partPointer[id].lock();

//	/* we couldn't get a lock. so create a new instance */
//	if(!d) {
//		d = new PartDataHDF(this, id);
//        d->load();
//		m_partPointer[id] = WeakSharedDataPointer<PartDataHDF>(d);
//	}

//	return *d;
    return Part();

}

ProjectDataHDF::~ProjectDataHDF()
{
	/* by default we are saving data */
	this->save();

//	std::cout << "ProjectDataHDF::~ProjectDataHDF()" << std::endl;
}

bool ProjectDataHDF::isValid() const
{
	return m_valid;
}

bool ProjectDataHDF::isModified() const
{
    return m_modified;
}

GeometryList ProjectDataHDF::geometries() const
{
    GeometryList list;
    std::list<std::string> parts = m_root.group("/project/geometries").entries();

    for(auto & entry : parts) {
        int id = GeometryDataHDF::idStringToNumber(entry);

        list.push_back(this->getGeometry(id));
    }

    return list;

}

Geometry ProjectDataHDF::addGeometry(const std::string &name)
{

    HDFGroup sequences(m_pfile.getGroupHandle("/project/geometries"));
    std::list<std::string> sequencesEntries = sequences.entries();

    /* check if a geometry with given name already exists */
    if(std::find(sequencesEntries.cbegin(),
                 sequencesEntries.cend(), name) != sequencesEntries.cend())
        return Geometry();

    /* get the highest possible id */
    int newId = 1;
    for(auto it = sequencesEntries.cbegin();
        it != sequencesEntries.cend(); it++) {

        /* get id from part name */
        int id = GeometryDataHDF::idStringToNumber(*it);
        newId = std::max(newId, id+1);
    }


    GeometryDataHDF * sd = new GeometryDataHDF(this, newId, name);
    sd->create();

    m_geometryPointer[newId] = WeakSharedDataPointer<GeometryDataHDF>(sd);
    return *sd;

}


SequenceList ProjectDataHDF::sequences() const
{
    SequenceList list;
    std::list<std::string> sequences = m_root.group("/project/sequences").entries();

    for(auto & entry : sequences) {
        int id = SequenceDataHDF::idStringToNumber(entry);
        list.push_back(this->getSequencePtr(id).data());
    }

    return list;
}

Sequence ProjectDataHDF::addSequence(const std::string &name)
{

    /* there is already a sequence with given label */
    if(this->sequences().byLabel(name).isValid())
        return Sequence();

    HDFGroup sequences(m_pfile.getGroupHandle("/project/sequences"));
    std::list<std::string> sequencesEntries = sequences.entries();

    /* get the highest possible id */
    int newId = 0;
    for(auto it = sequencesEntries.cbegin();
        it != sequencesEntries.cend(); it++) {

        /* get id from sequence name */
        int id = SequenceDataHDF::idStringToNumber(*it);
        newId = std::max(newId, id+1);
    }


	SequenceDataHDF * sd = new SequenceDataHDF(this, newId, name);
	sd->create();

	m_sequencePointer[newId] = WeakSharedDataPointer<SequenceDataHDF>(sd);
	return *sd;
}


void ProjectDataHDF::save()
{

	/** @bug parts aren't going to be saved */
//	for(auto p: parts()) {
//		p.save();
//	}

	/** @bug sequences aren't going to be saved */
//	for(auto s: m_sequencePointer)
//		s.second.save();

    m_pfile.flush();
}

Project ProjectDataHDF::create(const std::string &path, const std::string &name)
{

	ProjectDataHDF * data = new ProjectDataHDF();

    data->m_id = Id::create();
	data->m_path = path;
	data->m_name = name;


	Project project = *data;


	/* make sure delimiter is set when needed */
	std::string fullPath = path;
	if(!fullPath.empty())
		fullPath.append("/");

	try {
        std::string filePath = fullPath+name+".hp";
        data->m_pfile.open(filePath, hdf5::File::OpenMode::New);
        data->m_root = HDFGroup(HDFHandle(data->m_pfile.getGroupHandle("/"), &H5Gclose));
		data->createFileStructure();
        data->m_valid = true;
	} catch (...) {
		project = Project();
	}

	return project;
}

Project ProjectDataHDF::open(const std::string &path, const std::string &name)
{


    /* assemble file path */
	std::string fullPath = path;
	if(!fullPath.empty())
		fullPath.append("/");
    std::string filePath = fullPath+name+".hp";

    ProjectDataHDF * data = 0;
	try {

        /* check if the file exist, before we open it with hdf api.
         * This should avoid file creation, because we would open it
         * for read/write */
        {
            std::ifstream testFile;
            testFile.open(filePath);
            if(!testFile.is_open())
                throw std::runtime_error("File does not exist.");
        }

        /* we can try to create a new project instance and open the project's
         * hdf file. */
        data = new ProjectDataHDF();
        data->m_path = path;
        data->m_name = name;
        data->m_pfile.open(filePath, hdf5::File::OpenMode::ReadWrite);
        data->m_root = HDFGroup(HDFHandle(data->m_pfile.getGroupHandle("/"), &H5Gclose));
		data->loadFromFile();

        return *data;

    } catch (std::exception & e) {

        if(data)
            delete data;

        std::cout << "ProjectDataHDF::open(): Failed to open file " << path
                  << std::endl << e.what() << std::endl;

	}

    return Project();
}
