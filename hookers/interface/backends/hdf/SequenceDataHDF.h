#ifndef BOSCHGT_SEQUENCE_DATA_H
#define BOSCHGT_SEQUENCE_DATA_H

#include <unordered_map>
#include <string>
#include <vigra/hdf5impex.hxx>


#include <hookers/interface/SequenceData.h>
#include <hookers/interface/TrackList.h>

#include "ProjectDataHDF.h"
#include "HDFFile.h"
#include "Vigra.h"

#include <iostream>
#include <limits>

namespace hookers {
namespace interface {

class TrackDataHDF;
class FrameDataHDF;
class Sequence;

/**
 * @page hdf
 *
 * @latexonly
 * \newpage
 * @endlatexonly
 *
 * ********
 * ********
 * @section hdf_sequences_sequence /sequence/sequence/
 *
 * @anchor hdf_sequences_sequence__revision
 * @anchor hdf_sequences_sequence__id
 * @h5attr{revision,uint} Revision number of the sequence file.
 * @h5attr{uuid,v16uint8} Unique id of the sequence. \n
 *   Must be unique throughout a project. Used to check against uuid in project file.
 * @h5attr{projectUuid,v16uint8} Unique id of project. \n
 *
 * @revision{1}
 *
 * This is the root group of a sequence file.
 *
 * A the file name for each sequence should be derived like the following:
 *
 * `{project}_{seqnumber}.bgts`
 *
 * where \e {project} is the project name and \e {seqnumber} is the 5 digit number of the sequence.
 *
 * When opening a sequence file, the following should be checked:
 *
 * - revision number is equal to the c++ interfaces' revision number
 * - uuid of sequence file is the same as the uuid registered in the project file
 * - projectUuid is the same as the project files uuid.
 *
 * If any of these don't match, the sequence instance should be invalid.
 *
 * ***********
 *
 * @subsection hdf_sequence_geometry /sequence/geometries/
 *
 * Contains linking information between sequence and geometries.
 *
 *
 * The subgroups which belong to tracks, frames, etc. can be found at their
 * respective parts where theses object are defined.
 *
 *
 * *** */
class SequenceDataHDF : public SequenceData
{


	friend class FrameDataHDF;
	friend class TrackDataHDF;


public:
    static const int TRACK_CHUNK_SIZE = 32768;
    static const int FRAME_CHUNK_SIZE = 1024;
    static const int GEOMETRY_CHUNK_SIZE = 1;

    static const uint32_t OBJECT_MAX_NUMBER = 0xFFFFFFFF;
	static const int32_t TRACK_MAX_NUMBER = 0xEFFFFFFF;
	static const uint16_t FRAME_VIEWS_MAX_NUMBER = 0xFFFF;

    template<class T, unsigned N>
    using TV = vigra::TinyVector<T,N>;

    template<unsigned N, class T>
    using MA = vigra::MultiArray<N,T>;

    typedef MA<2,vigra::UInt32> GeometriesHeader;


    class TrackHeaderFields { public: enum { Type = 0, CreationContext, ModificationContext, MAX}; };
    typedef vigra::MultiArray<1, vigra::TinyVector<vigra::Int32, int(TrackHeaderFields::MAX)>> TrackHeader;
    typedef Vector3dArray TrackPositions;
    typedef vigra::MultiArray<3, hookers::hdf5::UInt64> TrackAnnotations;


    class FrameHeaderFields { public: enum {Status, Views, MAX}; };
    typedef vigra::MultiArray<1, vigra::TinyVector<vigra::UInt16, int(FrameHeaderFields::MAX)> > FrameHeader;

    typedef vigra::MultiArray<2, double> FrameTime;
    typedef vigra::MultiArray<2, Vector6d> FramePose;
    typedef vigra::MultiArray<2, Vector4d> FrameIntrinsics;
    typedef vigra::MultiArray<2, Vector6d> FrameStereoTranslations;


    /** @name FrameView Bookkeeping */
    /** @{ */
    class FrameViewDimension {
    public: enum { W = 0, H, C, V, N, MAX };
    };
    typedef vigra::MultiArrayShape<int(FrameViewDimension::MAX)>::type FrameViewShape;
    typedef vigra::MultiArray<FrameViewDimension::MAX, Image::value_type> FrameView;
    typedef vigra::MultiArrayView<FrameViewDimension::MAX, Image::value_type> FrameViewView;

    class FrameViewHeaderDimensions { public: enum {D, V, N};};
    class FrameViewHeaderFields { public: enum {Status, Width, Height, Tracks, MAX}; };
    typedef MA<2, TV<vigra::UInt16, int(FrameViewHeaderFields::MAX)> >FrameViewHeader;


    /** @} */


    class FrameTrackDimensions { public: enum { D = 0, T, V, N, MAX};};
    class FrameTrackHeaderFields { public: enum {TrackId = 0, Context, MAX}; };
    class FrameTrackPositionFields {public: enum {x, y, MAX}; };
    typedef vigra::MultiArray<int(FrameTrackDimensions::MAX)-2, TV<vigra::Int32, FrameTrackHeaderFields::MAX> >  FrameTrackHeader;
    typedef vigra::MultiArray<int(FrameTrackDimensions::MAX)-2, TV<vigra::Int32, FrameTrackHeaderFields::MAX> >  FrameTrackHeaderView;
    typedef vigra::MultiArray<int(FrameTrackDimensions::MAX)-2, TV<float, FrameTrackPositionFields::MAX> >  FrameTrackPositions;
    typedef vigra::MultiArray<int(FrameTrackDimensions::MAX)-2, TV<float, FrameTrackPositionFields::MAX> >  FrameTrackPositionsView;

    typedef vigra::MultiArrayShape<int(FrameTrackDimensions::MAX)>::type FrameTrackShape;


    typedef vigra::MultiArray<1,Id::IdType> IdAttributeType;

    /** defines a shared pointer on this object such that the specific referece counter
     * for the cache will be tracked */
    typedef SharedDataPointer<SequenceDataHDF, vigra::HDF5File> CacheLock;

private:
	SharedDataPointer<ProjectDataHDF> p;

	int m_id;
    bool m_valid;
    std::string m_label;
    double m_time;
    bool m_modified;

    Id m_uuid;


    std::atomic_int m_cacheUsage;

    hookers::hdf5::File m_sfile;

	/** @brief HDF file contains computed information of sequence eg. depth map.
	*/
	hookers::hdf5::File m_cfile;
    bool m_cfileIsOpen;


    /** @defgroup track_vars Track Management Variables */
    /** @{ */

    /** tracks the number of tracks that referencing this sequence */
    std::atomic_int m_trackUsage;



    GeometriesHeader m_geometriesHeader;



    /**
     * @brief stores the position of the last used slot in the memory table.
     *
     * this is usefull when adding a bunch of tracks.
     * @warning reset this hint if you delete a track */
    int m_trackInsertionHint;

    CacheLock m_trackCacheLock;
    mutable std::unordered_map<int, WeakSharedDataPointer<TrackDataHDF> > m_trackPointer;
    TrackHeader m_trackHeader;
    TrackAnnotations m_trackAnnotations;
    TrackPositions m_trackPositions;
    /** @} */

    /** @defgroup frame_vars Frame Management Variables */
    /** @{ */

    /** tracks the number of frames that referencing this sequence */
    std::atomic_int m_frameUsage;

    /**
     * @brief stores the position of the last used slot in the memory table.
     *
     * this is usefull when adding a bunch of tracks.
     * @warning reset this hint if you delete a track */
    int m_frameInsertionHint;

    CacheLock m_frameCacheLock;
    mutable std::unordered_map<int, WeakSharedDataPointer<FrameDataHDF> > m_framePointer;
    FrameHeader m_frameHeader;

    FramePose m_framePose;
    FramePose m_framePoseUncertainty;
    FrameTime m_frameTime;
    FrameIntrinsics m_frameIntrinsics;
    FrameIntrinsics m_frameIntrinsicsUncertainty;

    FrameStereoTranslations m_frameViewTransformation;
    FrameStereoTranslations m_frameViewTransformationUncertainty;

    FrameViewHeader m_frameViewHeader;

    /** @} */


private:



//	/**
//	 *
//	 * @return a cache data
//	 *
//	 * If you want to access the CacheData, always retrieve the date with this getter
//	 * as it makes sure the cache entry is valid.
//	 *
//	 * If you require the existence within a whole scope, consider a shared copy:
//	 * @code
//	 * SequenceCacheObject::Ptr data = d();
//	 * @endcode
//	 *
//	 * @note function is thread-safe
//	 *
//	 *
//	 */
//	SequenceCacheObject::Ptr d();


    std::string fileName() const;
    std::string sequenceFilePath() const;
    std::string dataFilePath() const;

    Track getTrack(int id) const;
    TrackList getTracks(const std::list<int> &ids) const;

    Frame getFrame(int id);
    SharedDataPointer<FrameDataHDF> getFramePtr(int id) const;


    unsigned geometryRegistry(const GeometryDataHDF *gd) const;

    /**
     * returns the number of the geometry for local geometry
     * tables */
    unsigned geometryRegistry(GeometryDataHDF * gd);

public:

	/** creates an empty sequence without an existing hdf file */
    SequenceDataHDF(const ProjectDataHDF * project, int id, const std::string & name = std::string());

	SequenceDataHDF();

	~SequenceDataHDF();



    template<class T> void ref();
    template<class T> void deref();


    bool isValid() const;


    Project project() const;

    void close();
    void save();
    void create();
    void load();
    void unload() {}
    void loadMeta();

    /** loads data from HDF file when needed */
    void loadCache();

    /** saves recent changes in cache to HDF file */
    void saveCache();

    /** drops the cache data when not needed anymore */
    void dropCache();

    /**
     * @brief opens the auxiliary data file
     * if the file is not opened yet, it will open it.
     */
    void loadData();


    Id id() const;
    std::string name() const;

    double time() const;
    void setTime(const double & time);


    FrameList frames() const;
	Frame addFrame();

    TrackList tracks() const;
	Track addTrack();
    void removeTrack(TrackData *td);


    inline void setModified() {
        m_modified = true;
    }


    static std::string idNumberToString(int num) {
        std::ostringstream ss;
        ss << std::setw(8) << std::setfill( '0' ) << std::hex << num;
        return ss.str();
    }

    static int idStringToNumber(const std::string & idString) {
        int id;
        std::stringstream(idString) >> std::hex >> id;
        return id;
    }


    template<typename T>
    static void resizeArray(T & array, int dim, int increment, typename T::value_type init = typename T::value_type()) {
        typename T::difference_type newShape = array.shape();
        newShape[dim] += increment;
        T newArray(newShape);
        newArray = init;

        newArray.subarray(typename T::difference_type(0), array.shape()) = array;

        array.swap(newArray);

    }
};

template<>
inline void SequenceDataHDF::ref<TrackDataHDF>()
{
    if(!m_trackUsage.fetch_add(1)) {
        /* aquire a lock on the cache */
        m_trackCacheLock = CacheLock(this);
    }    
}

template<>
inline void SequenceDataHDF::deref<TrackDataHDF>()
{
    /* if the track usage counter drops to zero,
     * we can safely remove any required caches and clear
     * lists associated with frames */
    if(m_trackUsage.fetch_sub(1) == 1) {

        /* remove the lock from the cache data */
        m_trackCacheLock.reset();

        /* clean up the track instance lookup */
        m_trackPointer.clear();
    }
}

template<>
inline void SequenceDataHDF::ref<FrameDataHDF>()
{
    if(!m_frameUsage.fetch_add(1)) {
        /* aquire a lock on the cache */
        m_frameCacheLock = CacheLock(this);
    }
}

template<>
inline void SequenceDataHDF::deref<FrameDataHDF>()
{
    /* if the frame usage counter drops to zero,
     * we can safely remove any required caches and clear
     * lists associated with frames */
    if(m_frameUsage.fetch_sub(1) == 1) {

        /* remove the lock from the data cache */
        m_frameCacheLock.reset();

        /* clean up the frame instance lookup */
        m_framePointer.clear();
    }
}

template<>
inline void SequenceDataHDF::ref<vigra::HDF5File>()
{
    if(!m_cacheUsage.fetch_add(1)) {
        this->loadCache();
    }
}

template<>
inline void SequenceDataHDF::deref<vigra::HDF5File>()
{
    if(m_cacheUsage.fetch_sub(1) == 1) {
        this->saveCache();
        this->dropCache();
    }
}

template<> inline void SequenceDataHDF::ref<void>() {}
template<> inline void SequenceDataHDF::deref<void>() {}

} // ns interface
} // ns hookers

#endif // BOSCHGT_SEQUENCE_DATA_H
