#include <assert.h>

#include "GeometryDataHDF.h"
#include "GeometryChunkDataHDF.h"
#include "PartDataHDF.h"
#include "LandmarkDataHDF.h"

#include <hookers/interface/TrackList.h>

using namespace hookers;
using namespace hookers::interface;

LandmarkDataHDF::LandmarkDataHDF(const GeometryChunkDataHDF *part,
                                 unsigned id, unsigned row)
    : c(part), m_id(id), row(row)
{
	assert(part);
}

TrackList LandmarkDataHDF::tracks() const {
    throw std::string("LandmarkDataHDF::tracks() not implemented");
}

LandmarkDataHDF::LandmarkDataHDF()
    : c(0), m_id(0), row(0)
{}


Vector3d LandmarkDataHDF::pos() const
{
    if(!c)
        return Vector3d();
    return c->xyz()(row);

}

void LandmarkDataHDF::invalidate()
{
    if(!c)
        return;
    c->points()(row) = Landmark::INVALID;
    c->m_modified = true;
}

hookers::hdf5::UInt64 LandmarkDataHDF::id() const {
    hookers::hdf5::UInt64 uid = c->g->m_id;
    uid = uid << 32;
    return uid+m_id;
}
