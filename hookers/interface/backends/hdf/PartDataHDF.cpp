#include <assert.h>

#include <hookers/interface/LandmarkList.h>

#include "SequenceDataHDF.h"
#include "PartDataHDF.h"
#include "ProjectDataHDF.h"
#include "LandmarkDataHDF.h"

using namespace hookers::hdf5;
using namespace hookers::interface;

const std::string PartDataHDF::HDF_PART_PATH_PREFIX = "/geometry/collections";

PartDataHDF::PartDataHDF(const GeometryDataHDF * project, int id, const std::string & name)
    : g(project), m_id(id), m_label(name),
      m_modified(false)
{

}

PartDataHDF::PartDataHDF(const GeometryDataHDF *project, int id)
    : g(project), m_id(id), m_modified(false)
{

}

Landmark PartDataHDF::getLandmark(int id) const
{

    if(id >= m_header.height())
        return Landmark();

//    SharedDataPointer<LandmarkDataHDF> d = m_landmarkPointer[id].lock();
//    if(!d) {
//        d = new LandmarkDataHDF(this, id);

//        /* note that d currently has not been shared so far.
//         * so we are still owner of the object */
//        m_landmarkPointer[id] = WeakSharedDataPointer<LandmarkDataHDF>(d);
//    }

    /** @todo implement new part concept */
    return Landmark();
}

PartDataHDF::PartDataHDF()
{
}

PartDataHDF::~PartDataHDF()
{
    this->save();
}

void PartDataHDF::create()
{

    m_header = PartHeader(PART_CHUNK_SIZE);

    /* get group reference to parts group */
    HDFGroup parts(g->m_gfile.getGroupHandle(HDF_PART_PATH_PREFIX));

    /* get id as zero paddded string */
    std::string idString = idNumberToString(m_id);

    /* create part group entry if it does not exist */
    if(!parts.contains(idString))
        parts.mkdir(idString);


    {
        auto shape = m_header.expandElements(0).shape();
        auto chunking = shape;
        auto max = shape;

        chunking[1] = PART_CHUNK_SIZE;
        max[1] = H5S_UNLIMITED;

        std::string partPath = HDF_PART_PATH_PREFIX + "/" + idNumberToString(m_id)+"/points";

        std::cout << "part path: " << partPath << std::endl;

        g->m_gfile.createDataset<hookers::hdf5::UInt32, 2>(partPath,
                                                           shape, 0, chunking, max,
                                                           0);
    }

    m_modified = true;
    this->save();
}

void PartDataHDF::load()
{
    std::string partPath = HDF_PART_PATH_PREFIX+"/"+idNumberToString(m_id);

    HDFGroup part(g->m_gfile.getGroupHandle(partPath));
    part.getAttribute("label", m_label);

    g->m_gfile.readAndResize(partPath+"/points", m_header);


}

void PartDataHDF::close()
{

	if(m_modified)
		std::cout << "PartDataHDF::close(): Closing modified part " << this->name() << std::endl;

}

std::string PartDataHDF::name() const
{
	return m_label;
}

bool PartDataHDF::isValid() const
{
	return true;
}

void PartDataHDF::addLandmark(const LandmarkData * l)
{

    using namespace vigra::multi_math;

    const LandmarkDataHDF * landmark = static_cast<const LandmarkDataHDF*>(l);

    for(int i = 0; i <= m_header.size(); i++) {

        if(i >= m_header.size()) {
            SequenceDataHDF::resizeArray(m_header, 0, PART_CHUNK_SIZE);
        }

        if(m_header(i)[0] != Landmark::INVALID)
            continue;

        m_header(i)[0] = Landmark::MANUAL;
        m_header(i)[1] = landmark->m_id;

        m_modified = true;
        return;
    }
}

LandmarkList PartDataHDF::landmarks() const
{

    GeometryDataHDF::CacheLock lock(g);
	LandmarkList lml;
    int landmarkSize = m_header.size();
    for(int i = 0; i < landmarkSize; i++) {

        /* if the header states an invalid entry, we safely can discard this here */
        if(m_header(i)[0] == Landmark::INVALID)
            continue;
        lml.push_back(g->getLandmark(m_header(i)[1]));
    }
    return lml;
}


void PartDataHDF::save()
{

	/* check some things that shouldn't happen in productive enviroment */
	assert(!m_label.empty());
	assert(m_id < (2^16));
    assert(g);

	/* make sure we don't save anything if we aren't modified */
	if(!m_modified)
		return;



    /* get id as zero paddded string */
    std::string idString = idNumberToString(m_id);

    /* get part group */
    HDFGroup partGroup(g->m_gfile.getGroupHandle(HDF_PART_PATH_PREFIX+"/"+idString));
    partGroup.setAttribute("label", m_label);


    /* make sure we create datasets that contain landmark data */
    auto offset = m_header.expandElements(0).shape();
    offset = 0;
    g->m_gfile.write(partGroup.path()+"/points", offset, m_header.expandElements(0));
    g->m_gfile.flush();

    m_modified = false;

}


