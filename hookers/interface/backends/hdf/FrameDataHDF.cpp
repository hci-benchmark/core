#include <cassert>

#include <hookers/interface/Project.h>
#include <hookers/interface/Sequence.h>

#include "FrameDataHDF.h"
#include "SequenceDataHDF.h"
#include "TrackDataHDF.h"

using namespace hookers;
using namespace hookers::interface;

const std::string FrameDataHDF::FRAME_BASE_PATH = "/sequence/frames";

FrameDataHDF::FrameDataHDF(const SequenceDataHDF *sequence, int id)
    : s(sequence), m_id(id), m_modified(false), m_valid(false)
{
    assert(sequence);
    assert(id >= 0);

    /* initialize cache usage counter */
    m_cacheUsage = 0;
}

FrameDataHDF::FrameDataHDF()
    : m_id(-1), m_modified(false), m_valid(false)
{
    /* initialize cache usage counter */
    m_cacheUsage = 0;
}


void FrameDataHDF::create()
{

    FrameDataHDF * fco = this;

    fco->trackHeader = TrackHeader(0, 2);
    fco->trackPositions = TrackPositions(0, 2);


//    this->saveCache();
//    this->dropCache();


    fco->m_valid = true;

}

void FrameDataHDF::load()
{

    /** @todo check if the mandatory data structured on frames exist */
    m_valid = true;
}

void FrameDataHDF::loadCache()
{
    FrameDataHDF * fco = this;
    SequenceDataHDF * sco = s.data();

    /**
     * This function checks if we can open the frame group and leaves
     * the instance invalid if it cannot be found.
     *
     * It can fail due to two reasons:
     * - The HDF file is fucked up. Leaving the FCO uninitialized and flagged as
     *   invalid is a sane choice.
     * - The Frame has recently been created and a FCO has been requested by loadCache().
     *   In this case initializing of FCO will be handled by create() and we don't
     *   have to bother here.
     *
     *
     */

    try {


        /* create instances of tracktables according to the size found in the header */

        FVHT tracks = this->getMaxTracks();


        int views = sco->m_frameHeader(m_id)[FHF::Views];
        fco->trackHeader = TrackHeader(tracks, views);
        fco->trackPositions = TrackPositions(tracks, views);

        SequenceDataHDF::FrameTrackShape offset(0);
        offset[FTD::N] = m_id;

        sco->m_sfile.read(FRAME_BASE_PATH+"/views/tracks", offset, fco->trackHeader.expandElements(0));
        sco->m_sfile.read(FRAME_BASE_PATH+"/views/tracksPosition", offset, fco->trackPositions.expandElements(0));

    }
    catch(std::runtime_error & e) {
        std::cerr << "[ERROR]: Could not inizialize Frame "
                  << this->m_id << ": " << e.what() << std::endl;
        m_valid = false;
    }
    catch (...) {
        std::cerr << "[ERROR]: Could not inizialize Frame "
                  << this->m_id << std::endl;
        m_valid = false;
    }


}

void FrameDataHDF::saveCache()
{
    SequenceDataHDF * sco = s.data();
    FrameDataHDF * fco = this;

    /* well, if we have done no modification to the cache or frame in general, then we don't
     * need to save anything */
    if(!m_modified)
        return;

    SequenceDataHDF::FrameTrackShape offset(0);
    offset[SequenceDataHDF::FrameTrackDimensions::N] = m_id;

    sco->m_sfile.write(FRAME_BASE_PATH+"/views/tracks", offset, fco->trackHeader.expandElements(0));
    sco->m_sfile.write(FRAME_BASE_PATH+"/views/tracksPosition", offset, fco->trackPositions.expandElements(0));

    sco->m_sfile.flush();
}

void FrameDataHDF::dropCache()
{
    /* reset all datasets */
    trackHeader = TrackHeader();
    trackPositions = TrackPositions();

    /* and the view cache */
    m_cachedView = Image();
    m_cachedStereoView = Image();
}

FrameDataHDF::~FrameDataHDF()
{
    this->save();
}

Sequence FrameDataHDF::sequence() const
{
    return *this->s.data();
}

void FrameDataHDF::save()
{

    if(!isModified())
        return;

    /* we only can save cache if the cache is in use */
    if(m_cacheUsage)
        this->saveCache();

    m_modified = false;

}

void FrameDataHDF::setView(const ImageView &image, const FrameData::View &view)
{

    const std::string dsetPath = FRAME_BASE_PATH + "/views/data";


    /* define some shortcuts to array types etc */
    typedef SequenceDataHDF::FrameViewDimension FVD;
    typedef SequenceDataHDF::FrameViewHeaderFields FVHF;
    typedef SequenceDataHDF::FrameViewShape FVS;
    typedef SequenceDataHDF::FrameViewHeaderFields FVHF;

    SequenceDataHDF * sco = s.data();

    /* create an array view for convenience */
    auto viewHeader = sco->m_frameViewHeader.bindOuter(m_id);

    FVS offset(0);
    offset[FVD::N] = m_id;
    offset[FVD::V] = int(view);

    sco->m_sfile.write(dsetPath, offset, image);
    sco->m_sfile.flush();

    viewHeader(view)[FVHF::Width] = image.width();
    viewHeader(view)[FVHF::Height] = image.height();

    m_modified = true;

    /* flag that the given view has data */
    viewHeader(view)[FVHF::Status] = 0x000f & 1;

    /* flag the sequence as modified */
    s->setModified();


    /* decide whether it is the base or the stereo view */
    auto imgPtr = &m_cachedView;
    if(view == View::Stereo) {
        imgPtr = &m_cachedStereoView;
    }

    {
        Image &img = *imgPtr;
        /* check if we need to update the frame cache. */
        if(!img.hasData()) {
            Image tmpImg(img);
            img.swap(tmpImg);
        }
    }

}

void FrameDataHDF::getView(Image &image, const FrameData::View &view) const
{

    /* define some shortcuts to array types etc */
    typedef SequenceDataHDF::FrameViewDimension FVD;
    typedef SequenceDataHDF::FrameViewShape FVS;
    typedef SequenceDataHDF::FrameViewHeaderFields FVHF;
    if (!s) {
        return;
    }
    SequenceDataHDF * sco = s.data();
    if (!sco) {
        return;
    }

    /* create an array view for convenience */
    auto viewHeader = sco->m_frameViewHeader.bindOuter(m_id);


    /* check if the requested view is set */
    hookers::hdf5::UInt16 width =  viewHeader(view)[FVHF::Width];
    hookers::hdf5::UInt16 height =  viewHeader(view)[FVHF::Height];
    hookers::hdf5::UInt8  channels = viewHeader(view)[FVHF::Status] & 0x000F;

    /** If the requested view has no entry, then this function does nothing */
    if(!channels)
        return;

    /** @warning Currently we dont't support more then one channel on the interface side. */
    if(channels > 1) {
        throw std::runtime_error("Channel Count for Views greater one is currently not supported.");
    }

    std::string frameGroupPath = "/sequence/frames";

    /* decide whether it is the base or the stereo view */
    auto imgPtr = &m_cachedView;
    if(view == View::Stereo) {
        imgPtr = &m_cachedStereoView;
    }

    {
        Image & img = *imgPtr;
        /* if we haven't read the image data yet, make sure we do */
        if(!img.data()) {
            const std::string dsetPath = frameGroupPath+"/views/data";
            try {

                ImageShape shape(0);
                shape[0] = width;
                shape[1] = height;

                FVS offset(0);
                offset[FVD::N] = m_id;
                offset[FVD::V] = int(view);

                /* read the data slice from hdf file */
                Image tmpImg(shape);
                sco->m_sfile.read(dsetPath, offset,  tmpImg);

                /* finally we have to set the data on the cache.
             * We got here, because of no cache data */
                tmpImg.swap(img);
            } catch (...) {
                std::cerr << "[INFO] Frame::getView() failed for frame " << m_id << std::endl;
            }
        }

        /* make sure that cached image and the given image have equal size */
        if(image.shape() != img.shape()) {
            image.reshape(img.shape());
        }

        /* vigra's copy function is darn fucking slow for some reason.
         * Even in release mode, there is a factor of the order 10^3...
         * So we use memcpy here... */
        memcpy(image.data(), img.data(), img.size()*sizeof(Image::value_type));
        //image.copy(boundImage);
    }

}

void FrameDataHDF::linkTrack(TrackData *td, const Vector2d &pos, const FrameData::View &view)
{
    assert(view == FrameData::View::Base || view == FrameData::View::Stereo);


    using namespace vigra::multi_math;

    TrackDataHDF * t = static_cast<TrackDataHDF*>(td);
    int trackId = t->m_id;

    typedef SequenceDataHDF::FrameTrackDimensions FTD;
    typedef SequenceDataHDF::FrameTrackHeaderFields FTHF;
    typedef SequenceDataHDF::FrameViewHeaderFields FVHF;

    /* defines a local enum that updates the dimensions according to
     * the local dimension binding */
    class BFTD { public: enum {T = FTD::T-1, V = FTD::V-1};};


    /* search if either the entry exists or if no space is left */
    bool hasSpace = vigra::multi_math::any(trackHeader.bindOuter(view).expandElements(0).bind<0>(0) == -1);
    bool entryExists = vigra::multi_math::any((trackHeader.bindOuter(view).expandElements(0).bind<0>(FTHF::TrackId) == trackId));

    /* the view dimension is still zero, we need to increase it here */
    if(trackHeader.shape()[BFTD::V] < 2) {
        int delta = 1+view-trackHeader.shape()[BFTD::V];
        SequenceDataHDF::resizeArray(trackHeader, BFTD::V, delta,
                                     TrackHeader::value_type(-1));
        SequenceDataHDF::resizeArray(trackPositions, BFTD::V, delta,
                                     TrackPositions::value_type(0.0));

    }

    /* if the entry is non-existent and we don't have any space left,
     * resize the array */
    if(!entryExists && !hasSpace) {
//        std::cout << "aquiring new chunk for frame<->track correspondence for "
//                  << trackId <<  ", " << headerSize << std::endl;
        SequenceDataHDF::resizeArray(trackHeader, BFTD::T, TRACKS_CHUNK_SIZE,
                                     TrackHeader::value_type(-1));
        SequenceDataHDF::resizeArray(trackPositions, BFTD::T, TRACKS_CHUNK_SIZE,
                                     TrackPositions::value_type(0.0));

        m_modified = true;

    }

    /* for convenience, bind the view dimension to the given view */
    auto header = trackHeader.bindOuter(view);
    auto positions = trackPositions.bindOuter(view);

    static_assert(header.actual_dimension == 1,
                  "must have only one dimension");
    int headerSize = header.size();

    for(int i = 0; i < headerSize; i++) {

        /* we know that the entry exists. so we search for
         * the position of the entry, that we want to update */
        if(entryExists &&
           (header(i)[FTHF::TrackId] != trackId))
            continue;

        /* we know that the entry does not exist. So we search
         * for the next free slot */
        if(!entryExists && (header(i)[FTHF::TrackId] != -1))
            continue;

        header(i)[FTHF::TrackId] = trackId;
        header(i)[FTHF::Context] = Project::Context();
        positions(i) = pos;


        /* flag FCO as modified. */
        m_modified = true;

        /* update the number of tracks in the Sequence's frame tables */
        {
            auto viewHeader = s->m_frameViewHeader.bindOuter(m_id);
            viewHeader(view)[FVHF::Tracks] = headerSize;
            s->setModified();
        }


        return;
    }

    /* we actually shouldn't get here */
    return;

}

void FrameDataHDF::unlinkTrack(TrackData *td, const FrameData::View &view)
{

    assert(view == FrameData::View::Base || view == FrameData::View::Stereo);

    TrackDataHDF * t = static_cast<TrackDataHDF*>(td);
    int trackId = t->m_id;

    FrameDataHDF * fco = this;    

    /* use references to tables in order to genericly link the track to either bae
     * or stereo view */
    auto header = fco->trackHeader.bindOuter(view);
    auto positions = fco->trackPositions.bindOuter(view);
    static_assert(header.actual_dimension == 1,
                  "must have only one dimension");


    int headerSize = header.size();
    for(int i = 0; i < headerSize; i++) {

        if(header(i)[FTHF::TrackId] != trackId)
            continue;

        header(i)[FTHF::TrackId] = -1;
        positions(i)[0] = -1;
        positions(i)[1] = -1;

        fco->m_modified = true;

        return;
    }

}

Vector2d FrameDataHDF::trackPosition(TrackData *td, const FrameData::View & view) const
{

    assert(view == FrameData::View::Base || view == FrameData::View::Stereo);

    TrackDataHDF * t = static_cast<TrackDataHDF*>(td);
    const int &trackId = t->m_id;

    auto header = this->trackHeader.bindOuter(view);
    auto positions = this->trackPositions.bindOuter(view);
    static_assert(header.actual_dimension == 1,
                  "must have only one dimension");


    int headerSize = header.size();
    for(int i = 0; i < headerSize; i++) {

        if(header(i)[FTHF::TrackId] != trackId)
            continue;
        return positions(i);

    }

    return Vector2d(-1, -1);
}

TrackList FrameDataHDF::tracks(const FrameData::View &view) const
{


    assert(view == FrameData::View::Base
           || view == FrameData::View::Stereo
           || view == FrameData::View::All);


    /* well, if there are absolutely no entries, then we
     * can abort here */
    if(!trackHeader.size())
        return TrackList();

    SequenceDataHDF * sco = s.data();

    TrackList trackList;
    TrackSet trackSet;



    /* handle the View::All argument
     * we need to iterate over all available entries */
    std::vector<int> views;
    if(view == FrameData::View::All) {
        int numViews = sco->m_frameHeader(m_id)[FHF::Views];
        views.clear();

        for(int i = 0; i < numViews; i++) views.push_back(i);
    } else {
         views = {int(view)};
    }


    for(const int v : views) {

        auto header = trackHeader.bindOuter(v);
        static_assert(header.actual_dimension == 1,
                      "must have only one dimension");

        int headerSize = header.width();
        for(int i = 0; i < headerSize; i++) {

            if(header(i)[FTHF::TrackId] == -1)
                continue;

            int  & tid = header(i)[FTHF::TrackId];

            Track t = sco->getTrack(tid);

            /* mind that the || depends on the order.
             * so we always pushing the value to the list
             * when view is unequal to View::All.
             * Only if view is equal to View::all then we secondly check
             * if the track is not already the the list */
            if(view != FrameData::View::All || !trackSet.contains(t)) {
                trackList.push_back(t);

                /* only if we are retrieving tracks for all views
                 * we have to make sure we don't have duplicates */
                if(view == FrameData::View::All)
                    trackSet.insert(t);
            }

        }
    }

    return trackList;

}

Vector6d FrameDataHDF::pose() const
{
    if(!isValid())
        return Vector6d();

    return s->m_framePose(m_id, 0);
}

void FrameDataHDF::setPose(const Vector6d &pose)
{
    if(!isValid())
            return;

    /** @todo add geometry here */
    s->m_framePose(m_id, 0) = pose;
    s->setModified();
    m_modified = true;

}

Vector6d FrameDataHDF::poseUncertainty() const
{
    if(!isValid())
        return Vector6d();

    return s->m_framePoseUncertainty(m_id);

}

void FrameDataHDF::setPoseUncertainty(const Vector6d &pose)
{
    if(!isValid())
            return;

    s->m_framePoseUncertainty(m_id) = pose;
    s->setModified();
    m_modified = true;

}

double FrameDataHDF::time() const
{
    if(!isValid())
        return 0;

    return s->m_frameTime(m_id);

}

void FrameDataHDF::setTime(const double &time)
{
    if(!isValid())
            return;

    s->m_frameTime(m_id) = time;
    s->setModified();
    m_modified = true;

}

Vector4d FrameDataHDF::intrinsics(const FrameData::View& view) const
{
    if(!isValid())
            return Vector4d(0.0);
    return s->m_frameIntrinsics(view, m_id);

}

void FrameDataHDF::setIntrinsics(const Vector4d &intrinsics, const FrameData::View& view)
{
    if(!isValid())
            return;
    s->m_frameIntrinsics(view, m_id) = intrinsics;
    s->setModified();
    m_modified = true;
}

Vector4d FrameDataHDF::intrinsicsUncertainty(const FrameData::View& view) const
{
    if(!isValid())
            return Vector4d();
    return s->m_frameIntrinsicsUncertainty(view, m_id);

}

void FrameDataHDF::setIntrinsicsUncertainty(const Vector4d &intrinsics, const FrameData::View& view)
{
    if(!isValid())
            return;
    s->m_frameIntrinsicsUncertainty(view, m_id) = intrinsics;
    s->setModified();
    m_modified = true;
}

Vector6d FrameDataHDF::viewTransformation(const FrameData::View& view) const
{
    if(!isValid())
            return Vector6d(0.0);
    return s->m_frameViewTransformation(view, m_id);

}

void FrameDataHDF::setViewTransformation(const Vector6d &trans, const FrameData::View& view)
{
    if(!isValid())
            return;
    s->m_frameViewTransformation(view, m_id) = trans;
    s->setModified();
    m_modified = true;
}

Vector6d FrameDataHDF::viewTransformationUncertainty(const FrameData::View& view) const
{
    if(!isValid())
            return Vector6d(0.0);
    return s->m_frameViewTransformationUncertainty(view, m_id);

}

void FrameDataHDF::setViewTransformationUncertainty(const Vector6d &trans, const FrameData::View& view)
{
    if(!isValid())
            return;
    s->m_frameViewTransformationUncertainty(view, m_id) = trans;
    s->setModified();
    m_modified = true;

}

void FrameDataHDF::writeMapMeta()
{
    SequenceDataHDF * sco = s.data();
    sco->loadData();

    /* calculate path in hdf file for new datasets */
    std::string idString = zeroPadId(m_id);
    const std::string frameGroupPath = FRAME_BASE_PATH + "/" + idString;

    /* write current sequence label to data file */
    sco->m_cfile.writeAttribute("/sequence", "label", this->sequence().name());

    /* save camera pose if available */
    {
        auto pose = this->pose();
        if(pose != Vector6d()) {
            Array1dView poseView(Array1dShape(6), pose.data());
            sco->m_cfile.writeAttribute(frameGroupPath, "pose", poseView);
        }
    }

    /** @todo view should be base actually. This is a quick hack due to data inconsistency */
    /* write perframe intrinsics */
    {
        auto intrin = this->intrinsics(Frame::View::Stereo);
        if(intrin != Vector4d()) {
            Array1dView intrinView(Array1dShape(4), intrin.data());
            sco->m_cfile.writeAttribute(frameGroupPath, "intrinsics", intrinView);
        }
    }

    /* get transformation for stereo view */
    auto stereoTrans = this->viewTransformation(Frame::View::Stereo);
    Array1dView stereoTransView(Array1dShape(6), stereoTrans.data());
    sco->m_cfile.writeAttribute(frameGroupPath, "stereoTransformation", stereoTransView);


    sco->m_cfile.flush();

}

std::string FrameDataHDF::getDataFilePath()
{
    SequenceDataHDF * sco = s.data();
    if(sco)
        return sco->dataFilePath();
    return "";
}

void FrameDataHDF::setMap(const std::string & name, const MapView & map)
{
    SequenceDataHDF * sco = s.data();
    sco->loadData();

    /* calculate path in hdf file for new datasets */
    std::string idString = zeroPadId(m_id);
    const std::string frameGroupPath = FRAME_BASE_PATH + "/" + idString;
    const std::string dsetPath = frameGroupPath + "/maps/" + name;


    /* only set a map if the name is not empty */
    if(!name.empty()) {
        /* write dataset itself */
        sco->m_cfile.write(dsetPath, map, 0, 0);
    }

    /* note that this function flushes data hdf file */
    /* write out the meta data like sequence label etc */
    this->writeMapMeta();

}

void FrameDataHDF::getMap(const std::string & name, Map & map) const
{

    SequenceDataHDF * sco = s.data();
    sco->loadData();

	/* get the path in hdf file for the  datasets */
	std::string idString = zeroPadId(m_id);
	std::string frameGroupPath = "/sequence/frames/" + idString;

    const std::string dsetPath = frameGroupPath + "/maps/" + name;
    try {
        sco->m_cfile.readAndResize(dsetPath, map);
    }
    catch (...) {
        std::cerr << "[INFO] Frame::getMap() failed for frame " << dsetPath << std::endl;
    }

}

bool FrameDataHDF::hasMap(const std::string & name)
{
	SequenceDataHDF * sco = s.data();
	sco->loadData();

	/* get hte path in hdf file for the  datasets */
	std::string idString = zeroPadId(m_id);
	std::string frameGroupPath = "/sequence/frames/" + idString;
	const std::string dsetPath = frameGroupPath + "/maps/" + name;

	return sco->m_cfile.existsDataSet(dsetPath);
}

void FrameDataHDF::setList(const std::string & name, const ListView & list)
{
	SequenceDataHDF * sco = s.data();
    sco->loadData();

	/* get the path in hdf file for new datasets */
	std::string idString = zeroPadId(m_id);
	std::string frameGroupPath = FRAME_BASE_PATH + "/" + idString;
    const std::string dsetPath = frameGroupPath + "/lists/" + name;
    sco->m_cfile.write(dsetPath, list, 0, 9);
    sco->m_cfile.flush();

}

void FrameDataHDF::getList(const std::string & name, List & list) const
{

    SequenceDataHDF * sco = s.data();
    sco->loadData();

    /* get the path in hdf file for the  datasets */
    std::string idString = zeroPadId(m_id);
    std::string frameGroupPath = "/sequence/frames/" + idString;

    const std::string dsetPath = frameGroupPath + "/lists/" + name;
    try {
        sco->m_cfile.readAndResize(dsetPath, list);
    }
    catch (...) {
        std::cerr << "[INFO] Frame::getList() failed for frame " << dsetPath << std::endl;
    }

}

bool FrameDataHDF::hasList(const std::string & name)
{
	SequenceDataHDF * sco = s.data();
	sco->loadData();

	/* get hte path in hdf file for the  datasets */
	std::string idString = zeroPadId(m_id);
	std::string frameGroupPath = "/sequence/frames/" + idString;
	const std::string dsetPath = frameGroupPath + "/lists/" + name;

	return sco->m_cfile.existsDataSet(dsetPath);
}
