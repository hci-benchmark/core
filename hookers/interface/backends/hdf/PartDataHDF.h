#ifndef PARTDATAHDF_H
#define PARTDATAHDF_H

#include <string>
#include <sstream>
#include <memory>
#include <map>

#include <vigra/multi_array.hxx>


#include <hookers/interface/PartData.h>

#include "HDFGroup.h"
//#include "ProjectDataHDF.h"
//#include "LandmarkDataHDF.h"

namespace hookers {
namespace interface {

class GeometryDataHDF;
class LandmarkDataHDF;
class Landmark;

/**
 * @page hdf
 *
 * ********
 * @section hdf_geometry_parts /geometry/collections/
 * @h5attr{label,string} label of the part (eg. a house name)
 *
 * This group contains a list of @link Part Parts @endlink.
 * It is only allowed to have @f$ 2^{16} @f$ number of parts.
 * Each part is only allowed to have @f$ 2^{32} @f$ number of landmarks.
 *
 * ********
 * @subsection hdf_geometry_collections /geometry/collections/hhhhhhhh/
 *
 * Each collection has a own group. The group name is a 5 digit number,
 * zero padded. The number must not start with zero.
 *
 */


class PartDataHDF : public PartData
{
    friend class GeometryDataHDF;
	friend class LandmarkDataHDF;
	friend class TrackDataHDF;

public:

	typedef SharedDataPointer<PartDataHDF> Ptr;

	static const std::string HDF_PART_PATH_PREFIX;

public:
    /**
     * @page hdf
     *
     * ***********
     * @subsection hdf_geometry_collections_points /geometry/collections/hhhhhhhh/points
     * Type: uint32, chunksize: 1024, expandable \n
     * Shape: @f$ N \times (status, id) @f$
     *
     *
     * *** */
    typedef vigra::MultiArray<1, vigra::TinyVector<vigra::UInt32,2>> PartHeader;
    static const int PART_CHUNK_SIZE = 1024;

private:
    PartHeader m_header;




private:

    /**
     * @brief reference to geometry object
     *
     * By specifying the caller type to hookers::hdf5::File,
     * we make sure that
     * the geometry's hdf file stays open during the lifetime of this object.
     *
     */
    SharedDataPointer<GeometryDataHDF, hookers::hdf5::File> g;

	int m_id;
	std::string m_label;


	bool m_modified;

    PartDataHDF(const GeometryDataHDF * project, int id, const std::string & name);
    PartDataHDF(const GeometryDataHDF * project, int id);


    Landmark getLandmark(int id) const;

public:


public:
	PartDataHDF();
	~PartDataHDF();


	int id() { return m_id; }

    void create();
    void load();
	void save();

	void close();
	std::string name() const;
	bool isValid() const;
    void addLandmark(const LandmarkData *l);
    LandmarkList landmarks() const;


    static std::string idNumberToString(int num) {
        std::ostringstream ss;
        ss << std::setw(8) << std::setfill( '0' ) << std::hex << num;
        return ss.str();
    }

    static int idStringToNumber(const std::string & idString) {
        int id;
        std::stringstream(idString) >> std::hex >> id;
        return id;
    }

};

} // ns interface
} // ns hookers


#endif // PARTDATAHDF_H
