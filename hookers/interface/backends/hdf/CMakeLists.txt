set(SOURCES "SequenceDataHDF.cpp"
            "GeometryDataHDF.cpp"
            "GeometryChunkDataHDF.cpp"
            "ProjectDataHDF.cpp"
            "PartDataHDF.cpp"
            "LandmarkDataHDF.cpp"
            "FrameDataHDF.cpp"
            "TrackDataHDF.cpp"

            "HDFFile.cpp"
            "HDFGroup.cpp"
            "HDFDataset.cpp"
            "HDFAttribute.cpp"

   )
set(HEADERS "HDFHandle.h"
            "Vigra.h"
   )

add_files(libInterface_FILES ${SOURCES} ${HEADERS})

