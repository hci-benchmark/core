#ifndef HDFFILE_H
#define HDFFILE_H

#include <string>
#include <type_traits>

#include <vigra/timing.hxx>
#include <vigra/hdf5impex.hxx>
#include <vigra/multi_array.hxx>

#include "HDFHandle.h"
#include "HDFDataset.h"

namespace hookers {
namespace hdf5 {

class File
{
public:
    enum class OpenMode {
        ReadOnly,
        ReadWrite,
        New
    };


    /** stolen from vigra hdf5 impex */
    class SplitString: public std::string {
    public:
        SplitString(std::string &sstring): std::string(sstring) {}

        // return the part of the string before the delimiter
        std::string first(char delimiter = '/')
        {
            size_t last = find_last_of(delimiter);
            if(last == std::string::npos) // delimiter not found --> no first
                return "";

            return std::string(begin(), begin()+last+1);
        }

        // return the part of the string after the delimiter
        std::string last(char delimiter = '/')
        {
            size_t last = find_last_of(delimiter);
            if(last == std::string::npos) // delimiter not found --> only last
                return std::string(*this);
            return std::string(begin()+last+1, end());
        }
    };

    /** stolen from vigra hdf5 impex */

    template<unsigned int N>
    std::vector<hsize_t>
    defineChunks(Dataset::Shape<N> const & chunks,
                 Dataset::Shape<N> const & shape,
                 int numBands, int compression = 0)
    {
        if(chunks[0] > 0)
        {
            std::vector<hsize_t> res(chunks.begin(), chunks.end());
            if(numBands > 1)
                res.insert(res.begin(), numBands);
            return res;
        }
        else if(compression > 0)
        {
            // set default chunks to enable compression
            // (arbitrarily include about 300k pixels into each chunk, but make sure
            //  that the chunk size doesn't exceed the shape)
            std::vector<hsize_t> res(shape.begin(), shape.end());
            hsize_t chunk_length = (hsize_t)std::pow(300000.0, 1.0 / shape.size());
            for(unsigned int k=0; k < shape.size(); ++k)
                if(res[k] > chunk_length)
                    res[k] = chunk_length;
            if(numBands > 1)
                res.insert(res.begin(), numBands);
            return res;
        }
        else
        {
            return std::vector<hsize_t>();
        }
    }



private:
    /** master file */
    mutable vigra::HDF5File m;


public:
    File();
    File(const std::string & path, const OpenMode & op = OpenMode::New);
    File(Handle handle);

    template<typename T, unsigned int N>
    Dataset createDataset(
            const std::string & path,
            const Dataset::Shape<N> & initialShape,
            const T & initialValue,
            const Dataset::Shape<N> & chunking = Dataset::Shape<N>(),
            const Dataset::Shape<N> & maxShape = Dataset::Shape<N>(),
            const int & compression = 0
            )
    {

        std::string datasetName = path;
        int compressionParameter = compression;

        std::string groupname = SplitString(datasetName).first();
        std::string setname = SplitString(datasetName).last();

        vigra::HDF5Handle parent = m.getGroupHandle(groupname);

        /* delete the dataset if it already exists */
//        deleteDataset_(parent, setname);

        /* create dataspace
           add an extra dimension in case that the data is non-scalar */
        vigra::HDF5Handle dataspaceHandle;


        // create dataspace
        dataspaceHandle = vigra::HDF5Handle(H5Screate_simple(N, initialShape.data(),
                                                             maxShape?maxShape.data():nullptr),
                                            &H5Sclose, "HDF5File::createDataset(): unable to create dataspace for scalar data.");

        // set fill value
        vigra::HDF5Handle plist ( H5Pcreate(H5P_DATASET_CREATE),
                                  &H5Pclose,
                                  "HDF5File::createDataset(): unable to create property list." );
        H5Pset_fill_value(plist,hdf5::getH5Type<T>(), &initialValue);

        // turn off time tagging of datasets by default.
        H5Pset_obj_track_times(plist, 0);

        // enable chunks
        std::vector<hsize_t> chunks(defineChunks(chunking, initialShape, 1, compressionParameter));
        if(chunks.size() > 0)
        {
            H5Pset_chunk (plist, chunks.size(), chunks.data());
        }

        // enable compression
        if(compressionParameter > 0)
        {
            H5Pset_deflate(plist, compressionParameter);
        }

        //create the dataset.
        vigra::HDF5Handle datasetHandle ( H5Dcreate(parent, setname.c_str(),
                                             hdf5::getH5Type<T>(),
                                             dataspaceHandle, H5P_DEFAULT,
                                             plist,
                                             H5P_DEFAULT),
                                  &H5Dclose, "HDF5File::createDataset(): unable to create dataset.");

        return Dataset();
    }


    template<typename T, unsigned int N>
    Dataset createDataset(
            const std::string & path,
            const typename vigra::MultiArrayShape<N>::type & initialShape,
            const T & initialValue,
            const typename vigra::MultiArrayShape<N>::type & chunking = vigra::MultiArrayShape<N>::type(),
            const typename vigra::MultiArrayShape<N>::type & maxShape = vigra::MultiArrayShape<N>::type(),
            const int & compression = 0
            ) {

        /* vigra has column major memory layout. we need to reverse
         * the dimensions for that reason. */
        Dataset::Shape<N> is, c, ms;
        for(unsigned int i = 0; i < N; i++) {
            is[i] = initialShape[N-1-i];
            c[i] = chunking[N-1-i];
            ms[i] = maxShape[N-1-i];
        }

        return createDataset(path, is, initialValue, c, ms, compression);

    }



    vigra::HDF5Handle getGroupHandle(const std::string & path) const {
        return m.getGroupHandle(path);
    }

    template<unsigned int N, class T, class Alloc >
    void readAndResize (std::string datasetName, vigra::MultiArray< N, T, Alloc > &array) {

        /* first get the shape of the stored dataset.
         * we need this to decide whether we can load the dataset or not */
        auto shape = m.getDatasetShape(datasetName);


        /* if the type is not a scalar,
         * we have to take care of further dimensions. */
        if(!hdf5::TypeTraits<T>::scalar()) {
            if(*(shape.begin()) != (size_t)hdf5::TypeTraits<T>::numberOfBands()) {
                std::stringstream err;
                err << "hdf5::readAndResize(): fails due to shape mismatch in inner dimension."
                    << datasetName << ", " << N << "," << shape.size();
                throw std::runtime_error(err.str());
            }
            shape.erase(shape.begin());
        }

        if(shape.size() != N) {
            std::stringstream err;
            err << "hdf5::readAndResize(): fails due to dimensions mismatch: "
                << datasetName << ", " << N << "," << shape.size();
            throw std::runtime_error(err.str());
        }

        typename vigra::MultiArrayShape<N>::type newShape;
        for(size_t i = 0; i < shape.size(); i++) {
            newShape[i] = shape[i];
        }
        array.reshape(newShape);

        /* check if the dataset is empty. if so, don't proceed */
        {
            long elemCount = 1;
            for(auto n: shape) {
                elemCount *= n;
            }
            if(!elemCount) return;
        }

        m.readAndResize(datasetName, array);
    }

    template<unsigned int N, class T, class Alloc>
    void read(const std::string & datasetName, vigra::MultiArrayView<N, T, Alloc> &array) {

    }

    template<unsigned int N, class T, class Stride >
    void write (std::string datasetName,
                const vigra::MultiArrayView< N, T, Stride > &array,
                int iChunkSize=0,
                int compression=0) {
        m.write(datasetName, array, iChunkSize, compression);
    }

    template<unsigned int N, typename T, class Stride >
    void write (std::string datasetName,
                const vigra::MultiArrayView< N, T, Stride > &array,
                typename vigra::MultiArrayShape<N>::type chunkSize,
                int compression=0) {
        m.write(datasetName, array, chunkSize, compression);
    }

    /**
     * @brief writes a subset from a dataset
     * @warning Striding of arrays is currently not supported.
     *          The provided view must be a contigous slice of it's
     *          underlying array.
     */
    template<int N, unsigned int M, class T, class Stride>
    inline void write(std::string datasetName,
                      const vigra::TinyVector<vigra::MultiArrayIndex,N> & offset,
                      const vigra::MultiArrayView<M, T, Stride> & array)
    {

        /* make sure the template arguments are valid */
        static_assert(M <= N, "N < M");
        static_assert(N > 0 && M > 0, "M and N needs to be larger.");

//        /* we cannot check against the template stride tag here,
//         * because we eventually want to provide a view on a matrix.
//         * However, that view, must fulfill to be contigous in memory */
//        if(!array.isUnstrided()) {
//            throw std::runtime_error("Strided array views are currently unsupported.");
//        }

        auto shape = m.getDatasetShape(datasetName);
        //auto stride = array.stride();

        if(shape.size() != N) {
            throw std::runtime_error("Requested write to dataset '"
                                     + datasetName
                                     + "rejected: Dimensions don't match.");
        }

        /* default the dimension binding to one, copy the M inner dimensions to
         * the outer shape */
        typename vigra::MultiArrayShape<N>::type outerShape(1);
        std::copy(array.shape().begin(), array.shape().end(), outerShape.begin());


        /* by assuming array to be unstrided, we can simply wrap array
         * to the inner dimensions of outerView.
         */
        vigra::MultiArrayView<N, T, Stride> outerArray(outerShape, array.data());


        /* calculate the new bounding dimensions that are required to store
         * the given block */
        const auto oldShape(shape);
        for(int i = 0; i < N; i++) {
            shape[i] = std::max(shape[i], hsize_t(offset[i] + outerShape[i]));
        }

        /* if the new shape is different from the original shape,
         * we have to resize the dataset */
        if(oldShape != shape) {

            /* reverse dimensions due to the different memory layout */
            std::reverse(shape.begin(), shape.end());

            herr_t status = H5Dset_extent (m.getDatasetHandle(datasetName), shape.data());
            if(status < 0) {
                throw std::runtime_error("Could not extend the dimensions on dataset '"
                                         + datasetName + "'");
            }
        }
        /* for now, use the rest of vigra's implementation to read hyperslabs */
        m.writeBlock(datasetName, offset, outerArray);
    }

    /**
     * @brief reads a subset from a dataset
     * @see write() for further information
     * @warning striding is not supported currently.
     */
    template<int N, unsigned int M, class T, class Alloc>
    void read(const std::string & datasetName,
              const vigra::TinyVector<vigra::MultiArrayIndex,N> & offset,
              vigra::MultiArrayView<M, T, Alloc> array) {

        /* make sure the template arguments are valid */
        static_assert(M <= N, "N < M");
        static_assert(N > 0 && M > 0, "M and N needs to be larger.");

//        /* we cannot check against the template stride tag here,
//         * because we eventually want to provide a view on a matrix.
//         * However, that view, must fulfill to be contigous in memory */
//        if(!array.isUnstrided()) {
//            throw std::runtime_error("Strided array views are currently unsupported.");
//        }

        /* default the dimension binding to one value, copy the M inner dimensions to
         * the outer shape */
        typename vigra::MultiArrayShape<N>::type outerShape(1);
        std::copy(array.shape().begin(), array.shape().end(), outerShape.begin());


        /* create a view on the unstrided data of array.
         * this is the reason why we currently don't support strided array inputs */
        vigra::MultiArrayView<N, T, Alloc> outerArray(outerShape, array.data());

        m.readBlock(datasetName,offset, outerShape, outerArray);

    }



    template<typename T>
    inline void readAttribute(std::string object_name, std::string attribute_name, T &data) {
        m.readAttribute(object_name, attribute_name, data);
    }
    template<typename T>
    inline void writeAttribute(std::string object_name, std::string attribute_name, T data) {
        m.writeAttribute(object_name, attribute_name, data);
    }

    inline bool existsAttribute(std::string object_name, std::string attribute_name) {
        return m.existsAttribute(object_name, attribute_name);
    }

	inline bool existsDataSet(std::string dataSet_name){
		return m.existsDataset(dataSet_name);
	}


    void flush() {
        m.flushToDisk();
    }

    void open(const std::string & path, const OpenMode & mode) {
        vigra::HDF5File::OpenMode om = vigra::HDF5File::OpenReadOnly;
        switch(mode) {
            case OpenMode::ReadOnly: om = vigra::HDF5File::OpenReadOnly; break;
            case OpenMode::ReadWrite: om = vigra::HDF5File::Open; break;
            case OpenMode::New: om = vigra::HDF5File::New; break;
        }
        m.open(path, om);
    }

    void close() {
        m.close();
    }



};

} // ns hdf
} // ns hookers

#endif // HDFFILE_H
