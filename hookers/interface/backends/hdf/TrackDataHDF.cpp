#include <assert.h>

#include <hookers/interface/Project.h>
#include <hookers/interface/Track.h>

#include "ProjectDataHDF.h"
#include "SequenceDataHDF.h"
#include "TrackDataHDF.h"
#include "LandmarkDataHDF.h"

using namespace hookers;
using namespace hookers::interface;

TrackDataHDF::TrackDataHDF(const SequenceDataHDF * sequence, int id)
	: s(sequence), m_id(id),
	  m_modified(false)
{
}


TrackDataHDF::TrackDataHDF()
{
}

TrackDataHDF::~TrackDataHDF()
{
    //	std::cout << "TrackDataHDF::~TrackDataHDF(): " << std::endl;
}

Sequence TrackDataHDF::sequence() const
{
    return *s;
}



bool TrackDataHDF::isValid() const
{
    return (s->m_trackHeader(m_id)[0] != Track::INVALID);
}

Vector3d TrackDataHDF::position() const
{

	if(!s || m_id < 0)
		return Vector3d();

//    SequenceCacheObject::Ptr sco = s->d();
    SequenceDataHDF * sco = s.data();

    Vector3d position = sco->m_trackPositions(m_id);
	return position;

}

void TrackDataHDF::setPosition(const Vector3d & pos)
{
	if(!s)
		return;

//    SequenceCacheObject::Ptr sco = s->d();
    SequenceDataHDF * sco = s.data();

	if(!isValid())
		return;

    sco->m_trackHeader(m_id)[SequenceDataHDF::TrackHeaderFields::ModificationContext] = Project::context();
    sco->m_trackPositions(m_id) = pos;
	m_modified = true;

}

bool TrackDataHDF::hasLink(const GeometryData *g) const
{
    SequenceDataHDF * sco = s.data();

    const GeometryDataHDF * gdh = static_cast<const GeometryDataHDF*>(g);

    if(!gdh)
        return false;

    /* check if the requested geometry is registered with this sequence */
    unsigned gid = s->geometryRegistry(gdh);
    if(!gid)
        return false;

    vigra::UInt64 lid = sco->m_trackAnnotations(0, m_id, gid);

    /* if the landmark id is zero, then there is no
     * annotation */
    return lid;
}

Landmark TrackDataHDF::linkedLandmark(const GeometryData *g) const
{


    SequenceDataHDF * sco = s.data();

    const GeometryDataHDF * gdh = static_cast<const GeometryDataHDF*>(g);

    if(!gdh)
        return Landmark();

    /* check if the requested geometry is registered with this sequence */
    unsigned gid = s->geometryRegistry(gdh);
    if(!gid)
        return Landmark();

    vigra::UInt64 lid = sco->m_trackAnnotations(0, m_id, gid);
    if(!lid)
        return Landmark();


    /* seperate project and landmark id  */
    int landmarkId = lid & 0xFFFFFFFF;

    /* if the linked landmark id is equal to zero then this track is not linked */

    return gdh->getLandmark(landmarkId);
}

/** @todo take different geometries into account */
bool TrackDataHDF::linkToLandmark(const LandmarkData * l)
{
    if(!l)
        return false;

    SequenceDataHDF * sco = s.data();

    const LandmarkDataHDF * ld = static_cast<const LandmarkDataHDF*>(l);

    unsigned gid = s->geometryRegistry(ld->c->g.data());

    if(!gid)
        return false;


    hookers::hdf5::UInt64 val = 1;
    val = val << 32;
    sco->m_trackAnnotations(0, m_id, gid) = val + ld->m_id;
    /*sco->m_trackAnnotations(1, m_id) = Project::context();*/

    sco->m_modified = true;

    return true;
}

/** @todo take different geometries into account */
void TrackDataHDF::unlinkLandmark(const GeometryData *g)
{
    SequenceDataHDF * sco = s.data();

    const GeometryDataHDF * gdh = static_cast<const GeometryDataHDF*>(g);

    if(!gdh)
        return;

    /* check if the requested geometry is registered with this sequence */
    unsigned gid = s->geometryRegistry(gdh);
    if(!gid)
        return;

    if(sco->m_trackAnnotations(0, m_id, gid) == Track::INVALID)
        return;

    sco->m_trackAnnotations(0, m_id, gid) = Track::INVALID;
    sco->m_modified = true;

}

Project::Context TrackDataHDF::creationContext() const
{
    SequenceDataHDF * sco = s.data();
    return Project::Context(sco->m_trackHeader(m_id)[SequenceDataHDF::TrackHeaderFields::CreationContext]);
}

Project::Context TrackDataHDF::modificationContext() const
{
    SequenceDataHDF * sco = s.data();
    return Project::Context(sco->m_trackHeader(m_id)[SequenceDataHDF::TrackHeaderFields::ModificationContext]);
}



