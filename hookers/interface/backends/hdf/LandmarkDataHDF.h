#ifndef LANDMARKDATAHDF_H
#define LANDMARKDATAHDF_H

#include "GeometryChunkDataHDF.h"
#include <hookers/interface/LandmarkData.h>


namespace hookers {
namespace interface {

class GeometryChunkDataHDF;

class LandmarkDataHDF : public LandmarkData
{
	friend class TrackDataHDF;
    friend class PartDataHDF;

public:
    typedef SharedDataPointer<LandmarkDataHDF> Ptr;

private:

    SharedDataPointer<GeometryChunkDataHDF, hookers::hdf5::File> c;
    unsigned m_id;
    unsigned row;

public:
    LandmarkDataHDF(const GeometryChunkDataHDF * part, unsigned id, unsigned row);
	LandmarkDataHDF();
	Vector3d pos() const;

    void invalidate();

    TrackList tracks() const;

    /** project wide unique id */
    hookers::hdf5::UInt64 id() const;

};

} // ns interface
} // ns hookers

#endif // LANDMARKDATAHDF_H
