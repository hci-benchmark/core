#include "GeometryChunkDataHDF.h"

using namespace hookers;
using namespace hookers::interface;

GeometryChunkDataHDF::GeometryChunkDataHDF(const GeometryDataHDF *g, unsigned id)
    : g(g), id(id), m_modified(false)
{
    m_cacheUsage = 0;
}

GeometryChunkDataHDF::~GeometryChunkDataHDF()
{
}

void GeometryChunkDataHDF::create()
{
    m_points = PointsHeader(GeometryDataHDF::LANDMARK_CHUNK_SIZE);
    m_points = PointsHeader::value_type(0);
    m_xyz = PointsXYZ(GeometryDataHDF::LANDMARK_CHUNK_SIZE);
    m_xyz = PointsXYZ::value_type(0.0);
    m_modified = true;
    this->saveCache();

}

void GeometryChunkDataHDF::loadCache()
{
    m_points = PointsHeader(GeometryDataHDF::LANDMARK_CHUNK_SIZE);
    m_points = PointsHeader::value_type(0);
    m_xyz = PointsXYZ(GeometryDataHDF::LANDMARK_CHUNK_SIZE);
    m_xyz = PointsXYZ::value_type(0.0);

    auto offset = m_points.expandElements(0).shape();
    offset = 0;
    offset[1] = id*GeometryDataHDF::LANDMARK_CHUNK_SIZE;

    g->m_gfile.read("/geometry/points/points", offset, m_points.expandElements(0));
    g->m_gfile.read("/geometry/points/xyz", offset, m_xyz.expandElements(0));

}

void GeometryChunkDataHDF::saveCache()
{
    typedef GeometryDataHDF::ChunkHeaderFields CHF;
    typedef GeometryDataHDF::ChunkStateFields CSF;
    typedef GeometryDataHDF::ChunkHeader GCH;
    typedef GeometryDataHDF::ChunkState GCS;

    if(!m_modified)
        return;

    /* first write the new chunk data to disk */
    {
        auto offset = m_points.expandElements(0).shape();
        offset = 0;
        offset[1] = id*GeometryDataHDF::LANDMARK_CHUNK_SIZE;

        g->m_gfile.write("/geometry/points/points", offset, m_points.expandElements(0));
        g->m_gfile.write("/geometry/points/xyz", offset, m_xyz.expandElements(0));
    }


    /* if the last didn't fail, then we can update the
     * chunk meta information */
    {
        /* the following code only works under given assumptions.
         * escpecially the array indices etc are dependant of this */
        static_assert(std::is_same<GCH, decltype(g->m_chunkHeader)>::value, "Types must match");
        static_assert(GCH::actual_dimension == 2, "Dimensions is not correct");
        static_assert(std::is_same<GCS, decltype(g->m_chunkState)>::value, "Types must match");
        static_assert(GCS::actual_dimension == 2, "Dimensions is not correct");

        auto header = g->m_chunkHeader.bindOuter(id);
        auto state = g->m_chunkState.bindOuter(id);

        auto boundPointsHeader = m_points.expandElements(0).bindInner(0);
        unsigned numPoints = std::count_if(boundPointsHeader.begin(), boundPointsHeader.end(),
                                           [](unsigned char & v)  {
                                           return v > 0;
        });

        /* update the number of children */
        header(CHF::Children) = numPoints;

        std::cout << "saveCache: " << numPoints << "," << id << std::endl;

        PointsXYZ::difference_type p(0),q(numPoints);
        auto boundXYZ = m_xyz.subarray(p,q).expandElements(0);

        /* update the boundaries of this chunk */
        for(int i = 0; i < 3; i++) {
            float min, max;
            boundXYZ.bindInner(i).minmax(&min, &max);

            /* calculate the center and the half of the edge length */
            state(CSF::R1 + i) = (min+max)/2.0;
            state(CSF::B1 + i) = (max-min)/2.0;
        }

    }

    m_modified = false;

}

void GeometryChunkDataHDF::dropCache()
{
    m_pointsPointers.clear();
    m_points = PointsHeader();
    m_xyz = PointsXYZ();
}

unsigned GeometryChunkDataHDF::numberChildren() const
{
    /* number of children are stored in a header table within geometry object.
     * we only need to refer to this table here */
    const auto header = g->m_chunkHeader.bindOuter(id);
    return header(GeometryDataHDF::ChunkHeaderFields::Children);
}

Vector3f GeometryChunkDataHDF::center() const
{
    typedef GeometryDataHDF::ChunkStateFields CSF;
    auto boundState = g->m_chunkState.bindOuter(id);
    return Vector3f(boundState(CSF::R1),
                    boundState(CSF::R2),
                    boundState(CSF::R3));

}

Vector3f GeometryChunkDataHDF::boundingBox() const
{
    typedef GeometryDataHDF::ChunkStateFields CSF;
    auto boundState = g->m_chunkState.bindOuter(id);
    return Vector3f(boundState(CSF::B1),
                    boundState(CSF::B2),
                    boundState(CSF::B3));
}

bool GeometryChunkDataHDF::intersecs(const GeometryChunkDataHDF::Sphere &b) const
{
    /* this is special case to specify that the ball has an infinite radius */
    if(std::get<1>(b) < 0.0)
        return true;

    if(this->contains(std::get<0>(b)))
        return true;

    Sphere ownBall;
    auto dx  = (std::get<0>(b)-this->center()).magnitude();
    auto dr  = boundingBox().magnitude()+std::get<1>(b);

    return dx <= dr;
}

bool GeometryChunkDataHDF::intersecs(const GeometryChunkDataHDF::Box &b) const
{
    /* this is a special case. if any of the box dimension entries
     * are less than zero, then this corresponds to a infinitly large box */
    if(vigra::min(std::get<1>(b)) < 0)
        return true;
    auto lower = std::get<0>(b) + std::get<1>(b);
    auto upper = std::get<0>(b) + std::get<1>(b);
    return contains(lower) | contains(upper);
}

void GeometryChunkDataHDF::getPoints(Vector3fArrayView view)
{
    CacheLock lock(this);

    if(view.size() != this->numberChildren()) {
        throw std::runtime_error("GeometryChunkDataHDF::getPoints(): View has wrong shape.");
    }

    for(int i=0,j=0; i < m_points.size(); i++) {

        /* check if landmark is valid.
         * if not, then skip entry */
        if(m_points(i)[0] <= Landmark::INVALID)
            continue;

        /* copy position */
        view(j++) = m_xyz(i);

    }
}

LandmarkList GeometryChunkDataHDF::landmarks() const
{
    CacheLock lock(this);

    LandmarkList result;
    result.reserve(m_points.size());

    for(int i = 0; i < m_points.size(); i++) {
        if(m_points(i)[0] == 0)
            continue;
        result.push_back(*this->getLandmarkPtr(id*GeometryDataHDF::LANDMARK_CHUNK_SIZE + i));
    }
    return result;
}

SharedDataPointer<LandmarkDataHDF> GeometryChunkDataHDF::getLandmarkPtr(unsigned id) const
{

    LandmarkDataHDF::Ptr d = m_pointsPointers[id].lock();
    if(!d) {
        d = new LandmarkDataHDF(this, id, rowFromLandmarkId(id));
        m_pointsPointers[id] = WeakSharedDataPointer<LandmarkDataHDF>(d);
    }

    if(!d) {
        std::stringstream err;
        err << "GeometryChunkDataHDF: loading Landmark with ID "
            << id << " failed.";
        throw std::runtime_error(err.str());
    }

    return d;

}

LandmarkList GeometryChunkDataHDF::radiusSearch(const Vector3d &c, double r) const
{
    throw std::runtime_error("GeometryChunkDataHDF::radiusSearch(): Not implemented.");
}

LandmarkList GeometryChunkDataHDF::knnSearch(const Vector3d &c, unsigned k) const
{
    static_assert(PointsHeader::actual_dimension == 1,
                  "Dimension must match");


    CacheLock lock(this);


    std::vector<unsigned> indices;
    indices.reserve(m_points.size());
    for(unsigned i = 0; i < m_points.size();i++) {
        if(m_points(i)[0] == 0)
            continue;
        indices.push_back(i);
    }

    /* sort the indices according to their distance to search center */
    vigra::indexSort(m_xyz.begin(), m_xyz.end(),
                     indices.begin(),[&](const Vector3f & a, const Vector3f & b){
        return (a-c).magnitude() < (b-c).magnitude();
    });

    /* return the k nearest entries */
    LandmarkList knns;
    for(size_t i = 0; i < indices.size(); i++) {
        knns.push_back(*this->getLandmarkPtr(id*GeometryDataHDF::LANDMARK_CHUNK_SIZE
                                         +indices[i]));
        if(knns.size() >= k)
            break;
    }

    return knns;
}

