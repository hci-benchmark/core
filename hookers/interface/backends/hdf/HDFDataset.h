#ifndef HOOKERS_HDF5_DATASET_H
#define HOOKERS_HDF5_DATASET_H

#include <typeinfo>
#include <iostream>
#include <vector>

#include <cstdint>
#include <exception>
#include <stdexcept>


#include <hdf5.h>

#include <vigra/tinyvector.hxx>

namespace hookers {
namespace hdf5 {

enum class Type {
    UInt8,
    UChar = UInt8,
    Int8,
    Char = Int8,
    UInt16,
    Int16,
    UInt32,
    Int32,
    UInt64,
    Int64,
    Float,
    Double,

    V1D = Double,
    V2D, /**< 2d double vector */
    V3D, /**< 3d double vector */
    V4D, /**< 4d double vector */
    V6D, /**< 6d double vector */

    V1UC = UChar,
    V2UC, /**< 2d unsigned char vector */
    V3UC, /**< 3d unsigned char vector */
    V4UC, /**< 4d unsigned char vector */
    V6UC,  /**< 6d unsigned char vector */

    Gray = V1UC,
    RGB = V3UC
};

template<typename T>
struct TypeTraits {

    /** hdf5 data type */
    static hid_t dataType() {
        throw std::runtime_error("hookers::hdf5::TypeTraits(): Invalid type:" + std::string(typeid(T).name() ));
    }

    /** number of dimensions */
    static int numberOfBands() {
        throw std::runtime_error("hookers::hdf5::TypeTraits(): Invalid type:" + std::string(typeid(T).name() ));
    }

    /** size of the datatype */
    static size_t size() {
        throw std::runtime_error("hookers::hdf5::TypeTraits(): Invalid type:" + std::string(typeid(T).name() ));
    }

    static Type type() {
        throw std::runtime_error("hookers::hdf5::TypeTraits(): Invalid type:" + std::string(typeid(T).name() ));
    }

    static bool scalar() {
        throw std::runtime_error("hookers::hdf5::TypeTraits(): Invalid type:" + std::string(typeid(T).name() ));
    }
};

template<typename T>
Type getType() {
    return TypeTraits<T>::type();
}

template<typename T>
hid_t getH5Type() {
    return TypeTraits<T>::dataType();
}


/** @name Defines some fixed size data types */
typedef std::uint8_t UInt8;
typedef std::int8_t Int8;
typedef std::uint16_t UInt16;
typedef std::int16_t Int16;
typedef std::uint32_t UInt32;
typedef std::int32_t Int32;
typedef std::uint64_t UInt64;
typedef std::int64_t Int64;
typedef float Float;
typedef double Double;


class Dataset
{
public:

    typedef hsize_t Type;
    typedef Type* iterator;
    typedef const Type* const_iterator;

    template<unsigned int N>
    class Shape {
    public:

        Type sizes[N];
    public:

        Shape (Type s = 0) {
            for(unsigned i = 0; i < N; i++)
                sizes[i] = s;
        }

        Type & operator[](int i) {
            return sizes[i];
        }
        const Type & operator[](int i) const {
            return sizes[i];
        }

        Type & operator()(int i) {
            return sizes[i];
        }
        const Type & operator()(int i) const {
            return sizes[i];
        }

        inline unsigned int size() const {
            return N;
        }

        inline Type * data() {
            return sizes;
        }

        inline const Type * data() const {
            return sizes;
        }

        inline iterator begin() {
            return sizes;
        }

        inline const_iterator begin() const {
            return sizes;
        }

        inline iterator end() {
            return sizes+N;
        }

        inline const_iterator end() const {
            return sizes+N;
        }

        operator bool() const {
            for(size_t i = 0; i < N; i++)
                if(sizes[i] != 0) return true;
            return false;
        }

        inline static hid_t unlimited() {
            return H5S_UNLIMITED;
        }

    };



public:
    Dataset();

};


/**
 * Defines Type Mapping Traits
 */
/** @{ */

#define HOOKERS_HDF_TYPE_MAPPING(T, h5type)       \
template<>                                        \
struct TypeTraits<T> {                            \
    static hid_t dataType() {                     \
        return h5type;                            \
    }                                             \
    static int numberOfBands() {                  \
        return 1;                                 \
    }                                             \
    static size_t size() {                        \
        return sizeof(T);                         \
    }                                             \
    static Type type() {                          \
        return Type::T;                           \
    }                                             \
    static bool scalar() {                        \
        return std::is_scalar<T>::value;          \
    }                                             \
                                                  \
};                                                \


HOOKERS_HDF_TYPE_MAPPING(UInt8, H5T_NATIVE_UINT8)
HOOKERS_HDF_TYPE_MAPPING(Int8, H5T_NATIVE_INT8)
HOOKERS_HDF_TYPE_MAPPING(UInt16, H5T_NATIVE_UINT16)
HOOKERS_HDF_TYPE_MAPPING(Int16, H5T_NATIVE_INT16)
HOOKERS_HDF_TYPE_MAPPING(UInt32, H5T_NATIVE_UINT32)
HOOKERS_HDF_TYPE_MAPPING(Int32, H5T_NATIVE_INT32)
HOOKERS_HDF_TYPE_MAPPING(UInt64, H5T_NATIVE_UINT64)
HOOKERS_HDF_TYPE_MAPPING(Int64, H5T_NATIVE_INT64)
HOOKERS_HDF_TYPE_MAPPING(Float, H5T_NATIVE_FLOAT)
HOOKERS_HDF_TYPE_MAPPING(Double, H5T_NATIVE_DOUBLE)
#undef HOOKERS_HDF_TYPE_MAPPING

/** @} */

} // ns hdf5
} // ns hookers

#endif // HOOKERS_HDF5_DATASET_H
