#ifndef HOOKERS_INTERFACE_BACKEND_HDF_VIGRA_H
#define HOOKERS_INTERFACE_BACKEND_HDF_VIGRA_H

#include <vigra/tinyvector.hxx>

namespace hookers {
namespace hdf5 {

/** specialization for vigras TinyVector */
template<class T, int N>
struct TypeTraits<vigra::TinyVector<T,N> > {
    static hid_t dataType() {
        return TypeTraits<T>::dataType();
    }
    static int numberOfBands() {
        return N;
    }
    static size_t size() {
        return N*sizeof(T);
    }
    static Type type() {
        return TypeTraits<T>::type();
    }
    static bool scalar() {
        return false;
    }
};


}
} // ns hookers

#endif // HOOKERS_INTERFACE_BACKEND_HDF_VIGRA_H

