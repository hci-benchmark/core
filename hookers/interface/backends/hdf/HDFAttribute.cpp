#include <assert.h>

#include "HDFAttribute.h"

using namespace hookers::hdf5;

HDFAttribute::HDFAttribute(hid_t object, const std::string &name)
	: m_handle(object), m_name(name), m_attr(-1), m_type(-1), m_space(-1), m_ndims(0), m_dims(0)
{
	assert(!name.empty());

    if(!H5Aexists(object, name.c_str()))
		return;

    m_attr = H5Aopen(object, name.c_str(), H5P_DEFAULT);

	/* attribute could not be opened */
	if(m_attr < 0)
			return;

	/* read in the data type */
	m_type = H5Aget_type(m_attr);
	if(m_type < 0) {
		return;
	}

	m_dataclass = H5Tget_class(m_type);

	/* read in the data shape */
	m_space = H5Aget_space(m_attr);
	if(m_space < 0) {
			return;
	}

	m_ndims = H5Sget_simple_extent_dims(m_space, NULL, NULL);
	m_dims = new hsize_t[m_ndims];
	H5Sget_simple_extent_dims(m_space, m_dims, NULL);

	m_isValid = true;

}

HDFAttribute::~HDFAttribute()
{
	/* make sure we close anything again */
	if(m_attr >= 0)
		H5Aclose(m_attr);
	if(m_space >= 0)
		H5Sclose(m_space);
	if(m_type >= 0)
		H5Tclose(m_type);
	if(m_dims)
		delete[] m_dims;
}

bool HDFAttribute::setAttribute(const std::string &value)
{
	H5LTset_attribute_string(m_handle, ".", m_name.data(), value.data());
	return true;
}

bool HDFAttribute::getAttribute(std::string &value, const std::string &def)
{

	value = def;

	if(!isValid())
		return false;

	if(m_dataclass != H5T_STRING)
		return false;

	/* check if the value is a scalar or one-dimensional*/
	if(!(m_ndims == 0 || (m_ndims == 1 && m_dims[0] == 1)))
		return false;


	herr_t status;
	char * stringVal;

	/* distinguish between variable length and fixed length */
	if(H5Tis_variable_str(m_type)) {
		hid_t memtype = H5Tcopy (H5T_C_S1);
		status = H5Tset_size (memtype, H5T_VARIABLE);
		status = H5Aread (m_attr, memtype, &stringVal);

		if(status >= 0) {
			value = std::string(stringVal);
		}

		H5Dvlen_reclaim (memtype, m_space, H5P_DEFAULT, &stringVal);
		H5Tclose(memtype);

	} else {
		stringVal = new char[H5Tget_size(m_type)];
		status = H5LTget_attribute_string(m_handle, ".", m_name.data(), stringVal);

		if(status >= 0) {
			value = std::string(stringVal);
		}

		delete[] stringVal;
	}

	return (status>=0)?true:false;
}
