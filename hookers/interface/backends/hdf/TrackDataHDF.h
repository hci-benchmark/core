#ifndef TRACKDATAHDF_H
#define TRACKDATAHDF_H

#include <string>
#include <sstream>
#include <memory>
#include <vigra/multi_array.hxx>


#include <hookers/interface/TrackData.h>

#include "HDFGroup.h"
#include "ProjectDataHDF.h"

namespace hookers {
namespace interface {


class SequenceDataHDF;

/**
 * @page hdf
 *
 * @latexonly
 * \newpage
 * @endlatexonly
 *
 * ********
 * @subsection hdf_tracks /sequence/tracks/
 *
 *
 * This group contains the data to \link Track Tracks \endlink stored in the sequence file.
 *
 * The whole information of tracks will be stored through out serveral datasets.
 * All datasets in tracks/ should be of the shape NxM where N is the number of tracks
 * and M is the particular number of data entries.
 * All datasets __must__ be chunked with a chunksize of 32768.
 * The maximum numer of tracks is limited to 2^31.
 *
 * The position of a track in a given frame can be found in @ref hdf_sequence_frames_frame_track_positions.
 *
 * The unique track id is the row number in @ref hdf_tracks_header.
 * The big advantage is that we easily can adress a Track in the table without
 * searching through the whole table first.
 *
 * ***********
 * @subsection hdf_tracks_header /sequence/tracks/tracks
 * Shape: @f$ T \times (type, creationContext, modificationContext) @f$ \n
 * Type: int, expandable in T \n
 * Chunking: @f$ (32768,3) @f$\n
 *
 *
 * For field *type* see Track::Type,
 * for *creationContext* and *modificationContext* see Project::Context.
 *
 * A deleted track must be flagged by setting *type* to Track::INVALID.
 * Any data in different tables like @ref hdf_tracks_positions
 * should be considered invalid as well.
 *
 * ***********
 * @subsection hdf_tracks_positions /sequence/tracks/positions
 * Shape: @f$ T \times (x,y,z) @f$ \n
 * Type: double, expandable in T\n
 * Chunking: @f$ (32768,3) @f$
 *
 *
 * This dataset stores the reference point @f$(x,y,z)@f$ for each track.
 * The row number must correspond to the same as in @ref hdf_tracks_header.
 *
 *
 * ********
 * @subsection hdf_tracks_annotations /sequence/geometries/tracks/annotations
 * Shape: @f$ G \times T \times (landmarkId) @f$ \n
 * Type: uint64, chunksize: 32768, expandable in G,T\n
 * Chunking: @f$ (2,32768,1) @f$\n
 *
 *
 * This dataset links tracks to 3D Landmarks of a given geometry.
 *
 * If track is not assigned, entry should be flagged with 0,
 * or else the unique landmark id should be set.
 *
 * Have a look at @ref hdf_project_parts_part_landmarks on how to find the Landmark id.
 *
 *
 * *** */

class TrackDataHDF : public TrackData
{

public:
	friend class ProjectDataHDF;
	friend class SequenceDataHDF;
    friend class FrameDataHDF;





private:

	/** reference to project */
    SharedDataPointer<SequenceDataHDF, TrackDataHDF> s;


	int m_id;
	bool m_modified;

    TrackDataHDF(const SequenceDataHDF * sequence, int id);


public:

public:
	TrackDataHDF();
	~TrackDataHDF();


    Sequence sequence() const;

    inline Object::Id id() const {
        return Object::Id(s->p->m_id.project(),
                          s->m_id,
                          0x0,
                          m_id);
    }

	bool isValid() const;
	Vector3d position() const;
	void setPosition(const Vector3d & pos);

    bool hasLink(const GeometryData * g) const;
    Landmark linkedLandmark(const GeometryData * g) const;
    bool linkToLandmark(const LandmarkData * l);
    void unlinkLandmark(const GeometryData * g);

    Project::Context creationContext() const;
    Project::Context modificationContext() const;



};

} // ns interface
} // ns hookers

#endif // PARTDATAHDF_H
