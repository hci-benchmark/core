#ifndef HOOKERS_HDF5_HANDLE_H
#define HOOKERS_HDF5_HANDLE_H

#include <hdf5.h>
#include <memory>

#include <vigra/hdf5impex.hxx>


namespace hookers {
namespace hdf5 {


class Handle
{

public:
	typedef herr_t (CloseCallback)(hid_t);

private:
	struct Data {
		hid_t hid;
		CloseCallback * cb;

		Data(hid_t hid, CloseCallback cb)
			: hid(hid), cb(cb) {}

		~Data() {
			if(hid >= 0)
				cb(hid);
		}

	};

	std::shared_ptr<Data> d;

public:

    Handle() {}
    Handle(hid_t id, CloseCallback cb)
		: d(new Data(id, cb))
	{
	}

    Handle(vigra::HDF5Handle handle, CloseCallback cb)
		: d(new Data(handle, cb))
	{
		hid_t id = handle;
		H5Iinc_ref(id);
	}

	operator hid_t() {
		if(!d)
			return -1;
		return d->hid;
	}

	bool isValid() {
		if(!d)
			return false;
		return d->hid>=0;
	}

	void close() {
		if(!d)
			return;

		d->cb(d->hid);
		d->hid = -1;
	}
};

} // ns hdf5
} // ns hookers

/** @todo remove the this typedef */
typedef hookers::hdf5::Handle HDFHandle;

#endif // HOOKERS_HDF5_HANDLE_H
