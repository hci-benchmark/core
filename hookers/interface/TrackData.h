#ifndef BOSCHGT_PROJECT_TRACKDATA_H
#define BOSCHGT_PROJECT_TRACKDATA_H

#include "NumericTypes.h"
#include "SharedType.h"
#include "Project.h"
#include "Geometry.h"

namespace hookers {
namespace interface {

class Sequence;
class Track;
class Landmark;
class LandmarkData;

class TrackData : public SharedData {

public:

    virtual ~TrackData() {}

    virtual Sequence sequence() const = 0;

    virtual Object::Id id() const = 0;
	virtual bool isValid() const = 0;
	virtual Vector3d position() const = 0;
	virtual void setPosition(const Vector3d &pos) = 0;

    virtual bool hasLink(const GeometryData * g) const = 0;
    virtual Landmark linkedLandmark(const GeometryData * g) const = 0;
    virtual bool linkToLandmark(const LandmarkData * l) = 0;
    virtual void unlinkLandmark(const GeometryData * g) = 0;


    virtual Project::Context creationContext() const = 0;
    virtual Project::Context modificationContext() const = 0;


	operator Track();

};

} // ns interface
} // ns hookers

#endif //BOSCHGT_PROJECT_TRACKDATA_H


