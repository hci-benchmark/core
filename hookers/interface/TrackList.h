#ifndef BOSCHGT_PROJECT_TRACKLIST_H
#define BOSCHGT_PROJECT_TRACKLIST_H

#include "NumericTypes.h"
#include "AbstractObjectList.h"
#include "Track.h"
#include "Frame.h"

namespace hookers {
namespace interface {

class LandmarkList;

class TrackList : public AbstractObjectList<Track>
{
public:
	
public:
	
    TrackList() {}

    TrackList(const AbstractObjectList<Track> & other) {
        AbstractObjectList<Track>::operator =(other);
    }

    TrackList(AbstractObjectList<Track> && other) {
        AbstractObjectList<Track>::swap(other);
    }

	/**
	 * @returns a filtered list of tracks belonging to the same frame 
	 */	
    TrackList tracksByFrame(const Frame & f) const;

    /**
     * @brief filter tracks to be linked with given landmark
     */
    TrackList tracksByLandmark(const Landmark & l) const;

    /** @brief filters tracks that have provided creation context */
    TrackList tracksByCreationContext(const Project::Context & context);

    /**
     * @returns a filtered list of tracks that are annotated
     */
    TrackList annotated( const Geometry & g = Geometry()) const;


    /** @returns a list of all Landmarks that are linked to tracks
     *           found in annotated()
     */
    LandmarkList landmarks() const;

    /** @returns a list of all corresponding landmarks.
     *           if a track is not linked, an invalid landmark will
     *           appear it it's index.
     */
    LandmarkList unfilteredLandmarks() const;


    void getPositions(Vector3fArray & view) const;
    void setPositions(const Vector3fArrayView& view);

    /** @brief view on all positions of tracks in this list
     * This function creates a contigous array of the same size
     * as the object list length.
     * The given array-view represents all positions of Tracks stored
     * in this list.
     *
     * For further reference read in AbstractObjectList.
     *
     * @see TrackList
     */
    void getPositionsInFrame(Vector2fArray & pos, const Frame & frame,
                             const Frame::View & view = Frame::View::Base) const;

    /** @brief sets positions requested by getPositions()
     *
     *
     */
    void setPositionsInFrame(const Vector2fArrayView & pos, Frame & frame,
                             const Frame::View & view = Frame::View::Base);

};
typedef AbstractObjectSet<Track> TrackSet;

} // ns interface
} // ns hookers

#include "LandmarkList.h"

#endif // BOSCHGT_PROJECT_TRACKLIST_H
