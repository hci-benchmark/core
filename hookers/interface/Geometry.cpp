#include <iostream>

#include <hookers/interface/Geometry.h>
#include <hookers/interface/Project.h>


using namespace hookers::interface;

Geometry::Geometry(const GeometryData *data)
	: d(data)
{

}

Geometry::Geometry()
{

}

Geometry::~Geometry()
{

}

Project Geometry::project() const
{
    if(!d)
        return Project();
    return d->project();
}

Object::Id Geometry::id() const
{
    if(!d)
        return Object::Id();
    return d->id();
}


bool Geometry::isModified() const
{

	if(!d)
		return false;

	return false;
}

std::string Geometry::name() const
{
	if(!d)
		return std::string();
	return d->name();
}

void Geometry::addLandmarks(Vector3fArrayView pos)
{
    if(!d)
        return;
    d->addLandmarks(pos);
}

void Geometry::removeLandmarks(LandmarkList landmarks)
{
    for(auto & l: landmarks) {
        l.d.data()->invalidate();
    }
}


PartList Geometry::parts() const
{
	if(!d)
        return PartList();

    return d->parts();
}

Part Geometry::addPart(const std::string &name)
{
	if(!d)
        return Part();

    return d->addPart(name);
}

bool Geometry::isValid() const
{
	if(!d)
		return false;
	return d->isValid();
}

bool Geometry::operator==(const Geometry & other) const
{
	return this->d == other.d;
}


bool Geometry::operator<(const Geometry & other) const
{
	return this->d < other.d;
}


GeometryData::operator Geometry() const
{
	return Geometry(this);
}


Geometry GeometryList::byLabel(const std::string &label) {

    /* iterate over all sequences in the list and try to find the given label */
    auto it = this->cbegin();
    for(; it != this->cend(); it++) {

        /* check if labels match */
        if((*it).name() == label)
            return *it;

    }

    /* return a invalid sequence if none sequence with given label could be found */
    return Geometry();
}


LandmarkList Geometry::radiusSearch(const Vector3f &center, double radius)
{
    if(!d)
        return LandmarkList();

    return d->radiusSearch(center, radius);
}

std::vector<LandmarkList> Geometry::nearestNeighbourSearches(const std::vector<Vector3f> &center,
                                                             int k) const
{
    if(!d)
        return std::vector<LandmarkList>();

    return d->nearestNeighbourSearches(center, k);
}

LandmarkList Geometry::nearestNeighbourSearch(const Vector3f &center, int k)
{
    if(!d)
        return LandmarkList();
    return d->nearestNeighbourSearches({center}, k).front();
}
