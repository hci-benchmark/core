#ifndef BOSCHGT_PROJECT_FRAMELIST_H
#define BOSCHGT_PROJECT_FRAMELIST_H

#include "AbstractObjectList.h"
#include "Frame.h"


namespace hookers {
namespace interface {



/** 
 * Abstract class for object lists that handle contigous
 * objects
 */
class FrameList: public AbstractSharedTypeList<FrameData>
{
    typedef AbstractSharedTypeList<FrameData> AbstractType;

public:
    FrameList() {}
    FrameList(const AbstractType & other)
        : AbstractType(other) {}
    FrameList(AbstractType && other)
        : AbstractType(other) {}


};

} // ns interface
} // ns hookers

#endif // BOSCHGT_PROJECT_FRAMELIST_H
