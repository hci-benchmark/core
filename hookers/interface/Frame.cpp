#include <hookers/interface/Sequence.h>
#include <hookers/interface/Frame.h>
#include <hookers/interface/Track.h>

using namespace hookers;
using namespace hookers::interface;

Frame::Frame(const FrameData *data)
	: d(data)
{

}

Frame::Frame()
{

}

Object::Id Frame::id() const {
    if(!d)
        return Object::Id();

    return d->id();
}

bool Frame::isValid() const
{
	if(!d)
		return false;
    return d->isValid();
}

int Frame::number() const
{
    if(!d)
        return -1;
    return d->id().object();
}

void Frame::getView(Image & image, const Frame::View &view) const
{
    if(!d)
        image = Image();

     d->getView(image, view);
}

void Frame::setView(const ImageView &image, const Frame::View &view)
{
	if(!d)
		return;
    d->setView(image, view);
}

Vector6d Frame::pose(const CameraPoseFrame & frame) const
{
    if(!d)
        return Vector6d();
    return d->pose();

}

void Frame::setPose(const Vector6d &t, const CameraPoseFrame & frame)
{
    if(!d)
        return;
    d->setPose(t);
}

Vector6d Frame::poseUncertainty(const Frame::CameraPoseFrame &frame) const
{
    if(!d)
        return Vector6d(0);
    return d->poseUncertainty();
}

void Frame::setPoseUncertainty(const Vector6d &pose, const Frame::CameraPoseFrame &frame)
{
    if(!d)
        return;
    d->setPoseUncertainty(pose);
}

double Frame::time() const
{
    if(!d)
        return 0.0;
    return d->time();
}

void Frame::setTime(const double &time)
{
    if(!d)
        return;
    d->setTime(time);
}

Vector4d Frame::intrinsics(const View &view) const
{
    if(!d)
        return Vector4d(0);
    return d->intrinsics(Frame::View::Stereo);
}

void Frame::setIntrinsics(const Vector4d &intrinsics, const View &view)
{
    if(!d)
        return;
    d->setIntrinsics(intrinsics, view);

}

Vector4d Frame::intrinsicsUncertainty(const View &view)
{
    if(!d)
        return Vector4d(0);
    return d->intrinsicsUncertainty(view);
}


void Frame::setIntrinsicsUncertainty(const Vector4d &intrinsics, const View &view)
{
    if(!d)
        return;
    d->setIntrinsicsUncertainty(intrinsics, view);

}

Vector6d Frame::viewTransformation(const View &view) const
{
    if(!d)
        return Vector6d(0);
    return d->viewTransformation(view);
}

void Frame::setViewTransformation(const Vector6d &translation, const View &view)
{
    if(!d)
        return;
    d->setViewTransformation(translation, view);
}

Vector6d Frame::viewTransformationUncertainty(const Frame::View & view) const
{
    if(!d)
        return Vector6d(0);
    return d->viewTransformationUncertainty(view);

}

void Frame::setViewTransformationUncertainty(const Vector6d &uncert, const View &view)
{
    if(!d)
        return;
    d->setViewTransformationUncertainty(uncert, view);

}


void Frame::linkTrack(const Track &t, const Vector2d &pos, const Frame::View &view)
{
    if(!d || !t.d)
        return;
    d->linkTrack(t.d.data(), pos, view);
}

void Frame::unlinkTrack(const Track &t, const Frame::View &view)
{
    if(!d || !t.d)
        return;
    d->unlinkTrack(t.d.data(), view);

}

TrackList Frame::tracks(const Frame::View &view) const
{
    if(!d)
        return TrackList();
    return d->tracks(view);
}

Vector2d Frame::trackPosition(const Track &t, const Frame::View &view) const
{
 if(!d || !t.d)
     return Track::INVALID_FRAME_POS;

 return d->trackPosition(t.d.data(), view);

}

Sequence Frame::sequence() const
{
    if(!d)
        return Sequence();
    return d->sequence();
}



void Frame::save()
{
    if(!d)
        return;
    d->save();
}

bool Frame::operator ==(const Frame & other) const
{
    return (this->d == other.d);
}

bool Frame::operator<(const Frame & other) const
{
	return (this->d < other.d);
}


FrameData::operator Frame() const
{
	return Frame(this);
}

void Frame::setMap(const std::string & name, const Map & map){/*Does it needed to define from Frame::Map*/
	if (!d)
		return;
	d->setMap(name, map);
}

std::string Frame::getDataFilePath()
{
    if (!d)
        return "";
    return d->getDataFilePath();
}

bool Frame::hasMap(const std::string & name){
	if (!d)
		return(false);
	return d->hasMap(name);
}

void Frame::getMap(const std::string & name, Map & map ){
	if (!d)
		map = Map();
	 d->getMap(name, map);
}

void Frame::setList(const std::string & name, const List & list){
	if (!d)
		return;
	d->setList(name, list);
}

void Frame::getList(const std::string & name, List & list){
	if (!d)
		list = List();
	 d->getList(name, list);
}

bool Frame::hasList(const std::string & name){
	if (!d)
		return(false);
	return d->hasList(name);
}
Frame::Size Frame::size() const{

    /* if this is a null instance, it has no size */
    if (!d)
        return Size();

    Image  image;
    d->getView(image);
    if (image.hasData())
        return image.shape();
    else
        return Frame::Size();
}
