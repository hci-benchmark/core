Requirements
############

In order to install the boschgt framework, you need some prerequisites:

Libraries
----------

- Qt5
- OpenCV
- TCLAP
- Vigra
- HDF5



Configure
#########

use cmake!

You have to specify hdf vigra,hdf5 and openCV cmake prefixes as mentioned in their documentations

TCLAP is ia header only library. You can set the include path by

TCLAP_INCLUDE_DIR as a cmake or enviroment variable

Interface
#########

You can use the interface library to access project data.


Tools
#####

You can manage your projects and sequences with the tools subproject


GUI
#####

If you need GUI, you have to enabled tools as well in order to be built


