cmake_minimum_required(VERSION 3.0.0)
project(hookers-libs)

SET(CMAKE_MODULE_PATH CMAKE_MODULE_PATH ${CMAKE_CURRENT_SOURCE_DIR}/cmake)


if(CMAKE_CXX_COMPILER_ID MATCHES "MSVC")
  # Force to always compile with W4
  if(CMAKE_CXX_FLAGS MATCHES "/W[0-4]")
    string(REGEX REPLACE "/W[0-4]" "/W4" CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS}")
  else()
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /W4")
  endif()
elseif((CMAKE_CXX_COMPILER_ID MATCHES "Clang") OR (CMAKE_CXX_COMPILER_ID MATCHES "GNU"))
  # Update if necessary
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wno-long-long -pedantic -std=c++11")
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wno-unused-local-typedefs -Wno-unused-variable -Wno-unused-but-set-variable -Wno-nonnull ")
endif()



###
# This macro takes a list of sources and prepends the relative path of all files.
# finally it exports the list to a global scope variable with name GLOBAL_VAR.
# this is handy for assembling from subdirectories
macro (add_files GLOBAL_VAR)
    file (RELATIVE_PATH _relPath "${${PROJECT_NAME}_SOURCE_DIR}" "${CMAKE_CURRENT_SOURCE_DIR}")
    foreach (_src ${ARGN})
        if (_relPath)
            list (APPEND SRC "${_relPath}/${_src}")
        else()
            list (APPEND SRC "${_src}")
        endif()
    endforeach()
    if (_relPath)
        # propagate SRCS to parent directory
        set (${GLOBAL_VAR} ${${GLOBAL_VAR}} ${SRC} PARENT_SCOPE)
    endif()
    set(LOCAL_LIST GLOBAL_VAR)
    message(STATUS "${GLOBAL_VAR}: adding ${ARGN}")
endmacro()


# this is a core component of the project
add_subdirectory(hookers/interface)
add_subdirectory(money)

#####
##### Build Options
#####

### toolbox
#
# contains helper classes and algorithmns that go beyond the access to
# project files.
option(WITH_TOOLBOX "Build toolbox" ON)
if(WITH_TOOLBOX)
        add_subdirectory(hookers/toolbox)
endif()

### test cases
#   build test cases
#
option(WITH_TESTS "Builds tests" OFF)
if(WITH_TESTS)
	enable_testing()
	add_subdirectory(test)
endif()



